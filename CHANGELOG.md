# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Significantly expanded README.md file.

## [0.2.5] - 2025-03-02 
### Fixed
 - Unittesting. Added pytest.approx() to remove failing tests.

## [0.2.4] - 2025-02-19
### Added
 - Unittesting. Covers most importers and the single-spectra analysis.

## [0.2.3] - 2023-07-20 
### Changed
- Citation (.cff file) was update to reflect the published works about this software package.
- README.md was updated to refer to the CITATION.cff file.

## [0.2.2] - 2023-05-09 
### Added
- Citation in *.cff format
- Removed some unnecessary files in test/*.

## [0.2.1] - 2022-11-21
### Fixed
- Initialize `SingleSpectrum` class variables to None or np.nan.
- In `DopplerMeasurement` catch errors thrown by the analyze function.
- Update version number in setup.cfg.

## [0.2.0] - 2022-10-27
### Added
- The standard `ratio_plotter()` function in measurement.py now features multiprocessing, significantly decreasing the
runtime of the program especially for plots with many ratio curves.
- The `ratio_plotter()` function now features the ability to "double check" the energy calibration of measurements done
with a 22Na source using the 1200 keV prompt gamma peak. Currently only a warning is given out, in later updates this
will be expanded on.
- A legacy `ratio_plotter_win()` function is a clone of the old function without multiprocessing. This is required 
because currently Windows does not support the "fork" method of creating subprocesses in multiprocessing. For 
further info see: https://docs.python.org/3/library/multiprocessing.html
- Updated README. Now the basic functions of STACS for both DBS and CDBS are explained and examples are provided.

## [0.1.6] - 2022-08-31
### Changed
- Minor addition to README.md to inform users of newer branch soon to be merged to main.

## [0.1.5] - 2022-08-22
### Fixed
- Fixed a wrong usage of "is" when comparing with a literal in measurement.py.

## [0.1.4] - 2022-06-05
### Changed
- Add option to correct the energy calibration (peak shift) before calculating lineshape parameters.
- Set new option to default.
- Value of positron restmass is now imported from CODATA scipy subpackage instead of using the value 511.0.

## [0.1.3] - 2022-05-18
### Fixed
- Fix severe bug in lineshape parameter calculation.

## [0.1.2] - 2022-05-10
### Fixed
- Fix x-axis unit in depth profile plots.

## [0.1.1] - 2022-05-06
### Fixed
- Fix errors when calibrating with only two peaks.

## [0.1.0] - 2022-05-03
### Added
- Energy calibration and fitting functions.
