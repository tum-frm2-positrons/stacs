def ratiofromfile(filename):

    """Imports an old spectrum, created by LabView, from .txt file."""

    file = open(filename)

    count = []
    energy = []
    error = []

    for i in file:
        line = i.replace(',', '.')
        line = line.split('\t')
        energy.append(float(line[0]))
        count.append(float(line[1]))
        error.append(float(line[2]))

    return count, energy, error


def exportproj(energy, counts, error, name):

    """Exports a projection into .txt file"""

    filename = 'projection_{}.txt'.format(name)
    output = open(filename, 'w')

    for i in range(len(counts)):
        outstr = '{}\t{}\t{}'.format(energy[i], counts[i], error[i])
        output.write(outstr)

    output.close()
