import numpy
import matplotlib.pyplot as plt
import matplotlib.colors
import warnings
from scipy.optimize import curve_fit
from scipy.stats import linregress
from scipy.optimize import OptimizeWarning
import scipy.odr as so
from .visualizer import heatmap, fancy_3d
import math as m
import numpy as np

def energy(channel, e_calib):
    """Apply energy calibration to channel numbers.

    Use energy calibration in the form of a second-order polynomial equation to
    convert channel position into energy.

    Parameters
    ----------
    channel : int
        Channel number of a detector.
    e_calib : iterable object
        Energy calibration in the form of second-order polynomial equation
        coefficients (there shalt always be three values).

    Returns
    -------
    float
        Corresponding energy of the channel.
    """

    if len(e_calib) != 3:
        raise ValueError("e_calib must be an iterable of length 3")
    return e_calib[0] + e_calib[1]*channel + e_calib[2]*channel*channel
    

def gauss_norm(x, a=1, b=511, c=40):
    return a / c / np.sqrt(2 * np.pi) * numpy.exp(-((x - b) / c) ** 2 * 0.5)

def deconvolute_energy_resolution(spectrum_x, spectrum_y, energyresolution):
    ''''''
    center_f_eres = int(0.1 * spectrum_x)
    eres_sigma = energyresolution / 2. / np.sqrt(2. * np.log(2.)) # convert to sigma
    f_eres = gauss_norm(spectrum_x, b=center_f_eres, c=eres_sigma)
    spec_decov = np.fft.ifft(np.fft.fft(spectrum_y) / np.fft.fft(f_eres))
    return spec_decov


def fit_peak_gauss(data_x, data_y, center, w): # TODO LEGACY wird nur in alter energy-resolution aufgerufen!
    '''Fits a gaussian to a peak dound in the data region given. 
    Parameters
    ----------
    data_x : np.array
        x values of the data.
    data_y : np.array
        y values of the data.
    center : int
        Center channel of the fit interval.
    w : int
        Width of the fit interval in channels.
    
    Returns
    -------
    FWHM : float
        FWHM of the fitted gaussian.
    FWHM_err : float
        Uncertainty of the FWHM given as 1 standard deviation.
    opt : list(len=4)
        Fit parameters of the gaussian.'''
    data_x_sli = data_x[center-w:center+w+1]
    data_y_sli = np.array(data_y[center-w:center+w+1])
    if (np.sum(data_y_sli) / len(data_y_sli)) > 1.0 and np.sum(data_y_sli) > 100.0:
        #print('left: ', data_y[center-w:center-w+10])
        #print('right: ', data_y[center+w-10:center+w])
        p_in = (data_y[center], data_x[center], data_x[center]/1000.0, 
	np.mean([data_y[center-w:center-w+10], data_y[center+w-10:center+w]]))
        try:
            opt, cov = curve_fit(gauss_offset, data_x_sli, data_y_sli, p_in, 
	    sigma=data_to_sigma_abs(data_y_sli, True), absolute_sigma=True)
            #plt.plot(data_x_sli, gauss_offset(data_x_sli,p_in[0],p_in[1],p_in[2],p_in[3]))
            #plt.plot(data_x_sli, data_y_sli)
            #plt.plot(data_x_sli, gauss_offset(data_x_sli,opt[0],opt[1],opt[2],opt[3]))
            #plt.show()    
        except:
            print("fit error: ", center)
            print(p_in)
            #print(opt)
            #plt.plot(data_x_sli, data_y_sli)
            #plt.plot(data_x_sli, gauss_offset(data_x_sli,p_in[0],p_in[1],p_in[2],p_in[3]))
            #plt.show()
            opt, cov = np.zeros(4), np.zeros((4,4))
        sig = opt[2]
        sig_err = np.sqrt(np.diag(cov))[2]
        FWHM = 2 * np.sqrt(2 * np.log(2)) * sig
        FWHM_err = 2 * np.sqrt(2 * np.log(2)) * sig_err
    else:
        FWHM, FWHM_err, opt = 0, 0, np.zeros(4)  
    return FWHM, FWHM_err, opt

def energy_fit(arr, peaks=6, polyfit=True, name='', fiveeleven=False, fwhm=False,
              debug1=False, debug2=False):
    """This function will use the peaks of a calibration measurement done with a Eu-152 source,
    in order to perform an energy calibration of the detectors.

    Parameters
    ----------
    arr : np.array
        1D input array of the calibration measurement.
    peaks : int, optional
        Number of peaks to be used for fitting.
    polyfit : bool, optional
        ?
    name : str, optional
        Name for debug plot.
    fiveeleven : bool, optional
        True if the calibration measurement was performed with positron beam.
    debug1 : bool, optional
        Debug function.
    debug2 : bool, optional
        Debug function.

    """
    arr = np.array(arr)
    sli = arr[:-10].copy()
    maxima_unsrt = []
    w_h = int(110 * len(arr) / 2**14) #peakfitregion halfwidth

    if peaks > 6:
        print("No more then 6 peaks are accepted")
        peaks = 6

    i = 0
    while i < peaks:
        max_i = np.argmax(sli)
        if max_i-w_h > 0 and max_i+w_h < len(sli):
            maxima_unsrt.append([max_i, 0.0])
            if not fiveeleven and i == 2:
                sli[0: int(maxima_unsrt[1][0])] = 0
            i += 1
        sli[max(0, max_i-w_h): min(len(sli), max_i+w_h+1)] = 0
            
    maxima_unsrt = np.array(maxima_unsrt, dtype=float)
    maxima = np.sort(maxima_unsrt,axis=0)
    maxima_sig = maxima[:,1]
    maxima = maxima[:,0]
    if fiveeleven:
        energies = np.array([121.78, 244.7, 344.28, 510.999, 778.91, 964.08, 1112.1])
        # energies taken from T.Schmidt, 2019, MA Thesis TUM
    else:
        energies = np.array([121.78, 244.7, 344.28, 778.91, 964.08, 1112.1])
        # energies taken from T.Schmidt, 2019, MA Thesis TUM

    x_dim = np.arange(-w_h, w_h + 1, 1)

    #polyC = lambda x, a, b, c : a + (b * x) + (c * x ** 2)
    peak_width = []
    peak_width_error = []

    for peak in range(peaks):
        center = int(maxima[peak])
        slice = arr[center-w_h:center+w_h+1]
        p_in = (arr[center], 0, 20, 20)
        opt, pas = curve_fit(gauss_offset, x_dim, slice, p_in, 
                             sigma=data_to_sigma_abs(slice, True))
        center += opt[1]
        opt[1] += int(maxima[peak])
        if debug1:
            plt.plot(x_dim + int(maxima[peak]), gauss_offset(x_dim + int(maxima[peak]),
                    *opt), color='red', alpha=0.8)
        maxima[peak] = center
        if fwhm:
            peak_width.append(np.abs(opt[2]) * 2.355)
            peak_width_error.append(pas[2, 2] * 2.355)
        maxima_sig[peak] = np.sqrt(np.diag(pas))[1]
        
    if debug1:
        ch_maxima = peaks_predicted_qnd(peaks_largest_ch=maxima_unsrt[:,0], peaks_e=energies)
        plt.vlines(x=ch_maxima,ymin=0,ymax=max(arr),color='grey',linestyles='dashed', alpha=0.8)
        print('Literature Energies:\t', energies[:len(maxima)])
        print('Positions of Maxima:\t', maxima)
        plt.title(name)
        plt.xlabel('channel')
        plt.ylabel('counts')
        plt.plot(arr)
        plt.plot(sli)
        plt.xlim([0.0,1.05 * len(arr)])
        plt.show()

    if debug1 or debug2:
        print('Gauss Corrected Maxima:\t', maxima)
        
    mod_l = so.Model(lin)
    odr_l_dat = so.RealData(x=maxima[:peaks], y=energies[:peaks], sx=maxima_sig[:peaks])
    odr_l_set = so.ODR(odr_l_dat, mod_l, beta0=[1., 2.])
    odr_l_output = odr_l_set.run()
    
    c0, c1, c2 = odr_l_output.beta[1], odr_l_output.beta[0], 0
    c0err, c1err, c2err = odr_l_output.sd_beta[1], odr_l_output.sd_beta[0], 0
    
    if polyfit:
        p_in = (c0, c1, 0)
        
        mod_poly = so.Model(poly)
        odr_poly_dat = so.RealData(x=maxima[:peaks], y=energies[:peaks], sx=maxima_sig[:peaks])
        odr_poly_set = so.ODR(odr_poly_dat, mod_poly, beta0=[1., 1., 1.])
        odr_poly_output = odr_poly_set.run()
        #print(maxima[:peaks])
        #print(energies[:peaks])
        #plt.scatter(maxima[:peaks],energies[:peaks])
        #plt.show()
        c0, c1, c2 = odr_poly_output.beta[0], odr_poly_output.beta[1], odr_poly_output.beta[2]
        c0err, c1err, c2err = odr_poly_output.sd_beta[0], odr_poly_output.sd_beta[1], odr_poly_output.sd_beta[2]
        
    if debug2:
        plt.scatter(maxima[:peaks], energies[:peaks])
        # print(opt_l)
        x_r = np.arange(0, len(arr), 200)
        plt.plot(x_r, poly(x_r, c0, c1, c2), color='navy')

    if fwhm:
        return ((c0, c1, c2, c0err, c1err, c2err), np.array(peak_width),
                np.array(peak_width_error), energies[:len(maxima)])
    return c0, c1, c2, c0err, c1err, c2err

    

def fit_odr(x, y, sx=1e-5, sy=1e-5, mode='lin', debug=False):
    ''''''
    if mode == 'lin':
        mod_l = so.Model(lin)
        m_guess = (y[2] -  y[0]) / (x[2] - x[0])
        initval = [m_guess, 1.]
    else:
        print("Error: Invalid model defined")
        
    odr_l_dat = so.RealData(x=x, y=y, sx=sx)
    odr_l_set = so.ODR(odr_l_dat, mod_l, beta0=initval)
    odr_l_output = odr_l_set.run()

    if debug == True:
        odr_l_output.pprint()
        print('Init: ',initval)
        #print(x,y)
        plt.scatter(x,y)
        plt.plot(x,lin(odr_l_output.beta,x))
        plt.show()
        print("opt: ", odr_l_output.beta)
        print("stdev: ", odr_l_output.sd_beta)
    return odr_l_output

def lin(B, x):
    '''Linear function y = m*x + b
    B: is a vector of the parameters.
    x: is an array of the current x values.'''
    return B[0]*x + B[1]
    
def poly(B, x):
    '''2nd order polynomial: y = a + b*x + c*x^2
    B: is a vector of the parameters.
    x: is an array of the current x values.'''
    return B[0] + B[1] * x + B[2] * x ** 2

def data_to_sigma_abs(data, zerovaluecorrection=False):
    '''Converts a 1D data array to an array containing the standard dev of the 
    given data. It is assumed that small probabilites are valid. A correctio 
    for zero values in the data can be applied''' 
    if zerovaluecorrection == True:
        data = np.where(data<=1, 1, data)
        
    sigma = np.sqrt(data)
    return sigma
    
def peaks_predicted_qnd(peaks_largest_ch, peaks_e, peaks_largest_e=np.array([121.78, 344.28]), p=2):
    '''This function uses the two Eu-152 peaks with the most counts to give a 
    rough estimate of the channels at which the peaks should be found'''
    opt_reg = linregress(peaks_largest_ch[:p], peaks_largest_e[:p])
    
    guessed_peak_ch = (peaks_e - opt_reg[0])  / opt_reg[0] 
    return abs(guessed_peak_ch)


"""
gauss_offset is only used in
- fit_peak_gauss (which is only used in legacy code)
- energy_fig (which is legacy code for Eu-152 calibration)
- leon2.py: probably some individual scripts
=> LEGACY
"""
def gauss_offset(x, a=100, b=511, c=40, o=0):
    return a * numpy.exp(-((x - b) / c) ** 2 * 0.5) + o


def gauss(x, a=100, b=511, c=40):
    return a * numpy.exp(-((x - b) / c) ** 2 * 0.5)

def gauss_x2(x, a1, a2, b1, b2, c1, c2):
    return a1 * numpy.exp(-((x - b1) / c1) ** 2) + a2 * numpy.exp(-((x - b2) / c2) ** 2)


def gauss_2d(xy, a=10000, mu_x=520, mu_y=520, sig_x=55, sig_y=55, roh=0.65):
    x, y = xy
    val = a * numpy.exp((-1 / (2 * (1 - roh ** 2))) * ((((x - mu_x) ** 2) / (sig_x ** 2)) +
                                                       (((y - mu_y) ** 2) / (sig_y ** 2)) +
                                                       (2 * roh * (x - mu_x) * (y - mu_y) / (sig_x * sig_y))))
    return val.ravel()


def gauss_2d2(xy, m=10000, x_0=520, y_0=520, sig_x=55, sig_y=55, theta=numpy.pi/4):

    x, y = xy
    a = (numpy.cos(theta) ** 2) / (2 * sig_x ** 2) + (numpy.sin(theta) ** 2) / (2 * sig_y ** 2)
    b = -(numpy.sin(2 * theta)) / (4 * sig_x ** 2) + numpy.sin(2 * theta) / (4 * sig_y ** 2)
    c = (numpy.sin(theta) ** 2) / (2 * sig_x ** 2) + (numpy.cos(theta) ** 2) / (2 * sig_y ** 2)

    z = m * numpy.exp(-(a * (x - x_0) ** 2 + 2 * b * (x - x_0) * (y - y_0) + c * (y - y_0) ** 2))

    return z.ravel()


def getmid(array, preset=0, n_fixpoints=5, spacing=20, halfwidth=20, debug=False, fine_debug=False):
    """This function calculates the center and slope of the coincident data.
    Function: getmid(array: {np.array([1024, 1024])}, debug: {bool}=False, fine_debug: {bool}=False)"""

    if preset == 1:
        halfwidth = 10
        n_fixpoints = 4
        spacing = 10

    print(preset)

    warnings.simplefilter("error", OptimizeWarning)

    yrange = np.arange(np.argmax(array[:, 5]) - 150, np.argmax(array[:,5]) + 150, 1)
    xrange = np.arange(np.argmax(array[5 , :]) - 150, np.argmax(array[5 , :]) + 150, 1)

    ylist = np.sum(array[yrange, 5:25], axis=1)
    xlist = np.sum(array[5:25, xrange], axis=0)

    px = (max(xlist), xrange[np.argmax(xlist)], 15)
    py = (max(ylist), yrange[np.argmax(ylist)], 15)

    xfit = None
    yfit = None

    try:
        xfit = curve_fit(gauss, xrange, xlist, px)
        yfit = curve_fit(gauss, yrange, ylist, py)
    except OptimizeWarning:
        print('The gauss fitting of the background failed. Hence, the middle of the spectrum cannot be found. '
              '\nPlease verify data.')
        quit()

    xpopt, xppas = xfit
    ypopt, yppas = yfit

    xmidf = xpopt[1]
    ymidf = ypopt[1]

    xmid = np.rint(xpopt[1])
    ymid = np.rint(ypopt[1])

    # print(xpopt)
    # print(ypopt)

    # 2. Calculation of the tilt of the photo peak
    # center = xmidf, ymidf
    # print(center)

    #   First Gauß at +/-60

    fit_x = numpy.arange(- halfwidth, halfwidth + 1, 1)
    yslopepts = []
    xslopepts = []
    deviation = []
    fixpoints = list(range(n_fixpoints, -n_fixpoints - 1, -1))

    for i in fixpoints:
        offset = i * spacing
        yvals1mid = int(ymid) - offset
        xvals1mid = int(xmid) + offset
        vals1 = []

        for j in fit_x:
            vals1.append(array[yvals1mid + j, xvals1mid + j])

        p1v = (max(vals1[15:35]), 0, 5)
        fit1 = None

        try:
            fit1 = curve_fit(gauss, fit_x, vals1, p1v, maxfev=2000)
        except OptimizeWarning:
            print("Gauss fitting of one of the broadened peak boundaries failed. "
                  "\nThis error is most likely caused by a too few counts in the measuremet spectrum.")
            quit()

        popt1, ppas1 = fit1
        # print(popt1)
        deviation.append(popt1[1])
        #xslopepts.append(xmidf + offset)
        #yslopepts.append(popt1[1] * numpy.sqrt(2) - offset + ymidf)
        xslopepts.append(popt1[1] + offset + xmidf)
        yslopepts.append(popt1[1] - offset + ymidf)
        # includes correction for inversion of y axis in array

    slope, intercept, r, p, std = linregress(xslopepts, yslopepts)

    # recalculate middle
    '''
    x_fit_mid = [ix for ix in range(440, 611, 1)]
    y_fit_mid = [ix * slope + intercept for ix in x_fit_mid]
    z_fit_mid = []
    for i in range(len(x_fit_mid)):
        z_fit_mid.append(array[int(y_fit_mid[i]), int(x_fit_mid[i])] +
                         array[int(y_fit_mid[i]) + 1, int(x_fit_mid[i]) + 1] +
                         array[int(y_fit_mid[i]) - 1, int(x_fit_mid[i]) - 1])

    fit_mid = curve_fit(gauss, x_fit_mid, z_fit_mid, (max(z_fit_mid), 520, 40), maxfev=200)
    opt, pas = fit_mid

    if fine_debug:
        y = []
        for i in x_fit_mid:
            y.append(gauss(i, opt[0], opt[1], opt[2]))
        plt.scatter(x_fit_mid, z_fit_mid)
        plt.plot(x_fit_mid, y)
        plt.show()
        print(opt)
        print(pas)

    #'''

    x, y = numpy.meshgrid(xrange, yrange)
    mid_init = (array.max(), xmidf, ymidf, 25, 10, np.pi/4)

    # opt, pas = curve_fit(gauss_2d, (x, y), array[450:601, 450:601].reshape(22801), mid_init)
    opt, pas = curve_fit(gauss_2d2, (x, y),
            array[yrange[0]:yrange[-1]+1, xrange[0]:xrange[-1]+1].reshape(len(yrange)*len(xrange)), mid_init)

    perr = numpy.sqrt(numpy.diag(pas))

    a, x_0, y_0, sig_x, sig_y, theta = opt

    new_slope = -numpy.tan(theta)
    if new_slope > 0:
        new_slope = numpy.tan(theta)
    new_intercept = y_0 - new_slope * x_0

    x_mid = opt[1]
    y_mid = opt[2]

    intercept = y_mid - (slope * x_mid)

    if fine_debug:
        print('Fit params (errors below): ')
        print(opt)
        print(perr)
        print()
        print('Gauss slope: ', new_slope)
        print('Linear slope: ', slope)
        print('Gauss y_int: ', new_intercept)
        print('Linear y_int: ', intercept)

    center = x_mid, y_mid

    if debug:
        heatmap(array)
        plt.scatter(xslopepts, yslopepts, color='black')
        y_old = []
        y_new = []
        for i in range(200, 825, 1):
            y_old.append(intercept + slope * i)
            y_new.append(new_intercept + new_slope * i)
        plt.plot(range(200, 825, 1), y_old, color='red')
        plt.plot(range(200, 825, 1), y_new, color='green')
        # plt.scatter([5, xmidf, xmidf], [ymidf, 5, ymidf], color='white')
        plt.scatter(x_mid, y_mid, color='white')
        plt.show()

        print('  slope:', slope, '  y axis intercept:', intercept, ' std:', std)

    return slope, intercept, center


def coinc_e_approx(spec_2d, energy_cal=None):
    from stacs.coinc.roi_fit import energyres
    slope, intercept, center = getmid(spec_2d)
    coinc_eres = energyres(spec_2d, energy_cal)


def calcroi(arr, bg, w=15, debug=False, offset=0, *args, **kwargs):
    """ This function only determines the ROI region as an aliased rectangle and cuts it out of the heatmap.
    Function calcroi(arr: {np.array([1024, 1024])}, w: {int}=15). Takes input array and ROI width in pixels."""

    print(args, kwargs)

    slope, intercept, center = getmid(arr, *args, **kwargs)
    slope_rad = m.atan(slope)

    if bg is None:
        bg_matrix = np.zeros(arr.shape)
    else:
        bg_matrix = numpy.full(arr.shape, bg, float)

    # First find the Midpoint on the linear fit
    slope2 = (-1 / slope)
    intercept2 = center[1] - (slope2 * center[0])  # Perpendicular to slope line
    x_m = (intercept2 - intercept) / (slope - slope2)
    y_m = slope * x_m + intercept
    # fitcenter = (x_m, y_m)
    # print('center: ', fitcenter, 'slobe angle: ', slope_rad, 'slope', slope)

    array = arr.copy()
    intercept_off = intercept
    intercept -= offset
    intercept_u = intercept + (w / m.cos(slope_rad))
    intercept_l = intercept - (w / m.cos(slope_rad))

    def inv_upperline(y_in):
        return (y_in - intercept_u) / slope

    def inv_lowerline(y_in):
        return (y_in - intercept_l) / slope

    for y in range(array.shape[0]):
        for x in range(array.shape[1]):
            # y values of the border lines at current column
            upper_l = slope * (x - 0.5) + intercept_u  # upper left
            upper_r = slope * (x + 0.5) + intercept_u  # upper right
            lower_r = slope * (x + 0.5) + intercept_l  # lower right
            lower_l = slope * (x - 0.5) + intercept_l  # lower left

            #  all pixels outside the ROI are set to 0
            if upper_l < y - 0.5 or lower_r > y + 0.5:
                array[y][x] = 0
                bg_matrix[y][x] = 0

            #  pixels intercepting the upper ROI border are being multiplied by the percentage area
            #  that lies inside the ROI
            elif upper_l + 0.5 > y > upper_r - 0.5:
                if upper_l > y + 0.5:
                    if upper_r > y - 0.5:
                        a = (x + 0.5 - inv_upperline(y + 0.5))
                        b = (y + 0.5 - upper_r)
                        area = 1 - (a * b * 0.5)
                    else:
                        a = inv_upperline(y - 0.5) - inv_upperline(y + 0.5)
                        c = x + 0.5 - inv_upperline(y - 0.5)
                        area = 1 - (a * 0.5 + c)
                else:
                    if upper_r > y - 0.5:
                        a = upper_l - upper_r
                        c = y + 0.5 - upper_l
                        area = 1 - (a * 0.5 + c)
                    else:
                        a = upper_l - (y - 0.5)
                        b = inv_upperline(y - 0.5) - (x - 0.5)
                        area = a * b * 0.5

                array[y][x] *= area
                bg_matrix[y][x] *= area

            #  pixels intercepting the lower ROI border are being multiplied by the percentage area
            #  that lies inside the ROI
            elif lower_l + 0.5 > y > lower_r - 0.5:
                if lower_l > y + 0.5:
                    if lower_r > y - 0.5:
                        a = (x + 0.5 - inv_lowerline(y + 0.5))
                        b = (y + 0.5 - lower_r)
                        area = a * b * 0.5
                    else:
                        a = inv_lowerline(y - 0.5) - inv_lowerline(y + 0.5)
                        c = x + 0.5 - inv_lowerline(y - 0.5)
                        area = a * 0.5 + c
                else:
                    if lower_r > y - 0.5:
                        a = lower_l - lower_r
                        c = y + 0.5 - lower_l
                        area = a * 0.5 + c
                    else:
                        a = lower_l - (y - 0.5)
                        b = inv_lowerline(y - 0.5) - (x - 0.5)
                        area = 1 - (a * b * 0.5)

                array[y][x] *= area
                bg_matrix[y][x] *= area

    # Returns in order:     Array trimmed to ROI, slope of the ROI, perpendicular slope to ROI, y intercept of
    #                       linear trough ROI center, y intercept of upper ROI boundary, y intercept of lower ROI
    #                       boundary, coordinates of center of ROI.

    if debug:
        print(slope, intercept, intercept_u, intercept_l)

        heatmap(arr)

        x = np.linspace(0, arr.shape[1], 5)
        y_m = x.copy() * slope + intercept
        y_l = x.copy() * slope + intercept_l
        y_u = x.copy() * slope + intercept_u
        y_tan = x.copy() * slope2 +intercept2
        print(y_u)
        plt.plot(x, y_m, color='black', alpha=0.8)
        plt.plot(x, y_tan, color='black', alpha=0.8)
        plt.plot(x, y_l, color='navy', alpha=0.5)
        plt.plot(x, y_u, color='navy', alpha=0.5)
        plt.show()

    return array, slope, intercept_off, slope2, intercept2, intercept_u, intercept_l, (x_m, y_m), bg_matrix


def shortest(array, intercept2, d_intercept, slope, slope2, old_l, old_u, bg_matrix, animation=False, debug=False):
    if animation:
        import matplotlib.animation as animation
        heat = plt.imshow(array, origin='lower', interpolation=None,
                          norm=matplotlib.colors.LogNorm(vmin=array.min() + 1, vmax=array.max() + 1), cmap='jet')
        imlist = []
    count = 20
    countsum = []
    bg_sum = []
    subarray = array.copy()
    if debug:
        plt.subplot(121)

    for ch in range(len(d_intercept) - 1):

        cache = 0.
        cache_bg = 0.

        # crop testing region to improve peformance
        intercept_l = d_intercept[ch]
        intercept_u = d_intercept[ch + 1]

        x_min = int((intercept_l / slope2 - old_l / slope) / (1 / slope2 - 1 / slope)) - 3
        x_max = int((intercept_u / slope2 - old_u / slope) / (1 / slope2 - 1 / slope)) + 3

        y_min = int((old_l - intercept_u) / (slope2 - slope)) - 2
        y_max = int((old_u - intercept_l) / (slope2 - slope)) + 2

        # correct for x/y_min/max out ouf bounds
        array_dim = array.shape[0]
        y_min = max(0, y_min)
        y_max = min(array_dim, y_max)
        x_min = max(0, x_min)
        x_max = min(array_dim, x_max)

        fraclist = []

        for x in range(x_min, x_max, 1):
            '''
            x_min = int(((y - old_l) / slope) - 3)
            x_max = int(((y - old_u) / slope) + 3)
            '''

            for y in range(y_min, y_max, 1):
                d_u = (abs(slope2 * y - x + intercept_u)) / (numpy.sqrt(slope2 ** 2 + 1))  # Distance of current pixel to upper line
                d_l = (abs(slope2 * y - x + intercept_l)) / (numpy.sqrt(slope2 ** 2 + 1))  # Distance of current pixel to lower line

                dist = numpy.cos(numpy.arctan(slope2)) * (intercept_u - intercept_l)
                # perpendicular distance between the two bin boundaries

                if d_u > dist + numpy.sqrt(2) / 2 or d_l > dist + numpy.sqrt(2) / 2:
                    pass

                # '''
                elif d_u > numpy.sqrt(2) / 2 and d_l > numpy.sqrt(2) / 2:
                    cache += array[x, y]
                    cache_bg += bg_matrix[x, y]
                    if debug or animation:
                        subarray[x, y] = 0

                elif d_u < numpy.sqrt(2) / 2:
                    frac = - d_u ** 2 + numpy.sqrt(2) * d_u
                    fraclist.append(frac)
                    if d_l >= dist:
                        frac = 0.5 - frac
                    else:
                        frac = 0.5 + frac
                    cache += array[x, y] * frac
                    cache_bg += bg_matrix[x, y] * frac
                    if debug or animation:
                        subarray[x, y] -= frac * array[x, y]

                elif d_l < numpy.sqrt(2) / 2:
                    frac = - d_l ** 2 + numpy.sqrt(2) * d_l
                    fraclist.append(frac)
                    if d_u >= dist:
                        frac = 0.5 - frac
                    else:
                        frac = 0.5 + frac
                    cache += array[x, y] * frac
                    cache_bg += bg_matrix[x, y] * frac
                    if debug or animation:
                        subarray[x, y] -= frac * array[x, y]

        bg_sum.append(cache_bg)
        countsum.append(cache)

        if debug:
            x1 = [x_r for x_r in numpy.arange(y_min, y_max + 20)]
            x2 = [x_r for x_r in numpy.arange(y_min, y_max + 20)]
            y1 = [y_r * slope2 + intercept_u for y_r in x1]
            y2 = [y_r * slope2 + intercept_l for y_r in x2]
            plt.plot(x1, y1, color='black', linewidth=.5)
            plt.plot(x2, y2, color='black', linewidth=.5)

        if animation:

            fig = plt.figure(1)
            ax = fig.add_subplot(111)

            heat.set_data(subarray)

            #cbar = ax.colorbar(heat)
            #'''
            plt.xlim(left=0, right=1024)
            plt.ylim(bottom=0, top=1024)
            plt.xlabel('Detector 1 channel')
            plt.ylabel('Detector 2 channel')
            # heatmap(array)
            count -= 1
            x1 = [x_r for x_r in numpy.arange(y_min, y_max+20)]
            x2 = [x_r for x_r in numpy.arange(y_min, y_max+20)]
            y1 = [y_r * slope2 + intercept_u for y_r in x1]
            y2 = [y_r * slope2 + intercept_l for y_r in x2]
            x_diag = [i for i in range(0, 1024)]
            y_diag_u = [x_diag[i] * slope + old_u for i in range(len(x_diag))]
            y_diag_l = [x_diag[i] * slope + old_l for i in range(len(x_diag))]
            #ax.plot(x_diag, y_diag_l, color='black', linewidth=.5)
            #ax.plot(x_diag, y_diag_u, color='black', linewidth=.5)
            ax.plot(x1, y1, color='black', linewidth=.5)
            ax.plot(x2, y2, color='black', linewidth=.5)
            fig.canvas.draw()
            plt.pause(0.2)

            fig.canvas.flush_events()
            #'''

    if animation:
        print(slope, slope2)
        print(numpy.sum(subarray))
    if debug:
        heatmap(array, i_fig=False)
        plt.subplot(122)
        heatmap(subarray, i_fig=False)
        plt.show()
    return countsum, bg_sum


def chopper(array, intercept2, d_intercept, slope, slope2, old_l, old_u, debug):
    countsum = []
    for ch in range(len(d_intercept)):

        intercept_u = intercept2 + d_intercept[len(d_intercept) - ch - 1]

        intercept_l = intercept2 - d_intercept[len(d_intercept) - ch - 1]

        # reducing area of array to be checked for counts for optimization (factor 10ish faster):

        x_y_min = (intercept_l - old_l) / (slope - slope2)
        x_y_max = (intercept_u - old_u) / (slope - slope2)
        y_min = int((slope2 * x_y_min + intercept_l) - 5)
        y_max = int((slope2 * x_y_max + intercept_u) + 5)

        if y_min < 0:
            y_min = 0
        if y_max > 1023:
            y_max = 1023
        if ch == 0:
            y_min = 0
            y_max = 1024

        for y in range(y_min, y_max, 1):

            x_min = int(((y - old_l) / slope) - 5)
            x_max = int(((y - old_u) / slope) + 5)

            if x_min < 0:
                x_min = 0
            if x_max > 1023:
                x_max = 1023

            for x in range(x_min, x_max, 1):
                # y values of the border lines at current column
                upper_l = slope2 * (x - 0.5) + intercept_u  # upper left
                upper_r = slope2 * (x + 0.5) + intercept_u  # upper right
                lower_r = slope2 * (x + 0.5) + intercept_l  # lower right
                lower_l = slope2 * (x - 0.5) + intercept_l  # lower left

                #  all pixels outside the ROI are set to 0
                if array[y][x] == 0:
                    continue

                elif upper_r < y - 0.5 or lower_l > y + 0.5:
                    array[y][x] = 0

                #  pixels intercepting the upper ROI border are being multiplied by the percentage area
                #  that lies inside the ROI
                elif upper_r + 0.5 > y > upper_l - 0.5:
                    if upper_r > y + 0.5:
                        if upper_l > y - 0.5:
                            a = (((y + 0.5) - intercept_u) / slope2) - (x - 0.5)
                            b = (y + 0.5 - upper_l)
                            area = 1 - (a * b * 0.5)
                        else:
                            a = (((y + 0.5) - intercept_u) / slope2) - (((y - 0.5) - intercept_u) / slope2)
                            c = (((y - 0.5) - intercept_u) / slope2) - (x - 0.5)
                            area = 1 - (a * 0.5 + c)
                    else:
                        if upper_l > y - 0.5:
                            a = upper_r - upper_l
                            c = y + 0.5 - upper_r
                            area = 1 - (a * 0.5 + c)
                        else:
                            a = upper_r - (y - 0.5)
                            b = (x + 0.5) - (((y - 0.5) - intercept_u) / slope2)
                            area = a * b * 0.5

                    array[y][x] *= area

                #  pixels intercepting the lower ROI border are being multiplied by the percentage area
                #  that lies inside the ROI
                elif lower_r + 0.5 > y > lower_l - 0.5:
                    if lower_r > y + 0.5:
                        if lower_l > y - 0.5:
                            a = (((y + 0.5) - intercept_l) / slope2) - (x - 0.5)
                            b = (y + 0.5 - lower_l)
                            area = a * b * 0.5
                        else:
                            a = (((y + 0.5) - intercept_l) / slope2) - (((y - 0.5) - intercept_l) / slope2)
                            c = x + 0.5 - (((y - 0.5) - intercept_l) / slope2)
                            area = a * 0.5 + c
                    else:
                        if lower_l > y - 0.5:
                            a = lower_r - lower_l
                            c = y + 0.5 - lower_r
                            area = a * 0.5 + c
                        else:
                            a = lower_r - (y - 0.5)
                            b = (x + 0.5) - (((y - 0.5) - intercept_l) / slope2)
                            area = 1 - (a * b * 0.5)

                    array[y][x] *= area
        countsum.append(numpy.sum(array))
        if debug and ch % 25 == 0:
            heatmap(array)
            x1 = [x_r for x_r in numpy.arange(0, 1024)]
            x2 = [x_r for x_r in numpy.arange(0, 1024)]
            y1 = [y_r * slope2 + intercept_u for y_r in x1]
            y2 = [y_r * slope2 + intercept_l for y_r in x2]
            plt.plot(x1, y1)
            plt.plot(x2, y2)
            plt.show()

    return countsum
