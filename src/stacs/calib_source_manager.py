import numpy as np
import json
import os

"""
the jsons/dicts with literature values are always of this form:
dicts = {
            "name": str
            "peaks": [[Energy 0, Intensity 0], [E1, I1], [E2, I2], ...]
            "initial_guess_peaks": [Energy 0, E1, ...]
        }
"""

# TODO where to store the "database" files?
this_file_directory = os.path.dirname(os.path.abspath(__file__))
calib_source_directory = os.path.join(this_file_directory, "./data/calib_sources")
calib_source_directory = os.path.abspath(calib_source_directory)


def import_csv(csv_filename, isotope_name, initial_guess_peaks):
    """
    Reads csvs from https://www-nds.iaea.org/relnsd/vcharthtml/VChartHTML.html (tab "Decay Radiation")
    and converts them to the json format needed by e.g. SingleSpectrum.calibrate().

    Saves a json file with peaks sorted by intensity at the right location (next to this script) where this module
    automatically searches for jsons.

    Run this manually to add/modify database files.

    Parameters
    -------------
    csv_filename: str
        The csv file to read.
    isotope_name: str
        An identifier for the source, e.g. "Eu152".
    initial_guess_peaks: list of float
        For a first calibration guess, the n highest peaks in the spectrum are mapped to already known energies.
        For a certain source, these energies are specified with this parameter and are saved in the source json file.
        The selection will depend on the setup-specific observed spectrum: For example, absorption by metal between
        source and detector may remove peaks of the spectrum described in literature.
        Only enter the energies where you are very sure that their peaks will be the highest ones, as
        otherwise the calibration will fail. Copy those values manually from the csv you use.
        Do *not* add the 511 keV peak, it has a flag in the calibration function.
        Sorry, there's no better solution for this yet.
    json_out_filename: str
        The filename of the json file to output
        if None, f"{isotope_name}.json" is used.
    """
    import pandas as pd  # TODO add pandas to project requirements?
    table = pd.read_csv(csv_filename, skipinitialspace=True, index_col=False)
    table = table[["energy", "intensity %"]].sort_values(by="intensity %", ascending=False).replace("", np.nan).dropna()
    # print(table)
    tups = list(table.itertuples(index=False, name=None))
    json_o = {
        "name": isotope_name,
        "peaks": tups,
        "initial_guess_peaks": initial_guess_peaks
    }
    print("json to save:")
    print(json.dumps(json_o))
    saveloc = os.path.join(calib_source_directory, f"{isotope_name}.json")
    f = open(saveloc, "w")
    print("save location: ", saveloc)
    json.dump(json_o, f)
    f.close()


def import_json_file(filename):
    """
    Reads json from given filename and returns the resulting dict.
    The calibration source dicts will look like the one shown at the top of
    calib_source_manager.py
    """
    f = open(filename)
    o = json.load(f)
    f.close()
    return o


cache = {}


def get_radsource_info(name):
    """
    Attempts to return the radition source info dict for a source of "name".

    "name" is case-insensitive (without extension). The file is expected in
    cdb_evaluation/stacs/data/calib_sources
    TODO agree on location.
    (files have to be next to calib_source_manager.py)
    The calibration source dicts will look like the one shown at top of
    calib_source_manager.py
    """
    # cached?
    if name.lower() in cache:
        return cache[name.lower()]

    filenames = os.listdir(calib_source_directory)
    # read all jsons and return the first one with the right name
    for f in filenames:
        if f.endswith(".json"):
            d = import_json_file(os.path.join(calib_source_directory, f))
            try:
                if d["name"].lower() == name.lower():
                    cache[name.lower()] = d
                    return d
            except KeyError:
                raise Exception("malformed json: " + f)
    raise Exception(f"Element with name {name} not found.")
