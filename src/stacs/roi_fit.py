import matplotlib.pyplot as plt
import numpy
from stacs.core.algorithms import chopper, shortest, calcroi, getmid, gauss, gauss_x2
from stacs.core.classes import Spectrum
import matplotlib.patches as patch
from pathlib import Path
import numpy as np


def energyres(array, ecal=None, debug=False, ret_cha=False, *args, **kwargs):
    """This function takes a coincidence data sprectrum and calculates the coincidence energy resolution.
    In a coincidence spectrum the constant energy diagonal at 1022 keV contains the doppler broadened energies along
    its length. In an ideal world this line would be very thin, being only broadened by the binding energy of the
    atomic orbitals. In reality the diagonal is broadened due to finite detector resolution. As a result the detector
    resolution can be extracted from a coincidence spectrum. The resulting resolution is a combination of the energy
    resolutions of both detectors and about 1/sqrt(2) better than the individual detector resolution.

    Parameters
    ----------
    array : np.array
        2D numpy array containing coincidence data.
    ecal : tuple, optional
        Energy calibration as a tuple (default is None).
    debug : bool, optional
        If True a plot of the simple projection.
    ret_cha : bool, optional
        If True the FWHM will be returned in channels, not keV.

    Returns
    -------
    fwhm : float
        FWHM of the coincidence energy resolution.
    """
    import scipy.ndimage as nd
    from scipy.optimize import curve_fit
    slope, intercept, center = getmid(array, *args, **kwargs)
    rad = np.arctan(1 / np.abs(slope))
    angle = rad * (180 / np.pi)
    arr_rot = nd.rotate(array, angle, reshape=False, order=3)
    arr_map = arr_rot >= 0
    arr_rot = arr_rot * arr_map
    cent_n = np.unravel_index(np.argmax(arr_rot), arr_rot.shape)
    res1 = np.sum(arr_rot[cent_n[0]-3:cent_n[0]+3,:], axis=0)
    p_in = [np.max(res1), np.argmax(res1), 30]
    opt, pas = curve_fit(gauss, range(len(res1)), res1, p_in)


    fwhm = 2 * np.sqrt(np.log(2) * 2) * opt[2]

    if ret_cha:
        return fwhm

    vals = fwhm

    if ecal is None:
        print('No ecal found. Using default for NEPOMUC CDB!')
        #fwhm /= np.sin(rad) * 61 + np.cos(rad) * 62.44
        # fwhm *= (np.sin(angle) * 61 + (1 - np.cos(angle)) * 62.44)
        fwhm *= ((1 - np.abs(slope)) * 62.44 + np.abs(slope) * 61)
        #fwhm *= 61
    else:
        fwhm *= ((1 - np.abs(slope)) * ecal[1] + np.abs(slope) * ecal[0])

    fwhm /= 1000    # convert to keV

    if debug:
        center = opt[1]
        from stacs.core.visualizer import heatmap
        plt.figure(1)
        heatmap(arr_rot)
        plt.show()
        plt.figure(2)
        plt.axvspan(center - vals / 2, center + vals / 2, color='orange', alpha=0.5, label='FWHM')
        plt.scatter(range(len(res1)), res1, color='crimson', label='data')
        plt.plot(range(len(res1)), [gauss(i, *opt) for i in range(len(res1))], color='navy', label='fit')
        plt.xlabel('Channel')
        plt.ylabel('Counts')
        plt.xlim(left=center - 50, right=center+50)
        plt.legend()
        plt.show()

    return fwhm


def lineare(x, s=1., b=0.):
    return (s * x) + b


def importold(filename):
    oldfile = open(filename)
    old_e = []
    old_c = []
    old_err = []
    for i in oldfile:
        line = i.replace(',', '.')
        e, c, err = line.split('\t', 3)
        old_e.append(float(e)), old_c.append(float(c)), old_err.append(float(err))
    return old_e, old_c, old_err


def binning(measurement, binfile='Bins100.txt', detectorpair=None, norm=0,
            width=None, background_corr=False, export=False, mirrored=True,
            orbit_offset=0, debug=False, debug_anim=False, *args, **kwargs):
    """This function supervises the binning process of coincidence spectra. The actual cutting is performed elsewhere.

    This function performs important operations before and after the binning of spectrum.
    First the calcroi function is called, with cuts the roi section out of the coincidence spectrum.
    The energy calibration associated with the measurement is used to determine the locations of the bin boundaries.
    The function shortest from stacs.core.algorithms is then called to bin the sprectrum.
    The function returns the finished projection.

    Parameters
    ----------
    measurement : stacs.core.classes.Spectrum object
        Measurement object containing the coincidence data.
    binfile : str, optional
        Filename of the binfile used to determine the boundaries for the bins located in stacs/data/bins
        (default is 'Bins100.txt').
    detectorpair : str, optional
        Detectorpair to be used for evaluation. By default Spectrum.bettercoinc will be used (default is None).
    norm : int, optional
        Method used to normalize the spectrum, for ratiocurves use default (default is 0).
    width : float, int, optional
        Width of the Region of Interest, by default the FWHM of the coincidence energy resolution will be used
        (default is None)
    background_corr : bool, optional
        If True a constand background calculated from the spectrum will be subtracted. This can increase accuracy for
        high broadening momenta (default is False)
    export : bool, optional
        If True the data will be saved in a text file (default is False)
    mirrored : bool, optional
        If true the roughly symmetrical doppler boradening beak will be mirrored to one side, optimal for ratio curves
        (default is True)
    orbit_offset : int, float, optional
        This parameter will offset the Region of Interest to lower sum energies, required to analyse the electronical
        structure (default is 0)
    debug : bool, optional
        Debug parameter, if True the heatmap will be displayed showing the bin channels and the cut spectrum
        (default is False)
    debug_anim : bool, optional
        Debug parameter, if True a animation of the Roi cutting process will be shown (default is False).
    args : tuple, optional
        Will be passed on to calcroi and subsequently getmid.
    kwargs : dict, optional
        Will be passed on to calcroi and subsequently getmid.
    Raises
    ------
    KeyError
        If no energy calibration is specified the process will be aborted.

    Returns
    -------
    d_bin_n : np.array
        Energy data from projection
    projection : np.array
        Counts from projection
    div : np.array
        Error of count rate.
    """

    import time

    if detectorpair is None:
        detectorpair = measurement.bettercoinc

    background = measurement.coincbackground(detectorpair)
    if len(detectorpair) == 2:
        det_dict1 = measurement.singles[detectorpair[0]]
        det_dict2 = measurement.singles[detectorpair[1]]
    else:
        dets = detectorpair.split(' ')
        det_dict1 = measurement.singles[dets[0]]
        det_dict2 = measurement.singles[dets[1]]

    t1 = time.time()
    measarray = measurement.coinc[detectorpair]["MapData"].copy()
    if width is None:
        # calculate width from ecal
        width = measurement.fwhm_coinc(detectorpair, *args, **kwargs)

        if 'EnergyCalibration' in det_dict1:
            if det_dict1['EnergyCalibration'] is not None:
                width /= det_dict1['EnergyCalibration'][1] / 1000

            else:
                width /= 61 / 1000
                print('No ecal found. Using default for NEPOMUC CDB!')

        else:
            width /= 61 / 1000
            print('No ecal found. Using default for NEPOMUC CDB!')
    (array, slope, intercept, slope2, intercept2, old_u, old_l,
                                                        center, bg_matrix) \
            = calcroi(measarray, background, w=width, offset=orbit_offset, *args, **kwargs)
    print(np.sum(array))
    print('__________________________________________________________________')
    print(f'{measurement.name}:\n')
    print('Center found, ROI calculated successfully.')
    print(f'Starting binning with bins from file: "{binfile}"')
    if background_corr:
        print('Background Correction Active.')

    # BINNING

    x_m, y_m = center

    if detectorpair == 'AD':
        y_cal = 62.44 / 1000
        if 'EnergyCalibration' in det_dict1:
            if det_dict1['EnergyCalibration'] is not None:
                y_cal = det_dict1['EnergyCalibration'][1] / 1000

        x_cal = 61. / 1000
    elif detectorpair == 'BC':
        y_cal = 61.15 / 1000
        if 'EnergyCalibration' in det_dict1:
            if det_dict1['EnergyCalibration'] is not None:
                y_cal = det_dict1['EnergyCalibration'][1] / 1000

        x_cal = 59.15 / 1000
    else:
        # only ecal for det2 (y-axis) is needed,
        # the other one results from slope of heatmap diagonal
        if len(detectorpair) == 2:
            det2 = detectorpair[1]
        else:
            _, det2 = detectorpair.split(' ')
        try:
            # 2nd coefficient in keV/ch
            y_cal = measurement.singles[det2]["EnergyCalibration"][1] / 1e3
        except KeyError:
            print(f"DataWarning: Data is missing energy calibration values. \
Please specify <measurement>.singles[{det2}]['EnergyCalibration'] (tuple of \
three). Aborting.")
            quit()

    y_axis_corr = [y_m - (y_cal * i) for i in range(550, -550, -1)]

    y_dist = y_axis_corr[-1] - y_axis_corr[0]
    m_corr = y_dist / len(y_axis_corr)

    b_corr = 511 - m_corr * y_m

    binpath = Path('../data/bins')
    binnings = open((Path(__file__).parent.absolute() / binpath / binfile), 'r')
    bins = []

    # intercept2 += 15

    for line in binnings:
        tmp = line.replace(',', '.')
        tmp = float(tmp) * 1000
        bins.append(tmp)

    binnings.close()
    d_bin = []

    for val in range(len(bins) - 1):
        d_bin.append(bins[val + 1] - bins[0])

    d_bin_np_r = d_bin.copy()
    d_bin_np_r.reverse()

    d_bin_n = [i * (-1) for i in d_bin]

    d_bin_np = d_bin_np_r + d_bin_n.copy()

    d_bin_np = np.array(d_bin_np)

    d_bin_np /= 1000
    d_bin_np += 511


    d_bin_y = np.array([(i - b_corr)/m_corr for i in d_bin_np])
    d_bin_x = np.array([(i - intercept)/slope for i in d_bin_y])

    d_intercept = [(d_bin_y[i] - slope2 * d_bin_x[i]) * (1) for i in range(len(d_bin_y))]
    d_intercept.reverse()
    #return d_intercept

    # d_intercept = [(ij * numpy.sqrt(2)) / 44 for ij in d_bin]  # FIX
    # d_intercept = [ij * (4.4/(61 + 62.44)) for ij in d_bin]
    # return d_intercept
    # d_intercept = [(ij * 62.44 * numpy.tan(46 * (numpy.pi/180))) for ij in d_bin]
    print('Total events inside ROI: ', numpy.sum(array))

    countsum, bg_sum = shortest(array, intercept2, d_intercept, slope, slope2, old_l, old_u, bg_matrix,
                                debug_anim, debug)

    d_bin2 = [i * (- 1) for i in d_bin]
    d_bin.reverse()
    d_bin += d_bin2
    de_bin = [(abs(d_bin[i] - d_bin[i + 1])) / 1000 for i in range(len(d_bin) - 1)]
    d_bin_n = [(d_bin[i] - 0.5 * abs(d_bin[i] - d_bin[i + 1])) / 1000 for i in range(len(d_bin) - 1)]

    de_bin = numpy.array(de_bin)

    t2 = time.time()
    benchmark = t2 - t1

    totcounts = 1

    if norm == 0:
        totcounts = sum(countsum)
    elif norm == 1:
        totcounts = sum(countsum * de_bin)

    error = []

    for ij in range(len(countsum)):
        if background_corr:
            countsum[ij] -= bg_sum[ij]
            error.append(np.sqrt(countsum[ij] + bg_sum[ij]))
        else:
            error.append(np.sqrt(countsum[ij]))
        if norm != 3:
            countsum[ij] /= totcounts
            error[ij] /= totcounts
        d_bin_n[ij] += 511


    countsum /= de_bin
    error /= de_bin
    bg_sum /= de_bin

    #projection = countsum.copy()

    if mirrored:

        left = np.flip(countsum[0:len(countsum) // 2])
        right = countsum[len(countsum)//2 + 1:len(countsum)]
        center = countsum[len(countsum) // 2:len(countsum) // 2 + 1] * 2
        projection = np.append(center, left + right)

        left_e = np.flip(error[0:len(error) // 2])
        right_e = error[len(error) // 2 + 1:len(error)]
        center_e = error[len(error) // 2:len(error) // 2 + 1] * 2
        div = np.append(center_e, left_e + right_e)

        d_bin_n = d_bin_n[0:len(projection)]
    else:
        projection = countsum
        div = error
    d_bin_n.reverse()
    print('Calculation run time in [s]: ', benchmark, '\n')


    if export:
        # exportproj(d_bin[:-1], projection, error)
        pass

    return d_bin_n, projection, div


def orbitals(measurement, width, bins='Bins500red.txt', binsum=5, detector=None, debug=False, fit_gauss=False):
    from scipy.optimize import curve_fit
    import warnings

    rois = [width * 2 * i for i in range(0,15)]
    projections = []
    errors = []
    energy = None

    for offs in rois:
        e, proj, err = binning(measurement, binfile=bins, detectorpair=detector, orbit_offset=offs, width=width,
                               mirrored=False, norm=3)
        projections.append(proj)
        errors.append(err)
        if offs == 0:
            energy = e


    projections = np.array(projections, dtype=np.float)
    errors = np.array(errors)
    centers = []
    x = np.arange(0, projections.shape[0])

    warnings.filterwarnings('ignore')

    #for i in range(projections.shape[1]):
    #    fac = np.sum(projections[:,i])
    #    if fac != 0:
    #        projections[:,i] /= fac

    if fit_gauss:
        for en in range(binsum, projections.shape[1] - (binsum)):
            opt = [0, 0, 0, 0, 0, 0]
            try:
                if binsum != 0:
                    sl = np.sum(projections[:, en-binsum:en+binsum+1], axis=1)
                else:
                    sl = projections[:, en]

                opt, pas = curve_fit(gauss, x, sl, [np.max(sl), 8, 8])
            except RuntimeError:
                print(en)
            centers.append([opt[1], 0])
    else:
        for en in range(binsum, projections.shape[1] - binsum):
            sl = np.sum(projections[:, en-binsum:en+binsum+1], axis=1)
            centers.append([np.sum(sl * np.arange(1, len(sl) + 1)) /
                           np.sum(sl), 0])

    centers = np.array(centers)
    if debug:
        a, b, c, d, e = 18, 20, 23, 26, 33
        plt.figure(1)
        plt.semilogy(x, projections[:, a], label='18', color='crimson')
        plt.errorbar(x, projections[:, a], errors[:, a],
                     color='crimson', linestyle='None', capsize=2, elinewidth=1)
        plt.semilogy(x, projections[:, b], label='20', color='orange')
        plt.errorbar(x, projections[:, b], errors[:, b],
                     color='orange', linestyle='None', capsize=2, elinewidth=1)
        plt.semilogy(x, projections[:, c], label='23', color='blue')
        plt.errorbar(x, projections[:, c], errors[:, c],
                     color='blue', linestyle='None', capsize=2, elinewidth=1)
        plt.semilogy(x, projections[:, d], label='26', color='black', alpha=0.3)
        plt.errorbar(x, projections[:, d], errors[:, d],
                     color='blue', linestyle='None', capsize=2, elinewidth=1, alpha=0.3)
        plt.semilogy(x, projections[:, e], label='33', color='green')
        plt.errorbar(x, projections[:, e], errors[:, e],
                     color='green', linestyle='None', capsize=2, elinewidth=1)
        plt.legend()
        plt.show()

    print(projections.shape)
    center_y = [i for i in range(projections.shape[0])]
    return projections, centers, energy[binsum:len(energy)-binsum], energy


def compare(projection, oldfile, title=None):

    labellist = []
    old_e, old_c, old_err = importold(oldfile)
    e, c, err = projection
    print(sum(old_c))
    print(sum(c))
    plt.semilogy(e, c, color='navy')
    plt.errorbar(e, c, err, linestyle='None', capsize=2, elinewidth=1, errorevery=3, color='navy', alpha=0.4)
    plt.semilogy(old_e, old_c, 'crimson')
    plt.errorbar(old_e, old_c, old_err,
                 linestyle='None', capsize=2, elinewidth=1, errorevery=3, color='crimson', alpha=0.4)
    labellist.append(patch.Patch(label='python', color='navy'))
    labellist.append(patch.Patch(label='LabView', color='crimson'))
    if title is not None:
        plt.title(title)
    plt.legend(handles=labellist)
    plt.show()


def test_bins(mirr=False):

    test0 = Spectrum(filename='Messung001.mpa')
    test1 = Spectrum(filename='Messung001.mpa')

    projection1 = binning(test0, binfile='BinsVar.txt', mirrored=mirr, background_corr=True)
    projection2 = binning(test1, binfile='BinsVar.txt', mirrored=mirr, background_corr=False)

    plt.semilogy(projection1[0], projection1[1], color='blue')
    plt.errorbar(projection1[0], projection1[1], projection1[2], color='blue', linestyle='None',
                 capsize=2, elinewidth=1, alpha=0.2)
    plt.semilogy(projection2[0], projection2[1], color='orange')
    plt.errorbar(projection2[0], projection2[1], projection2[2], color='orange', linestyle='None',
                 capsize=2, elinewidth=1, alpha=0.2)
    plt.show()


def test_projec(file, mirr=False, bg=True):
    test0 = Spectrum(filename=file)

    projection1 = binning(test0, binfile='Bins100.txt', mirrored=mirr, background_corr=bg)

    plt.semilogy(projection1[0], projection1[1], color='blue')
    plt.errorbar(projection1[0], projection1[1], projection1[2], color='blue', linestyle='None',
                 capsize=2, elinewidth=1, alpha=0.2)
