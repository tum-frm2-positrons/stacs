import os, re
import numpy as np
import warnings

from .single import SingleSpectrum
from .coinc import CoincidenceSpectrum

def import_n42(filename, show=False, verbose=False):
    "Import function for standardized .n42 radiation measurement files."

    import xml.etree.ElementTree as ET
    root = ET.parse(filename).getroot()
    data = root.find("RadInstrumentInformation")

    metadata = {}

    for info in list(data):
        metadata[info.tag] = info.text
    meas = root.find("RadMeasurement")
    data = root.find("RadInstrumentInformation")

    metadata = {}
    for info in list(data):
        metadata[info.tag] = info.text

    # number values will be converted to float
    number_values = ["StepperX", "StepperY", "StepperZ", "SteppCorrX",
                     "SteppCorrY", "SteppCorrZ", "PiezoSetX", "PiezoSetY",
                     "PiezoCorrX", "PiezoCorrY", "PyrometerTemperature",
                     "PositronImplantationEnergy"]

    singles = []
    coinc = []

    for meta in list(meas):
        if meta.tag == "Spectrum":
            # get detector name
            det_id = meta.attrib["radDetectorInformationReference"]
            det = root.find(f".//RadDetectorInformation[@id='{det_id}']")
            detname = det.find("RadDetectorName").text

            # get energy calibration
            ecal_id = meta.attrib["energyCalibrationReference"]
            ecal = root.find(f".//EnergyCalibration[@id='{ecal_id}']")
            # check validity of the the imported energy calibration
            try:
                e_calib = np.fromstring(ecal.find("CoefficientValues").text,
                                        dtype=float, sep=" ")
                if np.size(e_calib) != 3: 
                    e_calib = None        
                    wrn = (f"Energy calibration for Det {detname} is malformed in "
                        "the datafile.")
                    warnings.warn(wrn)
            except AttributeError:
                e_calib = None
                wrn = (f"Energy calibration for Det {detname} is not given in "
                        "the datafile.")
                warnings.warn(wrn)
            try:
                e_res = float(ecal.find("EnergyResolution").text)
            except:
                e_res = 1.4
                wrn = (f"Energy resolution for Det {detname} is not given in "
                        "the datafile. Therefore it is set to 1.4 keV.")
                warnings.warn(wrn)
            # get live time and channel data
            live_time_tmp = meta.find("LiveTimeDuration").text
            try:
                live_time = float(live_time_tmp.replace("PT","").replace("S", ""))  # in seconds
            except ValueError:
                live_time = float("nan")
            channel_data = np.fromstring(meta.find("ChannelData").text,
                                         dtype=int, sep=" ")

            single = SingleSpectrum(
                        channel_data,
                        detname=detname,
                        ecal=e_calib,
                        eres=e_res,
                        live_time=live_time,
                        show=show,
                        verbose=verbose)

            singles.append(single)

        elif meta.tag == "Coincidence":
            namepair = meta.find("CoincidencePair").text
            e_window = np.fromstring(meta.find("EnergyWindow").text,
                                     dtype=int, sep=' ')
            coin_data = np.fromstring(meta.find("CoincidenceData").text,
                                      dtype=int, sep=' ')
            if len(coin_data) == 1024 ** 2:
                coin_data.reshape([1024, 1024])
            elif len(coin_data) == 2048 ** 2:
                coin_data.reshape([2048, 2048])
            else:
                wrn = ("Coincidence data has unknown shape "
                       f"{coin_data.shape} and will be ignored")
                warnings.warn(wrn)
                coin_data = None

            pair = namepair.split('-')
            coinc.append(CoincidenceSpectrum(
                            pair,
                            coin_data,
                            window=e_window,
                            show=show,
                            verbose=verbose))

        else:
            metadata[meta.tag] = meta.text
    try:
        implantation_energy = metadata["PositronImplantationEnergy"]
    except KeyError:
        print('No Implantation Energy set in n42 file.')
        implantation_energy = None

    return singles, coinc, implantation_energy


def import_mpa(filename, show=False, verbose=False):
    "Legacy import function for .mpa files (FastCom/MPANT)."

    # getting the part of the filedir which is the filename
    match_path = re.search(r'[a-z A-Z0-9._]+$', filename)
    # creating separate str with dir in which file is
    file_dir = filename[0:match_path.span()[0]]
    fname_plain = match_path.group()
    calfile = fname_plain.split('.')[0] + '.cal'

    # ensure that file_dir is the absolute path
    if not os.path.isdir(file_dir):
        file_dir = os.path.join(os.getcwd(), file_dir)

    # read ecal from a .cal file in file_dir
    ecal = []
    try:
        with os.scandir(file_dir) as files:
            for f in files:
                if 'Calibration.cal' == f.name or calfile == f.name:
                    with open(os.path.join(file_dir, f.name)) as cal_file:
                        lines = [l for l in cal_file.read().splitlines()
                                 if l != ""]
                    for line in lines:
                        ecal.append(float(line)/1e3)
        if verbose: print('(File with energy calibration found and imported.)')
    except FileNotFoundError:
        if verbose: print('No .cal file found.')

    n_ecal = len(ecal)
    if n_ecal == 0:
        warnings.warn("No energy calibration loaded. Setting it to 1.")
        ecal = [1] * 4
    elif n_ecal == 2:
        wrn = (f"CalibrationWarning: Only {n_ecal}/4 detectors are energy "
                "calibrated. We'll assume they are detectors A and D.")
        ecal = [ecal[0], float('nan'), float('nan'), ecal[1]]
    elif n_ecal == 4:
        pass
    else:
        wrn = (f"CalibrationWarning: Only {n_ecal}/4 detectors are energy "
                "calibrated. Please provide either 2 (AD) or 4 (ABCD) "
                "calibrations. Setting it to 1.")
        warnings.warn(wrn)
        ecal = [1] * 4

    line = 0
    single_data, coinc_data, tmp = [], [], []
    tmp_d = {k: None
             for k in ['DATA0', 'DATA1', 'DATA2', 'DATA3', 'CDAT0', 'CDAT1']}
    tmp_t = {'LiveTime': [], 'RealTime': []}
    curr = None
    found = False

    measurement = open(filename, "r")
    for i in measurement:
        line += 1

        if 'realtime' in i:
            tmp_t['RealTime'].append(float(i[9:]))
        elif 'livetime' in i:
            tmp_t['LiveTime'].append(float(i[9:]))
        elif 'DAT' in i:
            found = True
            curr = i[1:6]
            break

    for j in measurement:
        if 'DAT' in j:
            tmp_d[curr] = tmp.copy()
            curr = j[1:6]
            tmp = []
        else:
            tmp.append(int(j))

    tmp_d[curr] = tmp
    for key in tmp_d:
        if "CDAT" in key:
            coinc_data.append(tmp_d[key])
        else:
            s = tmp_d[key]
            if s:
                single_data.append(np.array(s))

    try:
        coinc_data[1] = np.reshape(coinc_data[1], [1024, 1024])
    except ValueError:
        coinc_data[1] = None
    try:
        coinc_data[0] = np.reshape(coinc_data[0], [1024, 1024])
    except ValueError:
        try:
            coinc_data[0] = np.reshape(coinc_data[0], [8192, 8192])
        except ValueError:
            coinc_data[0] = None

    measurement.close()

    singles = [SingleSpectrum(s,
                              "ABCD"[i],
                              ecal=[0, ecal[i], 0],
                              live_time=tmp_t['LiveTime'][i],
                              real_time=tmp_t['LiveTime'][i],
                              show=show, verbose=verbose)
               for i, s in enumerate(single_data)]

    coinc = []
    if coinc_data[0] is not None:
        coinc.append(CoincidenceSpectrum(('A','D'), coinc_data[0],
                     ecal=((0, ecal[0], 0), (0, ecal[3], 0))))

    if coinc_data[1] is not None:
        coinc.append(CoincidenceSpectrum(('B','C'), coinc_data[1],
                     ecal=((0, ecal[1], 0), (0, ecal[2], 0))))


    if not found:
        wrn = ("No spectrum data was found in the measurement file. "
               "Please make sure that the Spectra are seperated correctly.\n"
               "Singles: [DATA#, XXXXX ]\nCoincidence: [CDAT#, XXXXXXX ]")
        warnings.warn(wrn)

    return singles, coinc, None


def import_txt(filename, detnames=None, enrg_windows=None, verbose=False,
               show=False):
    """Import coincidence txt file, like the one you get from CDBConvert."""

    with open(filename, "r") as f:
        raw = f.read()

    data = np.fromstring(raw, dtype=int, sep="\t")
    dim = int(np.sqrt(data.shape[0]))
    coinc_data = np.reshape(data, (dim, dim))

    if detnames != None:
        det1, det2 = detnames
    else:
        det1, det2 = "A", "B"

    singles = [SingleSpectrum(None)] * 2
    coinc = [CoincidenceSpectrum((det1, det2), coinc_data,
             ecal=((0, 1, 0), (0, 1, 0)), name=det1+det2)]

    return singles, coinc, None
