from stacs.core.classes import Spectrum, SaveState
from stacs.core.visualizer import heatmap
import matplotlib.pyplot as plt
from stacs.coinc.ratiocurve import plotter
from stacs.core.utils import addfiles
from pathlib import Path
import pickle
import os.path

files = []
batch = {}
fig_count = 1

def menu():

    """Simple command line, text based interface for basic functionality."""


    global files
    global batch

    ref = None

    if os.path.isfile('saves/temp.pkl'):
        print('Loading previous state......')
        with open('saves/temp.pkl', 'rb') as loaded:
            load1 = pickle.load(loaded)
            ref, files, batch =load1.ref, load1.queue, load1.batch
        print('Complete.')

    else:
        if not os.path.isdir('saves'):
            Path('saves').mkdir(parents=True, exist_ok=True)
    if not os.path.isdir('fig_out'):
        Path('fig_out').mkdir(parents=True, exist_ok=True)

    filepath = Path('measurements')
    print(r'''_____/\\\\\\\\\\\____/\\\\\\\\\\\\\\\_____/\\\\\\\\\___________/\\\\\\\\\_____/\\\\\\\\\\\___        
 ___/\\\/////////\\\_\///////\\\/////____/\\\\\\\\\\\\\______/\\\////////____/\\\/////////\\\_       
  __\//\\\______\///________\/\\\________/\\\/////////\\\___/\\\/____________\//\\\______\///__      
   ___\////\\\_______________\/\\\_______\/\\\_______\/\\\__/\\\_______________\////\\\_________     
    ______\////\\\____________\/\\\_______\/\\\\\\\\\\\\\\\_\/\\\__________________\////\\\______    
     _________\////\\\_________\/\\\_______\/\\\/////////\\\_\//\\\____________________\////\\\___   
      __/\\\______\//\\\________\/\\\_______\/\\\_______\/\\\__\///\\\___________/\\\______\//\\\__  
       _\///\\\\\\\\\\\/_________\/\\\_______\/\\\_______\/\\\____\////\\\\\\\\\_\///\\\\\\\\\\\/___ 
        ___\///////////___________\///________\///________\///________\/////////____\///////////_____
        ''')
    import time
    time.sleep(3)
    print('-----------------------------------------------------------------------------------------------------------')
    print('''CDB Evaluation Program. Please select the menu options by entering the corresponding number. 
Please copy all measurement files, as well as reference files into the folder measurements.
Once new files are imported they will be added to a queue. 
If the program is quit unexpectedly the queue will be lost and files have to be re-imported.''')
    print('-----------------------------------------------------------------------------------------------------------')
    print()
    time.sleep(5)

    running = True

    while running:
        if len(files) > 0:
            print('To look at the list of currently imported files press (0), to delete the queue press (-).\n')
        print('''(1) Import multiple Measurements in order. (optional: and add them up)
    The filenames have to be evenly spaced, eg.: "Messung000.mpa", "Messung002.mpa", Messung004.mpa, ...\n''')
        print('''(2) Import random assortment of files by filename.\n''')
        print('''(3) Import a reference measurement for ratio curves.\n''')
        print('''(4) Calculate ratio curves of previously imported files.\n''')
        print('''(5) Calculate projection of previously imported files.\n''')
        print('''(6) Calculate projection of single file.\n''')
        print('''(7) Change the directory name measurements are taken from.\n''')
        print('''(8) Manipulate measurement queue.\n''')
        print('''(9) Some random operations on measurements, like heatmap output or simple projections.\n''')
        print('''Press (S / L) to save/load saves. 
To terminate the program press "*". The current state will be saved and reloaded next time.''')
        select = input('Please select a menu option, by typing corresponding number and pressing enter: ')
        if select == '1':
            f = multimport(filepath)
            if f is not None:
                files += f
        elif select == '2':
            files.append(singleimport(filepath))
        elif select == '3':
            file = input('Enter filename of reference measurement: ')
            appearance = input('Enter a name to appear in graphs: ')
            fname = filepath / file
            if os.path.isfile(fname):
                ref = Spectrum(filename=fname, name=appearance)
                print(f'Reference-File {appearance} has been imported. Press ENTER to return to main menu.')
                input()
            else:
                print('File does not exist in specified folder. Press ENTER to return to main menu.')
                input()
        elif select == '4':
            calcratio(ref, files)
        elif select == '5':
            project(files)
        elif select == '6':
            singleproject()
        elif select == '7':
            print('''Please enter a folder name for the measurements. 
The folder has to be located in the program directory and can be changed in between imports. ''')
            newpath = input('Enter folder name: ')
            if os.path.isdir(newpath):
                filepath = Path(newpath)
                print('Directory changed successfully! Press ENTER to return to main menu')
                input()
            else:
                print('This folder does not exist.')
                input('Press ENTER to return to main menu.')
        elif select == '8':
            files, batch = queue(files, batch)
        elif select == '9':
            submenu(files, ref)
        elif select == '0':
            for i in range(len(files)):
                print(i, ': \t', files[i].name)
            if ref is not None:
                print('ref : \t', ref)
            input('Press ENTER to return to main menu. ')
        elif select == 'S' or select == 's' or select == 'L' or select == 'l':
            chk = io_func(select, files, batch, ref)
            if chk is not None:
                files, batch, ref = chk
                print('Loading successful. Press ENTER to continue.')
                input()
        elif select == '*':
            with open('saves/temp.pkl', 'wb') as output:
                save = SaveState(files, batch, ref)
                pickle.dump(save, output, pickle.HIGHEST_PROTOCOL)
            return 0
        elif select == '-':
            files = []
        else:
            print('Invalid entry, only enter one number or "*" to terminate the program.\n')


def multimport(path):
    filelist = []
    while True:
        print('''Files can be added in three ways: 
    (1) You can add a range of files one by one to the queue.
    (2) You can add a range of files, sum them all up and then add the summed up file to the queue.
    (3) You can add a range of files in packages. For example in long measurements taken in 30 min intervals 4
        measurements can be added up to gain 2 hour summed up measurement intervals.
    (*) Return to main menu.\n''')
        select2 = input('Select an option: ')

        if select2 == '1':
            try:
                start = int(input('Enter the number of the first file (e.g.: 2 for "Messung002.mpa"): '))
                stop = int(input('Enter the number of the last file: '))
                step = int(input('Enter the step size (e.g: If you want to import every second measurement enter 2): '))
            except ValueError:
                print('Please only enter the Measurement Number.')
                break

            if os.path.isfile(path / 'Messung{0:03d}.mpa'.format(start)) and os.path.isfile(path / 'Messung{0:03d}.mpa'.format(stop)):
                pass
            else:
                print('File does not exist in the specified path! '
                      'All directory paths must be given in relation to the program files.')
                print('Press ENTER to return to main menu.\n')
                input()
                return None

            for i in range(start, stop+step, step):
                file = 'Messung{0:03d}.mpa'.format(i)
                file = path / file
                filelist.append(Spectrum(filename=file, name='Messung{0:03d}'.format(i)))

        elif select2 == '2':
            try:
                start = int(input('Enter the number of the first file (e.g.: 2 for "Messung002.mpa"): '))
                stop = int(input('Enter the number of the last file: '))
                step = int(input('Enter the step size (e.g: If you want to import every second measurement enter 2): '))
            except ValueError:
                print('Please only enter the Measurement Number.')
                break
            name = input('Enter a name you want the summed up measurement to appear with in graphs: ')
            if os.path.isfile(path / 'Messung{0:03d}.mpa'.format(start)) and os.path.isfile(path / 'Messung{0:03d}.mpa'.format(stop)):
                pass
            else:
                print('File does not exist in the specified path! '
                      'All directory paths must be given in relation to the program files.')
                print('Press ENTER to return to main menu.\n')
                input()
                return None
            filelist.append(addfiles(start, stop, step, fname=name, dpath=path))

        elif select2 == '3':
            print('The number of added interval steps hast to equal or smaller than the number of measurements. ')
            try:
                scale = float(input('Enter measurement time interval in hours: '))
                size = float(input('Enter desired time of added measurement (only multiples of time intervals): '))
                start = int(input('Enter the number fo the first file (e.g.: 2 for "Messung002.mpa"): '))
                stop = int(input('Enter the number of the last measurement: '))
                offset = float(input('Enter time offset (If measurement number 0 is not time 0 enter offset): '))
            except ValueError:
                print('Please only enter numbers.')
                break
            package = int(size / scale)

            if os.path.isfile(path / 'Messung{0:03d}.mpa'.format(start)) and os.path.isfile(path / 'Messung{0:03d}.mpa'.format(stop)):
                pass
            else:
                print('File does not exist in the specified path! '
                      'All directory paths must be given in relation to the program files.')
                print('Press ENTER to return to main menu.\n')
                input()
                return None

            if size % scale == 0:
                length = int((stop - start + 1) / package)
                counter = 0
                for i in range(length):
                    step = i * package
                    pos = int(start + step)
                    if pos+package-1 < stop:
                        filelist.append(addfiles(pos, pos+package-1, 1, fname='{} h'.format(pos+package),
                                                 dpath=path))
                    else:
                        filelist.append(addfiles(pos, stop, 1, fname='{} h'.format(stop), dpath=path))
                        print('Last interval has reduced statistics!')

            else:
                print('The desired summed up time hast to be divisible by the measurement time, try again.')


        else:
            break
    return filelist


def singleimport(path):
    file = input('Enter filename of measurement: ')
    appearance = input('Enter a name to appear in graphs: ')
    file = path / file
    if os.path.isfile(file):
        pass
    else:
        print('File does not exist in the specified path! '
              'All directory paths must be given in relation to the program files.')
        print('Press ENTER to return to main menu.\n')
        input()
        return None
    return Spectrum(filename=file, name=appearance)


def calcratio(refence, filelist):
    import matplotlib
    import matplotlib.pyplot as plt
    global fig_count
    fig_count += 1
    matplotlib.use('Qt5Agg')
    plt.ion()
    plt.figure(fig_count)
    title = input('Enter a title for the graph (this is also used as the filename): ')
    plotter(refence, filelist, title)
    plt.savefig(f'fig_out/{title}.pdf', format='pdf')
    plt.close()
    print('The figures are saved in the folder fig_out. Press ENTER to return to main menu')
    input()


def project(filelist):
    pass


def queue(filelist, batchdict):
    fileback, batchback = filelist, batchdict
    print('The following files are in the current queue:')
    for i in range(len(filelist)):
        print(i, ': \t', filelist[i].name)
    while True:
        print()
        print('''(1) Archive the current queue to save it for a later evaluation. \n''')
        print('''(2) Look at archived queues. \n''')
        print('''(3) Choose one of the archived queues and make it current. \n''')
        print('''(4) Show current queue again. \n''')
        print('''(5) Restore backup. \n''')
        print('''(S) Return to main menu WITH saving. \n''')
        print('''(D) Return to main menu WITHOUT saving. \n''')
        choose = input()

        if choose == '1':
            if len(filelist) != 0:
                name = input('Enter name for current batch: ')
                batch[name]  = filelist
            else:
                print('Current queue is empty. Nothing to archive.')

        elif choose == '2':
            for i in batchdict.keys():
                print('- ', i)

        elif choose == '3':
            for i in batchdict.keys():
                print('- ', i)
            sel = input('Enter the name of the archive you want to select: ')
            if sel in batchdict:
                if len(filelist) != 0:
                    batchdict['BACKUP'] = filelist
                filelist = batchdict[sel]
                print(f'The archive {sel} has been swapped in for the current queue.')
                print('The former current queue has been added to archive as BACKUP')
            else:
                print('There seems to be a typo in the archive name. Please check spelling.')

        elif choose == '4':
            if len(filelist) == 0:
                print('EMPTY')
            else:
                for i in range(len(filelist)):
                    print(i, ': \t', filelist[i].name)

        elif choose == '5':
            if 'BACKUP' in batchdict.keys():
                filelist = batchdict['BACKUP']
            else:
                print('No Backup in archives. ')

        elif choose == 'S' or choose == 's':
            print('Changes saved.')
            return filelist, batchdict

        elif choose == 'D' or choose == 'd' or choose == '*':
            print('Changes discarded.')
            return fileback, batchback

        else:
            print('Invalid Input! To try again press ENTER.')
            input()


def singleproject():
    pass


def io_func(sel, f, b, r):
    import re

    if sel == 'S' or sel == 's':
        while True:
            name = input('Enter file name (only letters and _ underscores): ')
            chk = re.match(r'[a-zA-Z_]+$', name)
            if not chk and len(name) < 20:
                with open(f'saves/{name}.pkl', 'wb') as output:
                    save = SaveState(f, b, r)
                    pickle.dump(save, output, pickle.HIGHEST_PROTOCOL)
                print('File saved in "saves" folder. Press ENTER to return to main menu.')
                return None
            else:
                print('Invalid file name, only use letters and _ underscores.')
                print('To try again press enter, to return to main menu press (*).')
                cont = input()
                if cont == '*':
                    return None

    elif sel == 'L' or sel == 'l':
        while True:
            name = input('Enter file name (only letters and _ underscores): ')
            if os.path.isfile(f'saves/{name}.pkl'):
                print('Loading previous state......')
                with open(f'saves/{name}.pkl', 'rb') as loaded:
                    load1 = pickle.load(loaded)
                    re, fi, ba = load1.ref, load1.queue, load1.batch
                return re, fi, ba
            else:
                print('No such file found in "saves" folder.')
                print('To try again press ENTER, to return to main menu press (*).')
                cont = input()
                if cont == '*':
                    return None


def submenu(f_list, r):
    for i in range(len(f_list)):
        print(i, ': \t', f_list[i])
    print()
    print('Ref\t', r)

    while True:
        f = input('First select the file you want to use (Number for files, "R" for reference, "*" to quit: ')
        if f == 'r' or f == 'R':
            if r is not None:
                f = r
                break
        elif f == '*':
            return None
        else:
            try:
                f = int(f)
                if f >= len(f_list):
                    raise ValueError
            except ValueError:
                print('Invalid entry try again.')
            finally:
                f = f_list[f]
                break

    while True:
        print(f'Selected file: {f}')
        print('Select an action: \n')
        if f.typec == 3:
            print('(0) to switch coincident spectra.')
        print('(1) to display heatmap of current coinc spectrum. ')
        print('(*) to quit.')
        sel = input()
        if sel == '0':
            f.switch_coinc()
        elif sel == '1':
            f.heatmap()
            plt.savefig(f'fig_out/{f.name}_heatm.pdf', format='pdf')
            plt.close()
        elif sel == '*':
            return None
        else:
            print('Invalid entry, press ENTER to try again. \n')
            input()


if __name__ == '__main__':
    menu()
