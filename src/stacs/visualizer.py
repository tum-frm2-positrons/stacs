import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import json


def plot_projections(proj_cluster, labellist, binlist=None, layout=(0, 0), symmetric=True, plot_error=True,
                     col='jet'):
    """Plots CDB Projections into Subplots.

    Parameters
    ----------
    proj_cluster : list of list of object
        List of list with Spectrum Objects, lenght will determine number of Subplots.
    labellist : list of list of str
        List of list with Labels according to proj_cluster order.
    binlist : list, str
        List of binfiles to be applied, if only one is given it will be applied to all subplots.
    layout : tuple, optional
        Layout of the subplots in rows, columns (default is (length of proj_cluster, 0)).
    symmetric : bool, optional
        If true projections wont be mirrored (default is True).
    plot_error : bool, optional
        If true errorbars will be added to the plots (default is True).
    col : str, optional
        Defines colormap to be used from matplotlib.colors (default is 'jet').

    Raises
    ------
    ValueError
        If the number of rows and columns does not correspond to the number of plots.
    """
    from stacs.coinc.roi_fit import binning
    import matplotlib.cm as cmx

    if layout == (0, 0):
        layout = (1, len(proj_cluster))
    elif layout[0]*layout[1] != len(proj_cluster):
        try:
            raise ValueError
        except ValueError:
            print('Number of rows and columns does not match the number of specified plots')
            return

    if binlist is None:
        binlist = ['Bins200.txt' for bin in range(len(proj_cluster))]
    elif type(binlist) == str:
        binlist = [binlist for bin in range(len(proj_cluster))]

    f, ax = plt.subplots(layout[0], layout[1])

    for plot in range(len(proj_cluster)):
        colnorm = colors.Normalize(vmin=0, vmax=len(proj_cluster[plot])-1)
        colmap = cmx.ScalarMappable(norm=colnorm, cmap=col)

        for graph in range(len(proj_cluster[plot])):
            gr_col = colmap.to_rgba(graph)
            if len(proj_cluster[plot]) == 2:
                if graph == 1:
                    gr_col = 'crimson'
            en, pr, er = binning(proj_cluster[plot][graph], binfile=binlist[plot], mirrored=not symmetric)
            ax[plot].semilogy(en, pr, label=labellist[plot][graph], color=gr_col)
            if plot_error:
                ax[plot].errorbar(en, pr, er, linestyle='None', capsize=2, elinewidth=.5, color=gr_col)

        ax[plot].legend()

    plt.show()


def heatmap(w, col='jet', name=None, a=1, label=True, i_fig=True):
    """Creates a 2D heatmap of the CDB spectrum, plt.show() must be called afterwards.
    If a list of arrays is given, along with a list of names Subplots will be created.

    Parameters
    ----------
    w : np.array, list of np.array
        2D Numpy array(s) containing coincidence data.
    col : str, optional
        Colormap from matplotlib.colors to be used for heatmap (default is 'jet').
    name : str, list of str, optional
        Title for the plot(s), if None is given no title will be displayed (default is None).
    a : float, optional
        Transparency of heatmap (0 is transparent, 1 is opaque) (default is 1).
    lim : bool, optional
        If False the x and y range will be determined automatically (default is False).
    label : bool, optional
        If True x and y axis will be labeled automatically (default is True).
    i_fig : bool, optional
        If True a figure will be created but not shown,
        if False the plot will be drawn in any existing figure (default is True).

    Returns
    -------
    object
        Figure object from matplotlib.
    """
    fig = None

    if type(w) == list:
        l = len(w)
        fig, ax = plt.subplots(1, l)
        for plot in range(l):
            heat = ax[plot].imshow(w[plot], origin='lower', interpolation=None,
                                   norm=colors.LogNorm(vmin=w[plot].min() + 1, vmax=w[plot].max() + 1), cmap=col, alpha=a)

            ax[plot].set_title(name[plot])
            fig.colorbar(heat, ax=ax[plot])
        plt.show()

    else:
        if i_fig:
            fig = plt.figure(name)
        heat = plt.imshow(w, origin='lower', interpolation=None,
                          norm=colors.LogNorm(vmin=w.min() + 1, vmax=w.max() + 1),
                          cmap=col, alpha=a)
        cbar = plt.colorbar(heat)

        if label:
            plt.xlabel('Detector 1 channel')
            plt.ylabel('Detector 2 channel')

    # plt.show()
        if i_fig:
            return fig


def display_measurement(spec, use_ecal=False, ecalfile=None, save=False):
    '''Displays all detectors for a given measurement'''

    # maybe we cann toss this part, because correct detnames will be in measurement files (Lucian)
    lookup1 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAC", "H": "OAE", "I": "OAF"}
    lookup2 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAE", "H": "OAF"}
    lookup3 = {"A": "CAC", "B": "CAF", "C": "CAA", "D": "CAB"}
    lookup4 = {"A": "OAG", "B": "CAZ"}

    title = spec.name[spec.name.rfind('/') + 1:]

    # get number of detectors in measurement file, to choose lookup
    l = len(spec.singles)
    if l == 8:
        detectors = lookup2
        #print(f"{l} detectors were active")
    elif l == 9:
        detectors = lookup1
        #print(f"{l} detectors were active")
    elif l == 4:
        detectors = lookup3
    elif l == 2:
        detectors = lookup4
    else:
        print(f"No suitable lookup table for {l} detectors")
        return

    keys = spec.singles.keys()
    rows = len(keys)
    cols = 1
    fig1, axs = plt.subplots(rows, cols, sharey='row')
    fig1.set_size_inches(10.0, 2 * rows)
    fig1.set_tight_layout(True)
    axs[0].set_title(title)
    i = 1
    if use_ecal == True:
        if ecalfile == None:
            # extract ecal dict from spec.singles
            ecal = dict.fromkeys(keys)
            for d in ecal:
                ecal[d] = {}
                ecal[d]["linefit"] = spec.singles[d]["EnergyCalibration"]
                ecal[d]["linefiterr"] = (0, 0, 0)
            print(f'Using energy calibration from measurement file.')
        else:
            with open(ecalfile) as f:
                ecal = json.load(f)
            print(f'Energy calibration imported.')
        xlabel = 'Energy [keV]'
        for key, ax in zip(keys, axs):
            try:
                c0 = ecal[detectors[key]]['linefit'][0]
                c1 = ecal[detectors[key]]['linefit'][1]
                ax.set_ylabel(f'{key}: {detectors[key]}\n counts/ch')
            except KeyError:
                c0 = ecal[key]['linefit'][0]
                c1 = ecal[key]['linefit'][1]
                ax.set_ylabel(f'{key}\n counts/ch')
            data = spec.singles[key]['ChannelData']
            data_x = np.linspace(0, len(data) - 1, len(data))
            data_x = c1 * data_x + c0
            ax.set_xlim((0.0, 1.0e3))
            ax.plot(data_x, data)
            ax.grid(axis='both')
    else:
        xlabel = 'Channel [ch#]'

        for key, ax in zip(keys, axs):
            data = spec.singles[key]['ChannelData']
            #data_x = np.linspace(0, len(data) - 1, len(data))
            ax.set_xlim((0.0, len(data)))
            ax.plot(data)
            try:
                ax.set_ylabel(f'{key}: {detectors[key]}\n counts/ch')
            except KeyError:
                ax.set_ylabel(f'{key}\n counts/ch')
            ax.grid(axis='both')
    axs[rows - 1].set_xlabel(xlabel)

    if save == True:
        plt.savefig(f'Spectra_{title}.png', dpi=600, papertype='a4',
                    constrained_layout=True, format="png", orientation='landscape')
        print(f'All spectra of measurement {title} have been saved')
    else:
        print(f'Displaying all spectra of measurement {title}')
        plt.show()


def fancy_3d(arr):
    w = arr.copy()
    '''
    for i in range(1024):
        for j in range(1024):
            if w[i, j] == 0:
                pass
            else:
                w[i, j] = np.log(w[i, j])
    #'''
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    x = np.arange(300, 700, 1)
    y = np.arange(300, 700, 1)
    x, y = np.meshgrid(x, y)
    r = np.sqrt(x**2 + y**2)
    z = w[x, y]

    surf = ax.plot_surface(x, y, z, norm=colors.LogNorm(vmin=w.min() + 1, vmax=w.max() + 1),
                           cmap='inferno', linewidth = 0, antialiased=True, shade=True)

    ax.set_zlim(w.min(), w.max())

    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def func_3d(f):

    w = np.zeros([1024, 1024])

    x = np.arange(400, 600, 1)
    y = np.arange(400, 600, 1)

    for i in x:
        for j in y:
            w[i, j] = f(i, j)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    x, y = np.meshgrid(x, y)
    r = np.sqrt(x**2 + y**2)
    z = w[x, y]

    surf = ax.plot_surface(x, y, z, norm=colors.Normalize(vmin=w.min(), vmax=w.max()),
                           cmap='inferno', linewidth = 0)

    ax.set_zlim(w.min(), w.max())

    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def gauss_2d(x, y, a=10000, mu_x=521, mu_y=521, sig_x=60, sig_y=40, roh=0.5):
    return a * np.exp(( - 1 /(2 * (1 - roh ** 2))) * ((((x - mu_x) ** 2 ) / (sig_x ** 2) ) +
                                                      (((y - mu_y) ** 2 ) / (sig_y ** 2) ) +
                                                      (2 * roh * (x - mu_x) * (y - mu_y) / (sig_x * sig_y))))


def gauss_2d2(x, y, A=5.72476100e+03, x_0=513, y_0=508, sig_x=26.259, sig_y=11.97, theta=0.7907518):
    #[5.72476100e+03 5.13003888e+02 5.08422730e+02 2.62590682e+01 1.19685588e+01 7.90751790e-01]
    a = (np.cos(theta) ** 2) / (2 * sig_x ** 2) + (np.sin(theta) ** 2) / (2 * sig_y ** 2)
    b = -(np.sin(2 * theta)) / (4 * sig_x ** 2) + np.sin(2 * theta) / (4 * sig_y ** 2)
    c = (np.sin(theta) ** 2) / (2 * sig_x ** 2) + (np.cos(theta) ** 2) / (2 * sig_y ** 2)

    z = A * np.exp(-(a * (x - x_0) ** 2 + 2 * b * (x - x_0) * (y - y_0) + c * (y - y_0) ** 2))

    return z


def multiple_ratios(ratiolist, shareaxis=True, dim=None, refname=None):
    share = 'none'
    if refname is None:
        refname = 'Ref'

    if shareaxis:
        share = 'all'
    if dim is None:
        f, ax = plt.subplots(1, len(ratiolist), sharey=share, sharex=share)
        if len(ratiolist) == 4:
            f, ax = plt.subplots(2, 2, sharey=share, sharex=share)
    else:
        f, ax = plt.subplots(dim[0], dim[1], sharey=share, sharex=share)
    for i in range(len(ratiolist)):
        ax[i].grid()
        for err in range(len(ratiolist[i][1])):
            ax[i].errorbar(ratiolist[i][1][err][0], ratiolist[i][1][err][1], ratiolist[i][1][err][2],
                         color=ratiolist[i][1][err][3], linestyle='None', capsize=2, elinewidth=1,
                         markeredgewidth=1)
        for plot in range(len(ratiolist[i][0])):
            ax[i].plot(ratiolist[i][0][plot][0], ratiolist[i][0][plot][1], color=ratiolist[i][0][plot][2])
        ax[i].legend(handles=ratiolist[i][2])
        ax[i].set_xlabel(ratiolist[i][6])
        ax[i].set_ylabel('Ratio to' + refname)
        ax[i].set_title(ratiolist[i][7])
    ax[0].set_xlim(left=0)
    plt.show()


def compare_single(spectrum, detpair, singledet, simple_proj=False):
    from src.stacs.measurement import DopplerMeasurement
    from scipy.optimize import curve_fit
    from src.stacs.algorithms import gauss
    import matplotlib.patches as patch
    coinc = spectrum[detpair].binning(binfile='Bins200.txt',
                                         background_corr=False, mirrored=True)
    #'''
    coinc_x_r = list(coinc[0][0:len(coinc[0]) - 1])
    coinc_c_r = list(coinc[1][0:len(coinc[1]) - 1])
    coinc_e_r = list(coinc[2][0:len(coinc[2]) - 1])


    for i in range(len(coinc_x_r)):
        coinc_x_r[i] -= 511
        coinc_x_r[i] *= -1
        coinc_x_r[i] += 511

    coinc_x_r.reverse()
    coinc_c_r.reverse()
    coinc_e_r.reverse()
    
    coinc_x = coinc_x_r + list(coinc[0])
    coinc_c = np.array(coinc_c_r + list(coinc[1]))
    coinc_e = np.array(coinc_e_r + list(coinc[2]))

    '''
    
    coinc_x = coinc[0]
    coinc_c = coinc[1]
    coinc_e = coinc[2]
    
    #'''

    coinc_e /= coinc_c.max()
    coinc_c /= coinc_c.max()

    single = list(spectrum[singledet].spectrum.copy())

    channel = np.linspace(0, len(single), len(single))

    gauss_init = (max(single), single.index(max(single)), 2)
    p0 = curve_fit(gauss, channel, single, gauss_init)

    offset = 511 * (1/spectrum['OAE', 'OAF'].ecal[0][1]) - p0[0][1]

    channel += offset
    single = np.array(single, dtype='float64')

    print(p0[0])
    energ = channel * spectrum['OAE', 'OAF'].ecal[0][1]
    err = np.sqrt(single)
    print(err.max())

    err /= single.max()
    single /= single.max()

    print(err.max())

    if simple_proj:
        arr = spectrum[detpair].hist.copy()
        slope, intercept, center = spectrum[detpair].roi_position()
        center = center[0]
        e_x = spectrum[detpair].ecal[1][1]
        proj = np.sum(arr, axis=0)
        proj_x = np.array([center - (center*e_x) + i * e_x for i in range(len(proj))])
        proj_x -= 511 * 2
        proj_x += 12.05
        proj /= np.max(proj)

    #coi_e -= 0.61
    labellist = []



    energ -= 511
    coinc_x = np.array(coinc_x)
    coinc_x -= 511

    energ_fl = np.linspace(energ[0], energ[-1], len(coinc_x))
    energ_int = []

    for i in energ_fl:
        energ_int.append(int(i))

    print(energ)
    labellist.append(patch.Patch(label='DBS', color='navy'))
    plt.plot(energ, single, color='navy', linewidth = 2)

    if simple_proj:
        labellist.append(patch.Patch(label='CDBS simple', color='orange'))
        plt.plot(proj_x, proj, color='orange', linewidth=2)

    labellist.append(patch.Patch(label='CDBS', color='crimson'))
    plt.plot(coinc_x, coinc_c, color='crimson', linewidth=2)
    #plt.errorbar(energ, single, err, linestyle='None', capsize=2, elinewidth=1, color='navy', errorevery=6)
    #plt.errorbar(coinc_x, coinc_c, coinc_e, linestyle='None', capsize=2, elinewidth=1, color='crimson')
    plt.xlabel('$\Delta$E (keV)', fontsize=11)
    plt.ylabel('normalized counts', fontsize=11)
    plt.yscale('log')
    plt.legend(handles=labellist, fontsize=11)
    plt.xlim(left=-25, right=25)

    plt.show()
