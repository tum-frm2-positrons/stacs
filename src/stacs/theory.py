def calc_defect_concentration(DiffusionCoefficent, DiffusionLength, tau_bulk,
                              mu_d):
    """Calculte the defect concentration.

    Assumes only one type of defect is present, in order to calculte the defect
    concentration from S(E) measurements.

    Parameters
    ----------
    DiffusionCoefficent : float
    The diffusion coefficent of positrons in the perfect lattice in [m2/s].
    DiffusionLength : float
    The measured positron diffusion length in [m]
    tau_bulk : float
    The positron lifetime in the perfect lattice [s].
    mu_d : float
    The trapping coefficent of the defect in [m3/s].
    Returns
    -------
    C_d : float
    The absolute defect concentration [m-3].
    """
    C_d = (DiffusionCoefficent / DiffusionLength ** 2 - 1 / tau_bulk) / mu_d
    return C_d
