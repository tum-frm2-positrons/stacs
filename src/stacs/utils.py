import json
import numpy as np
from xml.dom import minidom
from lxml import etree as et
from scipy.special import erf
from datetime import datetime
from stacs.classes import Spectrum
from pathlib import Path


def list2str(l):
    return ' '.join(map(str, l))


def addfiles(begin, end, step=1, out_name='default', f_name='Messung', dpath='measurements', f_type_n42=True):

    """Sums up multiple files in order (similar to range function)."""

    ending = '.mpa'
    if f_type_n42:
        ending = '.n42'
    name = f_name + '{0:03d}' + ending

    startfile = name.format(begin)
    print('Startfile: ', startfile)
    location = Path(dpath)
    startfile = location / startfile
    startfile = str(startfile)
    res = Spectrum(filename=startfile)



    for i in range(begin+step, end+step, step):

        file = name.format(i)
        file = location / file
        file = str(file)
        temp = Spectrum(filename=file)
        res = res + temp

    res.name = out_name

    return res


def to_datetime(time_input):
    # 02.03.2020 14:19:02 vs. 2004-11-03T08:36:04.3-06:00
    date, t = time_input.split(' ')
    day, month, year = map(int, date.split('.'))
    hour, minute, sec = map(int, t.split(':'))
    return datetime(year,month,day,hour,minute,sec)


def mod_erf(x, a, offset, center, smooth):
    """Modificated error function"""

    return a * erf(smooth * (x - center)) + offset


def find_nearest(array, value, idx=False):
    """Find array element closest to value.

    Returns closest element by default, and its index if idx is True. Note that
    array must be sorted.
    """

    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or abs(value - array[idx-1]) <
                                           abs(value - array[idx])):
        if idx:
            return idx-1
        else:
            return array[idx-1]
    else:
        if idx:
            return idx
        else:
            return array[idx]


def import_parameter(filename='Parameter.txt'):
    """Import the 'Parameter.txt' file generated by LabView."""

    data = {}
    with open(filename, 'r') as file:
        header = file.readline().replace('\r\n', '').replace('\n','').split('\t')
        for var in header:
            data[var] = []
        for line in file.readlines():
            values = line.replace(',', '.').replace('\r\n', '').replace('\n','').split('\t')
            for i in range(len(header)):
                if header[i] == 'File':
                    data[header[i]].append(int(float(values[i])))
                elif header[i] == 'Time':
                    data[header[i]].append(values[i])
                else:
                    data[header[i]].append(float(values[i]))
    return data


def write_xml(data, filename="meas.n42"):
    # Create a new xml file with the results
    rough_string = et.tostring(data, encoding=str).replace('  ','').replace('\n','')
    reparsed = minidom.parseString(rough_string)
    out = reparsed.toprettyxml(indent="  ")

    myfile = open(filename, "w")
    myfile.write(out)
    myfile.close()
    return


def write_json(dictionary, output_path, pretty=True):
    f = open(output_path, "w")
    if pretty:
        out = json.dump(dictionary, f, sort_keys=True, indent=4)
    else:
        out = json.dump(dictionary, f)
    f.close()
    return out

