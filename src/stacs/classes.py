import json

from stacs.visualizer import heatmap, display_measurement
import numpy as np
import scipy, scipy.special
import pickle

class Spectrum:
    """Class object, embodying a (C)DBS measurement.

    This class was created to accommodate data created by Coincidence Doppler Broadening Spectroscopy.
    The Spectrum Object not only contains the measured data itself, but also several parameters about the measurement.

    Attributes
    ----------
    filename : str, optional
        Path to and name of the file. If not specified (i.e., to initiate
        an empty Spectrum object, empty needs to be True. Variable filename
        is required for a file import (default is 'default').
    name : str, optional
        Name of the imported Spectrum object. Set to filename, if not
        specified (default is 'default').
    singles : dict
        Dictionary containing the data from each detector
    coinc : dict
        Dictionary containing the coincidence data from opposing detector pairs.
    typec : int
        Defines the type of measurement, e.g. coincidence measurement or single with an integer.
    type : str
        Defines the type of measurement, e.g. coincidence measurement or single with a string.
    metadata : dict
        Contains various parameter from the measurement itself or calculated.
    bettercoinc : str
        String with the best detector pair for coincidence measruements.
    filetype : str
        File type of the recorded raw data.
    usecache : bool
        Specifies weather this object is to be added to the caching queue (default is True).
    detnames : list of str
        List of detector names, only necessary for heatmap txt-files (default is None).
    save_all : bool
        If set to True the cache.pkl file will be updated after each import (default is True).

    Methods
    -------
    calcarrays()
        Extracts the measurement data from the input file.
    heatmap(dets)
        Creates a visual representation of the coincidence data in the form of a 2D histogram.
    counts(dets)
        Returns the count rate of a coincidence sprectrum.
    projection2d(dets)
        Returns an array containing a simple projection of the coincidence data.
    quickfit(dets)
        Creates a plot showing how well the coincidence data can be fitted.
    getarray(dets)
        Returns the coincidence data as an array.
    backgroundcheck(dets)
        Returns the average background within one Spectrum for each of the quadrants.
    coincbackground(dets)
        Returns the background of one Spectrum averaged over all quadrants.
    fwhm_coinc(detpair)
        Calculates the energy resolution of the coincidence spectrum.
    show_singles(args)
        Displays the single detector spectra.
    single_eval(width_s=1, width_w=1, dist_w=4, peakwidth=60, bg_frac=0.25, e_cal=True, use_wcoeff=False, show=False)
        Evaluates the single Spectra for S and W parameters and more.
    deconv():
        Attempts to perform a deconvolution of the single spectra with the detector resolution.

    """

    def __init__(self, filename='default', name='default', detnames=None,
                 empty=False, save_all=True):
        """
        Init function of Spectrum class.

        Parameters
        ----------
        filename : str, optional
            Path to and name of the file. If not specified (i.e., to initiate
            an empty Spectrum object, empty needs to be True. Variable filename
            is required for a file import (default is 'default').
        name : str, optional
            Name of the imported Spectrum object. Set to filename, if not
            specified (default is 'default').
        detnames : list of str, optional
            List of detector names, only necessary for heatmap txt-files (default is None).
        empty : bool, optional
            Indicates if file contains data.
            This is required for adding measurements so that empty classes can be generated (default is False).
        save_all : bool
            If set to True the cache.pkl file will be updated after each import (default is True)
        """

        self.usecache = True
        self.filename = filename
        self.switched = False # True if the two coincident have been switched
        self.filetype = None  # .mpa, .n42 or .txt
        if name == 'default':
            self.name = filename
        else:
            self.name = name

        self.singles = {}
        self.detnames = None
        self.coinc = {}
        self.typec = 0
        self.type = 'empty'
        self.metadata = None
        self.bettercoinc = 'AD'       # For old .mpa Measurements
        self.save_all = save_all      # For caching function

        self.detnames = detnames

        if empty is False:
            (self.singles, self.coinc, self.metadata, self.type, self.typec) \
                = self.calcarrays
            self.detnames = list(self.singles)
            cache.add_file(self)

    @property
    def calcarrays(self):
        """This function retrieves data from the file specified when the object is initialized

        Returns
        -------
        summ : tuple
            Dict with single data, dict with coincidence data, dict with metadata, str with measurement type,
            int with measurement type.
        """
        import stacs.core.importer

        if self.name == 'default':
            self.name = self.filename

        if self.filename in cache.cache and self.usecache:
            print(f'Importing from cache: {self.filename}')
            d = cache.cache[self.filename]
            return d.singles, d.coinc, d.metadata, d.type, d.typec

        print('Import of file: ', self.name)

        if '.mpa' in self.filename:
            self.filetype = '.mpa'
            return stacs.core.importer.import_mpa(self.filename)
        elif '.n42' in self.filename:
            self.filetype = '.n42'
            return stacs.core.importer.import_n42(self.filename)
        elif self.filename.endswith('.txt'):
            self.filetype = '.txt'
            return stacs.core.importer.import_txt(self.filename, self.detnames)
        else:
            print(f"DataWarning: Filetype '{self.filename.split('.')[-1]}' \
not supported.")
            return

    def __add__(self, other):
        # RESTRUCT measurement
        """Add two Spectrum objects together (Only advised for related measurements).

        Parameters
        ----------
        other : object
            Other Spectrum object to be added to the current one.

        Returns
        -------
        summ : object
            Returns new Spectrum object which contains summed up arrays of the inputs.
        """
        summ = Spectrum(empty=True)
        for key in self.singles.keys():
            if key in other.singles.keys():
                e_cal = None
                live_t = None
                if self.filetype == '.n42':
                    e_cal = self.singles[key]['EnergyCalibration']
                    live_t = self.singles[key]['LiveTimeDuration']
                    #if self.singles[key]['EnergyCalibration'] != other.singles[key]['EnergyCalibration']:
                    #    print("DataWarning: Added Spectra have different \
                #energy calibrations.\nProbably rather foolish to add these.")

                elif 'EnergyCalibration' in self.singles[key]:
                    e_cal = self.singles[key]['EnergyCalibration']

                summ.singles[key] = {'ChannelData':
                                         (self.singles[key]['ChannelData'] +
                                          other.singles[key]['ChannelData']),
                                     'EnergyCalibration': e_cal,
                                     'LiveTimeDuration': live_t}



        for key in self.coinc.keys():
            if key in other.coinc.keys():
                summ.coinc[key] = {"MapData":
                                       (self.coinc[key]["MapData"] + other.coinc[key]["MapData"]),
                                   "EnergyWindows":
                                       self.coinc[key]["EnergyWindows"]}


        summ.name = '{} + {}'.format(self.name, other.name)
        summ.type = self.type

        return summ


    def __str__(self):
        return self.name

    def heatmap(self, dets):
        """Creates a 2D heatmap of one or more of the coincidence spectra.

        Parameters
        ----------
        dets : str, list of str
            String of one detector pair name (e.g. 'AD') or list of multiple pair names (e.g. ['AD', 'BC']).
        """

        if type(dets) == str:
            heatmap(self.coinc[dets]["MapData"], name=self.name)

        elif type(dets) == list:
            try:
                arr_l = [self.coinc[det]['MapData'] for det in dets]
            except KeyError:
                print('Detector pair does not exist.')
            else:
                heatmap(arr_l, name=dets)


    def counts(self, dets):
        return np.sum(self.coinc[dets]["MapData"])

    def projection2d(self, dets):
        arr2d = np.zeros([1024])
        for i in range(1024):
            arr2d += np.sum(self.coinc[dets]["MapData"][i])
        return arr2d

    def quickfit(self, dets, *args, **kwargs):
        """Applies the getmid function to the given coincidence spectrum with debug functions turned on.
        This can be helpful to determine the quality of a coincidence spectrum.

        Parameters
        ----------
        dets : str
            Detector Pair name for which the analysis should be performed. (eg.: 'AD', 'OAG CAZ')
        args : list, optional
            Arguments to be passed on to getmid.
        kwargs : dict, optional
            Keyword arguments to be passed on to getmid.
        """

        from stacs.coinc.roi_fit import getmid
        getmid(self.coinc[dets]["MapData"], debug=True, fine_debug=True, *args, **kwargs)

    def eres_from_coinc(self, dets, e_cal):
        """This function tries to estimate the energy resolution of the individual detectors from a coincidence
        spectrum. WIP!

        Parameters
        ----------
        dets : str
            Detector pair to be used.
        e_cal :

        Returns
        -------

        """
        from stacs.core.algorithms import coinc_e_approx
        res = {}
        for det_pair in dets:
            res[det_pair] = coinc_e_approx(self.coinc[det_pair]['MapData'], e_cal)

    def getarray(self, dets):
        return self.coinc[dets]["MapData"]

    def showroi(self):
        pass

    def projection(self, bin_file):
        pass

    def backgroundcheck(self, dets):  # Compares background of the four quadrants in the heatmap
        quad1 = 0  # top left
        quad2 = 0  # top right
        quad3 = 0  # bottom left
        quad4 = 0  # bottom right
        area = (100 ** 2) * 2

        quad1 += np.sum(self.coinc[dets]["MapData"][1023-320:1023-220, 20:120] +
                        self.coinc[dets]["MapData"][1023-120:1023-20, 220:320])
        quad2 += np.sum(self.coinc[dets]["MapData"][1023-320:1023-220, 1023-120:1023-20] +
                        self.coinc[dets]["MapData"][1023-120:1023-20, 1023-320:1023-220])
        quad3 += np.sum(self.coinc[dets]["MapData"][20:120, 220:320] + self.coinc[dets]["MapData"][220:320, 20:120])
        quad4 += np.sum(self.coinc[dets]["MapData"][20:120, 1023-320:1023-220] +
                        self.coinc[dets]["MapData"][220:320, 1023-120:1023-20])

        quad1 /= area
        quad2 /= area
        quad3 /= area
        quad4 /= area
        return quad1, quad2, quad3, quad4

    def coincbackground(self, detpair='AD'):
        """Calculate the background of coincidence spectra by averaging square regions off the diagonal.

        Parameters
        ----------
        detpair : str, optional
            Detector pair for which the backgrouns should be calculated.

        Returns
        -------
        coincbg : float
            Average background per pixel.
        """

        q1, q2, q3, q4 = self.backgroundcheck(detpair)
        coincbg = (q1 + q4) / 2
        return coincbg

    def fwhm_coinc(self, detpair, *args, **kwargs):
        """This function calculates the energy resolution of the coincidence measurement.
        The coincidence energy resolution is a result of the diagonal projection and superposition of the energy
        resolution of both detectors. The resulting energy resolution is usually better than the energy resolution
        of either detector.

        Parameters
        ----------
        detpair : str
            The coincidence pair for which to do the calculation.
        args : list, optional
            Arguments to be passed on to energyres function.
        kwargs : dict, optional
            Keyword arguments to be passed on to energyres function.

        Returns
        -------
        energy : float
            Calculated energy resolution in keV.

        """
        from stacs.coinc.roi_fit import energyres
        # actually, why do we need both ecals here?
        # we use only ecal for det2 (y-axis)
        if len(detpair) == 2:
            det1, det2 = detpair[0], detpair[1]
        else:
            det1, det2 = detpair.split(' ')
        e_c = [None, None]
        for i, det in enumerate((det1, det2)):
            try:
                e_c[i] = self.singles[det]["EnergyCalibration"][1]
            except (KeyError, TypeError):
                pass

        if e_c == [None, None]:
            e_c = None
            print(f"DataWarning: '{self.name}': Missing energy calibration \
for detectors {det1} and {det2}. Using default values.")
        elif None in e_c:
            if e_c[0] == None:
                e_c[0] = e_c[1]
                det_fault, det_ecal = det1, det2
            else:
                e_c[1] = e_c[0]
                det_fault, det_ecal = det2, det1
            print(f"DataWarning: '{self.name}': Missing energy calibration \
for detector {det_fault}. Using same as for detector {det_ecal}.")

        return energyres(self.coinc[detpair]['MapData'], e_c, *args, **kwargs)

    def show_singles(self, **kwargs):
        display_measurement(self, **kwargs)

    def single_eval(self, width_s=1, width_w=1, dist_w=4, peakwidth=60,
                    bg_frac=0.25, e_cal=True, use_wcoeff=False, show=False):
        import stacs.single.single_evaluation as se
        single_res = {}
        for key, data in self.singles.items():
            spec = data['ChannelData']
            if e_cal:
                try:
                    e_calib = data['EnergyCalibration']
                except KeyError:
                    # for mpa files
                    e_calib = (0, 1, 0)
            else:
                e_calib = None
            if use_wcoeff:
                try:
                    width_coeff = data['EnergyWidthCoeff']
                except KeyError:
                    print(f"No width coefficient specified for Det {key}. Continuing with width_coeff = 1.")
                    width_coeff = 1
            else:
                width_coeff = 1
            s, ds, w, dw, p2v, dp2v, n, n0, n_sp, e_calib = se.extract_data(spec, width_s=width_s,
                                                                            width_w=width_w, dist_w=dist_w, peakwidth=peakwidth,
                                                                            width_coeff=width_coeff, bg_frac=bg_frac, e_calib=e_calib,
                                                                            detname=key, show=show)
            res = {'S' : s, 'dS' : ds, 'W' : w, 'dW' : dw, 'p2v' : p2v, 'dp2v' : dp2v, 'CountsPeakBgcorr' : n,
                   'CountsPeak' : n0, 'CountsSpec' : n_sp}
            data['EnergyCalibration'] = e_calib
            del data['ChannelData']

            single_res[key] = dict(**data, **res)

        return single_res

    def deconv(self):
        ''''''
        from stacs.core.algorithms import energy, deconvolute_energy_resolution
        for key, data in self.singles.items():
            spec = data['ChannelData']
            channel = np.arange(0, len(data['ChannelData']))
            spec_ch_in_e = energy(channel, data['EnergyCalibration'])
            spec = deconvolute_energy_resolution(spec_ch_in_e, spec, data['EnergyResolution'])

            self.singles[key]['ChannelEnergy'] = spec_ch_in_e
            self.singles[key]['SpecDeconv'] = spec
        return

    def detector_resolution(self, calib_filename=None, calib_order=2, detector_names=None,
                            plot_spectrum=None, plot_fit=None,
                            fit_function="sqrt", poly_fit_order=1):
        """
        Calculates detector resolution based on peak fwhm and interpolates them for all energies with a polynomial.
        Handles errors for all data used.
        1. fit_single_peaks_with_gauss: searches peaks and fits gauss functions onto them
        2. apply calibration to energy values (GaussFit.center) and gauss fwhm (GaussFit.fwhm)
        3. check if a 511 keV peak is present, if yes remove it before fitting
        4. odr-fit results

        Parameters
        ----------
        calib_filename: str
            if given, will try to get calibration
        calib_order:
            the order of the calibration being used (only 1 and 2 are possible)
        detector_names: list of str
            list of detector names to calibrate. If None, all detectors of this object are calibrated.
        plot_spectrum: filename-str, "show" or None
            decides wether to show/save the spectrum with the fitted peaks for manual checks.
            green gausses are used, a red one is detected as the 511 keV peak (which will be excluded)
            if str: save-path without file-ending ([det-name].png will be added, one file for each detector)
            if "show" then plot will be shown instead of saved
            if None, no plotting
        plot_fit
            same as plot_spectrum, but for interpolation fit
        fit_function: str
            determines the function type to be fitted to the fwhm over energy data.
            "sqrt" or "poly" allowed.
            - "poly" means a polynomial of order poly_fit_order
            - "sqrt" means y = sqrt(a * x) + b
        poly_fit_order: int
            if fit_function=="poly", this sets the order of the polynomial used

        Returns
        -------
        a dict of callables: {detector_name_0 : f_0 , detector_name_1 : f1, ...}
        where f_n is a function f(energy in keV) which returns fwhm, fwhm_stdev at this energy
        so, to get interpolated fwhm at 511 keV, do:
        fwhm, fwhm_stdev = spectrum_object.detector_resolution(...)[detector_name](e511keV)
        """
        calib_dict_all = None
        if calib_filename:
            jf = open(calib_filename)
            calib_dict_all = json.load(jf)
            jf.close()

        funcsout = {}
        for det_name in self.singles:
            if detector_names:
                if det_name not in detector_names:
                    continue
            print(f"INFO: energy resolution for detector {det_name}")

            data = self.singles[det_name]["ChannelData"]

            # fit gauss functions onto peaks
            gausses = Spectrum.fit_single_peaks_with_gauss(data)

            # get gauss data in channels
            E_ch, E_ch_stdev = [], []
            fwhm_ch, fwhm_ch_stdev = [], []
            for g in gausses:
                E_ch.append(g.center)
                E_ch_stdev.append(g.center_stdev)
                fwhm_ch.append(g.fwhm)
                fwhm_ch_stdev.append(g.fwhm_stdev)

            # retrieve calibration
            if calib_dict_all:
                try:
                    calib_dict = calib_dict_all[det_name]
                    print(f"INFO: retrieving calibration for detector {det_name} from json file.")
                except KeyError:
                    print(f"ERROR: Detector {det_name} not found in calibration json, getting calibration from spectrum object instead")
                    calib_dict = self.singles[det_name]["EnergyCalibration"]
            else:
                print(f"INFO: retrieving calibration for detector {det_name} from spectrum object.")
                calib_dict = self.singles[det_name]["EnergyCalibration"]

            if type(calib_dict) is not dict:
                raise Exception("""ERROR: found old calibration format saved in this object, which is not supported anymore.
Please provide energy calibration via json file.""")

            try:
                if calib_order == 1:
                    calib_poly = calib_dict["linefit"].append(0)
                    calib_poly_err = calib_dict["linefiterr"].append(0)
                    # applying calibration always for 2nd order poly
                elif calib_order == 2:
                    calib_poly = calib_dict["polyfit"]
                    calib_poly_err = calib_dict["polyfiterr"]
                else:
                    raise Exception("ERROR: calibration order not supported, only 1 and 2 valid")
            except KeyError:
                raise Exception(f"ERROR: malformed calibration dict")

            # functions for applying calibration and fitting
            from numpy.polynomial.polynomial import polyval

            def polyval_err(x, xe, b, be):
                """
                calculates error of 2nd order polynomial evaluation y = b[0] + b[1] * x + b[2] * x**2
                with error propagation
                Parameters
                ----------
                x
                    x-value to evaluate at
                xe
                    x-value error
                b
                    polynomial coeffs as list
                be
                    polynomial coeff errors
                Returns
                -------
                y_error for y = b[0] + b[1] * x + b[2] * x**2
                """
                if len(b) == 2 and len(be) == 2:
                    # got linear (np array), expand to 2nd order for calculations
                    b = list(b)
                    b.append(0)
                    be = list(be)
                    be.append(0)
                return np.sqrt(be[0]**2 + (x*be[1])**2 + (x**2*be[2])**2 + ((b[1]+2*x*b[2])*xe)**2)

            def sqrtval(x, b):
                return np.sqrt(b[0] * x) + b[1]

            def sqrtval_err(x, xe, b, be):
                return np.sqrt(
                        x/4/b[0]*be[0]**2
                      + b[0]/4/x*xe**2
                      + be[1]**2
                )

            def apply_calibration(vals, vals_err, calib_poly, calib_poly_err):
                vals_new = [polyval(x, calib_poly) for x in vals]
                vals_err_new = [polyval_err(x, xerr, calib_poly, calib_poly_err) for x, xerr in zip(vals, vals_err)]
                return vals_new, vals_err_new

            # apply calibration to get energies from channels
            E_keV, E_keV_stdev = apply_calibration(E_ch, E_ch_stdev, calib_poly, calib_poly_err)
            fwhm_keV, fwhm_keV_stdev = apply_calibration(fwhm_ch, fwhm_ch_stdev, calib_poly, calib_poly_err)

            # check if 511 keV peak is present and remove it before fit
            gauss511 = None # variable to remember the 511 keV gauss (if found) for marking in the plot later
            import scipy.constants.codata
            e511keV = scipy.constants.codata.value("electron mass energy equivalent in MeV") * 1000
            for e, fw in zip(E_keV, fwhm_keV):
                if e - fw/2 < e511keV < e + fw/2:
                    print("WARNING: 511 keV peak found in measurement. Removing that peak before fit.")
                    # remember the corresponding gauss object for later
                    # values are still in the same order as gausses
                    e_index = E_keV.index(e)
                    gauss511 = gausses[e_index]
                    # exclude values from fit
                    del(E_keV[e_index])
                    del(E_keV_stdev[e_index])
                    del(fwhm_keV[e_index])
                    del(fwhm_keV_stdev[e_index])
                    break # only one 511 peak

            # fit fwhm data
            xvals_plot = np.linspace(min(E_keV), max(E_keV), 200)
            if fit_function == "poly":
                fitparams, fitparam_errs =\
                    Spectrum.odr_polyfit(E_keV, fwhm_keV, poly_fit_order, E_keV_stdev, fwhm_keV_stdev)
                funcsout[det_name] = lambda x: (polyval(x, fitparams), polyval_err(x, 0, fitparams, fitparam_errs))
                yvals_plot = polyval(xvals_plot, fitparams)
                fit_plot_label = f"order {poly_fit_order} poly"
            elif fit_function == "sqrt":
                fitparams, fitparam_errs =\
                    Spectrum.odr_fit(lambda b, x: sqrtval(x, b), # ugly, sry... odr wants params in this order
                                     E_keV, fwhm_keV, E_keV_stdev, fwhm_keV_stdev, beta0=[1.]*2)
                funcsout[det_name] = lambda x: (sqrtval(x, fitparams), sqrtval_err(x, 0, fitparams, fitparam_errs))
                yvals_plot = sqrtval(xvals_plot, fitparams)
                fit_plot_label = "sqrt(a * x) + b"
            else:
                raise Exception(f"fit function {fit_function} not supported, only 'sqrt' and 'poly'")

            # plots
            import matplotlib.pyplot as plt
            filn = self.filename.split("/")[-1]
            if plot_fit:
                f = plt.figure()
                plt.errorbar(E_keV, fwhm_keV, xerr=E_keV_stdev, yerr=fwhm_keV_stdev, ls="", marker="o")
                plt.plot(xvals_plot, yvals_plot, label=fit_plot_label)
                plt.xlabel("E / keV")
                plt.ylabel("fwhm / keV")
                plt.legend()
                plt.title(f"Energy resolution interpolation ({filn})")
                plt.tight_layout()
                if plot_fit == "show":
                    plt.show()
                else:
                    saveloc = f"{plot_fit}-res-fit-{det_name}.png"
                    plt.gcf().savefig(saveloc)
                    print("INFO: saving resolution fit at", saveloc)
                plt.close(f)
            if plot_spectrum:
                f = plt.figure()
                plt.plot(data)
                plt.xlabel("channel number")
                plt.ylabel("counts")
                plt.title(f"peaks for resolution calculation: {filn}")
                for g in gausses:
                    gausscol = "green"
                    if g == gauss511:
                        gausscol = "red"
                        plt.text(g.center, g.y_offset + g.amplitude, "511 keV WILL BE EXCLUDED", rotation="vertical")
                    g.plot(color=gausscol)
                plt.tight_layout()
                if plot_spectrum == "show":
                    plt.show()
                else:
                    saveloc = f"{plot_spectrum}-res-spectrum-{det_name}.png"
                    plt.gcf().savefig(saveloc)
                    print("INFO: saving resolution spectrum at", saveloc)
                plt.close(f)

        return funcsout # dict of callables!

    def calibrate_detectors(self, source_name, plot_spectrum=None, plot_fits=None,
                            override_warnings=False, json_save_filename=None, detector_names=None):
        """
        Calibrate all or some detectors found in self.singles

        Does:
        fit_single_peaks():
            seach peaks for each detector
            fit peaks with gauss functions
        match_calibration():
            find as many gauss peaks belonging to literature peak positions as possible
        odr_gauss_polyfit():
            fit the calibration data, linear and parabolic
        do some plots for user confirmation if needed
        saves output to this spectrum object and if needed to a json file


        Parameters
        ----------
        source_name: str
            Isotope name to pass to calib_source_manager.py to get literature data for the source used, e.g. "Eu152"
        plot_spectrum: filename-str, "show" or None
            decides wether to show/save the spectrum with the fitted peaks and matched energies for manual checks.
            if str: save-path without file-ending ([det-name].png will be added, one file for each detector)
            if "show" then plot will be shown instead of saved
            if None, no plotting
        plot_fits
            same as plot_spectrum, but for fit
        override_warnings: bool
            if False (default), calibration will not be saved. Set to True if you know better.
        json_save_filename: str
            if not None, a json with the calibration data will be saved to this location
        detector_names: list of str
            list of detector names to calibrate. If None, all detectors of this object are calibrated.

        Returns
        -------
        Nothing
        """

        warning = False

        filn = self.filename.split("/")[-1]
        jsondict = {}
        for det_name in self.singles:
            if detector_names:
                if det_name not in detector_names:
                    continue
            print(f"INFO: calibration for detector {det_name}")
            data = self.singles[det_name]["ChannelData"]

            gausses = Spectrum.fit_single_peaks_with_gauss(data)

            # read probe_info-file
            import stacs.data.calib_sources.calib_source_manager as CSM
            literature_dict = CSM.get_element(source_name)

            from scipy.odr.odrpack import OdrError
            calib_linear, calib_linear_sigma = [0,1],[0,0]
            calib_quadr, calib_quadr_sigma = [0,1,0],[0,0,0]
            try:
                gausses_match, literature_match = Spectrum.match_calibration(gausses, literature_dict)
                calib_linear, calib_linear_sigma = Spectrum.odr_gauss_polyfit(gausses_match, literature_match, 1)
                calib_quadr, calib_quadr_sigma = Spectrum.odr_gauss_polyfit(gausses_match, literature_match, 2)

                import scipy.constants.codata
                e511keV = scipy.constants.codata.value("electron mass energy equivalent in MeV") * 1000
                if e511keV in literature_match:
                    print("INFO: 511 keV peak found")

                # some checks if the results look good
                # TODO tresholds are just manually guessed magic numbers what we may expect...
                if max(calib_quadr[1], calib_linear[1]) / min(calib_quadr[1], calib_linear[1]) > 1.05:
                    warning = True
                    print("WARNING: linear and parabolic fit differ quite a bit.")
                if calib_quadr[2] > .1 * calib_quadr[1]:
                    warning = True
                    print("WARNING: fit is quite parabolic.")
                if len(gausses_match) == len(literature_dict["initial_guess_peaks"]):
                    warning = True
                    print("WARNING: No more peak matches found than those in the initial guess! This may be a bad calibration.")

                # plots
                import matplotlib.pyplot as plt
                if plot_fits:
                    f = plt.figure()
                    gauss_centers = [g.center for g in gausses_match]
                    xs = np.linspace(.95 * min(gauss_centers), 1.05 * max(gauss_centers), 100)
                    plt.plot(xs, np.polynomial.polynomial.polyval(xs, calib_linear),
                             label="linear fit", c="black", lw=4, ls=":")
                    plt.plot(xs, np.polynomial.polynomial.polyval(xs, calib_quadr),
                             label="quadratic fit", c="red")
                    plt.scatter(gauss_centers, literature_match, zorder=3)
                    # no errorbars as gauss_centers and literature values are very sharp
                    plt.xlabel("channel number of peaks")
                    plt.ylabel("matched energy / keV")
                    plt.title(f"calibration fits: {filn}")
                    plt.tight_layout()
                    plt.legend()
                    if plot_fits == "show":
                        plt.show()
                    else:
                        saveloc = f"{plot_fits}-calib-fit-{det_name}.png"
                        plt.gcf().savefig(saveloc)
                        print("INFO: saving calibration fits at", saveloc)
                    plt.close(f)

                if plot_spectrum:
                    # invert literature values to show them
                    literature_channels = [(v[0] - calib_linear[0]) / calib_linear[1]
                                           for v in literature_dict["peaks"]]

                    f = plt.figure()
                    plt.plot(data)
                    plt.xlabel("channel number")
                    plt.ylabel("counts")
                    plt.title(f"calibration measurement: {filn}")

                    # plot all known literature values for reference
                    for c in literature_channels:
                        if 0 < c < len(data):
                            plt.axvline(c, c="gray", ls="--", lw=.5)

                    import scipy.constants.codata
                    e511keV = scipy.constants.codata.value("electron mass energy equivalent in MeV") * 1000
                    e511ch = (e511keV - calib_linear[0]) / calib_linear[1]

                    plt.axvline(e511ch, c="gray", ls="-", lw=.5)

                    for g in gausses:
                        gausscol = "gray"
                        if g in gausses_match:
                            gausscol = "red"
                            lit = literature_match[gausses_match.index(g)]
                            if lit == e511keV:
                                gausscol = "green"
                            plt.text(g.center, g.y_offset + g.amplitude, f"{lit:.1f} keV", rotation="vertical")
                        g.plot(color=gausscol)
                        # write energy to peak

                    # plot noise level
                    # noise_level = 4*Spectrum.noise_level(data) # re-calculates noise level... not that nice
                    # plt.plot(noise_level, c="orange")

                    plt.tight_layout()
                    if plot_spectrum == "show":
                        plt.show()
                    else:
                        saveloc = f"{plot_spectrum}-calib-spectrum-{det_name}.png"
                        plt.gcf().savefig(saveloc)
                        print("INFO: saving calibration spectrum at", saveloc)
                    plt.close(f)
            except OdrError:
                warning = True
                print("WARNING: some fitting error occurred")

            # save the results
            if not warning or override_warnings:
                calibdict = {
                    "polyfit": calib_quadr,
                    "polyfiterr": calib_quadr_sigma,
                    "linefit": calib_linear,
                    "linefiterr": calib_linear_sigma
                }
                jsondict[det_name] = calibdict

                # save in this object
                print("INFO: saving calibration in spectrum object")
                self.singles[det_name]["EnergyCalibration"] = calibdict

        # export results as file
        if json_save_filename:
            print("INFO: saving calibration to file")
            jf = open(json_save_filename, "w")
            json.dump(jsondict, jf)
            jf.close()

    # TODO move the following static functions and maybe GaussFit class to some algoriths module!
    # they all contain their imports in-line. Maybe rearrange that after moving them.

    @staticmethod
    def match_calibration(gausses, literature_dict, fwhm_factor=1):
        """
        gets a list with GaussFits and a dict with literature information about the material which the gausses
        come from. Tries to find pairs of gausses and corresponding energy values

        NEXT STEPS:
        - maybe after 1st guess: choose next-highest peak, match it and then re-fit the guess!
        - multiple sources: find a way to avoid false-positive matches due to too dense literature lines

        Parameters
        ----------
        gausses: list of GaussFit objects
        literature_dict: dict of shape {
            "name": str
            "peaks": [[Energy 0, Intensity 0], [E1, I1], [E2, I2], ...] (sorted by intensity!)
            "peak_count_for_initial_guess": int
        }
        fwhm_factor: float
            fwhm_factor*gauss.fwhm is the interval width around peak center where literature values have to be in
            to be accepted as a match

        Returns
        -------
        - list of the GaussFit objects matched to literature values
        - list of the literature energy values of those gauss peaks
        """

        literature_peaks = np.array(literature_dict["peaks"])[:, 0] # slice away intensity data, is not used

        # sort gausses by descending amplitude
        gausses = sorted(gausses, key=lambda g: -g.amplitude)

        # do first guess with only few maxima
        ORDER_1st_GUESS = 1
        guess_literat = sorted(literature_dict["initial_guess_peaks"])
        if len(gausses) < len(guess_literat):
            raise Exception("not enough peaks found to do initial guess.")
        guess_gausses = sorted(gausses[:len(guess_literat)],key=lambda g: g.center) # sort by channel (~energy)

        guess_params, guess_sigma = Spectrum.odr_gauss_polyfit(guess_gausses, guess_literat, ORDER_1st_GUESS)

        """
        now we have a guess fit which we can use to match more gausses:
        for each remaining gauss:
            use the guess fit to estimate the energy it should have
            check if there is a literature value with approximately this energy
            if the literature value is inside the gauss's fwhm, then we take it.
        """

        def find_closest(arr, val):
            # return value in arr wich is closest to val
            d = float("inf") # a big number
            best = None
            for v in arr:
                if abs(v - val) < d:
                    d = abs(v - val)
                    best = v
            return best

        # gausses used for final fit, start with the one we already fitted and later append more
        good_gausses = list(guess_gausses)
        good_literature = list(guess_literat)

        # remove the gausses we already matched
        gausses = gausses[len(guess_literat):]

        # add 511 keV-Peak to also find it
        import scipy.constants.codata
        e511keV = scipy.constants.codata.value("electron mass energy equivalent in MeV") * 1000

        literature_peaks = list(literature_peaks)
        literature_peaks.append(e511keV)

        for g in gausses:
            # get guessed energy
            guess_energy = np.polynomial.polynomial.polyval(g.center, guess_params)
            literature_best_energy = find_closest(literature_peaks, guess_energy)
            max_dist_energy = abs(guess_energy - np.polynomial.polynomial.polyval(g.center - fwhm_factor * g.fwhm / 2, guess_params))
            if abs(guess_energy - literature_best_energy) < max_dist_energy:
                # peak accepted, is in the region of width fwhm * fwhm_factor around peak center
                good_gausses.append(g)
                good_literature.append(literature_best_energy)

        return good_gausses, good_literature # return matched peaks for plotting

    @staticmethod
    def noise_level(data, n_intervals=100, crop_up_to=.15):
        """
        Calculates the estimated amplitude of the noise of the signal given in data.
        Uses fft and crops away low frequencies, then calculates the maximum of the noise signal in intervals
        of fixed length. This results in a rectangular function which envelopes the noise signal.

        Problems: Output still contains some peaks where the original peaks in the calibration signal were, thus the
        treshold to accept peaks may be too large for noisy data.

        magic numbers in argument-defaults should be fine, they should work with most measurements
        Parameters
        ----------
        data: list
            array with noisy signal
        n_intervals: int, optional
            the number of intervals to determine
        crop_up_to: float, optional
            from 0 to 1
            the spectrum of data is set to zero for all indices from 0 to crop_up_to*len(data).
            The bigger, the more low frequencies are cropped away. The default value was empirically
            determined from several lab-beam calibration measurements.
        Returns
        -------
        an numpy array with the length of data with the noise amplitudes of the intervals in data.
        """
        data = np.array(data)
        spectrum = np.fft.rfft(data)
        crop_i = int(len(data) * crop_up_to)
        spectrum[:crop_i] = 0

        noise = np.fft.irfft(spectrum)

        # get maximum noise values for intervals
        cuts = np.linspace(0, len(noise), n_intervals, dtype=int)
        for i in range(n_intervals - 1):
            v = np.max(noise[cuts[i]:cuts[i + 1]])
            noise[cuts[i]:cuts[i + 1]] = v

        return noise

    @staticmethod
    def fit_single_peaks_with_gauss(data, noise_factor=4):
        """
        Searches for narrow peaks in data and fits gauss curves onto them.
        At the moment this contains hardcoded parameters to find peaks relevant for calibration and energy resolution.
        Uses noise_level function and searches for peaks whose amplitude ("prominence") is larger than the noise level
        at the position.

        Parameters
        ----------
        data: list of count numbers per channel
            data with peaks
        noise_factor: int
            minimum peak prominence is noise_factor*noise_level(channel). 4 should be fine.
        Returns
        -------
        A list of GaussFit objects fitted to data.
        """
        from scipy.signal import find_peaks, peak_widths

        def keV_to_ch(energy, number_of_channels=len(data), min_energy=0, max_energy=1400):
            """
            linearly translate an energy to a channel number. Useful to give tresholds for widths
            in energy and not in channel numbers. Is only used for "guessing" which peaks to use and
            which not, so does not need to be very precise.
            TODO magic numbers, give parameters for min_energy and max_energy?
            """
            return number_of_channels / (max_energy - min_energy) * energy

        # following magic numbers are in keV and should be acceptable because they depend on parameters in keV_to_ch
        max_gauss_width_ch = keV_to_ch(40)
        # noise level as estimation for peak prominence
        noise_level = Spectrum.noise_level(data)
        peak_positions, peak_data = find_peaks(data,
                        prominence=noise_factor*noise_level, # noise_level alone is not high enough
                        width=(keV_to_ch(1), max_gauss_width_ch), # (minimum, maximum) width: empirical
                        distance=keV_to_ch(2)) # to avoid two fits at the same peak

        # maybe an alternative (especially for noisy data), but don't know how to use it
        # peak_positions = find_peaks_cwt(data, widths=np.arange(1, len(data)/10))

        # get widths quite low at "foot" of the peak as crop estimation for gauss
        peak_width_foot = peak_widths(data, peak_positions, rel_height=.8)[0]

        gausses = []
        for peak_pos, foot_width in zip(peak_positions,peak_width_foot):
            if foot_width > max_gauss_width_ch:
                # too wide
                continue
            g = GaussFit(range(len(data)), data, peak_pos, 2*foot_width)
            if abs(g.center - peak_pos) > g.fwhm:
                # fit went to a wrong peak!
                continue
            if g.error or g.amplitude < 0 or g.fwhm > max_gauss_width_ch or g.y_offset < 0:
                # gauss somehow malformed
                continue
            gausses.append(g)

        return gausses

    @staticmethod
    def odr_gauss_polyfit(gausses, literature, order):
        """
        wrapper for odr_polyfit to be called with GaussFit objects as x-data and a list of
        literature values (peak energy) as y data
        used for energy calibration, where peaks in detector channels are matched to energies.
        gets stdevs from gauss objects and treats literature values as precise.
        fits a polynomial of order and returns the output of odr_polyfit.
        (which is: tuple: (coeffs, coeff_stdevs) where coeffs, coeff_stdevs are lists with polynomial values )

        Parameters
        -----------
        gausses: list of gauss objects, order for fitting has to match with values in literature!
        literature: list of energy values (not [energy, intensity]!!)

        Returns
        ---------
        the result of odr_polyfit
        """
        xdata, xsigma = [],[]
        for g in gausses:
            xdata.append(g.center)
            xsigma.append(g.center_stdev)

        return Spectrum.odr_polyfit(xdata, literature, order, xsigma=xsigma)

    @staticmethod
    def odr_polyfit(xdata, ydata, order, xsigma=None, ysigma=None):
        """
        do a ODR-fit with a polynomial of (param) order to given data.
        Parameters
        ----------
        order: int
            order of the polynomial to fit
        xdata: list
            x data
        ydata: list
            y data
        xsigma: list
            standard deviation of xdata
        ysigma: list
            standard deviation of ydata

        Returns
        -------
        tuple: (coeffs, coeff_stdevs) where coeffs, coeff_stdevs are lists with polynomial values
        calculate fit function via np.polynomial.polynomial.polyval(x, coeffs)
        order of polynomial coefficients:
        y = B[0] + B[1] * x + B[2] * x**2 + ...
        """
        import scipy.odr as odr

        # fitting function, depending on poly order
        def f(B, x):
            out = 0
            for o in range(order + 1):
                out += B[o] * x**o
            return out

        return Spectrum.odr_fit(f, xdata, ydata, xsigma, ysigma, [1.] * (order +1))

    @staticmethod
    def odr_fit(f, xdata, ydata, xsigma=None, ysigma=None, beta0=None):
        """
        ODR fit wrapper
        Parameters
        ----------
        f: callable
            fit function: f(B, x) with B a list of parameters
        xdata: list
            x data
        ydata: list
            y data
        xsigma: list
            standard deviation of xdata
        ysigma: list
            standard deviation of ydata
        beta0: list
            1st guess parameters

        Returns
        -------
        tuple: (B_opt, B_stdevs) where B_opt, B_stdevs are lists with optimized parameters and their stdevs
        """
        import scipy.odr as odr
        model = odr.Model(f)
        data = odr.RealData(x=xdata, y=ydata, sx=xsigma, sy=ysigma)
        fit = odr.ODR(data, model, beta0=beta0).run()
        return fit.beta, fit.sd_beta


class GaussFit:
    # fit function
    @staticmethod
    def gauss_offset(x, amplitude, center, y_offset, sigma):
        return amplitude * np.exp(-0.5 * ((x - center) / sigma) ** 2) + y_offset

    def __str__(self):
        return f"GaussFit, center={self.center}, ampl={self.amplitude}"

    def __repr__(self):
        return self.__str__()

    def __init__(self, data_x, data_y, center_guess=None, width_guess=None):
        """
        Creates a gauss-fit for given data. If *both* center_guess and width_guess are given, data will be
        cropped first like
        data will be cropped like data[center_guess-width_guess/2:center_guess+width_guess/2]
        Otherwise data is expected to be properly cropped and fittable!
        Uses sqrt(y) as y-error estimation.

        Parameters
        ----------
        data_x: list
            x data
        data_y: list
            y data
        center_guess: int, optional
            estimated center of the peak
        width_guess: int, optional
            width to crop data around given center
        """
        from scipy.optimize import curve_fit

        # crop data if necessary
        if center_guess is None and width_guess is not None or center_guess is not None and width_guess is None:
            # only one of xxxx_guess args given
            raise Exception("both center_guess and width_guess or none must be given")
        if center_guess is not None and width_guess is not None:
            data_x = data_x[max(0, center_guess - int(width_guess / 2))
                            : min(len(data_x) - 1, center_guess + int(width_guess / 2))]
            data_y = data_y[max(0, center_guess - int(width_guess / 2))
                            : min(len(data_y) - 1, center_guess + int(width_guess / 2))]

        # find start parameters for curve_fit
        ampl_guess = data_y[int(len(data_y) / 2)]
        center_guess = data_x[int(len(data_x) / 2)]
        y_offs_guess = data_y[int(len(data_y) / 10)] # some value at the side of the peak
        sigma_guess = 0.25 * len(data_x) # sigma ~ .5 * fwhm and fwhm ~~ .5 * width_guess
        guess = [ampl_guess, center_guess, y_offs_guess, sigma_guess]

        # y-error estimation TODO copied from old implementation, ok that way?
        y_errs = np.sqrt(data_y)
        y_errs = np.where(y_errs == 0, 1, y_errs) # correction needed, otherwise division by zero

        # fit
        try:
            self.opt, self.pcov = curve_fit(GaussFit.gauss_offset, data_x, data_y, p0=guess, sigma=y_errs, absolute_sigma=True)
            self.error = False
        except (Exception, Warning):
            self.error = True
            self.opt, self.pcov = [0,0,0,0], [0,0,0,0]
        self.perr = np.sqrt(np.diag(self.pcov))
        # object attributes
        self.amplitude, self.center, self.y_offset, self.sigma = self.opt
        self.amplitude_stdev, self.center_stdev, self.y_offset_stdev, self.sigma_stdev = self.perr
        # fwhm (https://en.wikipedia.org/wiki/Full_width_at_half_maximum)
        a = 2 * np.sqrt(2 * np.log(2))
        self.fwhm = abs(self.sigma * a) # sometimes negative, bad for comparisons later
        self.fwhm_stdev = self.sigma_stdev * a

        """
        # DEBUG plot fit section
        plt.figure()
        plt.plot(data_x, data_y)
        self.plot()
        plt.show()
        """

    def plot(self, color="red"):
        """
        plots the represented Gauss into the current plot, which should be setup already.
        """
        xvals = np.linspace(self.center-self.fwhm*2, self.center+self.fwhm*2, 100)
        import matplotlib.pyplot as plt
        plt.plot(xvals, GaussFit.gauss_offset(xvals, *self.opt), c=color)


class SaveState:
    def __init__(self, queue, batch, ref):
        self.queue = queue
        self.batch = batch
        self.ref = ref


class SaveData:
    """Class used to store imported Spectrum class objects as binary pickle file.

    This acts as a fake cache since importing the pickle files is much faster than reimporting the measurement files
    themselves.

    Attributes
    ----------
    path : object
        Path to the cache file.
    folder : object
        Path to the folder where cache files are stored.
    config : object
        Path to the config file used to store caching settings.
    cache : dict
        Dictionary containing all Spectrum objects to be stored in the cache.
    max_c : int
        Max number of coincidence measurements to be saved.
    max_s : int
        Max number of single measurements to be saved.
    ccount : int
        Number of currently stored coincidence measurements.
    scount : int
        Number of currently stored single measurements.
    incl_c : bool
        If set to False no coincidence measurements will be stored.
    active : bool
        If set to False caching is disabled.
    allsteps : bool
        If set to True the cache will be updated after each import (default is True).
    load_config : bool
        If set to True the config file will be used for default settings (default is True).

    Methods
    -------
    get_coinfig()
        Opens the config file for caching and loads the settings stored within.
    load_cache()
        Opens the previously saved pickle file and loads the Spectrum objects.
    reset()
        Empties current cache.
    save_cache()
        Writes current state into pickle file.
    add_filfe(spec)
        Adds given Spectrum object to cache.
    """
    def __init__(self, active=True, incl_c=True, max_s=300, max_c=15, save_every_step=True, load_config=True):
        import os
        self.path = os.path.dirname(os.path.realpath(__file__)) + '/../data/dump/cache.pkl'
        self.folder = os.path.dirname(os.path.realpath(__file__)) + '/../data/dump'
        self.config = os.path.dirname(os.path.realpath(__file__)) + '/../data/dump/cache_config.txt'
        self.cache = {}
        self.max_c = max_c
        self.max_s = max_s
        self.ccount = 0
        self.scount = 0
        self.incl_c = incl_c
        self.active = active
        self.allsteps = save_every_step
        if load_config:
            self.get_config()
        self.load_cache()

    def get_config(self):
        try:
            with open(self.config, 'r') as cfg:
                for line in cfg:
                    if 'active' in line:
                        t = line.replace(' ', '')
                        a, b = t.split('=')
                        self.active = bool(int(b))
                    elif 'incl_c' in line:
                        t = line.replace(' ', '')
                        a, b = t.split('=')
                        self.incl_c = bool(int(b))
                    elif 'max_s' in line:
                        t = line.replace(' ', '')
                        a, b = t.split('=')
                        self.max_s = int(b)
                    elif 'max_c' in line:
                        t = line.replace(' ', '')
                        a, b = t.split('=')
                        self.max_c = int(b)
                    elif 'all_steps' in line:
                        t = line.replace(' ', '')
                        a, b = t.split('=')
                        self.allsteps = int(b)
        except FileNotFoundError:
            self.active = False
            self.incl_c = False


    def load_cache(self):
        import os
        if os.path.isfile(self.path) and self.active:
            with open(self.path, 'rb') as dump:
                try:
                    c1 = pickle.load(dump)
                except EOFError:
                    print('Cache corrupted, rewriting cache...')
                    return
                self.cache = c1.cache.copy()
                self.ccount = c1.ccount
                self.scount = c1.scount
                self.incl_c = c1.incl_c
                del c1
                return
        else:
            return

    def reset(self):
        self.cache = {}
        self.ccount = 0
        self.scount = 0

    def save_cache(self):
        import os
        if os.path.isdir(self.folder) and self.active:
            with open(self.path, 'wb') as dump:
                pickle.dump(self, dump, pickle.HIGHEST_PROTOCOL)

    def add_file(self, spec):

        if spec.filename not in self.cache and self.active:
            do_cache = False
            if spec.type == 'Single':
                if self.scount <= self.max_s:
                    self.scount += 1
                    do_cache = True
            else:
                if self.ccount <= self.max_c and self.incl_c:
                    self.ccount += 1
                    do_cache = True
            if do_cache:
                self.cache[spec.filename] = spec
                if spec.save_all:
                    self.save_cache()

        return


cache = SaveData()

'''
if os.path.isfile('saves/temp.pkl'):
    print('Loading previous state......')
    with open('saves/temp.pkl', 'rb') as loaded:
        load1 = pickle.load(loaded)
        ref, files, batch =load1.ref, load1.queue, load1.batch
    print('Complete.')

    with open('saves/temp.pkl', 'wb') as output:
        save = SaveState(files, batch, ref)
        pickle.dump(save, output, pickle.HIGHEST_PROTOCOL)
    #'''
