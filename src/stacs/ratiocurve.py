import matplotlib.pyplot as plt
import matplotlib.patches as patch
import math as m
import matplotlib.colors
import matplotlib.cm as cmx
import matplotlib.axes
import scipy.interpolate
import numpy as np

def ratio(reference, measurement):
    energy, counts, error = measurement
    energyR, countsR, errorR = reference
    countRatio, errorRatio = [], []
    try:
        if len(energy) == len(energyR):
            for i in range(len(energy)):
                if countsR[i] == 0:
                    countRatio.append(0.)
                    errorRatio.append(0.)
                else:
                    countRatio.append(counts[i] / countsR[i])
                    errorRatio.append(m.sqrt(
                        (1/(countsR[i]**2)) * (error[i]**2)
                        + ((counts[i]**2)/(countsR[i]**4)) * (errorR[i]**2)))
        else:
            raise IndexError
    except IndexError:
        print('Reference spectrum and measurement spectrum have different \
energy resolutions. Evaluation not possible')
    return np.array(energy), np.array(countRatio), errorRatio


def plotter(reference, filelist, title, bins='BinsRes.txt', namelist=None,
            ploteverypoint='smooth', ploterror=True, linesmooth=0.002,
            y_max=2.8, y_min=.2, x_min=511.0, x_max=518.1, col='viridis',
            contrast=True, xaxe='dE', fix_ax=True, labels=True, w_ref=None,
            w_data=None, external=None, bg_sub=False, linewidth=1.5,
            legend_loc="best", do_plot=True):

    """
    Function to plot Ratio Curves.

    Takes a reference measurement and list of measurements as Spectrum Objects.
    """

    progstep = int(100 / (len(filelist) + 1)) # to pass to a progress bar
    prog = 0
    labellist = []
    meas_list = []
    err_list = []
    ref = binning(reference, bins, width=w_ref, background_corr=bg_sub)
    prog += progstep
    refratio = ratio(ref, ref)

    if xaxe == 'dE':
        x_min -= 511.0
        x_max -= 511.0
        for ij in range(len(refratio[0])):
            refratio[0][ij] -= 511.0
    elif xaxe == 'm0c':
        x_min = (x_min - 511.0) * 3.918
        x_max = (x_max - 511.0) * 3.918
        for ji in range(len(refratio[0])):
            refratio[0][ji] = (refratio[0][ji] - 511.0) * 3.918

    if do_plot:
        plt.plot(refratio[0], refratio[1], color='black', linewidth=2*linewidth)
    colnorm = matplotlib.colors.Normalize(vmin=0, vmax=len(filelist)-1)
    colmap = cmx.ScalarMappable(norm=colnorm, cmap=col)
    meas_list.append((refratio[0], refratio[1], 'black', 2))


    for i in range(len(filelist)):
        file = binning(filelist[i], bins, width=w_data, background_corr=bg_sub)
        prog += progstep
        if i == len(filelist) - 1 and contrast == True:
            col = 'orange'
        else:
            col = colmap.to_rgba(i)
        fileratio = ratio(ref, file)

        if xaxe == 'dE':
            for k in range(len(fileratio[0])):
                fileratio[0][k] -= 511.0
        elif xaxe == 'm0c':
            for jk in range(len(fileratio[0])):
                fileratio[0][jk] = (fileratio[0][jk] - 511.0) * 3.918
        if namelist == None:
            labellist.append(patch.Patch(label=filelist[i].name, color=col))
        else:
            labellist.append(patch.Patch(label=namelist[i], color=col))

        if ploterror:
            if do_plot:
                plt.errorbar(fileratio[0], fileratio[1], fileratio[2], color=col,
                        linestyle='None', capsize=2*linewidth,
                        elinewidth=linewidth, markeredgewidth=linewidth)
            else:
                err_list.append((fileratio[0], fileratio[1], fileratio[2], col))

        if ploteverypoint == 'rough':
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.spline(fileratio[0], fileratio[1], xnew)
        elif ploteverypoint == 'smooth':
            t, c, k = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)
        else:
            smoothratio = []
            smoothval = []
            cutoff = (fileratio[0].max() - fileratio[0].min()) / 0.57143
            for j in range(len(fileratio[0])):
                if fileratio[0][j] > cutoff:
                    smoothval.append(fileratio[1][i])
            average = sum(smoothval)/len(smoothval)
            for m in range(len(fileratio[0])):
                if fileratio[0][m] <= cutoff:
                    smoothratio.append(fileratio[1][m])
                else:
                    smoothratio.append(average)

            t, c, k = scipy.interpolate.splrep(fileratio[0], smoothratio, s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)

            '''
            t, c, k = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), 515., 250, endpoint=True)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)

            t2, c2, k2 = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=0.05, k=5)
            xnew2 = np.linspace(515., fileratio[0].max(), 250, endpoint=True)
            ysmooth2 = scipy.interpolate.BSpline(t2, c2, k2, extrapolate=False)
            ysmooth2 = ysmooth2(xnew2)
            #'''
        if do_plot:
            plt.plot(xnew, ysmooth, color=col, linewidth=linewidth)
        meas_list.append((xnew, ysmooth, col, 1.5))

    if external is not None:
        init = [external[0][i] - 511 for i in range(external[0].shape[0])]
        plt.plot(init, external[1], color='crimson')
        plt.errorbar(external[0], external[1], external[2], color='crimson',
                linestyle='None', capsize=2*linewidth, elinewidth=linewidth)

    if do_plot:
        plt.grid(linewidth=linewidth)
        if labels:
            plt.legend(handles=labellist, loc=legend_loc)
        if fix_ax:
            plt.ylim(top=y_max, bottom=y_min)
            plt.xlim(left=x_min, right=x_max)
        plt.ylabel('Ratio to {}'.format(reference.name))
        if xaxe == 'dE':
            plt.xlabel('$\Delta$E (keV)')
        elif xaxe == 'm0c':
            plt.xlabel('$10^{-3} \ \mathrm{m_0 c}$')
        else:
            plt.xlabel('E (keV)')
        plt.title(title)

        if __name__ == '__main__':
            plt.show()
        if not labels:
            return(labellist)
    else:

        xlabel = '$\Delta$E (keV)'
        return meas_list, err_list, labellist, (y_max, y_min), (x_min, x_max), reference.name, xlabel, title


def gui_plotter(reference, filelist, title, bins='BinsRes.txt', namelist=None, ploteverypoint='smooth', ploterror=True,
                linesmooth=0.01, y_max=2.8, y_min=.2, x_min=511.0, x_max=518.1, col='viridis', contrast=True, xaxe='dE',
                progress=None):

    """Similar functionality to ratiocurve.plotter, but designed to be integrated into pyqt gui."""

    progstep = int(100 / (len(filelist) + 1)) # to pass to a progress bar
    prog = 0
    #progress.emit(prog)
    labellist = []
    ref = binning(reference, bins)
    prog += progstep
    #progress.emit(prog)
    refratio = ratio(ref, ref)

    if xaxe == 'dE':
        x_min -= 511.0
        x_max -= 511.0
        for ij in range(len(refratio[0])):
            refratio[0][ij] -= 511.0
    elif xaxe == 'm0c':
        x_min = ((x_min - 511.0) * 1000 * 2 * 1.6022 * (10 ** (-19)) * 1000)/(9.1 * (10 ** (-31)) * 299792458 ** 2)
        x_max = ((x_max - 511.0) * 1000 * 2 * 1.6022 * (10 ** (-19)) * 1000)/(9.1 * (10 ** (-31)) * 299792458 ** 2)
        for ji in range(len(refratio[0])):
            refratio[0][ji] = (((refratio[0][ji] - 511.0) * 1000 * 2 * 1.6022 * (10 ** (-19)) * 1000)/
                               (9.1 * (10 ** (-31)) * (299792458 ** 2)))

    meas_list = []
    err_list = []

    meas_list.append((refratio[0], refratio[1], 'black', 2))
    # plt.plot(refratio[0], refratio[1], color='black')
    colnorm = matplotlib.colors.Normalize(vmin=0, vmax=(len(filelist) + 1))
    colmap = cmx.ScalarMappable(norm=colnorm, cmap=col)

    for i in range(len(filelist)):
        file = binning(filelist[i], bins)
        prog += progstep
        #progress.emit(prog)
        if i == len(filelist) - 1 and contrast == True:
            col = 'orange'
        else:
            col = colmap.to_rgba(i)
        fileratio = ratio(ref, file)

        if xaxe == 'dE':
            fileratio[0] -= 511.0
        elif xaxe == 'm0c':
            for jk in range(len(fileratio[0])):
                fileratio[0][jk] = (((fileratio[0][jk] - 511.0) * 1000 * 2 * 1.6022 * (10 ** (-19)) * 1000) /
                                    (9.1 * (10 ** (-31)) * (299792458 ** 2)))
        if namelist == None:
            labellist.append(patch.Patch(label=filelist[i].name, color=col))
        else:
            labellist.append(patch.Patch(label=namelist[i], color=col))
        if ploterror:
            err_list.append((fileratio[0], fileratio[1], fileratio[2], col))
            # plt.errorbar(fileratio[0], fileratio[1], fileratio[2], color=col, linestyle='None', capsize=2,
            #              elinewidth=1)
        if ploteverypoint == 'rough':
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.spline(fileratio[0], fileratio[1], xnew)
        elif ploteverypoint == 'smooth':
            t, c, k = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)
        else:
            smoothratio = []
            smoothval = []
            cutoff = (fileratio[0].max() - fileratio[0].min()) / 0.57143
            for j in range(len(fileratio[0])):
                if fileratio[0][j] > cutoff:
                    smoothval.append(fileratio[1][i])
            average = sum(smoothval)/len(smoothval)
            for m in range(len(fileratio[0])):
                if fileratio[0][m] <= cutoff:
                    smoothratio.append(fileratio[1][m])
                else:
                    smoothratio.append(average)

            t, c, k = scipy.interpolate.splrep(fileratio[0], smoothratio, s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), fileratio[0].max(), 500)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)

            '''
            t, c, k = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=linesmooth, k=3)
            xnew = np.linspace(fileratio[0].min(), 515., 250, endpoint=True)
            ysmooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
            ysmooth = ysmooth(xnew)

            t2, c2, k2 = scipy.interpolate.splrep(fileratio[0], fileratio[1], s=0.05, k=5)
            xnew2 = np.linspace(515., fileratio[0].max(), 250, endpoint=True)
            ysmooth2 = scipy.interpolate.BSpline(t2, c2, k2, extrapolate=False)
            ysmooth2 = ysmooth2(xnew2)
            #'''

        #plt.plot(xnew, ysmooth, color=col, linewidth=1.5)
        meas_list.append((xnew, ysmooth, col, 1.5))

    # ax = plt.axes()
    # ax.grid(True)
    # plt.legend(handles=labellist)
    # plt.ylim(top=y_max, bottom=y_min)
    # plt.xlim(left=x_min, right=x_max)
    # plt.ylabel('Ratio to {}'.format(reference.name))
    if xaxe == 'dE':
        xlabel = '$\Delta$E (keV)'
        # plt.xlabel('$\Delta$E (keV)')
    elif xaxe == 'm0c':
        xlabel = '$10^{-3} \ \mathrm{m_0 c}$'
        # plt.xlabel('$10^{-3} \ \mathrm{m_0 c}$')
    else:
        xlabel = 'E (keV)'
        # plt.xlabel('E (keV)')
    # plt.title(title)
    # plt.show()
    params = (meas_list, err_list, labellist, (y_max, y_min), (x_min, x_max), reference.name, xlabel, title)
    return params


def find_s_param(filelist, onlyS=False):
    slist = []
    selist = []
    wlist = []
    welist = []

    for j in range(len(filelist)):
        file = binning(filelist[j])
        energy, counts, error = file
        sratio = 0.
        serror = []
        wratio = 0.
        werror = []
        allcounts = 0.
        allcounterror = []

        for i in range(len(energy)):
            if energy[i] <= (511.0 + 0.75):
                sratio += counts[i]
                serror.append(error[i]**2)
            elif energy[i] >= (511.0 + 1.5) and energy[i] <= (511.0 + 3.0):
                wratio += counts[i]
                werror.append(error[i]**2)
            allcounts += counts[i]
            allcounterror.append(error[i]**2)

        dS = m.sqrt(sum(serror))
        dW = m.sqrt(sum(werror))
        dall = m.sqrt(sum(allcounterror))

        S = sratio/allcounts
        DS = m.sqrt((1/(allcounts**2)) * (dS ** 2) + ((sratio ** 2)/(allcounts ** 4)) * (dall ** 2))
        W = wratio/allcounts
        DW = m.sqrt((1/(allcounts**2)) * (dW ** 2) + ((wratio ** 2)/(allcounts ** 4)) * (dall ** 2))

        slist.append(S)
        selist.append(DS)
        wlist.append(W)
        welist.append(DW)

        if onlyS:
            print(filelist[j], '\n', '\t S:', S, '+/-', DS, '\n')
        else:
            print(filelist[j], '\n', '\t S:', S, '+/-', DS, '\t \t W:', W, '+/-', DW, '\n')

    print('S list and errors')
    print(slist)
    print(selist)
    print('W list and errors')
    print(wlist)
    print(welist)

if __name__ == '__main__':
    import matplotlib
    matplotlib.use('Qt5Agg')
    sp1 = (Spectrum(filename='DPG_Mess/Messung_13/Messung000.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung001.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung002.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung003.mpa'))
    sp1.name = '4h'
    sp2 = (Spectrum(filename='DPG_Mess/Messung_13/Messung004.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung005.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung006.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung007.mpa'))
    sp2.name = '8h'
    sp3 = (Spectrum(filename='DPG_Mess/Messung_13/Messung027.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung028.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung029.mpa') +
           Spectrum(filename='DPG_Mess/Messung_13/Messung030.mpa'))
    sp3.name = '30h'
    sp4 = (Spectrum(filename='DPG_Mess/Messung_10/Messung003.mpa')+
           Spectrum(filename='DPG_Mess/Messung_10/Messung008.mpa')+
           Spectrum(filename='DPG_Mess/Messung_10/Messung013.mpa')+
           Spectrum(filename='DPG_Mess/Messung_10/Messung018.mpa'))
    sp4.name = 'as received'

    ref = Spectrum(filename='DPG_Mess/Aluref.mpa')

    plotter(ref, [sp1, sp2, sp3, sp4], 'Deformed', bins='BinsVar.txt')

