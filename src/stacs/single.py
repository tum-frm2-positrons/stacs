import warnings
import numpy as np
from scipy.special import erf
from scipy.optimize import curve_fit
import scipy.constants.codata

M_POSITRON_keV = \
   scipy.constants.codata.value("electron mass energy equivalent in MeV") * 1e3

class SingleSpectrum:
    """
    Single Doppler broadening measurement spectrum.

    This class comprises the recorded spectrum of one detector and all of the
    detector's specifications, like energy calibration and resolution. It is
    used to calculate the line shape parameters S and W of the 511 keV
    annihilation line and the Valley-to-Peak parameter of Ps formation and
    provides the user with different possibilties for the calculations, e.g.,
    whether to perform a background subtraction before determining S and W
    parameter.

    Parameters
    ----------
    spectrum : iterable of int
        The recorded histogram of counts in each channel.
    ecal : tuple of float
        Energy calibration determining the channel energies. Tuple of three
        coefficients obtained from a second order Taylor fit to the peak
        locations of, e.g., Eu-152.
    ecal_stdev: tuple of float
        Standard deviations for each value of ecal.
    eres : float
        Energy resolution at 511 keV.
    live_time : float
        Live time of the detector.
    detname : str
        Name of the detector.
    show : bool
        Show info/debug plots for each data processing step. Multiple
        pyplot windows will pop up.
    verbose : bool
        Print (debug) info while processing.

    """

    def __init__(self, spectrum, detname=None, implantation_energy=None,
                 ecal=None, ecal_stdev=None, eres=None, live_time=None,
                 real_time=None, show=False, verbose=False):
        self.show = show
        self.verbose = verbose

        self.spectrum = spectrum
        self.implantation_energy = implantation_energy
        self.ecal = ecal
        self.ecal_stdev = ecal_stdev
        self.eres = eres
        self.live_time = live_time
        self.real_time = real_time
        self.detname = detname

        # list data
        self.energies = None
        self.peak = None
        self.peak_energies = None

        # single numbers
        self.counts = np.nan
        self.dcounts = np.nan

        self.peak_center = np.nan
        self.peak_counts = np.nan
        self.dpeak_counts = np.nan
        self.peak_countrate_real = np.nan
        self.dpeak_countrate_real = np.nan
        self.peak_countrate_live = np.nan
        self.dpeak_countrate_live = np.nan

        self.s = np.nan
        self.w = np.nan
        self.v2p = np.nan
        self.ds = np.nan
        self.dw = np.nan
        self.dv2p = np.nan

    def __add__(self, other):#
        try:
            if self.ecal != other.ecal or self.eres != other.eres:
                print(f'''Energy calibration/resolution of added single spectra differs for detector {self.detname}''')
        except ValueError:
            print('Summed SingleSpectrum Objects are missing energy calibration.')
        if self.detname != other.detname:
            print(f'Trying to add different detectors to one another, something went wrong! Returning None')
            return None
        else:
            if self.implantation_energy == other.implantation_energy:
                e_imp = self.implantation_energy
            else:
                e_imp = 0.5 * self.implantation_energy + 0.5 * other.implantation_energy
                print('Interpolating implantation energy!')

            return SingleSpectrum(self.spectrum+other.spectrum, detname=self.detname, implantation_energy=e_imp,
                                  ecal=self.ecal, eres=self.eres, live_time=self.live_time,
                                  real_time=self.real_time)

    def analyze(self, s_width=1.1, w_width=1.0, w_dist=3.0, peak_width=60,
                bg_frac=0.25, bg_corr=True, v2p_bounds=(450, 490, 506, 516),
                follow_peak="1st_order_correction"):
        """
        Perform analysis of the gamma annihilation spectrum.

        Calculate the line shape parameters S and W, and the Valley-to-Peak
        parameter of Ps formation. Consider the options to disable background
        subtraction before S and W determination, or to tune the energy regions
        defining each parameter.

        Parameters
        ----------
        s_width : float
            Width of the S parameter central peak region in keV. So that
            S is the quotient of counts inside 2*s_width and all counts in
            the peak.
        w_width : float
            Width of one W parameter wing region in keV. So that W is the
            quotient of counts inside both wings (with width w_width each) and
            all counts in the peak. For the location of the wings see w_dist.
        w_dist : float
            Distance between the 511 keV peak center and the W parameter wing
            regions in keV. So that the right wing starts at (511 keV + w_dist)
            and ends at (511 keV + w_dist + w_width). Left wing analogously.
        peak_width : float
            Total width of the peak region in keV used for background
            subtraction and S and W calculation. So that the peak starts at
            (- peak_width/2) and ends at (peak_width/2). Note that the peak
            region for the P2V parameter is defined inside v2p_bounds.
        bg_frac : float
            Fraction of the peak array used for background fitting. bg_frac/2
            channels at each end of the peak array are extracted and used for
            determining the background erf amplitude and offset.
        bg_corr : bool
            Perform background subtraction. Does not affect V2P.
        v2p_bounds : tuple of floats
            Energy region boundaries for V2P parameter calculation. Comprises
            valley left bound, valley right bound, peak left bound, peak right
            bound (in that order).
        follow_peak : str
            Addjust the energy calibration in oder to shift the fitted peak
            center to 511 keV. Options are:
                "0th_order_correction", if this is selected the offset
                (coefficent c1) is adjusted.
                "1st_order_correction": if this is selected the channel to
                energy conversion (coefficent c2) is modified.
            Else, all lineshape parameters are evaluated with respect to the
            (possibly inaccurate) energy calibration.

        """

        def gaussian(x, a, mu, sig, offset=0):
            """Gaussian function."""

            return a * np.exp(- (x-mu) * (x-mu) / sig / sig / 2) + offset

        def mod_erf(x, a, offset, center, smooth=1):
            """Modified error function."""

            return a * erf(smooth * (x - center)) + offset

        def comb(x, a_gauss, center, sig, a_erf, offset, smooth):
            """Sum of a Gaussian and a (modified) erf."""

            return gaussian(x, a_gauss, center, sig) \
                    + mod_erf(x, a_erf, offset, center, smooth)

        def find_closest(array, value):
            """Find the closest element in an array and return the index."""

            array = np.array(array)
            idx = array.searchsorted(value)
            idx = np.clip(idx, 1, len(array)-1)
            left = array[idx-1]
            right = array[idx]
            idx -= value - left < right - value

            return idx

        def channel_fraction(e_cut, e_left, e_right, counts,
                             left_boundary=True):
            """Calculate fraction of channel counts when cutting it in two."""

            if left_boundary:
                # include fraction of the channel that is to the right of the
                # cut energy
                e_decide = e_right
            else:
                e_decide = e_left

            return counts * abs(e_decide - e_cut) / (e_right - e_left)

        if self.spectrum is None:
            wrn = "Trying to analyze an empty single spectrum. Aborting..."
            warnings.warn(wrn)
            return

        if self.verbose: print("Analyzing single spectrum of detector "
                          + f"{self.detname} ...")

        if self.show: import matplotlib.pyplot as plt

        n_channels = len(self.spectrum)
        total_counts = np.sum(self.spectrum)

        # calculate channel energies from ecal
        c1, c2, c3 = self.ecal
        channel_energies = np.array(list(map(lambda x: c1 + c2*x + c3*x*x,
                                    np.arange(n_channels))))

        # extract 511 keV peak
        peak_energy = M_POSITRON_keV
        left_bound, right_bound = find_closest(channel_energies,
                (peak_energy - peak_width/2, peak_energy + peak_width / 2))

        peak_energies = np.array(channel_energies[left_bound:right_bound+1])
        peak = self.spectrum[left_bound:right_bound+1]

        if np.std(peak) < 1:
            # no peak visible in the cropped area
            wrn = ("No peak found. Maybe your energy calibration is faulty? "
                   "Could not analyze the single spectrum of detector "
                   f"{self.detname} (no S, W & V2P parameters calculated).")
            warnings.warn(wrn)

            if self.show:
                plt.plot(channel_energies, self.spectrum)
                plt.show()

            return

        if self.verbose:
            print(f"Extracted peak with {len(peak)} channels width.")

        # fit Gaussian to peak      ToDo: Use Gaussian class object!
        # (to get the center and check if ecal is correct)
        h = np.max(peak)
        # start with energy of maximum (better for faulty ecals)
        idx_max = np.argmax(peak)
        # use the ten most outer points for the peak offset guess
        outer_average = np.average([*peak[-10:], *peak[:11]])
        rough_guess = (h - outer_average, peak_energies[idx_max], 1.5,
                       outer_average)

        rough_popt, rough_pcov = curve_fit(gaussian, peak_energies, peak,
                                           rough_guess)
        center = rough_popt[1]

        if self.verbose:
            msg = (f"Peak center {center:.3f} keV deviates from {peak_energy} "
                   f"keV by {peak_energy - center:.3f} keV "
                    "(< 0.5 keV is typically okay).")
            print(msg)

        if bg_corr:
            # subtract background from peak
            bg_width = round(bg_frac * peak_width / 2)
            bl, br = find_closest(peak_energies,
                                    [center-peak_width/2+bg_width,
                                     center+peak_width/2-bg_width])

            bg_energies = np.append(peak_energies[:bl+1],
                                    peak_energies[br:])
            bg_left, bg_right = peak[:bl+1], peak[br:]
            background = np.append(bg_left, bg_right)

            # fit erf to background (for amplitude and offset)
            bg_guess = (np.average(bg_right) - np.average(bg_left),
                        np.average(background))
            bg_popt, bg_pcov = curve_fit(lambda x, a, offset:
                                         mod_erf(x, a, offset, center),
                                         bg_energies, background, bg_guess)
            a_erf, offset = bg_popt

            # Fit combination of Gaussian and erf
            guess = (h, center, 1.5)
            popt, pcov = curve_fit(lambda x, a_gauss, center, sig:
                           comb(x, a_gauss, center, sig, a_erf, offset, 1/sig),
                           peak_energies, peak, guess)
            a_gauss, center, sig = popt

            # subtract background
            peak_raw = peak
            peak = np.array(peak).astype(float) \
                   - mod_erf(peak_energies, a_erf, offset, center, 1/sig)

            # calculate the subtracted fraction
            raw_counts = np.sum(peak_raw)
            subtracted_counts = raw_counts - np.sum(peak)
            subtracted_fraction = subtracted_counts / raw_counts

            if self.verbose:
                print("Performed background subtraction (subtracted fraction: "
                      + f"{subtracted_fraction*100:.2f}%).")

        if self.show:
            plot_title = (f"Det {self.detname}: Different fits performed for "
                           "Peak Extraction")
            plt.plot(peak_energies, peak_raw, label='data')
            plt.plot(peak_energies, gaussian(peak_energies, *rough_guess),
                      label='initial guess at peak maximum')
            plt.plot(peak_energies, gaussian(peak_energies, *rough_popt),
                     label='Gaussian fit')
            plt.vlines([peak_energies[0], peak_energies[-1]], 0, h,
                       label='peak width')
            if bg_corr:
                y = [0,0,h,h]
                x1 = [peak_energies[0], peak_energies[bl],
                      peak_energies[bl], peak_energies[0]]
                x2 = [peak_energies[br], peak_energies[-1],
                      peak_energies[-1], peak_energies[br]]
                plt.fill(x1, y, 'blue', alpha=0.2,
                         label='data used for background fit')
                plt.fill(x2, y, 'blue', alpha=0.2)
                plt.plot(peak_energies, comb(peak_energies, popt[0], popt[1],
                         popt[2], a_erf, offset, 1/popt[2]),
                         label='Gaussian + erf fit')
                plt.plot(peak_energies, mod_erf(peak_energies, a_erf, offset,
                         center, 1/sig), label='background')
                plot_title += "\n(with background substraction)"
            plt.title(plot_title)
            plt.legend()
            plt.show()

        # calculate S, W & V2P
        peak_counts = np.sum(peak)

        if follow_peak:
            if follow_peak == "0th_order_correction":
                # shift offset (c1) but retain channel width
                self.ecal[0] -= peak_energy - center
                msg = (f"Corrected offset of Ecal coefficient c1: "
                   f"{c1:.6f} -> {self.ecal[0]:.6f}")
            elif follow_peak == "1st_order_correction":
                # scale channel width (c2)
                self.ecal[1] *= (peak_energy - c1) / (center - c1)
                msg = (f"Corrected linear Ecal coefficient c2: "
                   f"{c2:.6f} -> {self.ecal[1]:.6f}")
            else:
                msg = f"{follow_peak} is not a valid key word for follow_peak"
                raise ValueError(msg)

            if self.verbose: print(msg)

            # recalculate peak_energies using new ecal
            c1, c2, c3 = self.ecal
            channel_energies = np.array(list(map(lambda x: c1 + c2*x + c3*x*x,
                                        np.arange(n_channels))))
            peak_energies = np.array(channel_energies[left_bound:right_bound+1])

        ## S parameter
        # sl stands for 'S parameter left boundary'
        sl = peak_energy - s_width
        sl_idx = peak_energies.searchsorted(sl)
        e_sl_left, e_sl_right = peak_energies[sl_idx-1:sl_idx+1]

        # sr stands for 'S parameter right boundary'
        sr = peak_energy + s_width
        sr_idx = peak_energies.searchsorted(sr)
        e_sr_left, e_sr_right = peak_energies[sr_idx-1:sr_idx+1]

        s_area = channel_fraction(sl, e_sl_left, e_sl_right, peak[sl_idx-1]) \
               + channel_fraction(sr, e_sr_left, e_sr_right, peak[sr_idx-1],
                                  left_boundary=False) \
               + np.sum(peak[sl_idx:sr_idx-1])

        s = s_area / peak_counts

        ## W parameter
        # wll stands for 'W parameter left wing, left boundary'
        wll = peak_energy - w_width - w_dist
        wll_idx = peak_energies.searchsorted(wll)
        e_wll_left, e_wll_right = peak_energies[wll_idx-1:wll_idx+1]

        # wlr stands for 'W parameter left wing, right boundary'
        wlr = peak_energy - w_dist
        wlr_idx = peak_energies.searchsorted(wlr)
        e_wlr_left, e_wlr_right = peak_energies[wlr_idx-1:wlr_idx+1]

        # wrl stands for 'W parameter right wing, left boundary'
        wrl = peak_energy + w_dist
        wrl_idx = peak_energies.searchsorted(wrl)
        e_wrl_left, e_wrl_right = peak_energies[wrl_idx-1:wrl_idx+1]

        # wrr stands for 'W parameter right wing, right boundary'
        wrr = peak_energy + w_width + w_dist
        wrr_idx = peak_energies.searchsorted(wrr)
        e_wrr_left, e_wrr_right = peak_energies[wrr_idx-1:wrr_idx+1]

        w_area = (
            # left wing
            channel_fraction(wll, e_wll_left, e_wll_right, peak[wll_idx-1])
            + channel_fraction(wlr, e_wlr_left, e_wlr_right, peak[wlr_idx-1],
                               left_boundary=False)
            + np.sum(peak[wll_idx:wlr_idx-1])
            # right wing
            + channel_fraction(wrl, e_wrl_left, e_wrl_right, peak[wrl_idx-1])
            + channel_fraction(wrr, e_wrr_left, e_wrr_right, peak[wrr_idx-1],
                               left_boundary=False)
            + np.sum(peak[wrl_idx:wrr_idx-1])
            )

        w = w_area / peak_counts

        ## V2P (Valley-to-Peak) parameter
        # vl (vr) stands for valley left (right) boundary
        # pl (pr) stands for peak left (right) boundary
        vl, vr, pl, pr = v2p_bounds

        vl_idx = channel_energies.searchsorted(vl)
        e_vl_left, e_vl_right = channel_energies[vl_idx-1:vl_idx+1]
        vr_idx = channel_energies.searchsorted(vr)
        e_vr_left, e_vr_right = channel_energies[vr_idx-1:vr_idx+1]
        pl_idx = channel_energies.searchsorted(pl)
        e_pl_left, e_pl_right = channel_energies[pl_idx-1:pl_idx+1]
        pr_idx = channel_energies.searchsorted(pr)
        e_pr_left, e_pr_right = channel_energies[pr_idx-1:pr_idx+1]

        valley_area = (
            channel_fraction(vl, e_vl_left, e_vl_right,
                             self.spectrum[vl_idx-1])
            + channel_fraction(vr, e_vr_left, e_vr_right,
                               self.spectrum[vr_idx-1], left_boundary=False)
            + np.sum(self.spectrum[vl_idx:vr_idx-1])
            )
        peak_area = (
            channel_fraction(pl, e_pl_left, e_pl_right,
                             self.spectrum[pl_idx-1])
            + channel_fraction(pr, e_pr_left, e_pr_right,
                               self.spectrum[pr_idx-1], left_boundary=False)
            + np.sum(self.spectrum[pl_idx:pr_idx-1])
            )

        v2p = valley_area / peak_area

        if self.verbose:
            print(f"Calculated line shape parameters: S = {s:.3f}, "
                    + f"W = {w:.3f}, V2P = {v2p:.3f}.")

        if self.show:
            # S and W parameter
            plt.plot(peak_energies, peak, label='data')
            y = [0, 0, h, h]
            x1 = [sl, sr, sr, sl]
            x2 = [wll, wlr, wlr, wll]
            x3 = [wrl, wrr, wrr, wrl]
            plt.fill(x1, y, 'green', alpha=0.2, label='S parameter')
            plt.fill(x2, y, 'blue', alpha=0.2, label='W parameter')
            plt.fill(x3, y, 'blue', alpha=0.2)
            plt.title(f'Det {self.detname}: S and W parameter areas')
            plt.legend()
            plt.show()

            # V2P parameter
            plt.plot(channel_energies, self.spectrum, label='data')
            y = [0, 0, h, h]
            x1 = [vl, vr, vr, vl]
            x2 = [pl, pr, pr, pl]
            plt.fill(x1, y, 'blue', alpha=0.2, label='valley parameter')
            plt.fill(x2, y, 'green', alpha=0.2, label='peak parameter')
            plt.title(f'Det {self.detname}: V2P parameter areas')
            plt.legend()
            plt.show()

        # set class arguments calculated in analyze()
        self.counts = total_counts
        self.dcounts = np.sqrt(total_counts)

        self.peak = peak
        self.peak_counts = peak_counts
        self.dpeak_counts = np.sqrt(peak_counts)
        if self.real_time:
            try:
                self.peak_countrate_real = self.peak_counts / self.real_time
                self.dpeak_countrate_real = self.dpeak_counts / self.real_time
            except TypeError:
                print("ERROR: Count rate could not be determined due to wrong \
data type of realtime")
        if self.live_time:
            self.peak_countrate_live = self.peak_counts / self.live_time
            self.dpeak_countrate_live = self.dpeak_counts / self.live_time
        else:
            print("No valid live time available.")
        self.peak_energies = peak_energies
        self.peak_center = center
        self.energies = channel_energies

        self.s = s
        self.w = w
        self.v2p = v2p
        self.ds = np.sqrt(s * (1 - s) / peak_counts)
        self.dw = np.sqrt(w * (1 - w) / peak_counts)
        self.dv2p = np.sqrt(valley_area / np.power(peak_area,2)
                            + np.power(valley_area,2) / np.power(peak_area,3))

        if self.verbose: print() # new line for readability

        return

    def calculate_eres(self, plot_spectrum=None, plot_fit=None, fit_function="sqrt",
                       poly_fit_order=1, return_foot_separation_mean=False):
        """
        Calculate detector resolution based on this spectrum.

        Requires this SingleSpectrum to be calibrated (self.ecal must not be None).
        Only works for a spectrum containing sharp peaks of known energy.
        It performs the following steps:

        1. fit_calib_peaks_with_gauss: searches for peaks and fits gauss functions to them
        2. apply calibration to energy values (GaussFit.center) and gauss fwhm (GaussFit.fwhm)
        3. check if a 511 keV peak is present, if found it will be disregarded for the fit
        4. odr-fit fwhm over energy

        Parameters
        ----------
        plot_spectrum : filename-str, "show" or None, optional
            Select whether to show/save the spectrum with the fitted peaks for manual checks.
            Green gausses are used, the red one has been identified as the 511 keV peak (which will is excluded in the calculations).
            if str: relative/absolute save-filename without file-ending ([detname].png will be added, one file for each detector)
            if "show" then plot will be shown instead of being saved.
            if None, no plotting.
        plot_fit : str, optional
            Same as plot_spectrum, but for interpolation fit.
        fit_function : str
            determines the function type to be fitted to the fwhm over energy data.
            "sqrt" or "poly" allowed.
            - "poly" --> a polynomial of order poly_fit_order
            - "sqrt" --> y = sqrt(a² * x) + b
        poly_fit_order : int, default=1
            if fit_function is "poly", this sets the order of the polynomial used.
        return_foot_separation_mean : bool, default=False
            if true, in addition to the resolution function, the mean of the "foot separations" of all peaks is
            returned. The foot separation is the height difference between the left and the right baseline of a
            Gaussian function fit with an added error function (see class GaussFit).

        Returns
        -------
        function:
            The energy resolution function fwhm(E) which returns (fwhm, fwhm_stdev) interpolated for the energy E.
            So, to get the FWHM at 511 keV, do:
            fwhm, fwhm_stdev = spectrum_object.detector_resolution(...)(M_POSITRON_keV)
        float:
            *optional*: if return_foot_separation_mean, then in addition this value is returned, too.
        """

        print(f"INFO: energy resolution for detector {self.detname}")

        if self.ecal is None:
            raise Exception("ERROR: No calibration found.")
        if list(self.ecal) == [0, 1, 0]:
            # the "standard" calibration if no real calibration is present.
            # TODO remove this if statement if at some point "missing calibration" means self.ecal == None.
            #  this should be (if so) implemented STACS-wide for consistency.
            raise Exception("ERROR: calibration [0, 1, 0] found, which probably means that no actual calibration is "
                            "present. Please provide a calibration.")

        from numpy.polynomial.polynomial import polyval
        # fit gauss functions onto peaks
        gausses = SingleSpectrum.fit_calib_peaks_with_gauss(self.spectrum,
                                                      min_energy=polyval(0, self.ecal),
                                                      max_energy=polyval(len(self.spectrum), self.ecal),
                                                      erf_background=True)

        footseps = [g.background_step / g.amplitude for g in gausses]

        # extract energies and widths from gausses (unit is channels, not keV, calibration will be applied afterwards)
        E_ch, E_ch_stdev = [], []
        fwhm_ch, fwhm_ch_stdev = [], []
        for g in gausses:
            E_ch.append(g.center)
            E_ch_stdev.append(g.center_stdev)
            fwhm_ch.append(g.fwhm)
            fwhm_ch_stdev.append(g.fwhm_stdev)

        if not len(E_ch):
            print("ERROR: Couldn't find peaks for calibration. Aborting.")
            return lambda x: (0, 0)

        # functions for applying calibration and fitting
        # polyval imported earlier as it is needed for min_/max_energy in fit_peaks_with_gauss

        def polyval_err(x, xe, b, be):
            """
            Calculates error of 2nd order polynomial evaluation y = b[0] + b[1] * x + b[2] * x**2
            with error propagation.

            Parameters
            ----------
            x
                x-value to evaluate at
            xe
                x-value error
            b
                polynomial coeffs as list
            be
                polynomial coeff errors
            Returns
            -------
            y_error for y = b[0] + b[1] * x + b[2] * x**2
            """
            if len(b) == 2 and len(be) == 2:
                # got linear (np array), expand to 2nd order for calculations
                b = list(b)
                b.append(0)
                be = list(be)
                be.append(0)
            return np.sqrt(
                be[0] ** 2 + (x * be[1]) ** 2 + (x ** 2 * be[2]) ** 2 + ((b[1] + 2 * x * b[2]) * xe) ** 2)

        def sqrtval(x, b):
            return np.sqrt(b[0] ** 2 * x) + b[1]

        def sqrtval_err(x, xe, b, be):
            return np.sqrt(
                x * be[0] ** 2
                + b[0] ** 2 / 4 / x * xe ** 2
                + be[1] ** 2
            )

        def apply_calibration(vals, vals_err, calib_poly, calib_poly_err):
            """
            Evaluates the given calibration function (a polynomial, see numpy.polynomial.polynomial) at values given
            in a list. Applies error propagation for std-devs given in the _err lists.
            """
            vals_new = [polyval(x, calib_poly) for x in vals]
            vals_err_new = [polyval_err(x, xerr, calib_poly, calib_poly_err) for x, xerr in zip(vals, vals_err)]
            return vals_new, vals_err_new

        # apply calibration to get energies from channels
        E_keV, E_keV_stdev = apply_calibration(E_ch, E_ch_stdev, self.ecal, self.ecal_stdev)
        fwhm_rel_errs = [st / val for st, val in zip(fwhm_ch_stdev, fwhm_ch)]
        fwhm_keV = []
        for e, e_stdev, fw, fw_stdev in zip(E_ch, E_ch_stdev, fwhm_ch, fwhm_ch_stdev):
            # where to evaluate
            e_eval_1_ch = e - fw / 2
            e_eval_2_ch = e + fw / 2
            # evaluation with error propagation, done right
            e_eval_1_keV = polyval(e_eval_1_ch, self.ecal)
            e_eval_2_keV = polyval(e_eval_2_ch, self.ecal)
            fwhm_keV_val = e_eval_2_keV - e_eval_1_keV

            fwhm_keV.append(fwhm_keV_val)
        # calc errors from relative errors, don't evaluate scaling errors here!!!
        fwhm_keV_stdev = [rel * val for rel, val in zip(fwhm_rel_errs, fwhm_keV)]

        # check if 511 keV peak is present and remove it before fit
        gauss511 = None  # variable to remember the 511 keV gauss (if found) for marking in the plot later
        for e, fw in zip(E_keV, fwhm_keV):
            if e - fw / 2 < M_POSITRON_keV < e + fw / 2:
                print("WARNING: 511 keV peak found in measurement. Removing that peak before fit.")
                # remember the corresponding gauss object for later
                # values are still in the same order as gausses
                e_index = E_keV.index(e)
                gauss511 = gausses[e_index]
                # exclude values from fit
                del (E_keV[e_index])
                del (E_keV_stdev[e_index])
                del (fwhm_keV[e_index])
                del (fwhm_keV_stdev[e_index])
                break  # since there is only one 511 peak

        print(f"INFO: using {len(E_keV)} peaks for eres calculation.")

        # fit fwhm data
        xvals_plot = np.linspace(min(E_keV), max(E_keV), 200)  # for plotting only
        if len(fwhm_keV) == 1:
            # only one peak, return constant function
            print("WARNING: only one peak for resolution, returning a constant function!")
            calibfunction = lambda x, fw=fwhm_keV[0], fwstdev=fwhm_keV_stdev[0]: (fw, fwstdev)
            yvals_plot = [fwhm_keV[0]] * len(xvals_plot)
            fit_plot_label = "const"
        else:
            if fit_function == "poly":
                fitparams, fitparam_errs = \
                    SingleSpectrum.odr_polyfit(E_keV, fwhm_keV, poly_fit_order, E_keV_stdev, fwhm_keV_stdev)
                calibfunction = lambda x, fitparams=fitparams, fitparam_errs=fitparam_errs: \
                    (polyval(x, fitparams), polyval_err(x, 0, fitparams, fitparam_errs))
                yvals_plot = polyval(xvals_plot, fitparams)
                fit_plot_label = f"{poly_fit_order}. order poly"
            elif fit_function == "sqrt":
                fitparams, fitparam_errs = \
                    SingleSpectrum.odr_fit(lambda b, x: sqrtval(x, b),  # ugly, sry... odr wants params in this order
                                     E_keV, fwhm_keV, E_keV_stdev, fwhm_keV_stdev, beta0=[1.] * 2)
                calibfunction = lambda x, fitparams=fitparams, fitparam_errs=fitparam_errs: \
                    (sqrtval(x, fitparams), sqrtval_err(x, 0, fitparams, fitparam_errs))
                yvals_plot = sqrtval(xvals_plot, fitparams)
                fit_plot_label = "sqrt(a²x) + b"
            else:
                raise Exception(f"Fit function '{fit_function}' not supported, only 'sqrt' and 'poly'.")

        # plots
        import matplotlib.pyplot as plt
        if plot_fit:
            f = plt.figure()  # TODO switch to object-orientated api
            plt.errorbar(E_keV, fwhm_keV, xerr=E_keV_stdev, yerr=fwhm_keV_stdev, ls="", marker="o")
            plt.plot(xvals_plot, yvals_plot, label=fit_plot_label)
            plt.xlabel("E / keV")
            plt.ylabel("fwhm / keV")
            plt.legend()
            plt.title(f"Energy resolution interpolation: {self.detname}")
            plt.tight_layout()
            if plot_fit == "show":
                plt.show()
            else:
                saveloc = f"{plot_fit}-res-fit-{self.detname}.png"
                plt.gcf().savefig(saveloc)
                print("INFO: saving resolution fit plot at", saveloc)
            plt.close(f)
        if plot_spectrum:
            f = plt.figure()  # TODO switch to object-orientated api
            plt.plot(self.spectrum)
            plt.xlabel("channel number")
            plt.ylabel("counts")
            plt.title(f"peaks for resolution calculation: {self.detname}")
            for g in gausses:
                gausscol = "green"
                if g == gauss511:
                    gausscol = "red"
                    plt.text(g.center, g.y_offset + g.amplitude, "511 keV WILL BE EXCLUDED", rotation="vertical")
                g.plot(color=gausscol)
            plt.tight_layout()
            if plot_spectrum == "show":
                plt.show()
            else:
                saveloc = f"{plot_spectrum}-res-spectrum-{self.detname}.png"
                plt.gcf().savefig(saveloc)
                print("INFO: saving resolution spectrum plot at", saveloc)
            plt.close(f)

        if return_foot_separation_mean:
            return calibfunction, np.mean(footseps)
        return calibfunction

    def calibrate(self, source_names, initial_guess_sources=None, use_511_for_initial_guess=False,
                  fit_order=1, plot_spectrum=None, plot_fits=None,
                  save_despite_warnings=False, min_energy=0, max_energy=1400,
                  exception_on_warning=True):
        """
        Perform a calibration for this spectrum.

        The spectrum must feature peaks of known energy, which are specified via the source database managed by
         `calib_source_manager.py`.

        1. fit_single_peaks(): search for peaks in self.spectrum and fit Gaussian functions to them.
        2. match_calibration(): Find as many gauss peaks belonging to literature peak positions as possible.
        3. odr_gauss_polyfit(): Fit a linear or parabolic interpolation function to the calibration.
        4. Do some plots for user confirmation if needed.
        5. The results are stored in self.ecal if no errors occur.

        Refer to the documentation of the individual functions for further details.

        Parameters
        ----------
        source_names: str or list of str
            List of radiation source names which are passed to calib_source_manager.py to get literature data
            for the source used, e.g. ["Eu152", "Cs137"].
            If there is only one source, a simple string with its name (e.g. "Eu152") is also accepted.
        initial_guess_sources: None or str or list of str
            List of source identifiers passed to match_calibration as initial guess sources. If not given, source_names
            is used instead. See match_calibration() for further details.
            All values given here have to be included in source_names as well.
            If the 511 keV line is one of the peaks with the highest amplitude (e.g. when calibrating with Na22),
            see parameter use_511_for_initial_guess to specify it.
        use_511_for_initial_guess: bool
            Is passed to match_calibration(). (Documented there.)
        fit_order: int, 1 or 2
            Sets the order of the polynomial used to fit the calibration for interpolation
        plot_spectrum: filename-str, "show" or None
            Decides whether to show/save the spectrum with the fitted peaks and matched energies for manual checks.
            If str: save-path without file-ending ([det-name].png will be added, one file for each detector)
            If "show" then plot will be shown instead of saved
            If None, no plotting
        plot_fits
            Same as plot_spectrum, but for fit
        save_despite_warnings: bool
            If a warning occurred, the calibration will usually not be saved to the SingleSpectrum object.
            Force to save it anyway by setting this to True. If this is True, exception_on_warning will have no effect.
        min_energy: float, dict
            The estimated energy at data[0] in keV
        max_energy: float, dict
            The estimated energy at highest channel data[-1] in keV.
            Default (1,4 MeV) is estimated based on sample calibration measurements.
            min_energy and max_energy are passed to fit_peaks_with_gauss(), see there for more info
        exception_on_warning: bool
            Whether to raise an exception if a warning occurs. Otherwise, the "default" calibration [0,1,0] is saved.

        Returns
        -------
        None
            If no errors occur, the results are stored in self.ecal.
        """
        if type(source_names) is str: # check whether only one source is given
            source_names = [source_names]
        if initial_guess_sources:
            if type(initial_guess_sources) is str:
                initial_guess_sources = [initial_guess_sources]
            for f in initial_guess_sources:
                if f not in source_names:
                    raise Exception("All sources in initial_guess_sources must be in source_names!")
        else:
            initial_guess_sources = source_names

        warning = None

        print(f"INFO: calibration for detector {self.detname}")

        gausses = SingleSpectrum.fit_calib_peaks_with_gauss(self.spectrum, min_energy=min_energy, max_energy=max_energy)

        # read probe_info-file
        from . import calib_source_manager as CSM
        literature_dict = {}
        for source in source_names:
            literature_dict[source] = CSM.get_radsource_info(source)

        from scipy.odr.odrpack import OdrError
        gausses_match, literature_match = None, None
        calib_linear, calib_linear_sigma = None, None
        calib_quadr, calib_quadr_sigma = None, None # these variables **must** only be read if do_parabolic_fit is True!
        do_parabolic_fit = True
        try:
            gausses_match, literature_match = \
                SingleSpectrum.match_calibration(gausses, literature_dict, initial_guess_sources,
                                                 initial_guess_with_511keV=use_511_for_initial_guess)
            if len(gausses_match) == 2:
                # only two peaks used, parabolic fit not possible.
                print("INFO: parabolic fit disabled, using only two peaks")
                do_parabolic_fit = False
            calib_linear, calib_linear_sigma = SingleSpectrum.odr_gauss_polyfit(gausses_match, literature_match, 1)
            if do_parabolic_fit:
                calib_quadr, calib_quadr_sigma = SingleSpectrum.odr_gauss_polyfit(gausses_match, literature_match, 2)

            if M_POSITRON_keV in literature_match:
                print("INFO: 511 keV peak found")

            if do_parabolic_fit:
                # Some checks to avoid common problems. Only empirical, double-check manually!
                if max(calib_quadr[1], calib_linear[1]) / min(calib_quadr[1], calib_linear[1]) > 1.05:
                    warning = "Linear and parabolic fit differ quite a bit."
                    print(f"WARNING: {warning}")
                if calib_quadr[2] / calib_quadr[1] > 1e-6:
                    warning = "Fit is quite parabolic."
                    print(f"WARNING: {warning}")
                # this check only makes sense if we have more than two peaks (beacuse we need at least two in the initial guess)
                if len(gausses_match) == sum(
                        [len(literature_dict[source]["initial_guess_peaks"]) for source in literature_dict]):
                    warning = "No more peak matches found than those in the initial guess! This may be a bad calibration."
                    print(f"WARNING: {warning}")
        except OdrError as e:
            warning = f"ODR ERROR: {e}"
            print(f"ODR ERROR: {e}")
        except Exception as e:
            warning = f"ERROR: {e}"
            print(f"ERROR: {e}")

        if (not warning) or save_despite_warnings:
            # save results to object
            if fit_order == 2 and do_parabolic_fit:
                self.ecal = calib_quadr
                self.ecal_stdev = calib_quadr_sigma
            elif fit_order == 1:
                self.ecal = np.concatenate((calib_linear, [0]))
                self.ecal_stdev = np.concatenate((calib_linear_sigma, [0]))
        else:
            print("WARNING: rejecting calibration due to warnings")
            if exception_on_warning:
                raise CalibrationError(f"Calibration warning: {warning}")

            # To make the fits work even when the calibration failed, let's set it to dummy values
            print("Using dummy values for calibration")
            calib_linear, calib_linear_sigma = [0, 1], [0, 0]
            calib_quadr, calib_quadr_sigma = [0, 1, 0], [0, 0, 0]

        # plots
        import matplotlib.pyplot as plt
        if plot_fits:
            if not gausses_match:
                print("INFO: as an error occurred, there is no fit to show")
            else:
                f = plt.figure()  # TODO switch to object-orientated api
                gauss_centers = [g.center for g in gausses_match]
                xs = np.linspace(.95 * min(gauss_centers), 1.05 * max(gauss_centers), 100)
                plt.plot(xs, np.polynomial.polynomial.polyval(xs, calib_linear),
                         label="linear fit", c="black", lw=4, ls=":")
                if do_parabolic_fit:
                    plt.plot(xs, np.polynomial.polynomial.polyval(xs, calib_quadr),
                            label="quadratic fit", c="red")
                plt.scatter(gauss_centers, literature_match, zorder=3)
                # no errorbars as gauss_centers and literature values are very sharp
                plt.xlabel("channel number of peaks")
                plt.ylabel("matched energy / keV")
                plt.title(f"calibration fits of {self.detname}")
                plt.tight_layout()
                plt.legend()
                if plot_fits == "show":
                    plt.show()
                else:
                    saveloc = f"{plot_fits}-calib-fit-{self.detname}.png"
                    plt.gcf().savefig(saveloc)
                    print("INFO: saving calibration fits at", saveloc)
                plt.close(f)

        if plot_spectrum:
            peaks = []
            for source in literature_dict:
                peaks += literature_dict[source]["peaks"]

            # invert literature values to show them
            literature_channels = [(v[0] - calib_linear[0]) / calib_linear[1]
                                   for v in peaks]

            f = plt.figure()  # TODO switch to object-orientated api
            plt.plot(range(1, len(self.spectrum) - 1), self.spectrum[1:-1])  # cut off ends because of overflowing bins
            plt.xlabel("channel number")
            plt.ylabel("counts")
            plt.title(f"calibration of {self.detname}")
            plt.plot(self.spectrum[1:-1])

            if gausses_match:  # only if no matching-error occurred
                # plot all known literature values for reference
                for c in literature_channels:
                    if 0 < c < len(self.spectrum):
                        plt.axvline(c, c="gray", ls="--", lw=.5)
                # plot 511 keV line
                e511ch = (M_POSITRON_keV - calib_linear[0]) / calib_linear[1]
                plt.axvline(e511ch, c="gray", ls="-", lw=.5)

            for g in gausses:
                gausscol = "gray"
                if gausses_match and g in gausses_match:
                    gausscol = "red"
                    lit = literature_match[gausses_match.index(g)]
                    if lit == M_POSITRON_keV:
                        gausscol = "green"
                    plt.text(g.center, g.y_offset + g.amplitude, f"{lit:.1f} keV", rotation="vertical")
                g.plot(color=gausscol)
                # write energy to peak

            # plot noise level debug
            # noise_level = Spectrum.noise_level(data) # re-calculates noise level... not that nice
            # plt.plot(noise_level, c="orange")

            plt.tight_layout()
            if plot_spectrum == "show":
                plt.show()
            else:
                saveloc = f"{plot_spectrum}-calib-spectrum-{self.detname}.png"
                plt.gcf().savefig(saveloc)
                print("INFO: saving calibration spectrum at", saveloc)
            plt.close(f)

    # TODO re-arrange inline imports in following code?
    # static methods required by calibrate() and calculate_eres()

    def autoadjust_ecal(self):
        """Auto adjust the offset of the energy calibration of an spectrum
        with unacceplatbly high deviation of the peak center."""

        raise NotImplementedError("Energy calibration auto-estimation.")

    @staticmethod
    def match_calibration(gausses, literature_dict, initial_guess_sources, initial_guess_with_511keV=False,
                          max_energy_dist_keV=3, fwhm_factor=1):
        """
        Tries to match the peaks observed in a calibration spectrum with corresponding energy values from literature.

        Gets a list with GaussFit objects and a dict with literature information about the material which the gausses
        (should) come from. Tries to find pairs of gausses and corresponding energy values:

        1. First, an "initial guess" is performed. We expect that the energies of the n highest peaks in the spectrum
        are known. (The choice of which peaks to include in this estimation is controlled by the parameters initial_guess_sources and
           initial_guess_with_511keV). The GaussFits (from parameter gausses) are sorted
           by their amplitude and the n ones with the highest amplitudes are mapped to the energies of
           literature_dict["initial_guess_peaks"] (sorted by channel number or energy respecively). This allows us to
           calculate an estimated calibration that will be further improved using the other gaussian fits.
        2. Now for all remaining peaks (GaussFit objects), the calibration obtained by the initial guess is applied.
           This gives us an energy value where we expect this peak to be at. We then search for the literature
           value closest to this energy. The GaussFit and the literature value are considered a match if:
            - the literature value is in the region of width fwhm * fwhm_factor around peak center
            - abs([GaussFit.center in keV, calculated with 1st guess] - [closest literature value])
              < max_energy_dist_keV

        The mapping resulting from those two steps is returned.

        TODO:
        - multiple sources: find a way to avoid false-positive matches due to too dense literature lines

        Parameters
        ----------
        gausses: list of GaussFit objects
        literature_dict: dict of shape {
            "Sourcename": {
            "name": str
            "peaks": [[Energy 0, Intensity 0], [E1, I1], [E2, I2], ...] (sorted by intensity!)
            "initial_guess_peaks": [Energy A, Energy B, ...]
            }, ...
        }
        The single sub-dicts are as obtained by calib_source_manager.py
        initial_guess_sources: list of str
            List of source names (that have to exist in literature_dict) to get the initial_guess_peaks from.
            If multiple are given, the literature_dict[source name]["initial_guess_peaks"] are concatenated.
            A more precise selection of the peaks for the initial guess is currently not supported
            (except toggling the 511 keV line with initial_guess_with_511keV).
            The contents of literature_dict[source name]["initial_guess_peaks"] are documented in
            calib_source_manager.py -> import_csv -> parameter initial_guess_peaks.
        fwhm_factor: float
            fwhm_factor*gauss.fwhm is the width of the interval around the peak center where literature values have
            to be in to be accepted as a match (1st condition)
        initial_guess_with_511keV: bool
            Chose whether to add the 511 keV line to the energies used for the initial guess.
        max_energy_dist_keV: float
            The maximum difference of GaussFit.center (in keV) and the nearest literature value for that the pair
            is accepted, in keV (2nd condition)

        Returns
        -------
        list:
            list of GaussFit objects for which a matched literature value has been found
        list:
            list of the matched literature energy values (floats) for the corresponding GaussFit objects
            (at the same index in the firstly returned list)
        """
        from numpy.polynomial.polynomial import polyval

        literature_peaks = []
        for source in literature_dict:
            literature_peaks += list(
                np.array(literature_dict[source]["peaks"])[:, 0])  # slice away intensity data, is not used

        # sort gausses by descending amplitude
        gausses = sorted(gausses, key=lambda g: -g.amplitude)

        # do initial guess with only few maxima
        ORDER_1st_GUESS = 1
        guess_literat = []
        for source in literature_dict:
            if source in initial_guess_sources:
                guess_literat += literature_dict[source]["initial_guess_peaks"]
        if initial_guess_with_511keV:
            guess_literat.append(M_POSITRON_keV)
        guess_literat = sorted(guess_literat)

        if len(gausses) < len(guess_literat):
            raise Exception("not enough peaks found to do initial guess.")
        guess_gausses = sorted(gausses[:len(guess_literat)], key=lambda g: g.center)  # sort by channel (~energy)

        guess_params, guess_sigma = SingleSpectrum.odr_gauss_polyfit(guess_gausses, guess_literat, ORDER_1st_GUESS)

        def find_closest(arr, val):
            # return value in arr which is closest to val
            d = float("inf")  # a big number
            best = None
            for v in arr:
                if abs(v - val) < d:
                    d = abs(v - val)
                    best = v
            return best

        # gausses used for final fit, start with the one we already fitted and later append more
        good_gausses = list(guess_gausses)
        good_literature = list(guess_literat)

        # remove the gausses we already matched
        gausses = gausses[len(guess_literat):]

        # add 511 keV-Peak to be able to find it
        literature_peaks.append(M_POSITRON_keV)

        for g in gausses:
            # get guessed energy
            guess_energy = polyval(g.center, guess_params)
            # and the width of a g.fwhm wide interval around g.center in keV
            guess_fwhm_interval_width = 2 * abs(
                polyval(g.center - fwhm_factor * g.fwhm / 2, guess_params) - guess_energy)

            closest_literature_value = find_closest(literature_peaks, guess_energy)
            if abs(guess_energy - closest_literature_value) < max_energy_dist_keV \
                    and abs(guess_energy - closest_literature_value) < fwhm_factor / 2 * guess_fwhm_interval_width:
                # peak accepted, is in the region of width fwhm * fwhm_factor around peak center
                # and less than max_energy_dist_keV away from literature value
                good_gausses.append(g)
                good_literature.append(closest_literature_value)

        return good_gausses, good_literature  # return matched peaks for plotting

    @staticmethod
    def noise_level(data, crop_up_to=.15):
        """
        Calculates the estimated amplitude of the noise of the signal given in data.
        1. apply fft to get the spectrum
        2. crop away the low frequency part
        3. apply reverse fft
        4. calculate a running mean of the absolute values of the result (without absolute, the mean would be ~zero)

        Magic numbers in argument-defaults should be fine, they should work with most measurements.
        Parameters
        ----------
        data: list
            1d array containing the raw data
        crop_up_to: float, optional
            must be in range from 0 to 1
            To eliminate low frequencies (to only see the noise in data), the following is executed:
            spectrum[:int(crop_up_to * len(data))] = 0
            which eliminates the low-frequency part of the spectrum. crop_up_to determines the amount of frequencies
            to be set to zero. The bigger, the more low frequencies are cropped away. The default value was empirically
            determined from several lab-beam calibration measurements.
        Returns
        -------
        An numpy array (with length of data) with the estimated noise amplitude at the same location in data.
        It may be necessary to scale the output data to achieve the desired effects.
        (See noise_factor in fit_calib_peaks_with_gauss for an example.)
        """
        data = np.array(data)
        spectrum = np.fft.rfft(data)
        crop_i = int(len(data) * crop_up_to)
        spectrum[:crop_i] = 0

        noise = np.fft.irfft(spectrum)

        noise = abs(noise)
        # running mean
        from scipy.ndimage.filters import uniform_filter1d
        noise = uniform_filter1d(noise, int(len(noise) / 100))

        if len(noise) < len(data):
            # it appears that noise is shorter by (1), if len(data) is odd
            noise = np.append(noise, 0)

        return noise

    @staticmethod
    def fit_calib_peaks_with_gauss(spectrum, noise_factor=2, fit_window_scaling=6, erf_background=True,
                                   min_energy=0, max_energy=1400, exclude_tail=0):
        """
        Searches for narrow peaks in data and fits Gaussian curves onto them.

        Parameters are tuned to work especially well with narrow peaks like those from calibration sources
        (e.g. Eu-152, Cs-137).

        1. runs scipy.signal.find_peaks on a smoothed version of data to find peaks.
        2. determines the width of the peaks to select a slice from data around the peaks ("fit window")
        3. fits a GaussFit object on this data, optionally with an erf function as background
        4. Peaks have to match several criteria to be returned:
            - Their prominence (difference between baseline and amplitude, see scipy.signal.find_peaks docs) must be
              bigger than noise_factor*[noise level at this location, calculated by the function noise_level()]
            - There are (hard-coded) thresholds for minimal and maximal peak width. See below, variables
              min_.../max_gauss_width_ch and inner function keV_to_ch() for implementation details.
            - The peak must have a certain distance to the previous one. Also hardcoded, see code for details.

        Parameters
        ----------
        spectrum: list of count numbers per channel
            Spectrum to scan for peaks.
        noise_factor: float
            minimum peak prominence is noise_factor*noise_level(channel). If a peak does not have this minimum
            prominence (distance from amplitude to peak foot), it will be discarded.
        fit_window_scaling: float
            fit-window in data will have the width fit_window_scaling * peak_fwhm
            Too large values may be bad because other peaks than the intended one can
            end up in the fitting window and confuse the fitter
        erf_background: bool
            Passed to GaussFit, toggles whether to additionally fit an erf function as background
        min_energy: float, dict
            the estimated energy at data[0] in keV, see also max_energy
        max_energy: float, dict
            The estimated energy at data[-1] in keV.
            min_energy and max_energy are used to determine the thresholds for the peak width, so this should at
            least be approximately true, +- 10% are probably no big problem, if this is off, quite wide peaks
            may be rejected. (If so, see variable max_gauss_width_ch, this is the mentioned threshold.)
            min_energy and max_energy can also be given as dicts keyed with detector names for more control.
        exclude_tail: int
            Number of channels to exclude from the high energy end of the spectrum before searching/fitting.

        Returns
        -------
        A list of GaussFit objects fitted to data.
        """
        from scipy.ndimage.filters import uniform_filter1d
        from scipy.signal import find_peaks, peak_widths

        def keV_to_ch(energy):
            """
            linearly translate an energy to a channel number based on a *very* rough "calibration estimation" based on
            min_energy and max_energy, which are mapped to spectrum[0] and spectrum[-1]. Useful to give thresholds for
            peak widths in energy and not in channel numbers (which is more intuitive and the number of channels in the
            spectrum may change for future MCA boards).
            """
            return len(spectrum) / (max_energy - min_energy) * energy

        if max_energy > 3000:
            # a very wrong estimation makes most threshold values unusable, leading to unwanted behaviour.
            print("WARNING: Are you sure you maximal spectrum energy is > 3 MeV? ")

        # following magic numbers are in keV and should be acceptable because they depend on parameters in keV_to_ch
        min_gauss_width_ch = max(keV_to_ch(.5), 5) # at least some channels to avoid fitting noise with Gaussians
        max_gauss_width_ch = keV_to_ch(40) # not too many channels to avoid very broad peaks
        # values manually tuned and hardcoded. Don't set min_... too low to avoid rejecting all peaks in noisy spectra.
        # but 40 is really much for sharp spectra. If there are no very noisy ones, 15 should also do it.

        # noise level as estimation for peak prominence
        noise_level = SingleSpectrum.noise_level(spectrum)
        # running mean to smooth data
        data_filtered = uniform_filter1d(spectrum, max(5, int(keV_to_ch(2))))
        # minimum filter width: 3 channels, otherwise 2 keV
        # removing the tail of the spectrum to avoid artefact fitting
        data_filtered[-1 - exclude_tail:len(data_filtered)] = 0

        peak_positions, peak_data = find_peaks(data_filtered,
                                               prominence=noise_factor * noise_level,
                                               # noise_level alone is not high enough
                                               width=(min_gauss_width_ch, max_gauss_width_ch),
                                               # (minimum, maximum) width: empirical
                                               distance=max(5, keV_to_ch(2)))  # to avoid two fits at the same peak

        # scipy.signal.find_peaks_cwt may be an alternative for find_peaks
        # (especially for noisy data), but I don't know how to use it.

        # get widths quite low at "foot" of the peak as crop estimation for gauss
        peak_width_foot = peak_widths(data_filtered, peak_positions, rel_height=.5)[0]
        # peak_widths sometimes returns 0 for low-count, high-noise peaks
        # to be sure, set width to a magic minimum value if needed
        # as peak_width_foot is used as data window width for the gauss fit, don't set the minimum it too low
        peak_width_foot = [max(p, keV_to_ch(3)) for p in peak_width_foot]

        gausses = []
        for peak_pos, foot_width in zip(peak_positions, peak_width_foot):
            if foot_width > max_gauss_width_ch:
                # too wide
                continue
            g = GaussFit(spectrum, crop_center=peak_pos, crop_width=fit_window_scaling * foot_width,
                         erf_background=erf_background)
            # now check if this fit is as expected
            if abs(g.center - peak_pos) > g.fwhm:
                # fit went to a wrong peak!
                continue
            if g.error or g.amplitude < 0 or g.fwhm > max_gauss_width_ch or g.y_offset < 0:
                # gauss somehow malformed
                continue
            gausses.append(g)

        if not gausses:  # empty list
            return []
        # as max_gauss_width is quite tolerant, now throw out peaks which are much wider than the average.
        avg_fwhm = sum(g.fwhm for g in gausses) / len(gausses)
        return [g for g in gausses if g.fwhm < 3 * avg_fwhm]  # magic value

    #
    # some helper functions for calibration and e-resolution

    @staticmethod
    def odr_gauss_polyfit(gausses, literature, order):
        """
        wrapper for odr_polyfit to be called with GaussFit objects as x-data and a list of
        literature values (peak energy) as y data

        used for energy calibration, where peaks in detector channels are matched to energies.
        gets stdevs from gauss objects and treats literature values as precise.
        fits a polynomial of order and returns the output of odr_polyfit.
        (which is: tuple: (coeffs, coeff_stdevs) where coeffs, coeff_stdevs are lists with polynomial values )

        Parameters
        -----------
        gausses: list of GaussFit
            gauss objects whose gauss_fit.center values serve as x values
        literature: list of float
            energy values (not [energy, intensity]!!) as y data, it must be len(literature) == len(gausses)
        order: int
            order of the polynomial to fit

        Returns
        ---------
        the result of odr_polyfit
        """
        xdata, xsigma = [], []
        for g in gausses:
            xdata.append(g.center)
            xsigma.append(g.center_stdev)

        return SingleSpectrum.odr_polyfit(xdata, literature, order, xsigma=xsigma)

    @staticmethod
    def odr_polyfit(xdata, ydata, order, xsigma=None, ysigma=None):
        """
        do a ODR-fit with a polynomial of (param) order to given data.
        Parameters
        ----------
        order: int
            order of the polynomial to fit
        xdata: list
            x data
        ydata: list
            y data
        xsigma: list
            standard deviation of xdata
        ysigma: list
            standard deviation of ydata

        Returns
        -------
        tuple: (coeffs, coeff_stdevs) where coeffs, coeff_stdevs are lists with polynomial values
        calculate fit function via np.polynomial.polynomial.polyval(x, coeffs)
        order of polynomial coefficients:
        y = B[0] + B[1] * x + B[2] * x**2 + ...
        """
        import scipy.odr as odr

        # fitting function, depending on poly order
        def f(B, x):
            out = 0
            for o in range(order + 1):
                out += B[o] * x ** o
            return out

        return SingleSpectrum.odr_fit(f, xdata, ydata, xsigma, ysigma, [1.] * (order + 1))

    @staticmethod
    def odr_fit(f, xdata, ydata, xsigma=None, ysigma=None, beta0=None):
        """
        ODR fit wrapper
        Parameters
        ----------
        f: function
            fit function: f(B, x) with B a list of parameters
        xdata: list
            x data
        ydata: list
            y data
        xsigma: list
            standard deviation of xdata
        ysigma: list
            standard deviation of ydata
        beta0: list
            1st guess parameters

        Returns
        -------
        tuple: (B_opt, B_stdevs) where B_opt, B_stdevs are lists with optimized parameters and their stdevs
        """
        import scipy.odr as odr
        model = odr.Model(f)
        data = odr.RealData(x=xdata, y=ydata, sx=xsigma, sy=ysigma)
        fit = odr.ODR(data, model, beta0=beta0).run()
        return fit.beta, fit.sd_beta


class CalibrationError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


# Gaussian function fit object required by calibrate() and calculate_eres()
from scipy.special import erf as erf_scipy
class GaussFit:
    """
    An object that represents a fitted Gaussian function on data passed to its constructor.

    The function fitting is performed in the constructor itself. After that, the results of the
    fitting result can be queried via object attributes like amplitude, center, ...

    If *both* center_guess and crop_width are given, data will be
    cropped first like data[center_guess-crop_width/2:center_guess+crop_width/2]
    Otherwise data is expected to be properly cropped and fittable!
    Uses sqrt(y) as y-error estimation (Poisson-statistics).

    Parameters
    ----------
    data_y: list
        y data to fit the Gaussian to (may be cropped if crop_center and crop_width are specified)
    data_x: list, optional
        x data, if None, range(len(data)) will be used
    crop_center: int, optional
        estimated center of the peak
    crop_width: int, optional
        width to crop data around given center
    erf_background: bool
        sets whether to use a erf function as background. If so, the object will have the following
        additional attibutes: erf_yscaling, erf_yscaling_stdev, background_step
        (background_step is the y-difference between the two background levels)

    Attributes
    ----------
    error: bool
        is True if a fitting error occurred.
    amplitude: float
    center: float
        the x-position of the amplitude
    y_offset: float
        the vertical offset of the fitted function
    sigma: float
        the Gaussian functions standard deviation
    background_step: float
        the y difference of the two "feet" of the Gaussian + error function.
    fwhm: float
        the full width at half maximum of the Gaussian function.
    """

    # fit function
    @staticmethod
    def gauss_offset(x, amplitude, center, y_offset, sigma):
        return amplitude * np.exp(-0.5 * ((x - center) / sigma) ** 2) + y_offset

    @staticmethod
    def gauss_erf(x, amplitude, center, y_offset, sigma, erf_yscaling):
        # using sigma as erf_xscaling
        # https://en.wikipedia.org/wiki/Error_function#Applications
        return GaussFit.gauss_offset(x, amplitude, center, y_offset, sigma) \
               + erf_yscaling * erf_scipy((x - center) / sigma / np.sqrt(2))

    def __str__(self):
        return f"GaussFit, center={self.center}, ampl={self.amplitude}"

    def __repr__(self):
        return self.__str__()

    def __init__(self, data_y, data_x=None, crop_center=None, crop_width=None, erf_background=False):
        from scipy.optimize import curve_fit

        self.has_erf_background = erf_background
        self.fit_function = GaussFit.gauss_erf if erf_background else GaussFit.gauss_offset

        if not data_x:
            data_x = range(len(data_y))

        # crop data if necessary
        if crop_center is None and crop_width is not None or crop_center is not None and crop_width is None:
            # only one of xxxx_guess args given
            raise Exception("both crop_center and crop_width or none of them must be given")
        if crop_center is not None and crop_width is not None:
            data_x = data_x[max(0, crop_center - int(crop_width / 2))
                            : min(len(data_x) - 1, crop_center + int(crop_width / 2))]
            data_y = data_y[max(0, crop_center - int(crop_width / 2))
                            : min(len(data_y) - 1, crop_center + int(crop_width / 2))]
            self.crop_window = (crop_center - int(crop_width / 2), crop_center + int(crop_width / 2))  # for plotting
        else:
            self.crop_window = (0, len(data_x))

        # find start parameters for curve_fit
        ampl_guess = data_y[int(len(data_y) / 2)] - data_y[0]
        y_offs_guess = data_y[0]  # some value at the side of the peak
        sigma_guess = 0.1 * len(data_x)  # magic number, empirically by plotting
        guess = [ampl_guess, crop_center, y_offs_guess, sigma_guess]
        if erf_background:
            erf_yscaling_guess = 0
            guess.append(erf_yscaling_guess)

        # plot fit window with guess fit function, may be useful for class reuse
        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots()
        # ax.plot(data_x, data_y)
        # ax.plot(data_x, self.fit_function(data_x, *guess))
        # plt.show()

        # y-error estimation via poisson statistics (inherited from previous implementation)
        y_errs = np.sqrt(data_y)
        y_errs = np.where(y_errs == 0, 1, y_errs)  # correction needed, otherwise division by zero

        # fit
        try:
            self.opt, self.pcov = curve_fit(self.fit_function, data_x, data_y, p0=guess, sigma=y_errs,
                                            absolute_sigma=True)
            self.error = False
        except (Exception, Warning):
            self.error = True
            self.opt, self.pcov = [0] * len(guess), [0] * len(guess)
        try:
            self.perr = np.sqrt(np.diag(self.pcov))
        except RuntimeWarning:  # TODO stop annoying output from those warnings and the OptimizeWarning!
            print(self.pcov)
        # object attributes
        if not erf_background:
            self.amplitude, self.center, self.y_offset, self.sigma = self.opt
            self.amplitude_stdev, self.center_stdev, self.y_offset_stdev, self.sigma_stdev = self.perr
        else:
            self.amplitude, self.center, self.y_offset, self.sigma, self.erf_yscaling = self.opt
            self.amplitude_stdev, self.center_stdev, self.y_offset_stdev, self.sigma_stdev, \
            self.erf_yscaling_stdev = self.perr
            self.background_step = 2 * abs(self.erf_yscaling)  # the difference between the two background levels

        # fwhm (https://en.wikipedia.org/wiki/Full_width_at_half_maximum)
        a = 2 * np.sqrt(2 * np.log(2))
        self.fwhm = abs(self.sigma * a)  # sometimes negative, bad for comparisons later
        self.fwhm_stdev = self.sigma_stdev * a

        """
        # DEBUG plot fit section
        plt.figure()
        plt.plot(data_x, data_y)
        self.plot()
        plt.show()
        """

    def plot(self, axes=None, color="red"):
        """
        plots the represented Gauss into the current plot, which should be set up already.
        """
        xvals = np.linspace(*self.crop_window, 100)
        import matplotlib.pyplot as plt
        if not axes:
            axes = plt.gca()
        axes.plot(xvals, self.fit_function(xvals, *self.opt), c=color)
