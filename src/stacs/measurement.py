import os
import warnings
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import math as m
import matplotlib.colors
import matplotlib.cm as cmx
import matplotlib.axes
import scipy
from . import importer

class MeasurementCampaign:
    """
    A complete measurement campaign with multiple Doppler measurements.

    ...

    Parameters
    ----------
    show : bool
        Show info/debug plots for each data processing step. Multiple
        pyplot windows will pop up.
    verbose : bool
        Print (debug) info while processing.

    """

    def __init__(self, directory=None, measurements=None, show=False,
                 verbose=False, **kwargs):

        self.show = show
        self.verbose = verbose

        supported_filetypes = ("n42", "mpa", "txt")

        self.measurements = []
        self.depthprofiles = []

        if measurements:
            self.measurements = list(measurements)
        elif isinstance(directory, str):
            if verbose: print(f"Single directory given: {directory}")
            files = sorted([os.path.join(directory, f)
                            for f in os.listdir(directory)
                            if any([f.endswith(ftype)
                                    for ftype in supported_filetypes])])

            self.measurements = [DopplerMeasurement(f,
                                                    show=self.show,
                                                    verbose=self.verbose,
                                                    **kwargs)
                                                        for f in files]
        elif isinstance(directory, list):
            if verbose: print("Importing data from multiple locations.")
            Nmeas = 0
            for subdir in directory:
                if verbose: print(f"Importing data from: {subdir}")
                files = sorted([os.path.join(subdir, f)
                                for f in os.listdir(subdir)
                                if any([f.endswith(ftype)
                                        for ftype in supported_filetypes])])

                self.measurements += [DopplerMeasurement(f,
                                                        show=self.show,
                                                        verbose=self.verbose,
                                                        **kwargs)
                                                        for f in files]
                self.depthprofiles += [[Nmeas, Nmeas + len(files) - 1]]
                Nmeas += len(files)
                if verbose: print(f"Imported: {len(files)} files")

        else:
            print(f"Input: {directory} and {measurements} are both invalid")
            # what should we do here?
            pass

    def __getitem__(self, item):
        for measurement in self.measurements:
            if item == measurement.name:
                return measurement

    def depth_profiles(self, parameters=["S","W"], plot_sw=True, detectors=None,
                       skip_detectors=None,
                       average=False, ave_equalpoints=False,
                       xy_keys=None, coords=None, verbose=None, show=None,
                       plotstyle="dark_background", labeling=None,
                       save=False, savename="stacs_out_single.svg"):
        """
        Extract the depth profiles from a measurement campaign.

        ...

        Parameters
        ----------
        coords : list of tuple, list of list of tuple
            List of measured coordinates to choose from. Only necessary if
            coordinates for the same spot differ slightly. List of lists for
            multiple folders.

        """
        # plotting parameters
        plt.style.use(plotstyle)

        # default show and verbose options are the class options
        if show is None: show = self.show
        if verbose is None: verbose = self.verbose

        if any([s.filetype!="n42" for s in self.measurements]):
            err = ("Depth profile generation is currently only supported for "
                   ".n42 files, which contain the necessary metadata. To "
                   "analyse, e.g., .mpa files, you can use the `DataSurgeon` "
                   "to create .n42 files (given that you logged the metadata "
                   "separately).")
            raise NotImplementedError(err)
        if type(detectors) == str:
            detectors_given = True
            detectors = [detectors]
        elif detectors is None:
            detectors_given = False
            msg = ("No detector identifiers have been provided. Will "
                    "determine these from the data files.")
            if verbose: print(msg)
        if xy_keys:
            # x/y_key are the names of the lateral dimension parameters
            # if not specified, all points will be in one depth profile
            x_key, y_key = xy_keys

            if coords:
                # correct small coordinate shifts
                pass

            # find unique coordinate points (and file folders)
            # for each of which a depth profile will be generated
            pass

        # sort measurements by implantation energy
        e = [m.implantation_energy for m in self.measurements]
        if len(self.depthprofiles) < 2:
            if verbose: print(f"Only one depth profile detected:")
            self.depthprofiles = [[0, len(self.measurements)- 1]]
        else:
            pass

        profiles = []
        profile_errors = []
        profile_detectors = []
        unique_e_lists = []
        for subdp in self.depthprofiles:
            if detectors_given:
                pass
            else:
                detectors = [s.detname for s in self.measurements[subdp[0]].singles]
                if verbose: print(f"Using data aquired by detectors {detectors}")
            idcs = np.arange(len(self.measurements[subdp[0]:subdp[1]+1])) # change for multiple profiles
            esort = np.rec.array(list(zip(e[subdp[0]:subdp[1]+1], idcs)),
                             dtype=[("e", float), ("idx", int)])
            esort.sort(order="e")
            idcs = esort.idx
            if ave_equalpoints:
                # find points with equal energy, to average them
                unique_e, unique_idcs = np.unique(esort.e, return_index=True)
                unique_e_lists += [unique_e]
                unique_idcs = [*unique_idcs, len(esort)]
                new_idcs = [idcs[unique_idcs[j]:unique_idcs[j+1]]
                            for j in range(len(unique_idcs)-1)]
            else:
                unique_e_lists += [esort.e]
                new_idcs = idcs

            # average points with equal energy
            unique_measurements \
                = [np.sum(np.array(self.measurements[subdp[0]:
                                                     subdp[1]+1])[i])
                               for i in new_idcs]

            for i, meas in enumerate(unique_measurements):
                # choose the detectors of interest
                unique_measurements[i].singles = [s for s in meas.singles
                                              if s.detname in detectors]
                if not len(unique_measurements[i].singles):
                    wrn = (f"Couldn't find detector(s) {detectors} in "
                           "measurement.")
                    warnings.warn(wrn)

                # compute the detector average
                if average:
                    unique_measurements[i].singles \
                                    = [np.sum(unique_measurements[i].singles)]

            # extract the desired parameters from the sorted list of measurements
            # (most likely S, W or V2P)

            # lower parameter names
            # (so that they can be specified as caps, e.g., 'S')
            if isinstance(parameters, str):
                parameters = [parameters]

            parameters = [p.lower() for p in parameters]

            subprofiles = np.transpose([[[s.__getattribute__(param)
                                   for param in parameters]
                                    for s in m.singles]
                                     for m in unique_measurements])

            subprofile_errors = np.transpose([[[s.__getattribute__(f"d{param}")
                                           for param in parameters]
                                            for s in m.singles]
                                             for m in unique_measurements])

            if len(subprofiles) == 0:
                wrn = ("subprofile is empty and therefore being skipped.")
                warnings.warn(wrn)
            else:
                profiles += [subprofiles]
                profile_errors += [subprofile_errors]
                profile_detectors += [detectors]

        if show or save:
            # plot the depth profiles
            if plot_sw:
                plot_columns = 2
                nr_s = parameters.index("s")
                nr_w = parameters.index("w")
            else:
                plot_columns = 1
            fig, axs = plt.subplots(len(parameters), plot_columns,
                                    sharex="col", squeeze=False)
            if plot_sw:
                gs = axs[0, 1].get_gridspec()
                # remove the underlying axes
                for ax in axs[0:, -1]:
                    ax.remove()
                axbig = fig.add_subplot(gs[0:, -1])
            if verbose: print("Nr of profiles:", len(profiles))
            for k, (profile, detectors) in enumerate(zip(profiles,
                                                       profile_detectors)):
                for i, param in enumerate(parameters):
                    for j, det in enumerate(detectors):
                        if skip_detectors:
                            if det in skip_detectors:
                                continue
                        if param in ("s", "w", "v2p"):
                            param = param.upper() + " parameter"
                        else:
                            param = param[0].upper() + param[1:].replace("_", " ")
                        if labeling:
                            label = f"DP of {labeling[k]} by Det {det}"
                        else:
                            label = f"DP {k} of Det {det}"
                        if verbose:
                            print(f"""DP has {len(unique_e_lists[k])} \
energy values for {len(profile[i][j])} data points""")

                        axs[i,0].errorbar(unique_e_lists[k],
                                         profile[i][j],
                                         profile_errors[k][i][j],
                                         label=label,
                                         linewidth=0,
                                         elinewidth=1.5,
                                         markeredgewidth=1.5,
                                         )
                        if plot_sw and (i < 1):
                            axbig.errorbar(x=profile[nr_w][j],
                                            xerr=profile_errors[k][nr_w][j],
                                            y=profile[nr_s][j],
                                            yerr=profile_errors[k][nr_s][j],
                                            label=label,
                                            linewidth=0,
                                            elinewidth=1.5,
                                            markeredgewidth=1.5
                                            )
                    axs[i,0].set_ylabel(f"{param}")
                axs[i,0].set_xlabel("Implantation Energy / eV")
            if plot_sw:
                axbig.set_xlabel("W parameter")
                axbig.set_ylabel("S parameter")
            plt.legend()

        if save:
            plt.savefig(savename)
        if show:
            plt.show()

        return unique_e_lists, profiles, profile_errors

    def calc_ratio(self, reference, measurement):
        """
        This function calculates ratio curves.
        
        Parameters
        ----------
        reference : tuple of list
            A tuple containing the energy bins, counts and error of the reference projection.
        measurement : tuple of list
            A tuple containing the energy bins, counts and error of the measurement that is to be compared to the 
            reference.

        Returns
        -------
        tuple of np.array
            A tuple containing the energy bins, counts and error of the ratio curve.

        """
        energy, counts, error = measurement
        energyR, countsR, errorR = reference
        countRatio, errorRatio = [], []

        if len(energy) == len(energyR):
            for i in range(len(energy)):
                if countsR[i] == 0:
                    countRatio.append(0.)
                    errorRatio.append(0.)
                else:
                    countRatio.append(counts[i] / countsR[i])
                    errorRatio.append(m.sqrt(
                        (1 / (countsR[i] ** 2)) * (error[i] ** 2)
                        + ((counts[i] ** 2) / (countsR[i] ** 4)) * (errorR[i] ** 2)))
        else:
            raise IndexError('Reference spectrum and measurement spectrum have different '
                             'energy resolutions. Evaluation not possible')

        return np.array(energy), np.array(countRatio), np.array(errorRatio)

    def ratio_plotter(self, references, ratios, bins='Bins200.txt', namelist=None,
                      bg_sub=True, w_ref=None, color_map='viridis', contrast=True,
                      labels=True, linewidth=1.5, smoothing=True, plot_error=True,
                      x_axis='de', legend_loc='best', smooth_factor=0.002, check_ecal=False):
        """
        This function takes in a structured list of reference and ratio names which have to correspond to measurement
        names imported to self.measurements. With the given information the corresponding ratio curves for the input
        data will be computed and displayed as matplotlib plots. The structure of the references and ratios list is
        important to make the function run in the correct mode. **There are three major modes:**

        **Normal Mode:**
        In normal mode ONE reference will be used for all the requested ratio curves. The input will look like this:

        references = [["reference_name", ["detector_1", "detector_2"]]]

        ratios = [["ratio_name_1", ["detector_1", "detector_2"]], ["ratio_name_2", ["detector_1", "detector_2"]], ...]

        ALL ratio pairs will be referenced to ONE reference pair. The detector combinations have to be valid coincidence
        pairs. The detector pairs of the individual references and ratios do NOT have to be the same, however if you
        mix and match detector pairs be careful. If the energy resolution of the pairs varies too much the data is
        skewed.

        **Individual Mode:**
        In individual mode each ratio curve gets its own reference curve. The input will look like this:

        references = [["reference_name_1", ["detector_1", "detector_2"]],
        ["reference_name_2", ["detector_1", "detector_2"]], ...]

        ratios = [["ratio_name_1", ["detector_1", "detector_2"]], ["ratio_name_2", ["detector_1", "detector_2"]], ...]

        Each ratio curve is referenced to an individual reference. Accordingly, here *the number of references has to be
        equal to the number of ratios*, i.e. len(references) == len(ratios).

        **Multi Detector Mode:**
        Multi detector mode can be combined with either of the above modes. Multi detector mode is always active if
        more than one detector pair is given. Data will be given like this:

        references = [["reference_name", ["detector_1", "detector_2"], ["detector_3", "detector_4"], ...]

        ratios = [["ratio_name_1", ["detector_1", "detector_2"], ["detector_3", "detector_4"]], ...]

        In this mode the ratio curves will be averaged over multiple detector pairs. ATTENTION: If only one reference
        is given with multiple detectors ALL ratios also have to have the same number of detectors.

        **INFO for Windows users:** This function uses multiprocessing to parallelize the calculation of the ratio
        curves. For this the "forking" method of multiprocessing is used which unfortunately does not work on windows
        due to platform limitations. For Windows please use the slower, linear ratio_plotter_win function. Further info
        on the multiprocessing documentation page: https://docs.python.org/3/library/multiprocessing.html

        Parameters
        ----------
        references : list of list
            List of references as lists with a name and detector pair inside.
        ratios : list of list
            List of ratios as lists with a name and detector pair inside
        bins : str, optional
            String containing one of the filenames in stacs/data/bins/.
        namelist : list of str, optional
            Names corresponding to the ratio curves shown in the plot. The first name in the list applies for ALL
            references and the following names will be applied to the ratio curves in the order they are given in,
            i.e. the list must contain 1+len(ratios) elements.
        bg_sub : bool, optional
            Toggles the background constant background subtraction on or off. Default is ON.
        w_ref : float, optional
            By default the width of the ROI will be calculated according the energy resolution of the system. This is
            also the recommended setting. If a value is given the ROI width will be set to the given amount in channel
            on the y axis of the 2D histogram.
        color_map : str, optional
            matplotlib.colors colormap to be used for the coloring of the different ratio curves.
        contrast : bool, optional
            If true the contrast of the first and last ratio curve will be increased, useful for plots containing only
            a few ratio curves.
        labels : bool, optional
            If set to false, no labels will be put inside the plot window.
        linewidth : float, optional
            The line width of the ratio curve plot lines as taken by matplotlib.
        smoothing : bool, optional
            If set to true the ratio curves will be approximated by bsplines to make them look more pleasing. If set
            to false the points will simply be connected by straight lines.
        plot_error : bool, optional
            If set to false the error bars will not be plotted.
        x_axis : str, optional
            By default "de" is used and the x axis will be labeled with the Doppler shift in keV. Putting "m0c" will
            cause the x axis to be converted into the doppler shift momentum in units of m0c. Putting anything else
            will cause the axis to be labeled with the absolute energy value in keV (511 keV in the center).
        legend_loc : int, optional
            Location of the legend. Corresponds to the matplotlib legend.loc.
        smooth_factor : float, optional
            Influences the amount of smoothing of the bspline used when smoothing is active.
        check_ecal : bool, optional
            If active the algorithm will look for the 1200 keV peak of 22Na and perform an energy calibration.

        """

        import platform
        if platform.system() == 'Windows':
            # As we use the fork method to create subprocesses this function does not work for Windows currently.
            # Refer to the multiprocessing docs: https://docs.python.org/3/library/multiprocessing.html
            print("""The "ratio_plotter" function currently only supports unix based systems due to a limitation in the
            multiprocessing python package. The non threaded legacy function will now be called automatically.""")
            self.ratio_plotter_win(self, references, ratios, bins=bins, namelist=namelist,
                      bg_sub=bg_sub, w_ref=w_ref, color_map=color_map, contrast=contrast,
                      labels=labels, linewidth=linewidth, smoothing=smoothing, plot_error=plot_error,
                      x_axis=x_axis, legend_loc=legend_loc, smooth_factor=smooth_factor)
        import multiprocessing as mp
        import time

        if type(references[0]) is str:
            references = list([references])
        if type(ratios[0]) is str:
            ratios = list([ratios])

        input_error = False

        number_references = len(references)
        number_ratios = len(ratios)

        if number_references > 1:
            if number_ratios != number_references:
                raise IndexError('References and ratios cannot be matched. You can either input the same amount of '
                                 'references and ratios or just one reference.')

        param_queue = []
        detpair_list = []
        queue = []

        for meas_set in (references + ratios):
            if self[meas_set[0]] is None:
                err = (f'Measurement Mismatch: "{meas_set[0]}" does not appear to be a correct measurement name.')
                raise ValueError(err)

            for det in range(1, len(meas_set)):
                if self[meas_set[0]][meas_set[det][0], meas_set[det][1]] is None:
                    err = (f'Detector Mismatch: "{meas_set[det]}" does not appear to be a valid detector pair for ',
                           f'measurement "{meas_set[0]}".')
                    raise ValueError(err)

            if len(meas_set) == 2:
                param_queue.append(meas_set)
                detpair_list.append(meas_set)
            else:
                sub_list = []
                for i in range(1, len(meas_set)):
                    sub_list.append([meas_set[0], meas_set[i]])
                    detpair_list.append(meas_set)
                param_queue.append(sub_list)

        if check_ecal:
            # Compares the stored energy calibration with an online energy calibration, given that a 1200 keV peak is
            # present.
            meas_str = [str(m) for m in self.measurements]

            for meas in detpair_list:
                meas_index = meas_str.index(f"DopplerMeasurement object '{meas[0]}'")
                saved_ecal = self.measurements[meas_index][meas[1]].ecal

                singles_detname = []
                for item in self.measurements[meas_index].singles:
                    singles_detname.append(str(item.detname))

                for i in range(1, len(meas)):
                    det1 = meas[i][0]
                    det2 = meas[i][1]
                    det1_index = singles_detname.index(det1)
                    det2_index = singles_detname.index(det2)
                    first_det = self.measurements[meas_index].singles[det1_index]
                    second_det = self.measurements[meas_index].singles[det2_index]
                    first_det.calibrate('Na22', use_511_for_initial_guess=True, plot_spectrum=True,
                                        exception_on_warning=False, save_despite_warnings=True)
                    second_det.calibrate('Na22', use_511_for_initial_guess=True, plot_spectrum=True,
                                         exception_on_warning=False, save_despite_warnings=True)
                    first_det = first_det.ecal
                    second_det = second_det.ecal
                    print('first det: ', first_det[1], ', saved: ', saved_ecal[0][1])
                    print('second det: ', second_det[1], ', saved: ', saved_ecal[1][1])
                    if (abs(first_det[1] - saved_ecal[0][1]) > saved_ecal[0][1] * 0.05) or \
                            (abs(second_det[1] - saved_ecal[1][1]) > saved_ecal[1][1] * 0.05):
                        print('Warning: Calibration error greater than 5%')

        try:
            # This is required mainly for mac (OpenBSD) based systems. "Fork" clones the current python process for
            # the subprocesses. This means that the multiprocessing code can run outside the if __name__ == "__main__"
            # statement. This, however, does not work on Windows. This can also only be called once per python instance
            # so the exception avoids executing it twice.
            mp.set_start_method("fork")
        except RuntimeError:
            pass

        for item in param_queue:
            if type(item[0]) is str:
                queue.append(self[item[0]][item[1]])
            else:
                for sub_item in item:
                    queue.append(self[sub_item[0]][sub_item[1]])

        process_queue = []
        manager = mp.Manager()
        return_dict = manager.dict()

        def binning_wrapper(func, shared_dict, name, *args, **kwargs):
            # Wrapper function to pass the binning function along with all arguments to the mp.Process
            if name in shared_dict.keys():
                if '_Mult_' in name:
                    mult_det = [func(*args, **kwargs)]
                    current_list = shared_dict[name]
                    shared_dict[name] = current_list + [mult_det]
            else:
                # deactivate duplicate skipping for accurate benchmarking
                shared_dict[name] = [[func(*args, **kwargs)]]

        start_time = time.time()
        print(f"Starting processes for parallelization...")
        for i, item in enumerate(queue):
            # This creates one process for each projection.
            pairname = item.detpair
            if type(item.detpair) is not str:
                pairname = f'{item.detpair[0]}{item.detpair[1]}'
            if len(detpair_list[i]) == 2:
                proc = mp.Process(target=binning_wrapper, args=(item.binning, return_dict,
                                  f'{detpair_list[i][0]}_Single_{pairname}'),
                                  kwargs={'binfile': bins, 'width': w_ref, 'background_corr': bg_sub})
            else:
                proc = mp.Process(target=binning_wrapper, args=(item.binning, return_dict,
                                  f'{detpair_list[i][0]}_Mult_{pairname}'),
                                  kwargs={'binfile': bins, 'width': w_ref, 'background_corr': bg_sub})
            proc.start()
            process_queue.append(proc)
        print(f"Started {len(process_queue)} processes...")
        for item in process_queue:
            # .join() waits for all the processes to finish so that the data can be gathered.
            item.join()

        ref_list = []
        ratio_list = []

        # The calculated projections are now structured in a way that makes them processable for the rest of the code.
        for ref in references:
            if len(ref) > 2:
                int_list = []
                for n in range(1, len(ref)):
                    int_list += return_dict[f'{ref[0]}_Mult_{ref[n][0]}'f'{ref[n][1]}'][0]
                ref_list.append(int_list)
            else:
                ref_list.append(return_dict[f'{ref[0]}_Single_{ref[1][0]}'f'{ref[1][1]}'][0])

        for proj in ratios:
            if len(proj) > 2:
                int_list = []
                for n in range(1, len(proj)):
                    int_list += return_dict[f'{proj[0]}_Mult_{proj[n][0]}'f'{proj[n][1]}'][0]
                ratio_list.append(int_list)
            else:
                ratio_list.append(return_dict[f'{proj[0]}_Single_{proj[1][0]}'f'{proj[1][1]}'][0])

        if len(ref_list) == 1 and len(ratio_list) != 1:
            ref_list = ref_list * len(ratio_list)

        print(f'Done, total time elapsed: {time.time()-start_time:.2f} s')

        # Plotting part of the function starts here...
        # First a plot is created that contains the reference, displayed as a black line at constant 1.
        plt.plot((-100, 600), (1, 1), color='black', linewidth=2*linewidth)
        # The color pallet for the rest of the ratios is created.
        colnorm = matplotlib.colors.Normalize(vmin=0, vmax=len(ratio_list) - 1)
        colmap = cmx.ScalarMappable(norm=colnorm, cmap=color_map)
        labellist = []

        for i in range(len(ratio_list)):
            # Iterate through the requested ratio curves and plot them into the canvas one by one.
            energy, count, error = self.calc_ratio(ref_list[i][0], ratio_list[i][0])
            if len(ratio_list[i]) > 1:
                error = error ** 2
                for detpair in range(1, len(ratio_list[i])):
                    res = self.calc_ratio(ref_list[i][detpair], ratio_list[i][detpair])
                    count += res[1]
                    error += res[2] ** 2
                error = np.sqrt(error)
                error /= len(ratio_list[i])
                count /= len(ratio_list[i])
            if i == len(ratio_list) - 1 and contrast == True:
                col = 'orange'
            else:
                col = colmap.to_rgba(i)
            if namelist == None:
                labellist.append(patch.Patch(label=ratios[i][0], color=col))
            else:
                labellist.append(patch.Patch(label=namelist[i+1], color=col))

            if x_axis == 'de':
                energy -= 511
            elif x_axis == 'm0c':
                energy -= 511
                energy *= 3.918

            if plot_error:
                plt.errorbar(energy, count, error, color=col, linestyle='None',
                             capsize=2*linewidth, elinewidth=linewidth,
                             markeredgewidth=linewidth)

            if smoothing:
                t, c, k = scipy.interpolate.splrep(energy, count, s=smooth_factor, k=3)
                e_smooth = np.linspace(np.min(energy), np.max(energy), 500)
                c_smooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
                c_smooth = c_smooth(e_smooth)
                plt.plot(e_smooth, c_smooth, color=col, linewidth=linewidth)
            else:
                plt.plot(energy, count, color=col, linewidth=linewidth)

        plt.xlim(left=511, right=525)
        plt.ylim(bottom=0, top=5)
        plt.grid(linewidth=linewidth)
        if labels:
            plt.legend(handles=labellist, loc=legend_loc)
        if namelist:
            y_label = namelist[0]
        else:
            y_label = references[0][0]
        plt.ylabel(f'Ratio to {y_label}', fontsize=11)
        if x_axis == 'de':
            plt.xlabel('$\Delta$E (keV)', fontsize=11)
            plt.xlim(left=0, right=10)
        elif x_axis == 'm0c':
            plt.xlabel('$10^{-3} \ \mathrm{m_0 c}$', fontsize=11)
            plt.xlim(left=0, right=40)
        else:
            plt.xlabel('E (kev)', fontsize=11)
        plt.show()
        return

    def ratio_plotter_win(self, references, ratios, bins='Bins200.txt', namelist=None,
                      bg_sub=True, w_ref=None, color_map='viridis', contrast=True,
                      labels=True, linewidth=1.5, smoothing=1, plot_error=True,
                      x_axis='de', legend_loc='best', smooth_factor=0.002):
        if references[0] is type(str):
            references = [[references[0], references[1]]]
        if ratios[0] is type(str):
            ratios = [[ratios[0], ratios[1]]]

        ref_list = []
        ratio_list = []

        for i, ref in enumerate(references):
            if len(ref) == 2:
                if len(ratios[i]) == 2:
                    print(ref[0], ref[1], 'bin')
                    proj = self[ref[0]][ref[1]].binning(binfile=bins, width=w_ref, background_corr=bg_sub)
                    ref_list.append([proj])
                else:
                    print('Detector mismatch between reference list and ratio list, quitting...')
                    return
            else:
                if len(ref) == len(ratios[i]):
                    proj_list = []
                    for pair in ref[1:]:
                        proj = self[ref[0]][pair].binning(binfile=bins, width=w_ref, background_corr=bg_sub)
                        proj_list.append(proj)
                    ref_list.append(proj_list)
                else:
                    print('Detector mismatch between reference list and ratio list, quitting...')
                    return

        if len(ref_list) == 1:
            ref_list *= len(ratios)

        for ratio in ratios:
            if len(ratio) == 2:
                proj = self[ratio[0]][ratio[1]].binning(binfile=bins, width=w_ref, background_corr=bg_sub)
                ratio_list.append([proj])
            else:
                proj_list = []
                for pair in ratio[1:]:
                    proj = self[ratio[0]][pair].binning(binfile=bins, width=w_ref, background_corr=bg_sub)
                    proj_list.append(proj)
                ratio_list.append(proj_list)

        plt.plot((-100, 600), (1, 1), color='black', linewidth=2*linewidth)
        colnorm = matplotlib.colors.Normalize(vmin=0, vmax=len(ratio_list) - 1)
        colmap = cmx.ScalarMappable(norm=colnorm, cmap=color_map)
        labellist = []

        for ratio in range(len(ratio_list)):
            energy, count, error = self.calc_ratio(ref_list[ratio][0], ratio_list[ratio][0])
            if len(ratio_list[ratio]) != 1:
                error = error ** 2
                for detpair in range(1, len(ratio_list[i])):
                    res = self.calc_ratio(ref_list[i][detpair], ratio_list[i][detpair])
                    count += res[1]
                    error += res[2] ** 2
                error = np.sqrt(error)
                error /= len(ratio_list[i])
                count /= len(ratio_list[i])
            if i == len(ratio_list) - 1 and contrast == True:
                col = 'orange'
            else:
                col = colmap.to_rgba(i)
            if namelist == None:
                labellist.append(patch.Patch(label=ratios[i][0], color=col))
            else:
                labellist.append(patch.Patch(label=namelist[i+1], color=col))

            if x_axis == 'de':
                energy -= 511
            elif x_axis == 'm0c':
                energy -= 511
                energy *= 3.918

            if plot_error:
                plt.errorbar(energy, count, error, color=col, linestyle='None',
                             capsize=2*linewidth, elinewidth=linewidth,
                             markeredgewidth=linewidth)

            if smoothing == 0:
                plt.plot(energy, count, color=col, linewidth=linewidth)
            elif smoothing == 1:
                t, c, k = scipy.interpolate.splrep(energy, count, s=smooth_factor, k=3)
                e_smooth = np.linspace(np.min(energy), np.max(energy), 500)
                c_smooth = scipy.interpolate.BSpline(t, c, k, extrapolate=False)
                c_smooth = c_smooth(e_smooth)
                plt.plot(e_smooth, c_smooth, color=col, linewidth=linewidth)

        plt.xlim(left=511, right=525)
        plt.ylim(bottom=0, top=5)
        plt.grid(linewidth=linewidth)
        if labels:
            plt.legend(handles=labellist, loc=legend_loc)

        plt.ylabel(f'Ratio to {references[0][0]}', fontsize=11)
        if x_axis == 'de':
            plt.xlabel('$\Delta$E (keV)', fontsize=11)
            plt.xlim(left=0, right=10)
        elif x_axis == 'm0c':
            plt.xlabel('$10^{-3} \ \mathrm{m_0 c}$', fontsize=11)
            plt.xlim(left=0, right=40)
        else:
            plt.xlabel('E (kev)', fontsize=11)
        plt.show()
        return




class DopplerMeasurement:
    """
    Doppler measurement including singles and coincidences.

    ...

    Parameters
    ----------
    filename : str
        Path to the file containing the measurement data. Currently supporting
        n42, mpa and txt (for plain 2D coincidence histograms).
    name : str
        Name of the DopplerMeasurement object.
    autocompute : bool
        Compute single lineshape parameters during initilization.
    show : bool
        Show info/debug plots for each data processing step. Multiple
        pyplot windows will pop up.
    verbose : bool
        Print (debug) info while processing.

    """

    def __init__(self, filename=None, name=None, autocompute=True, show=False,
                 verbose=False, **kwargs):

        self.show = show
        self.verbose = verbose

        self.filename = filename
        if not name:
            self.name = filename
        else:
            self.name = name

        if not filename:
            self.filetype = None
            self.coinc = None
            self.singles = None
            self.implantation_energy = None
        else:
            self.filetype = filename.split(".")[-1]

            filetype_lookup = {"n42": importer.import_n42,
                               "mpa": importer.import_mpa,
                               "txt": importer.import_txt}

            if self.filetype not in filetype_lookup.keys():
                err = f"Filetype '{self.filetype}' not supported."
                raise NotImplementedError(err)

            # import measurement data from file
            if self.verbose:
                print(f"Importing measurement data from {self.filename}.")

            import_function = filetype_lookup[self.filetype]
            self.singles, self.coinc, self.implantation_energy \
                        = import_function(self.filename, show=self.show,
                                          verbose=self.verbose)

            # calculate line shape parameters
            if autocompute and self.singles:
                for single in self.singles:
                    if (single.ecal is None) or (single.spectrum is None):
                        # skip non-existent spectra or spectra without
                        # energy calibration
                        continue
                    try:
                        single.analyze(**kwargs)
                    except (RuntimeError, ValueError):
                        print(f"Automatic S and W parameter calculation failed due to non converging fits"
                              f" (Measurement: {self.name}, Detector: {single.detname}).")

    def __getitem__(self, item):
        if type(item) is str:
            for single in self.singles:
                if single.detname == item:
                    return single

        if type(item) is list or type(item) is tuple:
            for coinc in self.coinc:
                if item[0] in coinc.detpair and item[1] in coinc.detpair and not item[0] == item[1]:
                    if coinc.parentname == None:
                        coinc.parentname = self.name
                    return coinc

    def __add__(self, other):
        """Add two DopplerMeasurement objects (each sub-object seperately)."""

        new_coinc = []

        for detpair_s in self.coinc:
            for detpair_o in other.coinc:
                if detpair_o.detpair == detpair_s.detpair:
                    new_coinc.append(detpair_o + detpair_s)

        new_singles = []
        for det_s in self.singles:
            for det_o in other.singles:
                if det_s.detname == det_o.detname:
                    new_singles.append(det_s + det_o)

        new_DoppMeas = DopplerMeasurement(name=self.name + ' + ' + other.name)
        new_DoppMeas.filename = self.filename + ' + ' + other.filename
        new_DoppMeas.coinc = new_coinc
        new_DoppMeas.singles = new_singles
        return new_DoppMeas

    def __repr__(self):
        """Print option for DopplerMeasurement objects."""

        return f"DopplerMeasurement object '{self.name}'"

    def show_singles(self, show=True, save=False, savepath=None,
                     logscale=False):
        """Plot all single spectra contained in this measurement."""

        fig, ax = plt.subplots()

        for s in self.singles:
            ax.plot(s.spectrum, label=f"Det {s.detname}")

        if logscale:
            ax.set_yscale("log")

        ax.set(xlabel="Channel",
               ylabel="Counts")
        ax.legend()

        if save or savepath is not None:
            savename = savepath or f"{self.name}_singles.png"
            plt.savefig(savename)

        if show:
            plt.show()
