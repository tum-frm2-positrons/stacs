import os
import numpy as np
from stacs.core.utils import mod_erf
from scipy.constants import physical_constants
from scipy.optimize import curve_fit
from stacs.core.algorithms import fit_odr, lin, energy_fit, fit_peak_gauss
from stacs.core.classes import Spectrum
import matplotlib.pyplot as plt
from matplotlib import rcParams
from datetime import datetime
import csv
import json

def energy_resolution(folderpath, nameout, load_ecal=False, ecalfile='',
                      output=False, showfittedpeaks=False,
                      exclude_files=None, correctnaming=False, naming="old"):
    '''Determines the energy resolution of a detector a at 511 keV from an
    Eu-152 calibration measurement for all detectors whose names appear in the
    filenames in the folder specified. The function acts as a wrapper for
    resolve_peaks, collecting the results for a calibration session an giving
    the results in a plot as well as a dictionary.

    Parameters
    ----------
    folderpath : str
        Location if the filesystem where the to be measurement data to be
        analized is stored.
    nameout : str
        This string will be placed in the name of the output files.
    load_ecal : bol, optional
        Whether to load an energy calibration from a .json file or not. If set
        to True (default is False) the file specified in pararmeter "ecalfile"
        is called.
    ecalfile : str, optional
        Name of the json-file from which the energy is to be imported (default
        is '')
    output : bol, optional
        Generate output? (default is False)
    showfittedpeaks : bol, optional
        Debugging option that will display the a plot showing which peaks have
        been selected from a spectrum and their fits during evaluation. (default
        is False)
    exclude_files : list, optional
        Measurements whose names are found in this list are not to be evaluated.
        (default is None)

    Returns
    -------
    eres : dict of floats
        Dictionary containing the energy resolution in FWHM as well as its stdev
        of each detector present in the data set given.
    '''

    peakcenter511 = physical_constants['electron mass energy equivalent in MeV'][0] * 1e3
    if load_ecal == True:
        with open(ecalfile) as f:
            ecal = json.load(f)
    else:
        ecal = energy_calibration_folder(folderpath=folderpath, nameout=nameout,
                                         save=False, skip = exclude_files, naming=naming)

    peaks = resolve_peaks_folder(folderpath=folderpath, ecal=ecal,
                                show=showfittedpeaks, exclude_mes=exclude_files,
                                correctnaming=correctnaming)

    if output == True:
        keys = list(peaks.keys())
        keys.sort()
        figsize = (30, 16)
        rows = 1
        cols = len(keys)+1
        fig1, axs = plt.subplots(rows, cols, sharey='row')
        i = 1
        eres = {}
        print(keys)
        for key,ax in zip(keys,axs):
            ax.set_title(str(key))
            #e,sig_e,sig_e_err =
            if i == 1:
                 ax.set_ylabel('FWHM [keV]')
                 i += 1
                 #print(i)
            else:
                i += 1

            FWHM_e = peaks[key]['FWHM']
            print(f'Detector {key} was found to see {len(FWHM_e)} valid Eu-152 peaks')
            if len(FWHM_e) > 0: #FWHM_e[0:1][0] > 1.0 and FWHM_e[0:1][0] < 5.0:
                #print(f'Detector {key} was found to see {len(FWHM_e)} valid Eu-152 peaks')
                #find eres at 511 keV:
                odr_out = fit_odr(mode='lin', x=peaks[key]['E'], y=FWHM_e, sy=peaks[key]['FWHMerr'])
                p_out = odr_out.beta
                sd_out = odr_out.sd_beta
                cov_out = odr_out.cov_beta

                var_out = cov_out * odr_out.res_var / (len(FWHM_e)-2.0) #stdev cov

                fwhm_511 = lin(p_out,peakcenter511)
                fwhm_511_err = np.sqrt(np.power(peakcenter511 * sd_out[0],2) +
                                       np.power(sd_out[1],2) + 2 * peakcenter511
                                       * var_out[0][1])

                e = np.linspace(0.0, 1.0e3)
                fwhm_err_fkt = np.sqrt(np.power(lin(p_out, e) * sd_out[0],2) +
                                       np.power(sd_out[1],2) + 2 * lin(p_out, e)
                                       * var_out[0][1])
                ax.plot(e,lin(p_out,e), linestyle='-', color='orange')
                ax.plot(odr_out.xplus, odr_out.y, linestyle='--', color='r')
                ax.fill_between(e, lin(p_out,e) + fwhm_err_fkt, lin(p_out,e) -
                                fwhm_err_fkt, alpha=0.3, color='orange')
                #plot output:
                ax.errorbar(peaks[key]['E'], FWHM_e, peaks[key]['FWHMerr'],
                            capsize=3,linestyle='')
                ax.set_xlabel('E[MeV]')
                ax.set_ylim((0.0, 4.0))
                ax.set_xticks([0.0,1.0e3])
                ax.set_xticklabels([0,1])
                ax.set_xlim((0.0, 1.0e3))
                ax.grid(axis='y')
                ax.axvline(peakcenter511,color='r', linestyle='--')
                print(f'Detector {key} has a FWHM of {fwhm_511:.2f}+-{fwhm_511_err:.2f}')
                eres[key] = {'fwhm': fwhm_511, 'fwhm_err': fwhm_511_err}
            else:
                #fig2, axs2 = plt.subplots(1, 1)
                #axs2.plot(spectr)
                #axs2.set_title(str(detectors[det]))
                #plt.show()
                pass


        plt.savefig(f'Ecal_{nameout}.png', dpi=1200, papertype='a4', figsize=figsize,
                    constrained_layout=True, format="png", orientation='landscape')
        # dump to json
        jdat = json.dumps(eres)
        f = open(f"Eres_{nameout}.json","w")
        f.write(jdat)
        f.close()

    return eres


def resolve_peaks(file, ecal, naming='old', show=False, correctname=False):
    '''Takes a single measurement file as input and determines the energy
    resolution for all spectra where the detector name was given in the file
    name. Only works with Eu-152 measurements.

    Parameters
    ----------
    file : str
        Path to the file that is to be evaluated.
    ecal : dict
        Energy calibration to be used.
    naming : str, optional
        Define the detector naming convention to be used. I.e. set to 'old' if
        the board channels ("A","B","C", ...) are used directly (default is
        'old')
    show : bol, optional
        Debugging option. Displays plots of the peaks after have been fitted for
        each detector (default is False).
    correctname : bol, optional
        Option needed when file name given is misslabled. Uses the dict
        "corrections" to make the function analyse the correct detectors output
        (default is True)

    Returns
    -------
    result : dict of dicts of floats
        Dictionary containing the FWHM as well as its stdev of each resonable
        peak in each detector evaluted.
    '''

    energies_Eu152_long = np.array([121.7818, 244.6976,  344.2789, 411.1171,
                                    443.966,  778.9066,  867.383,  964.08,
                                   1085.841, 1089.741,  1112.1,   1212.953,
                                   1299.148, 1408.013]) # Source: Monographie BIPM-5, Table of Radionuclides (Vol. 2 - A = 151 to 242), M.-M. Bé, V. Chisté, C. Dulieu, E. Browne, V. Chechev, N. Kuzmenko, R. Helmer, A. Nichols, E. Schönfeld, R. Dersch, 2004, BUREAU INTERNATIONAL DES POIDS ET MESURES, Pavillon de Breteuil, F-92310 SÈVRES

    lookup1 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAC", "H": "OAE", "I": "OAF"}
    lookup2 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAE", "H": "OAF"}
    lookup3 = {"A": "CAC", "B": "CAF", "C": "CAA", "D": "CAB"}

    lookup4 = {"C": "OAG"}
    lookup5b = {"C": "OAG", "D": "OAH"}
    lookup5 = {"C": "OAG", "D": "CAZ"}

    # correct for naming errors:
    corrections = {"CAA": "CAC", "CAB": "CAF", "CAC": "CAB", "CAF": "CAA"} # correction for misslabled measurement filenames

    eu1 = Spectrum(filename=file)

    # get number of detectors in measurement file, to choose lookup
    l = len(eu1.singles)
    if l == 8:
        detectors = lookup2
        print(f"{l} detectors were active")
    elif l == 9:
        detectors = lookup1
        print(f"{l} detectors were active")
    elif l == 4:
        detectors = lookup3
    elif l == 2:
        detectors = lookup5
        #file =['OAG','OAH']
        file =['OAG','CAZ']
    elif l == 1:
        detectors = lookup4
        file =['OAG']
    else:
        print(f"No suitable lookup table for {l} detectors")
        return

    queue = []
    det_inv = {}
    for key, val in detectors.items():
        det_inv[val] = key

    for det in det_inv:
        if correctname and det in corrections.keys():
            if det in file:
                if naming == '':
                    queue.append(det_inv[corrections[det]])
                else:
                    queue.append(corrections[det])
        else:
            if det in file:
                if naming == '':
                    queue.append(det_inv[det])
                else:
                    queue.append(det)

    if not len(queue):
        print('Queue empty. Please specify detectors to be used.')
        return

    result = {}
    for current in queue:
        print(current)
        data = eu1.singles[current]['ChannelData']
        ecal_det = ecal[current]['linefit']
        data_y = data
        data_x_ch = np.linspace(0,len(data)-1,len(data))
        data_x_e = ecal_det[0] + ecal_det[1] * data_x_ch
        temp = (energies_Eu152_long - ecal_det[0]) / ecal_det[1]
        peak_center_guess_ch = temp.astype(int)
        e_res_guess_e = 3.0 #[keV]
        w_ch = int((3.0 * e_res_guess_e - ecal_det[0]) / ecal_det[1] / 2.0)  #ch
        Energy = np.empty(1)
        FWHM_l = np.empty(1)
        FWHM_l_err = np.empty(1)

        width_Nsigma = 4
        for center, energy in zip(peak_center_guess_ch,energies_Eu152_long):
            if center + w_ch > len(data):
                break
            FWHM, FWHM_err, opt = fit_peak_gauss(data_x_ch, data_y, int(center), w_ch)
            if opt[0] > 3.0 * opt[3]:
                w_ch = int(FWHM / 2.0 / np.sqrt(2.0 * np.log(2.0))) * width_Nsigma # 4sigma
                FWHM, FWHM_err, opt = fit_peak_gauss(data_x_e, data_y, center, w_ch)
                Energy = np.append(Energy, energy)
                FWHM_l = np.append(FWHM_l, FWHM)
                FWHM_l_err = np.append(FWHM_l_err, FWHM_err)
        Energy = Energy[1:]
        FWHM_l = FWHM_l[1:]
        FWHM_l_err = FWHM_l_err[1:]
        newpeak = {'FWHM': FWHM_l, 'FWHMerr': FWHM_l_err, 'E': Energy}
        newdetector = {current: newpeak}
        result = {**result, **newdetector}
        if show == True:
            fig2 = plt.figure()
            plt.plot(data_x_e,data_y)
            ax = fig2.add_subplot(1, 1, 1)
            for e,fwhm in zip(Energy,FWHM_l):
                #ax.set_ylim((1.0,4.0))
                ax.set_xlim((0.0,max(data_x_e)))
                ax.axvline(e, ymin=0, ymax=max(data_y), color='r')
                ax.axvspan(e-fwhm/2, e+fwhm/2, alpha=0.5, color='red')
                sig = fwhm / 2.0 / np.sqrt(2.0 * np.log(2.0))
                ax.axvspan(e-width_Nsigma*sig, e+width_Nsigma*sig, alpha=0.5, color='g')
            ax.grid(axis='y')
            ax.set_ylim([0, max(data_y)])
            ax.set_xlabel('E[keV]')
            plt.show()
    return result


def resolve_peaks_folder(folderpath, ecal, show=False, exclude_mes = None,
                         correctnaming=False):
    print(f"Finding all peaks for the measurements in: {folderpath}")

    files = os.listdir(folderpath)
    measurements = [mes for mes in files if ('.n42' in mes) or ('.mpa' in mes)]
    if exclude_mes is not None:
        for exc in exclude_mes:
            measurements = [mes for mes in measurements if exc not in mes]

    out = {}
    for specfile in measurements:
        filepath = folderpath + specfile
        peaks = resolve_peaks(filepath, ecal=ecal, show=show, correctname=correctnaming)
        out = {**out, **peaks}

    return out


def energy_calibration_folder(folderpath, nameout='', save=False, skip=None,
                             detector=None, withbeam=False, correctnaming=False, naming="old"):
    print(f"Creating an energy calibration from folder: {folderpath}")
    measurements = os.listdir(folderpath)
    if skip is None:
        skip = []

    if len(skip) > 0:
        for exc in skip:
            measurements = [mes for mes in measurements if exc not in mes]

    out = {}
    for specfile in [f for f in measurements if (f.endswith('.n42') or
    f.endswith('.mpa'))]:
        filepath = folderpath + specfile
        ecal = energy_calibration(filepath, detector=detector, withbeam=withbeam,
                                 correctnaming=correctnaming, naming=naming)
        out = {**out, **ecal}
    if save == True:
        # dump to csv
        w = csv.writer(open(f"ecal_{nameout}.csv", "w"))
        for key, val in out.items():
            w.writerow([key, val])
        # dump to json
        jdat = json.dumps(out)
        f = open(f"ecal_{nameout}.json","w")
        f.write(jdat)
        f.close()
        # dump txt in wiki format
        fwiki = open(f"ecal_{nameout}_wiki.txt","w")
        keys = list(out.keys())
        keys.sort()
        for key in keys:
            # 2nd order polyinomial output
            polyfit_c0 = sci_not_out(out[key]["polyfit"][0],
                                     out[key]["polyfiterr"][0])
            polyfit_c1 = sci_not_out(out[key]["polyfit"][1],
                                     out[key]["polyfiterr"][1])
            polyfit_c2 = sci_not_out(out[key]["polyfit"][2],
                                     out[key]["polyfiterr"][2])
            # 1st order polynomial output
            linfit_c0 = sci_not_out(out[key]["linefit"][0],
                                    out[key]["linefiterr"][0])
            linfit_c1 = sci_not_out(out[key]["linefit"][1],
                                    out[key]["linefiterr"][1])

            outstr = f'|  {key}  |  {polyfit_c0} |  {polyfit_c1} |  {polyfit_c2} |  {linfit_c0} |  {linfit_c1} |\n'

            fwiki.write(outstr)
        fwiki.close()
    return out


def sci_not_out(val, valerr):
    '''Formats a value and stdev of the value in such a way that they can be
    displayed with the same exponent.

    Parameters
    ----------
    val : float
        Numerical value to be formated.
    valerr : float
        Stdev of val.

    Returns
    -------
    str_out : str
        String of shape value(stdev)e(exponen).
    '''
    mp = np.finfo(float).eps
    d1 = int(len(str(int(1. / (val + mp)))))
    d2 = int(len(str(int(1. / (valerr + mp)))))
    dec = max(d1,d2)
    valmod = val * np.power(10, dec)
    valmoderr = valerr * np.power(10, dec)
    str_out =f'{valmod:.1f}({valmoderr:.1f})e-{dec:0>2}'
    return str_out


def energy_calibration(file_n, detector=None, withbeam=False, correctnaming=False, naming='old'):
    res = do_e_calib(file=file_n, naming=naming, detector=detector, withbeam=withbeam,
                    correctnaming=correctnaming)

    for det in res:
        print()
        print(det, ':')
        for fit in res[det]:
            print(fit, ':\t', res[det][fit])

    # example:
    '''e_calib_poly = {'CAA': (0.040214355381063306, 0.03351098788606174, -4.680517233193169e-11),
                    'CAB': (0, 0, 0),
                    'CAC': (0.09583234917194908, 0.0324648411888089, 8.12272935475664e-10),
                    'CAF': (0.06853398492863615, 0.03493768383217028, 7.405601499615754e-10),
                    'OAA': (0.036749602194653196, 0.04335219296305111, 5.256560930120657e-09),
                    'OAB': (-0.0989692381170216, 0.04376257748564547, -7.285240460733366e-09),
                    'OAC': (0.17876501057334482, 0.04561899426999479, 6.383891583890011e-09),
                    'OAE': (0.021442328044091193, 0.047760699665714605, -6.695177738110773e-10),
                    'OAF': (0.16754750224168274, 0.04536741935018073, 2.793790937391675e-09)}

    e_calib_lin = {'CAA': (0.046561676176565925, 0.03350966667236831, 0),
                   'CAB': (0, 0, 0),
                   'CAC': (-0.021386415639881307, 0.032488492432957004, 0),
                   'CAF': (-0.023776152842231113, 0.03495772426924292, 0),
                   'OAA': (-0.08915449475000514, 0.043408184357413435, 0),
                   'OAB': (0.07310244913216479, 0.0436855106745822, 0),
                   'OAC': (0.04089792818322735, 0.04568356998904373, 0),
                   'OAE': (0.034678224830827276, 0.04775422013307502, 0),
                   'OAF': (0.10647983158958141, 0.04539585200845223, 0)}'''
    return res


def do_e_calib(file, naming='old', detector=None, withbeam=False,
              correctnaming=False):

    lookup1 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAC", "H": "OAE", "I": "OAF"}
    lookup2 = {"A": "CAA", "B": "CAB", "C": "CAC", "D": "CAF", "E": "OAA",
               "F": "OAB", "G": "OAE", "H": "OAF"}
    lookup3 = {"A": "CAC", "B": "CAF", "C": "CAA", "D": "CAB"}

    lookup4 = {"C": "OAG"}
    lookup5 = {"C": "OAG", "D": "OAH"}
    lookup5b = {"C": "OAG", "D": "CAZ"}

    # correct for naming errors:
    corrections = {"CAA": "CAC", "CAB": "CAF", "CAC": "CAB", "CAF": "CAA"} # correction for misslabled measurement filenames

    # import measurement
    eu1 = Spectrum(filename=file)

    # get number of detectors in measurement file, to choose lookup
    l = len(eu1.singles)
    print(f"Number of Detectors found is {l}.")
    if l == 8:
        detectors = lookup2
    elif l == 9:
        detectors = lookup1
    elif l == 4:
        detectors = lookup3
    elif l == 2:
        detectors = lookup5b
    elif l == 1:
        detectors = lookup4
    else:
        print(f"No suitable lookup table for {l} detectors")
        return
    print(f"Calibrating detectors {detectors}.")
    queue = []
    det_inv = {}
    for key, val in detectors.items():
        det_inv[val] = key
    for det in det_inv:
        if det in file:
            if naming == 'old':
                print(det_inv[det])
                queue.append(det_inv[det])
            else:
                print("3 letter detector naming convention is used")
                queue.append(det)

    if not len(queue):
        queue = detector
        if detector is None:
            print('Please specify detector name or use file with name inside.')

    result = {}
    for current in queue:
        print(current)
        if correctnaming and detectors[current] in corrections.keys():
                det_temp= corrections[detectors[current]]
                current = det_inv[det_temp]
                print("Name has been corrected")

        data = eu1.singles[current]['ChannelData']
        energy_fit(data, peaks=3, debug1=True, name=current,
                   fiveeleven=withbeam)
        plt.show()
        #plt.pause(100)
        while True:
            p = input('Please enter the number of peaks to be used: ')
            try:
                p = int(p)
                if p > 6 or p < 2:
                    raise ValueError
            except ValueError:
                if p > 6:
                    print('Invalid Entry (must be an integer and smaller than 6')
                else:
                    print('Invalid Entry (must be an integer and greater than 1')
            else:
                break
        if p > 3:
            poly = energy_fit(data, peaks=p, debug1=False, debug2=False, fiveeleven=withbeam)
            lin = energy_fit(data, polyfit=False, peaks=p, fiveeleven=withbeam)
        elif p > 2:
            print("Three parameters fitted to 3 points --> do not use poly fit out")
            poly = energy_fit(data, peaks=p, debug1=False, debug2=False, fiveeleven=withbeam)
            lin = energy_fit(data, polyfit=False, peaks=p, fiveeleven=withbeam)
        elif p > 1:
            print("Only 2 points available --> not poly fit and use lin fit put with care")
            poly = (0, 0, 0, 0, 0, 0)
            lin = energy_fit(data, polyfit=False, peaks=p, fiveeleven=withbeam)

        result[current] = {'polyfit': poly[0:3], 'linefit': lin[0:2], 'polyfiterr': poly[3:6], 'linefiterr': lin[3:5]}
    return result


def import_parameter(directory='', filename='Parameter.txt'):
    """Import the 'Parameter.txt' file generated by LabView."""

    data = {}
    with open(os.path.join(directory, filename), 'r') as file:
        header = file.readline().replace('\r\n', '').replace('\n','').split('\t')
        for var in header:
            data[var] = []
        for line in file.readlines():
            values = line.replace(',', '.').replace('\r\n', '').replace('\n','').split('\t')
            for i in range(len(header)):
                if header[i] == 'File':
                    data[header[i]].append(int(float(values[i])))
                elif header[i] == 'Time':
                    data[header[i]].append(values[i])
                else:
                    data[header[i]].append(float(values[i]))
    return data


def runs_of_coord(coord):
    """Find runs of same coordinate (which are scans of the other)."""

    bounded = np.hstack(([0], coord, [0]))
    difs = np.diff(bounded)
    run, = np.where(difs == 0)
    steps = np.diff(np.hstack(([0], run, [0])))
    runs_of_coord = np.split(run, np.where(steps > 1)[0])
    return runs_of_coord[1:]


def find_linescans(s, e, x, y):
    """Split data into x- and y-scans."""

    xscans, yscans = [], []
    yscan_idcs = runs_of_coord(x)
    xscan_idcs = runs_of_coord(y)
    for idcs in xscan_idcs:
        xscans.append({'Energy': e[idcs], 'Scan': x[idcs], 'Fixed': y[idcs], 'S': s[idcs]})
    for idcs in yscan_idcs:
        yscans.append({'Energy': e[idcs], 'Fixed': x[idcs], 'Scan': y[idcs], 'S': s[idcs]})
    return xscans, yscans


def fit_lsc(lsc):
    """Fit error function to a linescan and return optimized parameters."""

    coord = lsc['Scan']
    center_guess = coord[round(len(coord)/2)]
    offset_guess = np.average(lsc['S'])
    amplitude_guess = (lsc['S'][-1] - lsc['S'][0])/2
    guess = (amplitude_guess, offset_guess, center_guess, 4)
    try:
        popt, pcov = curve_fit(mod_erf, lsc['Scan'], lsc['S'], guess)
    except:
        print("Did not converge")
        popt = guess
    return popt, guess


def beam_charact(s, e, x, y, show=False):
    """Fit linescans and determine the beam FWHM in x- and y-direction."""

    xscans, yscans = find_linescans(s, e, x, y)
    beam_fwhm = []
    for xsc, ysc in zip(xscans, yscans):
        ei = np.average(np.append(xsc['Energy'], ysc['Energy']))
        xopt, xguess = fit_lsc(xsc)
        yopt, yguess = fit_lsc(ysc)
        xfwhm = 2*np.sqrt(np.log(2))/xopt[3]
        yfwhm = 2*np.sqrt(np.log(2))/yopt[3]
        beam_fwhm.append((xfwhm, yfwhm))
        print(f"FWHM ({ei:.2f} kV):\tx = {xfwhm:.4f}, y = {yfwhm:.4f}\tCenter:\tx = {xopt[2]:.4f}, y = {yopt[2]:.4f}")

        if show:
            import matplotlib.pyplot as plt
            co = np.append(xsc['Scan'], ysc['Scan'])
            xaxis = np.linspace(min(co)-1, max(co)+1, 100)
            # plot xscan
            #plt.plot(xaxis, mod_erf(xaxis, *xguess), label='x guess')
            plt.plot(xaxis, mod_erf(xaxis, *xopt), label='x fit')
            plt.scatter(xsc['Scan'], xsc['S'],
                label=f"E={ei}, Y={np.average(xsc['Fixed']):.3f}")

            # plot y scan
            #plt.plot(xaxis, mod_erf(xaxis, *yguess), label='y guess')
            plt.plot(xaxis, mod_erf(xaxis, *yopt), label='y fit')
            plt.scatter(ysc['Scan'], ysc['S'],
                label=f"E={ei}, X={np.average(ysc['Fixed']):.3f}")

            plt.legend()
            plt.show()
    return beam_fwhm


def find_depth_profiles(data, meta, coords=None):
    """
    Find individual depth profiles and sort them by implantation energy.

    Takes two dictionaries data and meta, e.g, created by 'import_folder'
    and locates unique coordinate points (and file folders). For each point
    a depth profile is created and sorted by energy.

    Parameters
    ----------
    data : dict
        Data dictionary containing a sub-dictionary with detector-specific
        information and data for each detector.
    meta : dict
        Metadata dictionary containing detector-independent info and data.
    coords : list of tuple, list of list of tuple
        List of measured coordinates to choose from. Only necessary if
        coordinates for the same spot differ slightly. List of lists for
        multiple folders.
    """

    nfiles = len(meta["FileName"])

    x, y = np.array(meta["xKey"], dtype=float), np.array(meta["yKey"],
                                                         dtype=float)
    unique_folders, indices = np.unique(meta["Folder"], return_index=True)
    indices = [*sorted(indices), len(meta["Folder"])]

    # correct small coordinate shifts
    print(coords)
    if coords != None:
        if type(coords[0][0]) == int or type(coords[0][0]) == float: # if only one target folder is given or only a single coordinate list is given.
            coords = [coords] * len(unique_folders)
        if len(coords) != len(unique_folders):
            print(f"UserWarning: Length of specified coordinates \
({len(coords)}) does not match number of folders ({len(unique_folders)}).")
        for i, folder in enumerate(unique_folders):
            if coords[i] != None:
                print(f"\nCorrecting coordinates of folder '{folder}' \
({coords[i]}) ...")
                for j in np.arange(indices[i], indices[i+1], 1):
                    idx = np.argmin(list(map(lambda xy:
                                              np.sqrt((x[j]-xy[0])**2 \
                                              + (y[j]-xy[1])**2), coords[i])))
                    x[j], y[j] = coords[i][idx]

        meta["xKey"], meta["yKey"] = x, y

    try:
        meta["REM"] = list(map(float, meta["REM"]))
    except KeyError:
        print("\nDataWarning: Your data is missing the key 'REM'. Filling \
with zeros.")
        meta.update({"REM": [0.0]*len(meta["Folder"])})

    # find unique coordinate points
    unique, indices = np.unique([meta["Folder"], meta["REM"], x, y],
                                axis=1, return_index=True)

    u = np.rec.array([*unique, indices],
        dtype=[("folder", object), ("remoderator", float), ("x", float),
               ("y", float), ("index", int)])
    u.sort(order="index")
    indices = [*u.index, nfiles]
    unique = np.transpose([u.folder, u.remoderator, u.x, u.y])
    npoints = len(unique)
    print(f"\n{npoints} unique coordinate pair(s) found (folder, remoderator, x, y):\n{unique}\n")

    profiles = []
    coordinates = np.array([meta["Folder"], meta["REM"], x, y]).transpose()
    for i, point in enumerate(unique):
        point = np.array(point, dtype=str)
        compare = np.all(coordinates==point, axis=1)
        idcs = np.where(compare)[0]

        # sort by energy
        e = np.array(meta["PositronImplantationEnergy"])[idcs]
        esort = np.rec.array(list(zip(e, idcs)),
                          dtype=[("e", float), ("idx", int)])
        esort.sort(order="e")
        idcs = esort.idx

        # find points with equal energy, to average them
        unique_e, unique_idcs = np.unique(esort.e, return_index=True)
        unique_idcs = [*unique_idcs, len(esort)]
        new_idcs = [idcs[unique_idcs[j]:unique_idcs[j+1]]
                    for j in range(len(unique_idcs)-1)]

        subdata, submeta = {}, {}

        # fill subdata
        for det in data:
            subdata.update({det: {}})
            for key in data[det]:
                try:
                    l = [np.average(np.array(data[det][key])[idcs])
                         for idcs in new_idcs]
                except (TypeError, AttributeError):
                    if key == "LiveTimeDuration" \
                                        and not data[det][key][0] == "ToDo":
                        try:
                            l = list(map(lambda y: f"PT{y:.3f}S",
                                [np.average(np.array(list(map(
                                lambda x: float(x[2:-1]), data[det][key])))[idcs])
                                for idcs in new_idcs]))
                        except TypeError:
                            l = [np.array(data[det][key])[idcs][0]
                                for idcs in new_idcs]
                            print(f"DataWarning: Det {det}: Can't average data at key \
'{key}'. Using first appearing value '{l[0]}'.")
                    else:
                        l = [np.array(data[det][key])[idcs][0]
                             for idcs in new_idcs]
                        print(f"DataWarning: Det {det}: Can't average data at key \
'{key}'. Using first appearing value '{l[0]}'.")

                subdata[det].update({key: l})

        # fill submeta
        for key in meta:
            try:
                l = [np.average(np.array(meta[key])[idcs])
                     for idcs in new_idcs]
            except TypeError:
                if key == "StartDateTime":
                    l = [min(list(map(datetime.fromisoformat,
                         np.array(meta[key])[idcs]))) for idcs in new_idcs]
                elif key == "RealTimeDuration":
                    l = list(map(lambda y: f"PT{y:.3f}S",
                        [np.sum(np.array(list(map(
                         lambda x: float(x[2:-1]), meta[key])))[idcs])
                         for idcs in new_idcs]))
                else:
                    l = [np.array(meta[key])[idcs][0] for idcs in new_idcs]

            submeta.update({key: l})

        profiles.append((subdata, submeta))

    return profiles


def plot_depth_profiles(profiles, param="S", detectors="all", norm=False,
                        title="Depth Profile", labels=None, xaxis="keV",
                        savename="depth_profile", average=False, color=None,
                        in_one_plot=False, separate=False, sharey=False,
                        style="default", font="DejaVu Sans", fontsize=11,
                        width=7, height=5, linewidth=1., show=False,
                        return_fig=False,
                        save=False, grid=True, dump=False,
                        labelsmanual=None, **kwargs):
    """
    Plot all depth profiles given in a list.

    Takes a list of depth profiles, e.g., created by 'find_depth_profiles'
    and plots them in the specified way and style.

    Parameters
    ----------
    param : str
        The desired parameter to be plotted ('S', 'W', 'P2V', 'CountsPeak').
    detectors : list of str
        Detectors to be plotted (or averaged).
    norm : str
        Normalize different depth profiles to their bulk state. Only  advisable
        for identical materials! Ignored, if average is False.
    title : str, list of str
        Title of the figure (above the graph), a list can be specified for
        multiple figures/subplots. Empty string for no title. Overwritten
        by labels if in_one_plot is False.
    labels : list of str
        One label for each depth profile, shown in legend if in_one_plot is
        True, used as title else.
    xaxis : str
        Units of the x-axis ('eV' or 'keV').
    savename : str
        Filename of the saved graph.
    average : bool
        Average over detectors.
    in_one_plot : bool
        Plot all depth profiles on one axis (i.e. no subplots)
    separate : bool
        Separate plots into different matplotlib figures.
    sharey : bool, str (True, 'row', False)
        Determines whether and how to share the y-axis.
    style : str
        Matplotlib style for plotting.
    font : str
        Matplotlib font family (use 'serif' for publications).
    fontsize : int
        Fontsize of axis labels and title.
    width, height : int
        Width and height of the figure(s) in inches.
    linewidth : float
        Linewidth of all axes, ticks, plots and errorbars.
    show : bool
        Show the plot.
    save : bool
        Save the plot to a file. Does not work, if show is also True!
    dump : bool
        Write profiles to file (like needed for VepFit).
    ** kwarg
        Additional keyword arguments are passed to the matplotlib
        errorbar function.
    """

    def round_up(x):
        a, b = str(x).split(".")
        if int(b) == 0:
            return int(a)
        else:
            return int(a) + 1

    # determine shape of plot
    npoints = len(profiles)
    ncols = round_up(np.sqrt(npoints))
    nrows = round_up(npoints / ncols)

    if in_one_plot:
        ncols = 1
        nrows = 1
        newtitle = title + "s" # plural s

    # correct specified detectors to be averaged
    if type(average) == bool:
        average = [average] * npoints
    if type(detectors) == str or not any(type(l)==list for l in detectors):
        detectors = [detectors] * npoints
    if not (all(average) == any(average)) and in_one_plot:
        print(f"UserWarning: Plot contains averaged and non-averaged profiles.\
 Legend may be confusing/incorrect.\n")

    # set up plot
    plt.style.use(style)
    rcParams["font.family"] = font
    rcParams["font.size"] = fontsize
    rcParams["figure.figsize"] = width, height
    rcParams["axes.linewidth"] = linewidth
    rcParams["lines.linewidth"] = linewidth

    if separate and not in_one_plot:
        figures = [plt.figure(i, constrained_layout=True)
                   for i in range(npoints)]
        axs = np.array([fig.add_subplot() for fig in figures])
    else:
        fig, axs = plt.subplots(ncols=ncols, nrows=nrows,
                                constrained_layout=True, sharey=sharey)
        figures = [fig]

    if ncols > 1:
        axs = axs.flat
    else:
        axs = [axs]

    for ax in axs[npoints:]:
        ax.set_visible(False)

    for i, (data, meta) in enumerate(profiles):
        name = os.sep.join(meta["Folder"][0].split(os.sep)[-2:])
        coord = f"({meta['xKey'][0]}, {meta['yKey'][0]})"
        rem = meta["REM"][0]
        if labelsmanual is None:
            labelsuffix = ""
        else:
            labelsuffix = f" - {labelsmanual[i]}"
        if np.isnan(rem):
            rem = 0  # should actually not happen...
        e = np.array(meta["PositronImplantationEnergy"]) + rem
        all_detectors = [d for d in data.keys() if not any(np.isnan(data[d]["S"]))]
        if detectors[i] == "all":
            detectors[i] = all_detectors
        print(f"'.../{name}/', {coord}, REM={rem}: Plot includes detectors {detectors[i]} \
({len(detectors[i])}/{len(all_detectors)}).")

        if meta["RadInstrumentModelName"][0] == "CDB Upgrade":
            e *= 1e3  # given in keV

        if xaxis == "keV":
            e /= 1e3
        elif xaxis == "eV":
            pass
        else:
            print(f"x-axis unit {xaxis} unknown. Please specify 'eV' or 'keV'.\
 Continuing with 'eV'.")
            xaxis = "eV"

        # plot
        if in_one_plot:
            ax = axs[0]
            try:
                label = labels[i] + labelsuffix
            except TypeError:
                label = f"{name} {coord}" + labelsuffix
            newtitle = title + "s"  # plural s
        else:
            ax = axs[i]
            try:
                newtitle = labels[i]
            except TypeError:
                newtitle = f"{title} of '.../{name}/' at {coord}"

        if not average[i]:
            for det in [d for d in detectors[i] if d in all_detectors]:
                label = f"{det} at {coord}" + labelsuffix
                if "Counts" in param:
                    ax.errorbar(e, data[det][param], np.sqrt(data[det][param]),
                                label=label)
                else:
                    ax.errorbar(e, data[det][param], data[det][f"d{param}"],
                                label=label)

            # sort legend alphabetically
            legend_handles, legend_labels = ax.get_legend_handles_labels()
            legend_labels, legend_handles = zip(*sorted(
                    zip(legend_labels, legend_handles), key=lambda t: t[0]))
            ax.legend(legend_handles, legend_labels)
        else:
            p = np.average([data[det][param] for det in detectors[i]
                            if det in all_detectors], axis=0)
            if "Counts" in param:
                dp = np.sqrt(p)
            else:
                dp = np.sqrt(np.average(np.square([data[det][f"d{param}"]
                        for det in detectors[i] if det in all_detectors]), axis=0))

            if norm: # normalize to bulk
                p0, dp0 = p[-1], dp[-1]
                p /= p0
                dp = np.linalg.norm([p*dp0, p0*dp], axis=0)

            if color == None:
                ax.errorbar(e, p, dp, label=label, **kwargs)
            else:
                ax.errorbar(e, p, dp, label=label, color=color[i], **kwargs)
                ax.plot(e, p, color=color[i])

        if dump and np.any(average):
            folder, x, y = meta["Folder"][0], meta["xKey"][0], meta["yKey"][0]
            # strip 'mod/' sub-folder created by data_surgeon
            folder = folder.strip(f"{os.sep}mod")
            name = f"{folder.split(os.sep)[-1]}_REM{int(rem)}_X{int(x)}_Y{int(y)}"
            dumpfile = os.path.join(os.getcwd(), name+".csv")

            with open(dumpfile, "w") as f:
                for ei, pi, dpi in zip(e, p, dp):
                    f.write(f"{ei}, {pi}, 0, {dpi}, 0\n")

            print(f"\tProfile saved to '{dumpfile}' (in VepFit file format).")

        if dump and not np.any(average):
            folder, x, y = meta["Folder"][0], meta["xKey"][0], meta["yKey"][0]
            # strip 'mod/' sub-folder created by data_surgeon
            folder = folder.strip(f"{os.sep}mod")

            for det in [d for d in detectors[i] if d in all_detectors]:
                name = f"{folder.split(os.sep)[-1]}_REM{int(rem)}_X{int(x)}_Y{int(y)}_{det}"
                dumpfile = os.path.join(os.getcwd(), name + ".csv")
                p = data[det][param]
                if "Counts" not in param:
                    dp = data[det][f"d{param}"]
                else:
                    dp = np.sqrt(data[det][param])
                with open(dumpfile, "w") as f:
                    for ei, pi, dpi in zip(e, p, dp):
                        f.write(f"{ei}, {pi}, 0, {dpi}, 0\n")

                print(f"""\tProfile saved to '{dumpfile}' (in VepFit file
                      format).""")

        if title != '' and title != None:
            ax.set_title(newtitle)
        ax.set_xlabel(f"Implantation Energy ({xaxis})")
        if param in ("S", "W"):
            ylabel = f"{param} Parameter"
        elif param == "p2v":
            ylabel = "Valley-to-Peak"
        elif "Counts" in param:
            ylabel = param
        if norm:
            ylabel += " (norm.)"
        ax.set_ylabel(ylabel)

    if grid:
        ax.grid(which="both", axis="both", linewidth=linewidth)

    if in_one_plot:
        # sort legend alphabetically
        legend_handles, legend_labels = ax.get_legend_handles_labels()
        legend_labels, legend_handles = zip(*sorted(
                zip(legend_labels,legend_handles), key=lambda t: t[0]))
        ax.legend(legend_handles, legend_labels)

    if not separate and not in_one_plot:
        # fullscreen if multiple subplots
        fig.set_size_inches(18, 10)

    if return_fig:
        return fig

    if show == True:
        plt.show()

    if save == True:
        for i, fig in enumerate(figures):
            outfile = savename + f"{i}.pdf"
            fig.savefig(outfile)
            print(f"\nPlot saved as: {outfile}")

    return


def depth_profiles(folder, coords=None, param="S", detectors="all",
                   average=False, show=False, save=False, dump=False,
                   width_s=1.0, labelsmanual=None,
                   **kwargs):
    """Import, find and plot depth profiles (wrapper).

    Parameters
    ----------
    coords : list of tuple, list of list of tuple
        List of measured coordinates to choose from. Only necessary if
        coordinates for the same spot differ slightly. List of lists for
        multiple folders.
    param : str
        The desired parameter to be plotted ('S', 'W', 'P2V').
    detectors : list of str
        Detectors to be plotted (or averaged).
    average : bool
        Average over detectors.
    show : bool
        Show the plot.
    save : bool
        Save the plot to a file. Does not work, if show is also True!
    dump : bool
        Write profiles to file (like needed for VepFit).
    **kwargs
        All additional keyword arguments are passed to 'plot_depth_profiles'.
    """

    data, meta = import_folder(folder, keys=None, labdbs_bias=3.3,
                               cdbs_bias=20., width_s=width_s)
    profiles = find_depth_profiles(data, meta, coords=coords)
    plot_depth_profiles(profiles, param=param, detectors=detectors,
                        average=average, show=show, save=save, dump=dump,
                        labelsmanual=labelsmanual,
                        **kwargs)

    return


def map_data_in_2D(pathtodata, outfile='2d_plot',
                   datafilter = ((None, None), (None, None), (None, None)),
                   limits=((None, None), (None, None), (None, None)),
                   controlkeyval=np.nan,
                   keyX='x', keyY='y', keyZ='e', keycontrol=None,
                   detectors=['all'],
                   avg=False, labelx='', labely='', labelz='',
                   title='', markers=False, points=None, outlines=None,
                   use="screen", save=False, show=False, csteps_auto=True,
                   contoursteps=20, linewidths=0.5):
    '''Generates 2-dimensional maps for selected parameters, optimized for x-y
     over S or W parameter plots.

    Parameters
    ----------
    pathtodata : str
        Folder in which the data to be plotted is stored. The data needs to be
        in .n42 format.
    outfile : str
        Name of the outputfile (default is '2d_plot').
    datafilter : tuple
        Give the lower and upper bounds for the parameters given to the
        plotting function in the order: x, y, z. (default is ((None, None),
        (None, None), (None, None)))
    limits : tuple
        Defines the lower and upper limits of the plot in the order of: x, y, z.
        (Default is ((None, None), (None, None), (None, None)))
    controlkeyval : float
        The value to which the controlkey usually "Energy" is set to. (default
        np.nan).
    keyX : str
        Value of the key to parameter x. (Default is 'x')
    keyY : str
        Value of the key to parameter y. (Default is 'y')
    keyZ : str
        Value of the key to parameter z. (Default is 'e')
    keycontrol : str
        Name of the controlkey (default is None).
    detectors : list of str
        A list containing the names of the detectors to be evealuated (default is
        ['all'])
    avg : bool
        Use the average over all detectors where possible (default is False)
    labelx : str
        Label of the x-axis of the plot (default is '').
    labely : str
        Label of the y-axis of the plot (default is '').
    labelz : str
        Label of the x-axis of the plot (default is '').
    title : str
        Title of the plot (default is '')
    save : bool
        Save the plot to a file (Default is False)
    csteps_auto=True,
        Automatically chose the number and size of increments of the parameter z
        (defalt is True)
    contoursteps : int
        Only used if csteps_auto is == True. Then it determines the numer of
        levels of the coulour map (Default is 20)
    '''

    data, metadata = import_folder(pathtodata)
    print("Data has been imported")
    # find relevant files
    data, metadata = filter_data(data, metadata, datafilter, keyX, keyY, keyZ,
                                 keycontrol=keycontrol, controlkeyval=np.nan)

    # generate plot
    x = metadata[keyX]
    y = metadata[keyY]
    if use == "screen":
        plt.style.use('dark_background')
    elif use == "print":
        plt.style.use('bmh')
    else:
        pass
    fig, axs = plt.subplots(ncols=len(detectors), constrained_layout=True)
    fig.suptitle(title)
    #print(data['CAA'].keys())
    if len(detectors) > 1:
        axes = axs.flat
    else:
        axes = [axs]
    for det,ax in zip(detectors, axes):
        z = data[det][keyZ]
        if len(z) < 3:
            print(f"Detector {det} has less than 3 valid measurements")
            break

        if limits[2][0] == None:
            lim_min = min(z)
        else:
            lim_min = limits[2][0]

        if limits[2][1] == None:
            lim_max = max(z)
        else:
            lim_max = limits[2][1]

        print(lim_min, lim_max)
        if csteps_auto == True:
            if keyZ == 'S':
                stepsize = min(data[det]['dS'])
                steps = int((lim_max - lim_min) / stepsize)
            elif keyZ == 'W':
                stepsize = min(data[det]['dW'])
                steps = int((lim_max - lim_min) / stepsize)
            else:
                steps = contoursteps
        else:
            steps = contoursteps

        levels = np.linspace(lim_min, lim_max, steps)
        ax.tricontour(x, y, z, levels=levels, linewidths=linewidths, colors='k')
        cntr = ax.tricontourf(x, y, z, levels=levels, cmap="plasma")

        cbar = fig.colorbar(cntr, ax=ax)
        cbar.set_label(labelz)
        ax.plot(x, y, 'w+', ms=5)
        #ax2.set(xlim=(-2, 2), ylim=(-2, 2))
        ax.set_title(f'detector {det}')
        ax.set_aspect('equal')
        ax.set_xlabel(labelx)
        ax.set_ylabel(labely)
        ax.set_xlim(limits[0][0], limits[0][1])
        ax.set_ylim(limits[1][0], limits[1][1])
        if markers:
            if points is not None:
                for point in points:
                    ax.scatter(point[0], point[1], marker='x', color='w')
            if outlines is not None:
                for outline in outlines:
                    ax.plot(outline[0], outline[1], linestyle='--', color='w')

        print(f"Range of z: [{min(z)}, {max(z)}]")

    #plt.subplots_adjust(hspace=0.5)
    #fig.set_size_inches(10, 4)

    if show == True:
        plt.show()

    if save == True:
        print(f"plot saved as: {outfile}")
        plt.savefig(outfile, bbox_inches='tight', dpi=200)

    return


def filter_data(data, metadata, limits, keyX, keyY, keyZ, keycontrol=None,
                controlkeyval=np.nan):
    """
    """
    #print(metadata)
    if keycontrol != None:
        keylist = [keyX, keyY, keyZ, keycontrol]
        lims = {keyX: limits[0], keyY: limits[1], keyZ: limits[2],
                keycontrol: (controlkeyval*0.9, controlkeyval*1.1)}
        print(f"Using controlkey {keycontrol}")
    else:
        keylist = [keyX, keyY, keyZ]
        lims = {keyX: limits[0], keyY: limits[1], keyZ: limits[2]}

    for key in keylist:
        j = 1
        for lim in lims[key]:
            if lim != None:
                if key in metadata.keys():
                    val = np.array(metadata[key])
                    selector = np.argwhere((j * val) < (j * lim))
                else:
                    selector = np.empty((0), dtype=int)
                    for k in data.keys():
                        val = np.array(data[k][key])
                        selec = np.argwhere((j * val) < (j * lim))
                        selector = np.append([selector], selec)
                    selector = np.unique(selector)
                for tkey in metadata.keys():
                    metadata[tkey] = np.delete(metadata[tkey], selector)
                for det in data.keys():
                    for tkey in data[det].keys():
                        data[det][tkey] = np.delete(data[det][tkey], selector)
                j = -j

    return data, metadata


def import_folder(folders, keys=None, labdbs_bias=3.3, cdbs_bias=20., width_s=1.0, **kwargs):
    import stacs.core.classes
    """Import all measurements in folder and return evaluated single data plus metadata."""

    if type(folders) == str:
        folders = [folders]

    files = []
    for folder in folders: # ordering is important
        files = np.append(files, sorted([os.path.join(folder, f)
                     for f in os.listdir(folder) if f.endswith(".n42")]))

    data, metadata = {}, {"FileName": [], "Folder": []}


    filelist = []
    for i, f in enumerate(files):
        filelist.append((i, Spectrum(f, save_all=False)))
        # Apparently this separate loop is necessary for our caching function.
        # If this loop is not separated from the following the cache will break.
    stacs.core.classes.cache.save_cache()

    for i, spec in filelist:
        # this for-loop is the bottleneck of depth_profiles
        # the import above is pretty fast

        res = spec.single_eval(width_s=width_s)

        # metadata dictionary with lists
        for key in [k for k in spec.metadata if not k == "Spectrum"]:
            if i == 0:
                metadata.update({key: [spec.metadata[key]]})
            else:
                try:
                    metadata[key].append(spec.metadata[key])
                except KeyError:
                    # new key appeared, fill with NaNs
                    metadata.update({key: [float('NaN')] * i
                                           + [spec.metadata[key]]})

            for key in [k for k in metadata.keys()
                        if not k in ("FileName", "Folder")]:
                # fill missing keys with NaNs
                if key not in spec.metadata.keys():
                    metadata[key].append(float('NaN'))

        f = spec.filename
        metadata["FileName"].append(f)
        metadata["Folder"].append(os.sep.join(f.split(os.sep)[:-1]))

        # data dictionary with lists
        for det in res:
            for key in res[det]:
                if i == 0:
                    try:
                        data[det].update({key: [res[det][key]]})
                    except KeyError:
                        data.update({det: {key: [res[det][key]]}})
                else:
                    try:
                        data[det][key].append(res[det][key])
                    except KeyError:
                        try:
                            # new key ?
                            data[det].update({key: [float('NaN')] * i
                                                    + [res[det][key]]})
                        except KeyError:
                            # new detector appeared
                            sample_dict = data[list(data.keys())[0]]
                            data.update({det: {sample_key: [float('NaN')]*i
                                               for sample_key in sample_dict}})
                            try:
                                data[det][key].append(res[det][key])
                            except KeyError:
                                # new det and new key
                                data[det].update({key: [float('NaN')] * i
                                                        + [res[det][key]]})

        for det in data.keys():
            if det not in res: # detector dropped out, fill with NaNs
                for key in data[det]:
                    data[det][key].append(float('NaN'))


    unique_folders, indices= np.unique(metadata["Folder"], return_index=True)
    indices = [*sorted(indices), len(metadata["Folder"])]

    xyz_keys = ["xKey", "yKey", "zKey"]
    metadata.update({key: [0]*len(metadata["Folder"]) for key in xyz_keys})

    if keys == None or not len(keys) == len(unique_folders):
        keys = [None] * len(unique_folders)

    if None in keys:
        for i, sub in enumerate(keys):
            if sub == None:
                sub = [None] * 3
                keys[i] = sub
            if None in sub:
                instr = metadata["RadInstrumentModelName"][indices[i]]
                if "Lab-DBS" in instr:
                    instr_keys = ["K10-H", "K10-V",
                                  "PositronImplantationEnergy"]
                elif instr == "CDB Upgrade":
                    instr_keys = ["PiezoCorrX", "PiezoCorrY",
                                  "PositronImplantationEnergy"]
                else:
                    unsp_keys = ", ".join([keynames[i]
                                for i in range(len(keys)) if keys[i] == None])
                    print(f"Experiment {instr} not supported. Please specify \
keys manually. ({unsp_keys} is/are currently unspecified.)")

                for j, k in enumerate(sub):
                    if k == None:
                        keys[i][j] = instr_keys[j]

    for i, folder in enumerate(unique_folders):
        for xyz_key, key in zip(xyz_keys, keys[i]):
            for j in np.arange(indices[i], indices[i+1], 1):
                try:
                    metadata[xyz_key][j] = float(metadata[key][j])
                    if key=="PositronImplantationEnergy":
                        if "Lab-DBS" in instr:
                            metadata[xyz_key][j] += float(metadata["REM"][j]) \
                                                    + labdbs_bias
                        elif instr == "CDB Upgrade":
                            metadata[xyz_key][j] += cdbs_bias
                except KeyError:
                    pass

    return data, metadata
