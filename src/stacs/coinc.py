import matplotlib.pyplot as plt
import matplotlib.colors
import warnings
import os
import scipy.optimize.optimize
from scipy.optimize import curve_fit
from scipy.stats import linregress
from scipy.optimize import OptimizeWarning
import scipy.odr as so
import math as m
import numpy as np
from pathlib import Path
from .algorithms import gauss, gauss_2d2, gauss_offset


class CoincidenceSpectrum:

    def __init__(self, detpair, hist, window=None, ecal=None, parentname=None,
                 show=False, verbose=False):
        self.detpair = detpair
        self.hist = hist
        self.window = window
        self.ecal = ecal
        self.parentname = parentname
        self.counts = np.sum(self.hist)
        self.show = show
        self.verbose = verbose
        self.export_dir = os.path.join(os.getcwd(), "stacs_projections")

    def plot_histogram(self, array=None, show=True, i_fig=False, col='jet', label=None,
                       alpha=1, contrast=True, cbar=True, title=None, dimensions=None,
                       ext_plot=None, *args, **kwargs):
        from matplotlib import colors
        if array is None:
            array = self.hist
        fig = None

        if not contrast:
            array += 1

        if i_fig:
            fig = plt.figure(self.detpair)
        heat = plt.imshow(array, origin='lower', interpolation='none',
                          norm=colors.LogNorm(vmin=array.min() + 1, vmax=array.max() + 1),
                          cmap=col, alpha=alpha, extent=dimensions)
        if cbar:
            plt.colorbar()
        if type(label) is tuple:
            plt.xlabel(label[0], fontsize=11)
            plt.ylabel(label[1], fontsize=11)
        elif label is None:
            plt.xlabel('Detector 1 channel', fontsize=11)
            plt.ylabel('Detector 2 channel', fontsize=11)

        if title is not None:
            plt.title(title)

        if i_fig:
            return heat
        if ext_plot is not None:
            plt.plot(ext_plot[0], ext_plot[1], label=ext_plot[2], *args, **kwargs)
            plt.legend()
        if show:
            plt.show()

    def simple_projection(self):
        return np.sum(self.hist, axis=0)

    def get_coinc_counts(self):
        return np.sum(self.hist)

    def in_flight_annihilation(self, kernel_area=11):
        import scipy.ndimage as nd
        self.plot_histogram(contrast=True, title='Original Measurement')
        slope, intercept, center = self.roi_position()

        def gaussian_smoothing(arr, dim=15, sigma=0.5):
            arr += 1
            gauss_y = np.linspace(-dim, dim, 2 * dim + 1, endpoint=True)
            gauss_x = np.linspace(-dim, dim, 2 * dim + 1, endpoint=True)
            yy, xx = np.meshgrid(gauss_y, gauss_x, indexing='ij')
            kernel_gauss = np.exp(-sigma * (yy ** 2 + xx ** 2))
            kernel_gauss /= kernel_gauss.sum()
            kernel_area_gauss = kernel_gauss.shape[0]

            sh = arr.shape[0]

            arr_ft = np.fft.fft2(arr)
            kernel = np.zeros_like(arr)  # / (kernel_area ** 2)
            kernel[sh // 2 - kernel_area_gauss // 2: sh // 2 + kernel_area_gauss // 2 + 1,
            sh // 2 - kernel_area_gauss // 2: sh // 2 + kernel_area_gauss // 2 + 1] = kernel_gauss
            kernel = np.fft.ifftshift(kernel)

            kernel_ft = np.fft.fft2(kernel)

            return np.real(np.fft.ifft2(arr_ft * kernel_ft)) - 1

        def rebin(arr, upper_b):
            arr[arr > upper_b] = upper_b
            arr *= (100/arr.max())
            return arr

        dim = self.hist.shape[0]
        #self.plot_histogram(arr_binned[int(dim - dim // 1.3) : dim, 0 : int(dim // 1.3)])
        upper_bin = 1

        arr = self.hist.copy()
        arr = rebin(arr, upper_bin)

        self.plot_histogram(arr, title='Binned', cbar=False)

        arr = gaussian_smoothing(arr, sigma=.01)

        left = 0 - (center[0] * self.ecal[0][1])
        right = 0 + ((arr.shape[1] - center[0]) * self.ecal[0][1])
        bottom = 0 - (center[1] * self.ecal[1][1])
        top = 0 + ((arr.shape[0] - center[1]) * self.ecal[1][1])

        self.plot_histogram(arr, cbar=False, dimensions=[left, right, bottom, top],
                            label=('Detector 1 energy (keV)', 'Detector 2 energy (keV)'))

        rad = np.arctan(np.abs(slope))
        angle = -rad * (180 / np.pi)
        arr = nd.rotate(arr, angle, reshape=True, order=3)

        factor = (np.sin(rad) * self.ecal[1][1] + np.cos(rad) * self.ecal[0][1]) * np.sin(rad)
        ec_x = ((1 - np.abs(slope)) * self.ecal[1][1] + np.abs(slope) * self.ecal[0][1])
        ec_y = ((1 - np.abs(slope)) * self.ecal[0][1] + np.abs(slope) * self.ecal[1][1])
        dim = arr.shape
        center_ix = 1453
        center_iy = 1410
        left = 0 - (center_ix * ec_x) + 0.74
        right = 0 + ((dim[1] - center_ix) * ec_x) + 0.74
        bottom = 0 - (center_iy * ec_y) - 1.25
        top = 0 + ((dim[0] - center_iy) * ec_y) - 1.25

        e_kin = lambda x: np.sqrt((x+511)**2 - 511**2)

        e_x = [e_kin(i) for i in range(-100, 101, 1)]

        label_y = '$\mathrm{E}_1 + \mathrm{E}_2 - 2 \mathrm{m_0 c^2}$ in keV'
        label_x = '$\mathrm{E}_1 - \mathrm{E}_2$ in keV'

        self.plot_histogram(arr, cbar=False, label=(label_x, label_y),
                            dimensions=[left, right, bottom,top],
                            ext_plot=[e_x, range(-100, 101, 1), r'$\Delta$E$_\mathrm{kin}$'],
                            linewidth=1.5, color='black')

    def backgroundcheck(self, test_arr=None, debug=False):  # Compares background of the four quadrants in the heatmap
        quad1 = 0  # top left
        quad2 = 0  # top right
        quad3 = 0  # bottom left
        quad4 = 0  # bottom right
        area = (100 ** 2) * 2

        if test_arr is None:
            arr = self.hist
        else:
            arr = test_arr

        dim = np.shape(arr)[0]

        quad1 += np.sum(arr[dim-320:dim-220, 20:120] +
                        arr[dim-120:dim-20, 220:320])
        quad2 += np.sum(arr[dim-320:dim-220, dim-120:dim-20] +
                        arr[dim-120:dim-20, dim-320:dim-220])
        quad3 += np.sum(arr[20:120, 220:320] + arr[220:320, 20:120])
        quad4 += np.sum(arr[20:120, dim-320:dim-220] +
                        arr[220:320, dim-120:dim-20])

        if debug:
            plt_arr = arr.copy()
            plt_arr[dim-320:dim-220, 20:120] = 0
            plt_arr[dim-120:dim-20, 220:320] = 0
            plt_arr[dim-320:dim-220, dim-120:dim-20] = 0
            plt_arr[dim-120:dim-20, dim-320:dim-220] = 0
            plt_arr[20:120, 220:320] = 0
            plt_arr[220:320, 20:120] = 0
            plt_arr[20:120, dim-320:dim-220] = 0
            plt_arr[220:320, dim-120:dim-20] = 0
            plt.imshow(plt_arr)
            plt.show()

        quad1 /= area
        quad2 /= area
        quad3 /= area
        quad4 /= area
        return quad1, quad2, quad3, quad4

    def coincbackground(self):
        """Calculate the background of coincidence spectra by averaging square regions off the diagonal.

        Parameters
        ----------
        detpair : str, optional
            Detector pair for which the backgrouns should be calculated.

        Returns
        -------
        coincbg : float
            Average background per pixel.
        """

        q1, q2, q3, q4 = self.backgroundcheck()
        coincbg = (q1 + q4) / 2
        return coincbg

    def energyres(self, debug=False, ret_cha=False, roi_params=None, *args, **kwargs):
        """This function takes a coincidence data sprectrum and calculates the coincidence energy resolution.
        In a coincidence spectrum the constant energy diagonal at 1022 keV contains the doppler broadened energies along
        its length. In an ideal world this line would be very thin, being only broadened by the binding energy of the
        atomic orbitals. In reality the diagonal is broadened due to finite detector resolution. As a result the detector
        resolution can be extracted from a coincidence spectrum. The resulting resolution is a combination of the energy
        resolutions of both detectors and about 1/sqrt(2) better than the individual detector resolution.

        Parameters
        ----------
        array : np.array
            2D numpy array containing coincidence data.
        ecal : tuple, optional
            Energy calibration as a tuple (default is None).
        debug : bool, optional
            If True a plot of the simple projection.
        ret_cha : bool, optional
            If True the FWHM will be returned in channels, not keV.

        Returns
        -------
        fwhm : float
            FWHM of the coincidence energy resolution.
        """
        import scipy.ndimage as nd
        from scipy.optimize import curve_fit
        
        array = self.hist.copy()

        if not debug:
            debug=self.show

        if roi_params is None:
            slope, intercept, center = self.roi_position(*args, **kwargs)
        else:
            slope, intercept, center = roi_params
        rad = np.arctan(1 / np.abs(slope))
        angle = rad * (180 / np.pi)
        arr_rot = nd.rotate(array, angle, reshape=False, order=3)
        arr_map = arr_rot >= 0
        arr_rot = arr_rot * arr_map
        cent_n = np.unravel_index(np.argmax(arr_rot), arr_rot.shape)
        res1_a = arr_rot[cent_n[0] - 2:cent_n[0] + 3, :]
        res1 = np.sum(res1_a, axis=0)
        p_in = [np.max(res1), np.argmax(res1), 30]
        opt, pas = curve_fit(gauss, range(len(res1)), res1, p_in)

        fwhm = 2 * np.sqrt(np.log(2) * 2) * opt[2]

        error = (2 * np.sqrt(np.log(2) * 2)) * pas[2, 2]

        if ret_cha:
            return fwhm, error

        vals = fwhm

        fwhm *= (np.sin(rad) * self.ecal[1][1] + np.cos(rad) * self.ecal[0][1]) * np.sin(rad)
        error = (2 * np.sqrt(np.log(2) * 2) *
                         (np.sin(rad) * self.ecal[1][1] + np.cos(rad) * self.ecal[0][1]) *
                         np.sin(rad)) * pas[2, 2]
          # FWHM in keV

        if debug:
            factor = (np.sin(rad) * self.ecal[1][1] + np.cos(rad) * self.ecal[0][1]) * np.sin(rad)
            ec_x = ((1 - np.abs(slope)) * self.ecal[1][1] + np.abs(slope) * self.ecal[0][1])
            ec_y = ((1 - np.abs(slope)) * self.ecal[0][1] + np.abs(slope) * self.ecal[1][1])
            dim = array.shape
            center_ix = int(center[0])
            center_iy = int(center[1])
            left = 0 - (center_ix * ec_x) + 0.74
            right = 0 + ((dim[1] - center_ix) * ec_x) + 0.74
            bottom = 0 - (center_iy * ec_y) - 1.25
            top = 0 + ((dim[0]- center_iy) * ec_y) - 1.25

            label_x = '$\mathrm{E}_1 + \mathrm{E}_2 - 2 \mathrm{m_0 c^2}$ in keV'
            label_y = '$\mathrm{E}_1 - \mathrm{E}_2$ in keV'

            center = opt[1]

            x = [(i - np.argmax(res1)) * factor for i in range(len(res1))]
            plt.figure(1)
            self.plot_histogram(arr_rot, show=False, dimensions=[left, right, bottom, top],
                                label=(label_x, label_y), contrast=False)
            plt.show()
            plt.figure(2)
            self.plot_histogram(res1_a[:,cent_n[1] - 100:cent_n[1] + 100], show=False)
            plt.show()
            plt.figure(3)
            plt.axvspan(0 - (vals / 2) * factor, 0 + (vals / 2) * factor,
                        color='orange', alpha=0.4, label='FWHM')
            plt.scatter(x, res1/np.max(res1), color='crimson', label='data')
            plt.plot(x, np.array([gauss(i, *opt) for i in range(len(res1))])/np.max(res1),
                     color='navy', label='fit')
            plt.xlabel('energy (keV)', fontsize=11)
            plt.ylabel('normalized counts', fontsize=11)
            plt.xlim(left=-4, right=4)
            plt.legend(fontsize=11)
            plt.show()

        return fwhm, error

    def roi_position(self, preset=0, n_fixpoints=5, spacing=20, halfwidth=20, debug=False, fine_debug=False,
                     *args, **kwargs):
        """This function calculates the center and slope of the coincident data.
        Function: getmid(array: {np.array([1024, 1024])}, debug: {bool}=False, fine_debug: {bool}=False)"""

        if not fine_debug:
            fine_debug = self.verbose
        if not debug:
            debug = self.show

        if preset == 1:
            halfwidth = 10
            n_fixpoints = 4
            spacing = 15

        if preset == 2:
            halfwidth = 10
            n_fixpoints = 4
            spacing = 10


        if preset == 3:
            print('Using orbitals preset for slope finding.')
            halfwidth = 20
            n_fixpoints = 2
            spacing = 55

        warnings.simplefilter("error", OptimizeWarning)

        ylist = np.sum(self.hist[:, 5:55], axis=1)
        xlist = np.sum(self.hist[5:55, :], axis=0)

        yrange = np.arange(np.argmax(ylist) - 150, np.argmax(ylist) + 150, 1)
        xrange = np.arange(np.argmax(xlist) - 150, np.argmax(xlist) + 150, 1)

        ylist = np.sum(self.hist[yrange, 5:55], axis=1)
        xlist = np.sum(self.hist[5:55, xrange], axis=0)

        if debug or self.show:
            plt.figure(10)
            plt.plot(ylist, color='navy', label='y find mid')
            plt.plot(xlist, color='crimson', label='x find mid')
            plt.title(f'Finding Center with Compton Tails {self.detpair}')
            plt.show()

        px = (max(xlist), xrange[np.argmax(xlist)], 15)
        py = (max(ylist), yrange[np.argmax(ylist)], 15)

        xfit = None
        yfit = None

        try:
            xfit = curve_fit(gauss, xrange, xlist, px)
            yfit = curve_fit(gauss, yrange, ylist, py)
        except OptimizeWarning:
            print('The gauss fitting of the background failed. Hence, the middle of the spectrum cannot be found. '
                  '\nPlease verify data.')
            quit()

        xpopt, xppas = xfit
        ypopt, yppas = yfit

        xmidf = xpopt[1]
        ymidf = ypopt[1]

        xmid = np.rint(xpopt[1])
        ymid = np.rint(ypopt[1])

        if debug or self.verbose:
            print(xpopt)
            print(ypopt)

        # 2. Calculation of the tilt of the photo peak
        # center = xmidf, ymidf
        # print(center)

        #   First Gauß at +/-60

        fit_x = np.arange(- halfwidth, halfwidth + 1, 1)
        yslopepts = []
        xslopepts = []
        deviation = []
        fixpoints = list(range(n_fixpoints, -n_fixpoints - 1, -1))

        for i in fixpoints:
            offset = i * spacing
            yvals1mid = int(ymid) - offset
            xvals1mid = int(xmid) + offset
            vals1 = []

            for j in fit_x:
                vals1.append(self.hist[yvals1mid + j, xvals1mid + j])

            p1v = (max(vals1[15:35]), 0, 5)
            fit1 = None

            try:
                fit1 = curve_fit(gauss, fit_x, vals1, p1v, maxfev=2000)
            except OptimizeWarning:
                if preset == 0 or preset == 1:
                    print(f"""Gauss fitting of one of the broadened peak boundaries failed. 
    Adjusting Preset, current: {preset}""")
                    return self.roi_position(preset=preset+1, debug=debug, fine_debug=fine_debug, *args, **kwargs)
                elif preset == 2:
                    print('Could not fit one of the peak boundaries, likely not enought counts in spectrum')
                    quit()

            popt1, ppas1 = fit1
            # print(popt1)
            deviation.append(popt1[1])
            # xslopepts.append(xmidf + offset)
            # yslopepts.append(popt1[1] * np.sqrt(2) - offset + ymidf)
            xslopepts.append(popt1[1] + offset + xmidf)
            yslopepts.append(popt1[1] - offset + ymidf)
            # includes correction for inversion of y axis in array

        slope, intercept, r, p, std = linregress(xslopepts, yslopepts)

        # recalculate middle
        '''
        x_fit_mid = [ix for ix in range(440, 611, 1)]
        y_fit_mid = [ix * slope + intercept for ix in x_fit_mid]
        z_fit_mid = []
        for i in range(len(x_fit_mid)):
            z_fit_mid.append(array[int(y_fit_mid[i]), int(x_fit_mid[i])] +
                             array[int(y_fit_mid[i]) + 1, int(x_fit_mid[i]) + 1] +
                             array[int(y_fit_mid[i]) - 1, int(x_fit_mid[i]) - 1])

        fit_mid = curve_fit(gauss, x_fit_mid, z_fit_mid, (max(z_fit_mid), 520, 40), maxfev=200)
        opt, pas = fit_mid

        if fine_debug:
            y = []
            for i in x_fit_mid:
                y.append(gauss(i, opt[0], opt[1], opt[2]))
            plt.scatter(x_fit_mid, z_fit_mid)
            plt.plot(x_fit_mid, y)
            plt.show()
            print(opt)
            print(pas)

        #'''

        x, y = np.meshgrid(xrange, yrange)
        mid_init = (self.hist.max(), xmidf, ymidf, 25, 10, np.pi / 4)

        # opt, pas = curve_fit(gauss_2d, (x, y), self.hist[450:601, 450:601].reshape(22801), mid_init)
        opt, pas = curve_fit(gauss_2d2, (x, y),
                             self.hist[yrange[0]:yrange[-1] + 1, xrange[0]:xrange[-1] + 1].reshape(
                                 len(yrange) * len(xrange)), mid_init)

        perr = np.sqrt(np.diag(pas))

        a, x_0, y_0, sig_x, sig_y, theta = opt

        new_slope = -np.tan(theta)
        if new_slope > 0:
            new_slope = np.tan(theta)
        new_intercept = y_0 - new_slope * x_0

        x_mid = opt[1]
        y_mid = opt[2]

        intercept = y_mid - (slope * x_mid)

        center = x_mid, y_mid

        if fine_debug:
            print('Fit params (errors below): ')
            print(opt)
            print(perr)
            print()
            print('Gauss slope: ', new_slope)
            print('Linear slope: ', slope)
            print('Gauss y_int: ', new_intercept)
            print('Linear y_int: ', intercept)
            print('Center:', center)

        if debug:
            self.plot_histogram(show=False, *args, **kwargs)
            plt.scatter(xslopepts, yslopepts, color='black', label='fit centers')
            y_old = []
            y_new = []
            for i in range(200, self.hist.shape[0]-200, 1):
                y_old.append(intercept + slope * i)
                y_new.append(new_intercept + new_slope * i)
            plt.plot(range(200, self.hist.shape[0]-200, 1), y_old, color='red', label='final slope')
            plt.plot(range(200, self.hist.shape[0]-200, 1), y_new, color='green', label='2D-Gauss slope')
            # plt.scatter([5, xmidf, xmidf], [ymidf, 5, ymidf], color='white')
            plt.scatter(x_mid, y_mid, color='white', label='center')
            plt.title(f'Roi Position Fit {self.detpair}')
            plt.legend()
            plt.show()

            print('  slope:', slope, '  y axis intercept:', intercept, ' std:', std)

        return slope, intercept, center

    def cut_roi(self, bg, w=15, debug=False, debug_plot=False, offset=0, *args, **kwargs):
        """ This function only determines the ROI region as an aliased rectangle and cuts it out of the heatmap.
        Function calcroi(arr: {np.array([1024, 1024])}, w: {int}=15). Takes input array and ROI width in pixels."""

        if not debug:
            debug = self.verbose
        if not debug_plot:
            debug_plot = self.show

        arr = self.hist.copy()
        
        # print(args, kwargs)

        slope, intercept, center = self.roi_position(*args, **kwargs)
        slope_rad = m.atan(slope)

        if bg is None:
            bg_matrix = np.zeros(arr.shape)
        else:
            bg_matrix = np.full(arr.shape, bg, float)

        # First find the Midpoint on the linear fit
        slope2 = (-1 / slope)
        intercept2 = center[1] - (slope2 * center[0])  # Perpendicular to slope line
        x_m = (intercept2 - intercept) / (slope - slope2)
        y_m = slope * x_m + intercept
        # fitcenter = (x_m, y_m)
        # print('center: ', fitcenter, 'slobe angle: ', slope_rad, 'slope', slope)

        array = arr.copy()
        intercept_off = intercept
        intercept -= offset
        intercept_u = intercept + (w / m.cos(slope_rad))
        intercept_l = intercept - (w / m.cos(slope_rad))

        def inv_upperline(y_in):
            return (y_in - intercept_u) / slope

        def inv_lowerline(y_in):
            return (y_in - intercept_l) / slope

        for y in range(array.shape[0]):
            for x in range(array.shape[1]):
                # y values of the border lines at current column
                upper_l = slope * (x - 0.5) + intercept_u  # upper left
                upper_r = slope * (x + 0.5) + intercept_u  # upper right
                lower_r = slope * (x + 0.5) + intercept_l  # lower right
                lower_l = slope * (x - 0.5) + intercept_l  # lower left

                #  all pixels outside the ROI are set to 0
                if upper_l < y - 0.5 or lower_r > y + 0.5:
                    array[y][x] = 0
                    bg_matrix[y][x] = 0

                #  pixels intercepting the upper ROI border are being multiplied by the percentage area
                #  that lies inside the ROI
                elif upper_l + 0.5 > y > upper_r - 0.5:
                    if upper_l > y + 0.5:
                        if upper_r > y - 0.5:
                            a = (x + 0.5 - inv_upperline(y + 0.5))
                            b = (y + 0.5 - upper_r)
                            area = 1 - (a * b * 0.5)
                        else:
                            a = inv_upperline(y - 0.5) - inv_upperline(y + 0.5)
                            c = x + 0.5 - inv_upperline(y - 0.5)
                            area = 1 - (a * 0.5 + c)
                    else:
                        if upper_r > y - 0.5:
                            a = upper_l - upper_r
                            c = y + 0.5 - upper_l
                            area = 1 - (a * 0.5 + c)
                        else:
                            a = upper_l - (y - 0.5)
                            b = inv_upperline(y - 0.5) - (x - 0.5)
                            area = a * b * 0.5

                    array[y][x] *= area
                    bg_matrix[y][x] *= area

                #  pixels intercepting the lower ROI border are being multiplied by the percentage area
                #  that lies inside the ROI
                elif lower_l + 0.5 > y > lower_r - 0.5:
                    if lower_l > y + 0.5:
                        if lower_r > y - 0.5:
                            a = (x + 0.5 - inv_lowerline(y + 0.5))
                            b = (y + 0.5 - lower_r)
                            area = a * b * 0.5
                        else:
                            a = inv_lowerline(y - 0.5) - inv_lowerline(y + 0.5)
                            c = x + 0.5 - inv_lowerline(y - 0.5)
                            area = a * 0.5 + c
                    else:
                        if lower_r > y - 0.5:
                            a = lower_l - lower_r
                            c = y + 0.5 - lower_l
                            area = a * 0.5 + c
                        else:
                            a = lower_l - (y - 0.5)
                            b = inv_lowerline(y - 0.5) - (x - 0.5)
                            area = 1 - (a * b * 0.5)

                    array[y][x] *= area
                    bg_matrix[y][x] *= area

        # Returns in order:     Array trimmed to ROI, slope of the ROI, perpendicular slope to ROI, y intercept of
        #                       linear trough ROI center, y intercept of upper ROI boundary, y intercept of lower ROI
        #                       boundary, coordinates of center of ROI.
        if debug:
            print(slope, intercept, intercept_u, intercept_l)

        if debug_plot:
            self.plot_histogram(array=arr)

            x = np.linspace(0, arr.shape[1], 5)
            y_m = x.copy() * slope + intercept
            y_l = x.copy() * slope + intercept_l
            y_u = x.copy() * slope + intercept_u
            y_tan = x.copy() * slope2 + intercept2
            print(y_u)
            plt.plot(x, y_m, color='black', alpha=0.8)
            plt.plot(x, y_tan, color='black', alpha=0.8)
            plt.plot(x, y_l, color='navy', alpha=0.5)
            plt.plot(x, y_u, color='navy', alpha=0.5)
            plt.show()

        return array, slope, intercept_off, slope2, intercept2, intercept_u, intercept_l, (x_m, y_m), bg_matrix

    def binning(self, binfile='Bins100.txt', norm=0, width_mult=1,
                width=None, background_corr=False, export=False,
                mirrored=True, orbit_offset=0, debug=False, debug_anim=False,
                justsum=False, *args, **kwargs):
        """This function supervises the binning process of coincidence spectra. The actual cutting is performed elsewhere.

        This function performs important operations before and after the binning of spectrum.
        First the calcroi function is called, with cuts the roi section out of the coincidence spectrum.
        The energy calibration associated with the measurement is used to determine the locations of the bin boundaries.
        The function shortest from stacs.core.algorithms is then called to bin the sprectrum.
        The function returns the finished projection.

        Parameters
        ----------
        measurement : stacs.core.classes.Spectrum object
            Measurement object containing the coincidence data.
        binfile : str, optional
            Filename of the binfile used to determine the boundaries for the bins located in stacs/data/bins
            (default is 'Bins100.txt').
        detectorpair : str, optional
            Detectorpair to be used for evaluation. By default Spectrum.bettercoinc will be used (default is None).
        norm : int, optional
            Method used to normalize the spectrum, for ratiocurves use default (default is 0).
        width : float, int, optional
            Width of the Region of Interest, by default the FWHM of the coincidence energy resolution will be used
            (default is None)
        background_corr : bool, optional
            If True a constand background calculated from the spectrum will be subtracted. This can increase accuracy for
            high broadening momenta (default is False)
        export : bool, optional
            If True the data will be saved in a text file (default is False)
        mirrored : bool, optional
            If true the roughly symmetrical doppler boradening beak will be mirrored to one side, optimal for ratio curves
            (default is True)
        orbit_offset : int, float, optional
            This parameter will offset the Region of Interest to lower sum energies, required to analyse the electronical
            structure (default is 0)
        debug : bool, optional
            Debug parameter, if True the heatmap will be displayed showing the bin channels and the cut spectrum
            (default is False)
        debug_anim : bool, optional
            Debug parameter, if True a animation of the Roi cutting process will be shown (default is False).
        args : tuple, optional
            Will be passed on to calcroi and subsequently getmid.
        kwargs : dict, optional
            Will be passed on to calcroi and subsequently getmid.
        Raises
        ------
        KeyError
            If no energy calibration is specified the process will be aborted.

        Returns
        -------
        d_bin_n : np.array
            Energy data from projection
        projection : np.array
            Counts from projection
        div : np.array
            Error of count rate.
        """

        import time

        if not debug:
            debug=self.show

        background = self.coincbackground()

        t1 = time.time()

        if width is None:
            # calculate width from ecal
            width, _ = self.energyres(*args, **kwargs)

            width /= self.ecal[1][1]

            width *= width_mult

        (array, slope, intercept, slope2, intercept2, old_u, old_l,
         center, bg_matrix) \
            = self.cut_roi(background, w=width, offset=orbit_offset, *args, **kwargs)
        if justsum:
            return np.sum(array)
        if self.verbose:
            print(np.sum(array))
            msg = ('------------------------------------------------------------------',
                  f'Measurement: {self.parentname}, Detpair: {self.detpair}',
                   '------------------------------------------------------------------',
                   'Center found, ROI calculated successfully.',
                  f'Starting binning with bins from file: "{binfile}"')
            print(msg)
            if background_corr:
                print('Background Correction Active.')

        # BINNING

        x_m, y_m = center
               
        y_cal = self.ecal[1][1]

        y_axis_corr = [y_m - (y_cal * i) for i in range(550, -550, -1)]

        y_dist = y_axis_corr[-1] - y_axis_corr[0]
        m_corr = y_dist / len(y_axis_corr)

        b_corr = 511 - m_corr * y_m

        binpath = Path('./data/bins')
        binnings = open((Path(__file__).parent.absolute() / binpath / binfile), 'r')
        bins = []

        # intercept2 += 15

        for line in binnings:
            tmp = line.replace(',', '.')
            tmp = float(tmp) * 1000
            bins.append(tmp)

        binnings.close()
        d_bin = []

        for val in range(len(bins) - 1):
            d_bin.append(bins[val + 1] - bins[0])

        d_bin_np_r = d_bin.copy()
        d_bin_np_r.reverse()

        d_bin_n = [i * (-1) for i in d_bin]

        d_bin_np = d_bin_np_r + d_bin_n.copy()

        d_bin_np = np.array(d_bin_np)

        d_bin_np /= 1000
        d_bin_np += 511

        d_bin_y = np.array([(i - b_corr) / m_corr for i in d_bin_np])
        d_bin_x = np.array([(i - intercept) / slope for i in d_bin_y])

        d_intercept = [(d_bin_y[i] - slope2 * d_bin_x[i]) * (1) for i in range(len(d_bin_y))]
        d_intercept.reverse()
        # return d_intercept

        # d_intercept = [(ij * np.sqrt(2)) / 44 for ij in d_bin]  # FIX
        # d_intercept = [ij * (4.4/(61 + 62.44)) for ij in d_bin]
        # return d_intercept
        # d_intercept = [(ij * 62.44 * np.tan(46 * (np.pi/180))) for ij in d_bin]
        if self.verbose:
            print('Total events inside ROI: ', np.sum(array))

        countsum, bg_sum = self.shortest(array, intercept2, d_intercept, slope, slope2, old_l, old_u, bg_matrix,
                                    debug_anim, debug)

        d_bin2 = [i * (- 1) for i in d_bin]
        d_bin.reverse()
        d_bin += d_bin2
        de_bin = [(abs(d_bin[i] - d_bin[i + 1])) / 1000 for i in range(len(d_bin) - 1)]
        d_bin_n = [(d_bin[i] - 0.5 * abs(d_bin[i] - d_bin[i + 1])) / 1000 for i in range(len(d_bin) - 1)]

        de_bin = np.array(de_bin)

        t2 = time.time()
        benchmark = t2 - t1

        totcounts = 1

        if norm == 0:
            totcounts = sum(countsum)
        elif norm == 1:
            totcounts = sum(countsum * de_bin)

        error = []

        for ij in range(len(countsum)):
            if background_corr:
                countsum[ij] -= bg_sum[ij]
                error.append(np.sqrt(countsum[ij] + bg_sum[ij]))
            else:
                error.append(np.sqrt(countsum[ij]))
            if norm != 3:
                countsum[ij] /= totcounts
                error[ij] /= totcounts
            d_bin_n[ij] += 511

        countsum /= de_bin
        error /= de_bin
        bg_sum /= de_bin
        # projection = countsum.copy()

        if mirrored:

            left = np.flip(countsum[0:len(countsum) // 2])
            right = countsum[len(countsum) // 2 + 1:len(countsum)]
            center = countsum[len(countsum) // 2:len(countsum) // 2 + 1] * 2
            projection = np.append(center, left + right)

            left_e = np.flip(error[0:len(error) // 2])
            right_e = error[len(error) // 2 + 1:len(error)]
            center_e = error[len(error) // 2:len(error) // 2 + 1] * 2
            div = np.append(center_e, left_e + right_e)

            d_bin_n = d_bin_n[0:len(projection)]
        else:
            projection = countsum
            div = error
        d_bin_n.reverse()
        if self.verbose:
            print('Calculation run time in [s]: ', benchmark, '\n')

        if export:
            name = self.parentname
            print(self.export_dir)
            if not os.path.isdir(self.export_dir):
                os.mkdir(self.export_dir)
            if self.parentname is None:
                import datetime
                date = datetime.datetime.now()
                name = f"{date.year}_{date.month}_{date.day}_{date.hour}{date.minute}{date.second}_"
            filepath = self.export_dir + name + f"{self.detpair[0]}{self.detpair[1]}.csv"
            with open(filepath, 'w') as file:
                file.write(f"Energy(keV),Counts(norm),Error\n")
                for i in range(len(d_bin_n)):
                    file.write(f"{d_bin_n[i]},{projection[i]},{div[i]}\n")
            print(f"Projection saved to file: {filepath}")

        return d_bin_n, projection, div

    def shortest(self, array, intercept2, d_intercept, slope, slope2, old_l, old_u, bg_matrix, animation=False,
                 debug=False):
        '''
        This method performs the interpolation of counts in pixels that intercept the energy bin lines provided by your
        bin file.

        The shortest method used the distance from the center of the pixel to the boundary line to calculate the faction
        of the pixel that liese inside the energy bin.

        Parameters
        ----------
        array : np.array
            2D Array containing the already pre-cut region of interest.
        intercept2 : float
            To be removed...
        d_intercept : list of float
            A list with the y-axis intercept values of the energy bin boundaries.
        slope : float
            The slope of the CEL.
        slope2 : float
            The slope orthogonal to the CEL, i.e. the slope of the boundary lines of the energy bins.
        old_l : float
            The y-axis intercept of the upper boundary line of the region of interest.
        old_u : float
            The y-axis intercept of the lower boundary line of the region of interest.
        bg_matrix : np.array
            2D Array containing the background to be subtracted from the spectrum.
        animation : bool, optional
            Graphical debugging option that provides animation of the cutting of the spectrum.
        debug : bool, optional
            Basic graphical debugging option displaying the boundary lines of the energy bins in the spectrum.

        Returns
        -------
        countsum : list of int
            List containing the binned counts.
        bg_sum : list of int
            List containing the background counts for each energy bin to be subtracted later.

        '''
        if animation:
            import matplotlib.animation as animation
            heat = plt.imshow(array, origin='lower', interpolation=None,
                              norm=matplotlib.colors.LogNorm(vmin=array.min() + 1, vmax=array.max() + 1), cmap='jet')
            imlist = []
        count = 20
        countsum = []
        bg_sum = []
        subarray = array.copy()
        if debug:
            plt.subplot(111)

        for ch in range(len(d_intercept) - 1):

            cache = 0.
            cache_bg = 0.

            # crop testing region to improve peformance
            intercept_l = d_intercept[ch]
            intercept_u = d_intercept[ch + 1]

            x_min = int((intercept_l / slope2 - old_l / slope) / (1 / slope2 - 1 / slope)) - 3
            x_max = int((intercept_u / slope2 - old_u / slope) / (1 / slope2 - 1 / slope)) + 3

            y_min = int((old_l - intercept_u) / (slope2 - slope)) - 2
            y_max = int((old_u - intercept_l) / (slope2 - slope)) + 2

            # correct for x/y_min/max out ouf bounds
            array_dim = array.shape[0]
            y_min = max(0, y_min)
            y_max = min(array_dim, y_max)
            x_min = max(0, x_min)
            x_max = min(array_dim, x_max)

            fraclist = []

            for x in range(x_min, x_max, 1):
                '''
                x_min = int(((y - old_l) / slope) - 3)
                x_max = int(((y - old_u) / slope) + 3)
                '''

                for y in range(y_min, y_max, 1):
                    d_u = (abs(slope2 * y - x + intercept_u)) / (
                        np.sqrt(slope2 ** 2 + 1))  # Distance of current pixel to upper line
                    d_l = (abs(slope2 * y - x + intercept_l)) / (
                        np.sqrt(slope2 ** 2 + 1))  # Distance of current pixel to lower line

                    dist = np.cos(np.arctan(slope2)) * (intercept_u - intercept_l)
                    # perpendicular distance between the two bin boundaries

                    if d_u > dist + np.sqrt(2) / 2 or d_l > dist + np.sqrt(2) / 2:
                        pass

                    # '''
                    elif d_u > np.sqrt(2) / 2 and d_l > np.sqrt(2) / 2:
                        cache += array[x, y]
                        cache_bg += bg_matrix[x, y]
                        if debug or animation:
                            subarray[x, y] = 0

                    elif d_u < np.sqrt(2) / 2:
                        frac = - d_u ** 2 + np.sqrt(2) * d_u
                        fraclist.append(frac)
                        if d_l >= dist:
                            frac = 0.5 - frac
                        else:
                            frac = 0.5 + frac
                        cache += array[x, y] * frac
                        cache_bg += bg_matrix[x, y] * frac
                        if debug or animation:
                            subarray[x, y] -= frac * array[x, y]

                    elif d_l < np.sqrt(2) / 2:
                        frac = - d_l ** 2 + np.sqrt(2) * d_l
                        fraclist.append(frac)
                        if d_u >= dist:
                            frac = 0.5 - frac
                        else:
                            frac = 0.5 + frac
                        cache += array[x, y] * frac
                        cache_bg += bg_matrix[x, y] * frac
                        if debug or animation:
                            subarray[x, y] -= frac * array[x, y]

            bg_sum.append(cache_bg)
            countsum.append(cache)

            if debug:
                x1 = [x_r for x_r in np.arange(y_min, y_max + 20)]
                x2 = [x_r for x_r in np.arange(y_min, y_max + 20)]
                y1 = [y_r * slope2 + intercept_u for y_r in x1]
                y2 = [y_r * slope2 + intercept_l for y_r in x2]
                plt.plot(x1, y1, color='black', linewidth=.5)
                plt.plot(x2, y2, color='black', linewidth=.5)

            if animation:
                fig = plt.figure(1)
                ax = fig.add_subplot(111)

                heat.set_data(subarray)

                # cbar = ax.colorbar(heat)
                # '''
                plt.xlim(left=0, right=1024)
                plt.ylim(bottom=0, top=1024)
                plt.xlabel('Detector 1 channel')
                plt.ylabel('Detector 2 channel')
                # heatmap(array)
                count -= 1
                x1 = [x_r for x_r in np.arange(y_min, y_max + 20)]
                x2 = [x_r for x_r in np.arange(y_min, y_max + 20)]
                y1 = [y_r * slope2 + intercept_u for y_r in x1]
                y2 = [y_r * slope2 + intercept_l for y_r in x2]
                x_diag = [i for i in range(0, 1024)]
                y_diag_u = [x_diag[i] * slope + old_u for i in range(len(x_diag))]
                y_diag_l = [x_diag[i] * slope + old_l for i in range(len(x_diag))]
                # ax.plot(x_diag, y_diag_l, color='black', linewidth=.5)
                # ax.plot(x_diag, y_diag_u, color='black', linewidth=.5)
                ax.plot(x1, y1, color='black', linewidth=.5)
                ax.plot(x2, y2, color='black', linewidth=.5)
                fig.canvas.draw()
                plt.pause(0.2)

                fig.canvas.flush_events()
                # '''

        if animation:
            print(slope, slope2)
            print(np.sum(subarray))
        if debug:
            self.plot_histogram(array=array, i_fig=False)
            #plt.subplot(122)
            #self.plot_histogram(array=subarray, i_fig=False)
            plt.show()
        return countsum, bg_sum

    def __add__(self, other):
        if self.ecal is None or other.ecal is None:
            pass
        elif self.ecal != other.ecal:
            print('The energy calibration of the added measurements is not identical!')
            print(f'{self.ecal} vs. {other.ecal}')
        if self.window != other.window:
            print('The coincidence window of the added measurements is not identical!')
            print(f'Pair {self.detpair} will not be added.')
            return None
        if np.shape(self.hist) == np.shape(other.hist):
            return CoincidenceSpectrum(self.detpair, self.hist+other.hist, window=self.window,
                                       ecal=self.ecal, parentname=self.parentname)
        else:
            print(f'Added coincidence spectra have different dimensions, skipping detpair {self.detpair}!')
            return None

    def orbitals(self, width, bins='Bins500.txt', binsum=5, detector=None, debug=False, fit_gauss=False,
                 *args, **kwargs):
        from scipy.optimize import curve_fit
        import warnings

        rois = [width * 2 * i for i in range(-15, 16)]
        projections = []
        errors = []
        energy = None

        for offs in rois:
            e, proj, err = self.binning(binfile=bins, orbit_offset=offs, width=width,
                                   mirrored=False, norm=3, *args, **kwargs)
            projections.append(proj)
            errors.append(err)
            if offs == 0:
                energy = e

        projections = np.array(projections, dtype=np.float)
        #projections = np.flip(projections, axis=1)
        errors = np.array(errors)
        centers = []
        x = np.arange(0, projections.shape[0])
        y = np.arange(0, projections.shape[1])

        warnings.filterwarnings('ignore')

        # for i in range(projections.shape[1]):
        #    fac = np.sum(projections[:,i])
        #    if fac != 0:
        #        projections[:,i] /= fac

        if fit_gauss:
            for en in range(binsum, projections.shape[1] - (binsum)):
                opt = [0, 0, 0, 0, 0, 0]
                try:
                    if binsum != 0:
                        sl = np.sum(projections[:, en - binsum:en + binsum + 1], axis=1) / binsum
                    else:
                        sl = projections[:, en]

                    opt, pas = curve_fit(gauss, x, sl, [np.max(sl), 8, 8])
                except RuntimeError:
                    print(en)
                centers.append([opt[1], 0])
        else:
            for en in range(binsum, projections.shape[1] - binsum):
                sl = np.sum(projections[:, en - binsum:en + binsum + 1], axis=1) / binsum
                centers.append([np.sum(sl * np.arange(1, len(sl) + 1)) /
                                np.sum(sl), 0])

        centers = np.array(centers)
        if debug:
            plotlist = [10, 16, 26]
            collist = ['crimson', 'orange', 'blue', 'black', 'green']
            plt.figure(1)
            for i, plot in enumerate(plotlist):
                plt.semilogy(x, projections[:, plot], label=f'{plot}', color=collist[i])
                plt.errorbar(x, projections[:, plot], errors[:, plot],
                             color=collist[i], linestyle='None', capsize=2, elinewidth=1)

            plt.title('Diagonal')
            plt.legend()
            plt.show()

            plt.figure(2)
            plotlist2 = [10, 16, 26]
            for j, plot in enumerate(plotlist2):
                plt.semilogy(y, projections[plot, :])


        print(projections.shape)
        center_y = [i for i in range(projections.shape[0])]
        return projections, centers, energy[binsum:len(energy) - binsum], energy

    def orbitals_new(self, debug=False, fine_debug=False):
        import scipy.ndimage as nd
        roi, slope, intercept, _, _, _, _, _, _ = self.cut_roi(None, w=30, preset=3)

        #self.plot_histogram(array=roi)

        rad = np.arctan(slope)
        angle = rad * (180 / np.pi)
        roi = nd.rotate(roi, angle, reshape=False, order=1)

        #self.plot_histogram(array=roi)

        proj_y = np.sum(roi, axis=1)
        roi = roi[proj_y>0, :]
        print(roi.shape)
        proj_x = np.sum(roi, axis=0)
        proj_y = np.sum(roi, axis=1)

        fit_x, pas_x = curve_fit(gauss, range(len(proj_x)), proj_x, p0=[np.max(proj_x), np.argmax(proj_x), 40])
        fit_y, pas_y = curve_fit(gauss, range(len(proj_y)), proj_y, p0=[np.max(proj_y), np.argmax(proj_y), 40])
        plt.scatter(range(len(proj_y)), proj_y)
        plt.plot([gauss_offset(i, fit_y[0], fit_y[1], fit_y[2]) for i in range(len(proj_x))], color='crimson')
        plt.show()
        center_x = fit_x[1]
        center_y = fit_y[1]
        center = int(center_x), int(center_y)

        print(center)

        offset_points_n = [center[0] - i ** 2 for i in range(1, 18)]
        offset_points_p = [center[0] + i ** 2 for i in range(1, 18)]

        self.plot_histogram(array=roi)

        offset_list = []
        offset_centers = []

        for point in offset_points_p:
            plt.axvline(point, 0, 64, color='black', linewidth=2)
        self.plot_histogram(array=roi)
        self.plot_histogram(array=roi[:, offset_points_n[3]:offset_points_p[3]])

        for op in range(len(offset_points_p) - 1):
            pro_p = np.sum(roi[:, offset_points_p[op]:offset_points_p[op+1]], axis=1)
            pro_n = np.sum(roi[:, offset_points_n[op]:offset_points_n[op+1]], axis=1)
            pro = pro_n + pro_p
            if fine_debug:
                plt.scatter(range(len(pro)), pro, color='navy')

            try:
                p = [np.max(pro), 0.5*len(pro), 0.2*len(pro)]
                #print(p)
                fit, pas = curve_fit(gauss, range(len(pro)), pro, p0=p)
                if fine_debug:
                    plt.plot(range(len(pro)), [gauss(x, fit[0], fit[1], fit[2]) for x in range(len(pro))], color='crimson')
            except scipy.optimize.optimize.OptimizeWarning:
                offset_list.append([None, None])
            except RuntimeError:
                offset_list.append([None, None])
            else:
                offset = [fit[1], pas[1, 1]**2]
                offset_list.append(offset)
            offset_centers.append((offset_points_p[op] + offset_points_p[op+1]) / 2)
            if fine_debug:
                plt.title(f'Boundaries: {offset_points_p[op]}, {offset_points_p[op+1]}')
                plt.show()
        offset_list = np.array(offset_list)
        plt.plot([offset_centers[0], offset_centers[-1]], [center_y, center_y], color='black')
        plt.scatter(offset_centers, offset_list[:, 0])
        plt.show()



