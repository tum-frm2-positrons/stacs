import os
import json
from stacs import utils
import subprocess
import numpy as np
from PIL import Image
from lxml import etree as et
from datetime import datetime


def bor_entry(date, dets, pairs, ecals, bor_file="book_of_references.json"):
    """Insert new entry into book-of-references, e.g., for a new beamtime.

    Add a new date to dictionary stored in book-of-references. One entry
    includes detectors, coincidence pairs and energy calibrations.

    Parameters
    ----------
    date : datetime.dateime object
        Date (and time) of the start of a new measurement period.
    dets : list of str
        Detectors used during measurements.
    pairs : list of tuples of str
        Coicident detector pairs (geometrically opposed to each other).
    ecals : dict
        Energy calibrations including 'linefit', 'linerr', 'polyfit', 'polyerr'
        sorted by detector names.
    bor_file : str
        Path to book-of-references file.

    Returns
    -------
    dict
        Updated book-of-reference.

    Notes
    -----
    The book-of-references dictionary is not necessarily ordered (since python
    dicts are not constructed that way). Therefore one should consider sorting
    the keys before manual search, as in data_surgeon this is automatically
    done using sorted() and numpy.searchsorted().
    """

    with open(bor_file, "r") as f:
        old_bor = json.load(f)
    bor = old_bor.copy()
    bor_keys = list(map(datetime.fromisoformat, bor.keys()))
    date = date.isoformat("T")
    bor.update({date: {"Detectors": dets,
                       "CoincidencePairs": pairs,
                       "EnergyCalibrations": ecals}})
    utils.write_json(bor, bor_file)
    return bor


def load_meas(filename):
    """Load *.n42 measurement file into xml tree and extract file number.

    Parameters
    ----------
    filename : str
        Path to measurement file.

    Returns
    -------
    data : ElementTree object
        Measurement file data in xml structure.

    nr : int
        Measurement file number (needed to find corresponding parameter-file
        entry).
    """

    # extract filenumber from filename
    try:
        nr = int(filename[-8:-4])
    except ValueError:
        nr = int(filename[-7:-4])

    tree = et.parse(filename)
    data = tree.getroot()
    return data, nr


def merge(data, meas_nr, param):
    """Merge measurement-file xml tree with corresponding parameter-file entry.

    Scan parameter-file dictionary for corresponding entry (by datetime and
    file number) and insert into measurement-file ElementTree object.

    Parameters
    ----------
    data : ElementTree object
        Measurement file data in xml structure.

    meas_nr : int
        Measurement file number.

    param : dict
        Meta data from parameter-file.

    Returns
    -------
    ElementTree object
        Updated measurement data (including meta data from parameter-file).
    """

    meas = data.find("RadMeasurement")
    start_time = datetime.fromisoformat(meas.find("StartDateTime").text)

    t = np.array(list(map(utils.to_datetime, param['Time'])))

    # searching for a similar timestamp
    if len(t):
        diff = t - start_time
        idx = np.argmin(abs(diff))
        fnr = param['File'][idx]

        # correct index, throw warning if it is too far off
        d = meas_nr - fnr
        if abs(d) < 2:
            idx += d
        else:
            print("Timestamp does not match.")

    # merge
    lookup = {"File": None, "Energy": "PositronImplantationEnergy",
              "Temp": "PyrometerTemperature", "Time": None}
    for key in param:
        try:
            node_name = lookup[key]
            if key == None:
                node_name = None
        except KeyError:
            node_name = key

        if node_name != None and len(param[key]) \
                             and not len(meas.findall(node_name)):
            node = et.SubElement(meas, node_name)
            node.text = str(float(param[key][idx]))
    return data


def corr_detnames(data, detnames=None):
    """Correct detector names in xml tree.

    Replace detector names in xml tree by those found in the book-of-references
    lookup table.

    Notes
    -----
    Input from book-of-references not yet implemented!
    """

    if detnames == None:
        return data

    dets = data.findall("RadDetectorInformation")
    for det in dets:
        detname = det.find("RadDetectorName")
        if detname.text in detnames.values():
            pass
        else:
            detname.text = detnames[detname.text]
    return data


def corr_ecal(data, ecals=None, eres=None):
    """Correct energy calibrations in xml tree.

    Take linear fit (plus its errors) from energy calibration dictionary and
    insert into xml tree. Replaces old values and creates new xml entries if
    there are none.

    Parameters
    ----------
    data : ElementTree object
        Measurement file data in xml structure.
    ecals : dict
        Energy calibration data sorted by detector names.

    Returns
    -------
    ElementTree object
        Updated xml tree.

    Notes
    -----
    For the sake of consistency, we use only linear fits (since some energy
    calibrations do not allow polynomial fits). Maybe we can fix this and later
    provide both, linear and 2nd degree polynomial fits.
    """

    if ecals == None:
        print("No Energycalibration available, therefore xml file data was returned as is.")
        return data

    meas = data.find("RadMeasurement")
    meas_class = meas.find("MeasurementClassCode").text

    # apply calibration only to foreground measurements
    if not meas_class == "Foreground":
        print('Warning: The measurement class is not "Foreground"')

    dets = data.findall("RadDetectorInformation")
    cals = data.findall("EnergyCalibration")
    for cal, det in zip(cals, dets):
        # energy calibration coefficients (plus errors)
        detname = det.find("RadDetectorName")
        coeff = cal.find("CoefficientValues")
        err = cal.find("CoefficientErrors")
        if err == None:
            err = et.SubElement(cal, "CoefficientErrors")
        c0, c1 = ecals[detname.text]["linefit"]
        dc0, dc1 = ecals[detname.text]["linefiterr"]
        coeff.text = utils.list2str([c0, c1, 0])
        err.text = utils.list2str([dc0, dc1, 0])

        # energy resolution (plus error)
        if eres != None:
            res = cal.find("EnergyResolution")
            dres = cal.find("EnergyResolutionError")
            if res == None:
                res = et.SubElement(cal, "EnergyResolution")
                dres = et.SubElement(cal, "EnergyResolutionError")
            res.text = str(eres[detname.text]["fwhm"])
            dres.text = str(eres[detname.text]["fwhm_err"])
    return data


def add_photo(data, image_path):
    """Add 300x300 grayscale array of a *.jpeg image of the sampleholder.

    Parameters
    ----------
    data : ElementTree object
        Measurement file data in xml structure.
    image_path : str
        Path to *.jpeg image.

    Returns
    -------
    ElementTree object
        Updated xml tree.
    """

    if image_path == None:
        return data

    im = Image.open(image_path).convert("L")

    # crop image to largest possible square (centered) and resize to 300x300
    width, height = im.size
    side = min(im.size)
    im = im.crop(((width-side)/2, (height-side)/2, width-(width-side)/2,
                   height-(height-side)/2))
    im = im.resize((300, 300))

    photo = np.array(im).flatten()
    multimedia = et.SubElement(data, 'MultimediaData')
    multimedia.text = utils.list2str(photo)
    return data


def corr_filepath(filename, directory, filetype):
    """Correct filepath and prompt user input if file not found.

    Takes assumed filepath and corrects it, if necessary even with user
    prompts. Returns fallback option or None, if nothing found.

    Parameters
    ----------
    filename : str
        Assumed name of the wanted file.
    directory : str
        Assumed directory of the wanted file.
    filetype : str
        Type of wanted file (mainly used in prompts for different location).

    Returns
    -------
    str
        Corrected filepath or fallback option (if available for filetype) or
        None if nothing found.
    """

    orig_filename = filename

    while True:
        path = os.path.join(directory, filename)
        try:
            open(path).close()
            break
        except IsADirectoryError:
            filename = os.path.join(path, orig_filename)
            continue
        except FileNotFoundError:
            try:
                open(filename).close()
                path = filename
                break
            except FileNotFoundError:
                pass
            filename = input(f"\nNo {filetype} file found. Please specify path and filename (press Enter to continue without {filetype} file): ")
        if filename == "":
            prompt = input(f"Are You sure You want to continue without {filetype} file?")
            if prompt == "" or prompt == "y" or prompt == "yes":
                path = None
                break

    missing_im = "missing_image.jpeg"
    if path == None:
        if filetype == "image":
            try:
                open(missing_im).close()
                path = missing_im
                print(f"Continuing with '{path}' as {filetype} file.")
            except FileNotFoundError:
                print(f"Continuing without {filetype} file.")
        else:
            print(f"Continuing without {filetype} file.")
    else:
        print(f"Continuing with '{path}' as {filetype} file.")
    return path


def add_heatmaps(data, nr, directory, coinc_pairs, show=False):
    """Add coincidence measurements to xml tree.

    Search for heatmaps (*.txt) in directory, compare filename with coinc_pairs
    list and add heatmap for every pair ('missing_heatmap.txt', fallback option
    if no data found).

    Parameters
    ----------
    data : ElementTree object
        Measurement file data in xml structure.
    nr : int
        Measurement file number.
    directory : str
        Directory of coincidence measurement data.
    coinc_pairs : list of tuples
        Coincidence detector pairs.
    show : bool, optional
        If True, show every single coincidence heatmap.

    Returns
    -------
    ElementTree object
        Updated xml tree.

    Notes
    -----
    Energy window should somehow also be drawn from filename or from an
    additional file in directory. Not yet implemented.
    """

    meas = data.find("RadMeasurement")
    coinc = meas.findall("Coincidence")
    # return if measurement file already contains heatmaps
    if coinc:
        print("Measurement file already contains heatmaps. Returning ...")
        return data

    # create lookup table for 'radDetectorInformationReference'
    lookup = {}
    for det in data.findall("RadDetectorInformation"):
        detname = det.find("RadDetectorName")
        lookup[detname.text] = det.attrib['id']

    # search for heatmaps in directory
    files = [f for f in os.listdir(directory) if f.endswith(".txt")]
    for i, pair in enumerate(coinc_pairs):
        det1, det2 = pair
        heatmap_file = "missing_heatmap.txt"
        for f in files:
            if str(nr) in f and det1 in f and det2 in f:
                heatmap_file = os.path.join(directory, f)
                break

        # load heatmap
        try:
            with open(heatmap_file, "r") as file:
                heatmap = np.fromstring(file.read(), dtype=int, sep="\t")
        except FileNotFoundError:
            continue

        size = int(np.sqrt(heatmap.shape[0]))
        assert size*size == heatmap.shape[0]

        # radDetectorInformationReferences
        try:
            det_ref = [lookup[det1], lookup[det2]]
            ecal_ref = list(map(lambda x: f"EnergyCalibration{x[8:]}", det_ref))
        except KeyError:
            det_ref = [None, None]
            ecal_ref = [None, None]

        attributes = {'energyCalibrationReferences':
                        utils.list2str(ecal_ref),
                      'radDetectorInformationReferences':
                        utils.list2str(det_ref),
                      'id': 'Coincidence'+str(i)}

        # write heatmap to xml tree
        coinc = et.SubElement(meas, "Coincidence", attributes)
        coinc_pair = et.SubElement(coinc, "CoincidencePair")
        enrg_window = et.SubElement(coinc, "EnergyWindow")
        coinc_data = et.SubElement(coinc, "CoincidenceData")

        coinc_pair.text = f"{det1} {det2}"
        enrg_window.text = "lower1 upper1 lower2 upper2"
        coinc_data.text = utils.list2str(heatmap.flatten())

        if show:
            import matplotlib.pyplot as plt
            heatmap = heatmap.reshape((size, size))
            plt.imshow(heatmap)
            plt.show()

    return data


def data_surgeon(directory, out_dir=None, param_file="Parameter.txt",
                 prefix="", show_image=False):
    """Correct measurement data in *.n42 files, add heatmaps and meta data.

    In every *.n42 measurement file found in directory, add meta data from
    parameter-file and sample image (*.jpeg), correct detector names and their
    energy calibrations and add coincidence heatmaps found in directory.

    Parameters
    ----------
    directory : str
        Directory with *.n42 measurement files in need of treatment.
    out_dir : str, optional
        Output directory for corrected measurement files. 'directory/mod/' if
        nothing specified.
    param_file : str, optional
        Path to parameter-file containing meta data to add.
    prefix : str, optional
        Prefix for corrected measurement files. Default is nothing, if out_dir
        is the same as directory, prefix is 'mod_'.
    show_image : bool, optional
        If True, show image of sample holder added to measurement files
        (default is False).
    """

    # specify output directory/prefix
    if directory == out_dir:
        prefix = "mod"
    elif out_dir == None:
        out_dir = os.path.join(directory, "mod")

    # listing all *.n42 measurement files in directory
    files = sorted([f for f in os.listdir(directory) if f.endswith(".n42")
                    and not f.startswith("mod")])
    l = len(files)
    if l == 0:
        print(f"'{directory}' contains no measurement files requiring surgery. Aborting...")
        return
    else:
        print(f"Fixing measurement files in '{directory}' ({l} found) ...")

    # import parameter file
    param_path = corr_filepath(param_file, directory, "parameter")
    if param_path == None:
        param = {"Time": []}
    else:
        param = utils.import_parameter(param_path)

    # search for book-of-reference file
    bor_file = corr_filepath("../data/book_of_references.json", directory, "book-of-references")
    with open(bor_file, "r") as f:
        bor = json.load(f)

    # get correct detector data from book-of-references by comparing timestamps (with first measurement file)
    data0, nr0 = load_meas(os.path.join(directory, files[0]))
    meas = data0.find("RadMeasurement")
    timestamps = sorted(list(map(datetime.fromisoformat, bor.keys())))
    start_time = datetime.fromisoformat(meas.find("StartDateTime").text)
    idx = np.searchsorted(timestamps, start_time, side="right") - 1
    if idx < 0:
        print(f"""\nNo entry matching the time/date of measurement start.
              Beginning at {timestamps[0].isoformat()}.
              Please insert detector data for the corresponding beamtime in
              book_of_references.json'.
              Aborting ...""")
        return
    else:
        det_ref = bor[timestamps[idx].isoformat()]

    # search for energy resolution entry
    try:
        eres_ref = det_ref["EnergyResolution"]
    except KeyError:
        eres_ref = None

    # search for image file
    images = [f for f in os.listdir(directory) if f.endswith(".jpeg")]
    if len(images) == 0:
        image_path = corr_filepath("image.jpeg", directory, "image")
    elif len(images) == 1:
        image_path = corr_filepath(images[0], directory, "image")
    else:
        print("Multiple images found in directory. Please choose one:")
        for i, image in enumerate(images):
            print(f"({i+1}) {image}")
        print("Choosing first image file (selection not yet implemented).")
        image_path = corr_filepath(images[0], directory, "image")

    if show_image and image_path != None:
        import matplotlib.pyplot as plt
        im = Image.open(image_path).convert("L")
        print(f"\nOriginal image resolution (will be cropped and resized to 300x300): {np.array(im).shape}")
        plt.imshow(im)
        plt.show()

    print()

    # make sure output directory exists
    mkdir = subprocess.getoutput(f"mkdir {out_dir}")
    if mkdir == "":
        print(f"Creating output directory '{out_dir}' ...")

    # correct prefix
    if prefix != "" and not prefix.endswith("_"):
        prefix += "_"

    # actual data treatment
    for i, f in enumerate(files):
        print(f"({i+1}/{l}) {f}")
        data, nr = load_meas(os.path.join(directory, f))
        data = merge(data, nr, param)
        data = corr_detnames(data, det_ref["DetectorNames"])
        data = corr_ecal(data, det_ref["EnergyCalibrations"], eres_ref)
        data = add_photo(data, image_path)
        data = add_heatmaps(data, nr, directory, det_ref["CoincidencePairs"])

        utils.write_xml(data, os.path.join(out_dir, prefix+f))
    return
