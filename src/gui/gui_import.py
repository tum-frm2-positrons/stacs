# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'import.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

import matplotlib
from PyQt5 import QtCore, QtGui, QtWidgets
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
matplotlib.use('Qt5Agg')

import os

class Ui_wdg_import(QtWidgets.QDialog):

    def setupUi(self, wdg_import):
        wdg_import.setObjectName("wdg_import")
        wdg_import.resize(347, 377)
        self.verticalLayout = QtWidgets.QVBoxLayout(wdg_import)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(wdg_import)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.sel_importmode = QtWidgets.QComboBox(wdg_import)
        self.sel_importmode.setObjectName("sel_importmode")
        self.sel_importmode.addItem("")
        self.sel_importmode.addItem("")
        self.horizontalLayout.addWidget(self.sel_importmode)
        self.btn_filebrows = QtWidgets.QPushButton(wdg_import)
        self.btn_filebrows.setObjectName("btn_filebrows")
        self.horizontalLayout.addWidget(self.btn_filebrows)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.lbl_fileselect = QtWidgets.QLabel(wdg_import)
        self.lbl_fileselect.setObjectName("lbl_fileselect")
        self.verticalLayout.addWidget(self.lbl_fileselect)
        self.text_selfiles = QtWidgets.QTextBrowser(wdg_import)
        self.text_selfiles.setObjectName("text_selfiles")
        self.verticalLayout.addWidget(self.text_selfiles)
        self.chk_sumup = QtWidgets.QCheckBox(wdg_import)
        self.chk_sumup.setToolTipDuration(100)
        self.chk_sumup.setStatusTip("")
        self.chk_sumup.setObjectName("chk_sumup")
        self.verticalLayout.addWidget(self.chk_sumup)
        self.txt_rename = QtWidgets.QLineEdit(wdg_import)
        self.txt_rename.setObjectName('txt_rename')
        self.txt_rename.setText('measurement_name')
        self.txt_rename.setEnabled(False)
        self.verticalLayout.addWidget(self.txt_rename)
        self.btn_import = QtWidgets.QPushButton(wdg_import)
        self.btn_import.setObjectName("btn_box_ok")
        self.verticalLayout.addWidget(self.btn_import)
        self.progressbar = QtWidgets.QProgressBar(wdg_import)
        self.progressbar.setObjectName("import_status")
        self.verticalLayout.addWidget(self.progressbar)
        self.filenames = None
        self.btn_filebrows.pressed.connect(self.filebrowser)

        # self.btn_box_ok.accepted.connect(self.success)
        # self.btn_box_ok.rejected.connect(self.cancel)
        self.status = None
        self.retranslateUi(wdg_import)
        QtCore.QMetaObject.connectSlotsByName(wdg_import)

        self.chk_sumup.stateChanged.connect(self.enabletext)

    def enabletext(self, state):
        if state > 0:
            self.txt_rename.setEnabled(True)
        else:
            self.txt_rename.setEnabled(False)


    def retranslateUi(self, wdg_import):
        _translate = QtCore.QCoreApplication.translate
        wdg_import.setWindowTitle(_translate("wdg_import", "Import Assistant"))
        self.label.setText(_translate("wdg_import", "Import Mode:"))
        self.sel_importmode.setItemText(0, _translate("wdg_import", "Measurement"))
        self.sel_importmode.setItemText(1, _translate("wdg_import", "Reference"))
        self.btn_filebrows.setText(_translate("wdg_import", "Open"))
        self.btn_import.setText(_translate("wdg_import", "Import Files"))
        self.lbl_fileselect.setText(_translate("wdg_import", "Selected Files:"))
        self.chk_sumup.setToolTip(_translate("wdg_import", "Files will be added up to create a summed up spectrum, recorded over time."))
        self.chk_sumup.setText(_translate("wdg_import", "Sum selected files up"))

    def filebrowser(self):
        filelist = QtWidgets.QFileDialog.getOpenFileNames(None, 'Open File', '/home',
                                                          'Files (*.txt *.mpa)')
        self.text_selfiles.clear()

        if len(filelist[0]) != 0:
            self.filenames = filelist

        if self.filenames != None:
            for i in self.filenames[0]:
                f_name = i
                f_split = f_name.split('/')
                text = str(f_split[-1])
                self.text_selfiles.insertHtml(f'<font color="black">{text}</font><br>')


class Ui_Canvas(QtWidgets.QDialog):

    def setupUi(self, wdg_canvas):
        wdg_canvas.setObjectName("wdg_import")


        '''
        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # set the layout
        layout = QtWidgets.QVBoxLayout(wdg_canvas)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout) #'''

    def retranslateUi(self, wdg_canvas):
        _translate = QtCore.QCoreApplication.translate
        wdg_canvas.setWindowTitle(_translate("wdg_canvas", "Ratio Curve"))

    def plot(self, plotparams):

        measlist, errlist, labellist, y_lim, x_lim, refname, xlabel, title = plotparams

        for i in errlist:
            plt.errorbar(i[0], i[1], i[2], color=i[3], linestyle='None', capsize=2, elinewidth=1)
            plt.scatter(i[0], i[1], color=i[3], marker='o', s=10)

        for i in measlist:
            plt.plot(i[0], i[1], color=i[2], linewidth=i[3])

        plt.grid(True)
        plt.legend(handles=labellist)
        plt.ylim(top=y_lim[0], bottom=y_lim[1])
        plt.xlim(left=x_lim[0], right=x_lim[1])
        plt.ylabel('Ratio to {}'.format(refname))
        plt.xlabel(xlabel)
        plt.title(title)
        plt.show()


        '''
        
        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.clear()

        for i in errlist:
            ax.errorbar(i[0], i[1], i[2], color=i[3], linestyle='None', capsize=2, elinewidth=1)

        for i in measlist:
            ax.plot(i[0], i[1], color=i[2], linewidth=i[3])


        
        ax.grid(True)
        ax.legend(handles=labellist)
        ax.ylim(top=y_lim[0], bottom=y_lim[1])
        ax.xlim(left=x_lim[0], right=x_lim[1])
        ax.ylabel('Ratio to {}'.format(refname))
        ax.xlabel(xlabel)
        ax.title(title)#'''
        # refresh canvas


class Ui_dlg_Help(object):

    def setupUi(self, dlg_Help):
        dlg_Help.setObjectName("dlg_Help")
        dlg_Help.resize(1000, 600)
        self.textBrowser = QtWidgets.QTextBrowser(dlg_Help)
        self.textBrowser.setGeometry(QtCore.QRect(10, 10, 981, 581))
        self.textBrowser.setObjectName("textBrowser")

        self.retranslateUi(dlg_Help)
        QtCore.QMetaObject.connectSlotsByName(dlg_Help)

    def retranslateUi(self, dlg_Help):
        _translate = QtCore.QCoreApplication.translate
        dlg_Help.setWindowTitle(_translate("dlg_Help", "Help"))
        self.textBrowser.setHtml(_translate("dlg_Help",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\'; font-size:14pt; font-weight:600;\">Import Assistant</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\'; font-size:14pt; font-weight:600;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">Before the first calculations can be done, the measurement files have to be imported into the program. In this step the plain text files (usually .mpa file extensions) are imported and converted to a python object. This makes it possible to easily add up measurements, in order to improve statistics. Measurements can be imported easily using the Import Assistant. Click on &quot;File&quot; --&gt; &quot;Import Assistant&quot;. A popup window will open. </span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">The program differentiates between &quot;Measurements&quot; and &quot;References&quot;. &quot;References&quot; are measurements of a reference material, which the CDB spectrum will later be divided by in order to allow comparative analysis of spectra. Any file that needs to be analyzed should hence be imported as a &quot;Measurement&quot;. The import process is the same for both, this function is just used to differenciate between measurements and references.</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">The &quot;Open&quot; butto will open a file browser. One or multiple files can be selected using the SHIFT or CTRL/CMD button. Once the files are opened they will be displayed in the &quot;Selected Files&quot; box, marking them as queued up to be imported. If the &quot;Sum selected files up&quot; checkbox is checked, the files will be imported and all of their spectra will be added up to improve measurement statistics. If the box is checked a name for the resulting single file has to be specified. Finally the &quot;Import Files&quot; button can be pressed, and the imported objects will be added to the two tables in the main window. Once an import is complete the process can be repeated simply by opening the file browser again and selecting other files. Once all desired files are imported the &quot;Import Assistant&quot; window may simply be closed. </span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">NOTE: Due to the internal data structure of the program, it is generally not possible to import several measurement files with the same name. If this happenes, the file that was imported first will be overwritten. This can be avoided by renaming the first file before importing the second one.</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\'; font-size:14pt; font-weight:600;\">Main Window</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\'; font-size:14pt; font-weight:600;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">Once all of the required files have been imported, they will be displayed in the two tables on the left side of the main window as a list. Now the Files can be renamed by double clicking them. The Files have to be renamed here in order for them to appear with a sensible name on the plots later. Once all files have been named as desired, the ratio curves can be calculated. Files can be selected by highlighting them (multiple can be selected with SHIFT and CMD/CTRL). Only highlighted files will be considered for the evaluation, which means that it is possible to import all files in advance and afterwards create the desired combinations. In order to calculate ratio curves at least one measurement and one reference object needs to be highlighted.</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">At the bottom left of the window a binning option can be selected. This determines the energy resolution of the ratio curves. By Default a Variable binning mode is selected. In this mode the energy resolution is decreased in steps, further away from 511 keV. The other varialble option decreases the resolution continously. The other modes use constant resolution steps. Smaller step sizes result in better resolution and slower calculations. The smaller stepsizes should be avoided for shorter measurements (less than 4 hours). </span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">By default the program will plot the calculated curves. For this reason the text box for a graph title should be filled out. If you desire to plot your own graphs the ratio curves and projections can also be exported into a .txt file.</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\'; font-weight:600;\">Known Issues:</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\'; font-weight:600;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">- </span><span style=\" font-family:\'.AppleSystemUIFont\'; font-style:italic;\">Not working yet:</span><span style=\" font-family:\'.AppleSystemUIFont\';\"> Settings, Projection, all progress bars, Exporting, Saving</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\'; font-weight:600;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">- When renaming measurements the popup window does not close upon pressing the \'ok\' button and has to be manually closed</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">- Calculating multiple curves in succession will quit the program (due to matplotlib not playing along)</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'.AppleSystemUIFont\';\">- Text in the \'Messages\' box is buggy and only appears after clicking into it</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'.AppleSystemUIFont\';\"><br /></p></body></html>"))

class Ui_rename(QtWidgets.QDialog):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(242, 111)
        Dialog.setMinimumSize(QtCore.QSize(242, 111))
        Dialog.setMaximumSize(QtCore.QSize(242, 111))
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 228, 91))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.line_name = QtWidgets.QLineEdit(self.layoutWidget)
        self.line_name.setObjectName("line_name")
        self.verticalLayout.addWidget(self.line_name)
        self.btn_okname = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_okname.setObjectName("btn_okname")
        self.verticalLayout.addWidget(self.btn_okname)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.newname = ''
        self.active = True

        self.btn_okname.pressed.connect(self.setname)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Please enter new name:"))
        self.btn_okname.setText(_translate("Dialog", "OK"))

    def setname(self):
        self.newname = self.line_name.text()
        self.destroy()



'''
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
#'''

if __name__ == "__main__":
    import sys
    app = QtWidgets.QDialog(sys.argv)
    wdg_import = QtWidgets.QWidget()
    ui = Ui_wdg_import()
    ui.setupUi(wdg_import)
    wdg_import.show()
    sys.exit(app.exec_())
