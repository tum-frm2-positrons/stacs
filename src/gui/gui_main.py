# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generatolsr 5.9.2
#
# WARNING! All changes made in this file will be lost!

from stacs.gui.gui_import import Ui_wdg_import, Ui_dlg_Help, Ui_rename, Ui_Canvas
from stacs.core.classes import Spectrum
from stacs.coinc.ratiocurve import gui_plotter
from stacs.gui.gui_worker import Worker
import time

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.threadpool = QtCore.QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMouseTracking(True)
        MainWindow.setTabletTracking(True)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.lbl_meas_head = QtWidgets.QLabel(self.centralwidget)
        self.lbl_meas_head.setObjectName("lbl_meas_head")
        self.verticalLayout.addWidget(self.lbl_meas_head)
        self.list_meas = QtWidgets.QListWidget(self.centralwidget)
        self.list_meas.setMinimumSize(QtCore.QSize(0, 200))
        self.list_meas.setMouseTracking(True)
        self.list_meas.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.list_meas.setObjectName("list_meas")
        self.verticalLayout.addWidget(self.list_meas)
        spacerItem = QtWidgets.QSpacerItem(378, 24, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.lbl_ref_head = QtWidgets.QLabel(self.centralwidget)
        self.lbl_ref_head.setObjectName("lbl_ref_head")
        self.verticalLayout.addWidget(self.lbl_ref_head)
        self.list_ref = QtWidgets.QListWidget(self.centralwidget)
        self.list_ref.setMaximumSize(QtCore.QSize(16777215, 100))
        self.list_ref.setObjectName("list_ref")
        self.verticalLayout.addWidget(self.list_ref)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.sel_binning = QtWidgets.QComboBox(self.centralwidget)
        self.sel_binning.setObjectName("sel_binning")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.sel_binning.addItem("")
        self.verticalLayout.addWidget(self.sel_binning)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)
        self.lbl_graph_title = QtWidgets.QLabel(self.centralwidget)
        self.lbl_graph_title.setObjectName("lbl_graph_title")
        self.verticalLayout_2.addWidget(self.lbl_graph_title)
        self.txt_graph_title = QtWidgets.QLineEdit(self.centralwidget)
        self.txt_graph_title.setObjectName("txt_graph_title")
        self.verticalLayout_2.addWidget(self.txt_graph_title)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.btn_ratiocurve = QtWidgets.QPushButton(self.centralwidget)
        self.btn_ratiocurve.setMouseTracking(True)
        self.btn_ratiocurve.setTabletTracking(True)
        self.btn_ratiocurve.setObjectName("btn_ratiocurve")

        self.horizontalLayout_2.addWidget(self.btn_ratiocurve)
        self.btn_settings = QtWidgets.QPushButton(self.centralwidget)
        self.btn_settings.setObjectName("btn_settings")
        self.horizontalLayout_2.addWidget(self.btn_settings)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btn_projection = QtWidgets.QPushButton(self.centralwidget)
        self.btn_projection.setObjectName("btn_projection")
        self.horizontalLayout.addWidget(self.btn_projection)
        self.chk_mirrored = QtWidgets.QCheckBox(self.centralwidget)
        self.chk_mirrored.setChecked(True)
        self.chk_mirrored.setObjectName("chk_mirrored")
        self.horizontalLayout.addWidget(self.chk_mirrored)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.prog_main = QtWidgets.QProgressBar(self.centralwidget)
        self.prog_main.setProperty("value", 24)
        self.prog_main.setObjectName("prog_main")
        self.verticalLayout_2.addWidget(self.prog_main)
        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem3)
        self.lbl_messages = QtWidgets.QLabel(self.centralwidget)
        self.lbl_messages.setObjectName("lbl_messages")
        self.verticalLayout_2.addWidget(self.lbl_messages)
        self.text_status = QtWidgets.QTextBrowser(self.centralwidget)
        self.text_status.setObjectName("text_status")
        self.verticalLayout_2.addWidget(self.text_status)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.chk_graph = QtWidgets.QPushButton(self.centralwidget)
        self.chk_graph.setEnabled(False)
        self.chk_graph.setObjectName("chk_graph")
        self.gridLayout.addWidget(self.chk_graph, 0, 0, 1, 1)
        self.chk_export = QtWidgets.QCheckBox(self.centralwidget)
        self.chk_export.setObjectName("chk_export")
        self.gridLayout.addWidget(self.chk_export, 0, 1, 1, 1)
        self.txt_export = QtWidgets.QLineEdit(self.centralwidget)
        self.txt_export.setObjectName("txt_export")
        self.gridLayout.addWidget(self.txt_export, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        self.gridLayout_2.addLayout(self.verticalLayout_2, 0, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_Import = QtWidgets.QAction(MainWindow)
        self.action_Import.setObjectName("action_Import")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionSave_2 = QtWidgets.QAction(MainWindow)
        self.actionSave_2.setObjectName("actionSave_2")
        self.actionHelp = QtWidgets.QAction(MainWindow)
        self.actionHelp.setObjectName("actionHelp")
        self.actionOnly_Ratio_Curves = QtWidgets.QAction(MainWindow)
        self.actionOnly_Ratio_Curves.setObjectName("actionOnly_Ratio_Curves")
        self.menuFile.addAction(self.action_Import)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionSave_2)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionOnly_Ratio_Curves)
        self.menuHelp.addAction(self.actionHelp)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        MainWindow.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.busy_import = False

        self.prog_main.setMaximum(100)
        self.prog_main.setValue(0)

        self.measurements = {}
        self.referneces = {}
        self.action_Import.triggered.connect(self.import_assistant)
        self.actionHelp.triggered.connect(self.help)
        self.actionSave.triggered.connect(self.savestate)
        self.btn_ratiocurve.pressed.connect(self.doratiocurve)
        self.chk_graph.pressed.connect(self.plot_window)
        self.list_meas.itemDoubleClicked.connect(lambda: self.edititem(self.list_meas,
                                                                       self.list_meas.currentItem(), True))
        self.list_ref.itemDoubleClicked.connect(lambda: self.edititem(self.list_ref,
                                                                      self.list_ref.currentItem(), False))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lbl_meas_head.setText(_translate("MainWindow", "Measurement Objects (select multiple):"))
        self.list_meas.setToolTip(_translate("MainWindow", "Select multiple with SHIFT or CTRL"))
        self.lbl_ref_head.setText(_translate("MainWindow", "Reference Objects (select one): "))
        self.label.setText(_translate("MainWindow", "Select a Binning:"))
        self.sel_binning.setItemText(0, _translate("MainWindow", "VarSteps"))
        self.sel_binning.setItemText(1, _translate("MainWindow", "Variable"))
        self.sel_binning.setItemText(2, _translate("MainWindow", "50 eV"))
        self.sel_binning.setItemText(3, _translate("MainWindow", "100 eV"))
        self.sel_binning.setItemText(4, _translate("MainWindow", "200 eV"))
        self.sel_binning.setItemText(5, _translate("MainWindow", "300 eV"))
        self.sel_binning.setItemText(6, _translate("MainWindow", "500 eV"))
        self.sel_binning.setItemText(7, _translate("MainWindow", "750 eV"))
        self.sel_binning.setItemText(8, _translate("MainWindow", "1 keV"))
        self.lbl_graph_title.setText(_translate("MainWindow", "Enter Graph Title:"))
        self.txt_graph_title.setText(_translate("MainWindow", "Graph Title"))
        self.btn_ratiocurve.setText(_translate("MainWindow", "Ratio Curves"))
        self.btn_settings.setText(_translate("MainWindow", "Settings"))
        self.btn_projection.setText(_translate("MainWindow", "Projection (no reference needed)"))
        self.chk_mirrored.setText(_translate("MainWindow", "Mirrored Projection"))
        self.lbl_messages.setText(_translate("MainWindow", "Messages:"))
        self.chk_graph.setText(_translate("MainWindow", "Show Plots"))
        self.chk_export.setText(_translate("MainWindow", "Export as .txt file"))
        self.txt_export.setText(_translate("MainWindow", "projection"))
        self.label_3.setText(_translate("MainWindow", "Export file name:"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.action_Import.setText(_translate("MainWindow", "Import Assistant"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionSave_2.setText(_translate("MainWindow", "Save"))
        self.actionHelp.setText(_translate("MainWindow", "Help"))
        self.actionOnly_Ratio_Curves.setText(_translate("MainWindow", "Only Ratio Curves"))

    def import_assistant(self):
        self.dialog = Ui_wdg_import()
        import_window = QtWidgets.QDialog()
        self.dialog.setupUi(import_window)

        self.dialog.btn_import.pressed.connect(self.doimport)

        import_window.exec_()

    def edititem(self, widget, item, boo):
        self.rename = Ui_rename()
        rename_window = QtWidgets.QDialog()
        self.rename.setupUi(rename_window)
        self.dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        rename_window.exec_()

        if boo:
            self.measurements[self.rename.newname] = self.measurements.pop(item.text())
            self.measurements[self.rename.newname].name = self.rename.newname
        else:
            self.referneces[self.rename.newname] = self.referneces.pop(item.text())
            self.referneces[self.rename.newname].name = self.rename.newname

        item.setText(self.rename.newname)
        widget.update()

    def doratiocurve(self):
        worker2 = Worker(self.ratiocurve)
        worker2.signals.progress.connect(self.prog_main.setValue)
        worker2.signals.result.connect(self.save_ratio)
        self.threadpool.start(worker2)
        time.sleep(2)

    def save_ratio(self, results):
        self.threadpool.clear()
        if results != None:
            self.r_curve_result = results
            print(len(self.r_curve_result))
            self.chk_graph.setEnabled(True)

    def plot_window(self):
        self.threadpool.clear()
        wdg_canvas = QtWidgets.QDialog()
        can = Ui_Canvas()
        can.setupUi(wdg_canvas)
        wdg_canvas.show()
        can.plot(self.r_curve_result)
        wdg_canvas.exec_()


    def ratiocurve(self, progress=None, result=None):
        '''
        print(self.list_meas.selectedItems())
        print(len(self.list_meas.selectedItems()))
        print(self.list_ref.selectedItems())
        print(len(self.list_ref.selectedItems()))
        #'''

        bin_dict = {'Variable': 'bins.txt', 'VarSteps': 'BinsVar.txt', '50 eV': 'Bins50.txt', '100 eV': 'Bins100.txt',
                    '200 eV': 'Bins200.txt', '300 eV': 'Bins300.txt', '400 eV': 'Bins400.txt', '500 eV': 'Bins500.txt',
                    '750 eV': 'Bins750.txt', '1 keV': 'Bins1000.txt',}
        meas = self.measurements.copy()
        ref = self.referneces.copy()
        print(self.measurements, self.referneces)

        if len(self.list_meas.selectedItems()) == 0 or len(self.list_ref.selectedItems()) == 0:
            print('empty')
        else:
            evalqueue = []
            refqueue = ref[self.list_ref.currentItem().text()]
            print(refqueue)
            for i in self.list_meas.selectedItems():
                evalqueue.append(meas[i.text()])
            print(evalqueue)
            data = gui_plotter(refqueue, evalqueue, self.txt_graph_title.text(),
                               bins=bin_dict[self.sel_binning.currentText()], progress=progress)
            progress.emit(100)
            result.emit(data)

    def doimport(self):
        worker = Worker(self.importer)
        worker.signals.result.connect(self.outputimp)
        worker.signals.progress.connect(self.dialog.progressbar.setValue)
        self.threadpool.start(worker)


    def importer(self, progress=None, result=None):
        import time

        while self.busy_import:
            time.sleep(2)

        self.busy_import = True
        imported = []

        q = self.dialog.filenames
        mode = str(self.dialog.sel_importmode.currentText())
        multiples = self.dialog.chk_sumup.isChecked()

        if q == None:
            return None
        prbar = 0
        progress.emit(0)
        prstep = int(100 / len(q[0]))

        if multiples:
            sumlist = []

            for i in q[0]:
                text = i.split('/')
                try:
                    sumlist.append(Spectrum(name=str(text[-1]), filename=i))
                except IndexError:
                    self.busy_import = False
                    self.dialog.text_selfiles.insertHtml(f'''<font color="#ff7627">File <u>{str(text[-1])}</u> is not formatted correctly.
                    Please open another file and try again. To see how files are supposed to be formatted please look
                    at "Help" in the context menu.</font><br>''')
                    self.dialog.text_selfiles.insertHtml('<font color="red">Import aborted.</font>')
                    return i
                prbar += prstep
                progress.emit(prbar)
            measobj = sumlist[0]
            for i in sumlist[1:]:
                measobj += i
            measobj.name = self.dialog.txt_rename.text()
            progress.emit(100)

            imported.append(measobj)

        else:
            for i in q[0]:
                text = i.split('/')
                try:
                    imported.append(Spectrum(name=str(text[-1]), filename=i))
                except IndexError:
                    self.busy_import = False
                    self.dialog.text_selfiles.insertHtml(f'''<font color="#ff7627">File <u>{str(text[-1])}</u> is not formatted correctly.
                    Please open another file and try again. To see how files are supposed to be formatted please look
                    at "Help" in the context menu.</font><br>''')
                    self.dialog.text_selfiles.insertHtml('<font color="red">Import aborted.</font>')
                    return i
                prbar += prstep
                progress.emit(prbar)
            progress.emit(100)
        for i in imported:
            if mode == 'Measurement':
                self.measurements[i.name] = i
                self.list_meas.addItem(i.name)
            else:
                self.referneces[i.name] = i
                self.list_ref.addItem(i.name)

        self.busy_import = False

    def outputimp(self, s):
        print('import complete')

    def savestate(self):
        duplicates = []
        for file in self.measurements:
            # check for duplicate names
            duplicates.append(file.name)
            savefile = open(file.name, 'w')

    def help(self):
        dlg_Help = QtWidgets.QDialog()
        ui = Ui_dlg_Help()
        ui.setupUi(dlg_Help)
        dlg_Help.show()
        dlg_Help.exec_()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
