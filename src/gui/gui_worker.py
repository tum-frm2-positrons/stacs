from PyQt5.QtCore import *

class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)

class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.signals = WorkerSignals()
        self.args = args
        self.kwargs = kwargs
        self.fn = fn

    @pyqtSlot()
    def run(self):
        result = self.fn(*self.args, **self.kwargs, progress=self.signals.progress, result=self.signals.result)
        self.signals.result.emit(result)
        self.signals.finished.emit()


