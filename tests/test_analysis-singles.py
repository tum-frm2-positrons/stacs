import pytest
import stacs
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


class MeasurementCampaignDummy:
    """Mirros a MeasurementCampaign
    """
    def __init__(self, list_of_measurementobjects=None, show_plot_in_gui=False,
                 verbose=False):
        self.measurements = list_of_measurementobjects
        self.show = show_plot_in_gui
        self.verbose = verbose
        self.depth_profiles = None


class DopplerMeasurementDummy:
    """Mirros the DopplerMeasurement class
    """
    def __init__(self, list_of_coincidencespectra=None,
                 positron_implantation_energy=None, filename="", filepath="",
                 list_of_singlespectra=None, filetype=None,
                 show_plot_in_gui=False, verbose=False):
        self.coinc = list_of_coincidencespectra
        self.implantation_energy = positron_implantation_energy
        self.filename = filename
        self.name = filepath
        self.singles = list_of_singlespectra
        self.filetype = filetype
        self.show = show_plot_in_gui
        self.verbose = verbose

    def show_singles(self):
        return True


class SingleSpectrumDummy(DopplerMeasurementDummy):
    """Mirrors the SingleSpectrum class
    """
    def __init__(self):
        self.counts = int
        self.dcounts = float
        self.detname = str
        self.dpeak_countrate_live = float
        self.dpeak_countrate_real = float
        self.dpeak_counts = float
        self.ds = float
        self.dv2p = float
        self.dw = float
        self.ecal = list()
        self.ecal_stdev = list()
        self.energies = None
        self.eres = float
        self.implantation_energy = float
        self.live_time = float
        self.peak = None
        self.peak_center = float
        self.peak_countrate_live = float
        self.peak_countrate_real = float
        self.peak_counts = float
        self.peak_energies = float
        self.real_time = float
        self.s = float
        self.show = False
        self.spectrum = list()
        self.v2p = float
        self.verbose = False
        self.w = float
    
    #TODO:  include test for methods
    #self.analyze(
    ## if meas is ecal
	#	self.calculate_eres(
    ## internal functions
	#	self.odr_fit(
	#	self.odr_gauss_polyfit(
	#	self.odr_polyfit(
	#	self.fit_calib_peaks_with_gauss(
    ## ??
    #self.autoadjust_ecal(
	#	self.calibrate(
	#	self.match_calibration(
	#	self.noise_level(


## Arrange
path2testdata = Path("./testdata")

# make dummy objects
@pytest.fixture
def campaign_dummy():
    attributes = {
        'verbose': False,
        'measurements': [1]*526,
    }
    dummy = MeasurementCampaignDummy()
    for attribute,value in attributes.items():
        setattr(dummy, attribute, value)
    return dummy


@pytest.fixture
def measurement_dummy():
    attributes = {
        'filepath': "tests/testdata/N42_singleDBS_singleFile_SLOPE/singleDBS_SLOPE.n42",
        'filename': "testdata/N42_singleDBS_singleFile_SLOPE/singleDBS_SLOPE.n42",
        'filetype': "n42",
        'positron_implantation_energy': "1780.0"
    }
    dummy = DopplerMeasurementDummy(**attributes)
    return dummy


@pytest.fixture
def spectrum_dummy(measurement_dummy):
    # spectrum
    spectrum_channels = 2**14
    energy_spectrum_min = 0.02509901
    energy_spectrum_max = 1174.423421686595
    # peak window
    peak_window_channels = 838
    energy_peak_window_min = 480.7377372245667
    energy_peak_window_max = 540.7372120509299
    attributes = {
        'detname': "OAA",
        'ecal': np.array([0.02509901, 0.07168396, 0. ]),
        'ecal_stdev': None,
        'eres': 1.51,
        'implantation_energy': None,
        'live_time': np.nan,
        'real_time': None,
        'counts': 204646,
        'dcounts': 452.37816039238675,
        's': 0.6278915789530878,
        'ds': 0.0019925090542693446,
        'w': 0.020678029879990765,
        'dw': 0.0005865979665307861,
        'v2p': 0.07488684707210017,
        'dv2p': 0.0011724306410946077,
        'peak': np.zeros(peak_window_channels),
        'peak_energies': np.linspace(energy_peak_window_min,
                                     energy_peak_window_max,
                                     peak_window_channels
                                    ),
        'peak_counts': 58850.95975934467,
        'dpeak_counts': 242.59216755564196,
        'peak_countrate_live': np.float64(np.nan),
        'dpeak_countrate_live': np.float64(np.nan),
        'peak_countrate_real': np.float64(np.nan),
        'dpeak_countrate_real': np.float64(np.nan),
        'peak_center': 511.279,
        'spectrum': np.zeros(spectrum_channels),
        'energies': np.linspace(energy_spectrum_min,
                                         energy_spectrum_max,
                                         spectrum_channels
                                        )

    }
    dummy = SingleSpectrumDummy()
    for attribute,value in attributes.items():
        setattr(dummy, attribute, value)
    return dummy

# make real objects from data

@pytest.fixture
def campaign():
    path = path2testdata / "N42_singleDBS_depth-resolved_multi-profile_SLOPE"
    return stacs.MeasurementCampaign(directory=str(path))


@pytest.fixture
def measurement():
    filename = "singleDBS_SLOPE.n42"
    filetype = "n42"
    path = path2testdata / "N42_singleDBS_singleFile_SLOPE" / filename
    return stacs.DopplerMeasurement(str(path.resolve()))


@pytest.fixture
def spectrum(measurement):
    return measurement.singles[0]


## run tests

class TestMeasurementCampaignSinglesN42:
    """Test batch processing of measurement (recorded in single mode)
    """
    def test_attibutes(self, campaign, campaign_dummy):
        """Test if the the importer configures the classes attributes
        correctly.
        """
        assert campaign.verbose == campaign_dummy.verbose
        assert campaign.show == campaign_dummy.show
        assert type(campaign.measurements[0]) == stacs.DopplerMeasurement
        assert len(campaign.measurements) == len(campaign_dummy.measurements)

    def test_creating_depth_profiles(self, campaign, campaign_dummy):
        """Tests making depth-profiles from collections of single spectra.
        """
        # Arrange
        output_folder = Path("./output/depth_profiles/")
        output_folder.mkdir(parents=True, exist_ok=True)
        # Act
        campaign.verbose = True
        campaign.depth_profiles()
        # until the merging of the large update to the depth-profiling methods
        # is due.
        plt.close('all')
        campaign.depth_profiles(parameters=["S"], plot_sw=False, save=True,
                                savename=str(output_folder)+"/singles_depth-profile_se.png")
        plt.close('all')
        campaign.depth_profiles(parameters=["S", "W"], plot_sw=True, save=True,
                                savename=str(output_folder)+"/singles_depth-profile_se-sw.png")
        plt.close('all')

    def test_attibutes_secondary(self, campaign, campaign_dummy):
        """Test the attributes set by the data analysis methods.
        """
        #Currently there are none
        pass

    # NOTE theses methods are only used in coincidence mode.
    #mcp.calc_ratio(         
    #mcp.ratio_plotter(
    #mcp.ratio_plotter_win(


class TestAnalysisDopplerMeasurementN42:
    """Test of Analysis singles

    Test the importing of datafiles by STACS into the DopplerMeasurement class
    from a file and the available methods.
    """
    path2testdata = Path("./testdata")
    filename = "singleDBS_SLOPE.n42"
    filetype = "n42"
    path = path2testdata / "N42_singleDBS_singleFile_SLOPE" / filename

    def test_singles_filetags(self, measurement, measurement_dummy):
        # Act

        assert measurement_dummy.name in measurement.name
        assert measurement_dummy.filename in measurement.filename
        assert measurement.filetype == measurement_dummy.filetype

    def test_singles_metadata(self, measurement, measurement_dummy):
        # Act
        assert measurement.implantation_energy == measurement_dummy.implantation_energy
        assert isinstance(measurement.implantation_energy, float) == False

    def test_singles_spectra(self, measurement):
        # Act
        assert measurement.singles != None
        assert type(measurement.singles[0]) == stacs.single.SingleSpectrum
        assert len(measurement.singles) == 2
        assert measurement.coinc == []

    def test_method_show_singles(self, measurement):
        # Act
        output_folder = Path("./output/show_singles/")
        output_folder.mkdir(parents=True, exist_ok=True)
        measurement.show_singles(show=False,
                                 savepath=f"{output_folder}/lin_scale.png")
        measurement.show_singles(logscale=True, show=False,
                                 savepath=f"{output_folder}/log_scale.png")


class TestAnalysisSingleDopplerSpectrum:
    """Test of the SingleSpectrum class
    """ 
    def test_input(self, spectrum, spectrum_dummy):
        # Act
        assert type(spectrum) == stacs.single.SingleSpectrum

    def test_metadata(self, spectrum, spectrum_dummy):
        # Act
        assert spectrum.verbose == spectrum_dummy.verbose
        assert spectrum.show == spectrum_dummy.show
        assert spectrum.implantation_energy == spectrum_dummy.implantation_energy

    def test_metadata_of_detection_system(self, spectrum, spectrum_dummy):
        # Act
        assert spectrum.detname == spectrum_dummy.detname
        assert spectrum.ecal.shape == spectrum_dummy.ecal.shape
        assert np.allclose(spectrum.ecal, spectrum_dummy.ecal, rtol=1e-5)
        assert spectrum.ecal_stdev == spectrum_dummy.ecal_stdev
        assert spectrum.eres == spectrum_dummy.eres

    def test_spectrum(self, spectrum, spectrum_dummy):
        # Act
        assert len(spectrum.spectrum) == len(spectrum_dummy.spectrum)
        assert len(spectrum.energies) == len(spectrum.spectrum)
        assert min(spectrum.energies) == min(spectrum_dummy.energies)
        assert max(spectrum.energies) == pytest.approx(max(spectrum_dummy.energies), 1.0e-3)

    def test_metadata_of_spectrum(self, spectrum, spectrum_dummy):
        # TODO update dummy values in future update (version > 0.2.3)
        # Act
        np.testing.assert_equal(spectrum.live_time,
                                spectrum_dummy.live_time)
        np.testing.assert_equal(spectrum.real_time, spectrum_dummy.real_time)

    def test_data_derived_from_spectrum(self, spectrum, spectrum_dummy):
        # Act
        assert spectrum.counts == spectrum_dummy.counts
        assert spectrum.dcounts == spectrum_dummy.dcounts
    
    def test_data_derived_from_peak_region(self, spectrum, spectrum_dummy):
        # Act
        assert len(spectrum.peak) == len(spectrum_dummy.peak)
        assert len(spectrum.peak_energies) == len(spectrum.peak)
        assert pytest.approx(min(spectrum.peak_energies), 1e-3) == min(spectrum_dummy.peak_energies)
        assert pytest.approx(max(spectrum.peak_energies), 1e-3) == max(spectrum_dummy.peak_energies)
        
        assert pytest.approx(spectrum.peak_counts, 0.1) == spectrum_dummy.peak_counts
        assert pytest.approx(spectrum.dpeak_counts, 0.1) == spectrum_dummy.dpeak_counts
        np.testing.assert_equal(spectrum.peak_countrate_live,
                                spectrum_dummy.peak_countrate_live)
        np.testing.assert_equal(spectrum.dpeak_countrate_live,
                                spectrum_dummy.dpeak_countrate_live)
        np.testing.assert_equal(spectrum.peak_countrate_real,
                                spectrum_dummy.peak_countrate_real)
        np.testing.assert_equal(spectrum.dpeak_countrate_real,
                                spectrum_dummy.dpeak_countrate_real)
        
        assert spectrum.peak_center == pytest.approx(spectrum_dummy.peak_center)
    
    def test_values_of_derived_parameters(self, spectrum, spectrum_dummy):
        assert pytest.approx(spectrum.s, 1e-6) == spectrum_dummy.s
        assert pytest.approx(spectrum.ds, 1e-6) == spectrum_dummy.ds
        assert pytest.approx(spectrum.v2p, 1e-6) == spectrum_dummy.v2p
        assert pytest.approx(spectrum.dv2p, 1e-6) == spectrum_dummy.dv2p
        assert pytest.approx(spectrum.w, 1e-6) == spectrum_dummy.w
        assert pytest.approx(spectrum.dw, 1e-6) == spectrum_dummy.dw

    ## Method testing starts here
    def test_doppler_peak_analysis(self, spectrum): 
        spectrum.analyze()

    #TODO:  include test for methods
    ## if meas is ecal
	#	self.calculate_eres( #NOTE requires ecal data
    ## internal functions #NOTE would not test these
	#	self.odr_fit(
	#	self.odr_gauss_polyfit(
	#	self.odr_polyfit(
	#	self.fit_calib_peaks_with_gauss(
    ## ?? #NOTE What are these?
    #self.autoadjust_ecal(
	#	self.calibrate(
	#	self.match_calibration(
	#	self.noise_level(

