"""Achtung
Does not work, needs to be fixed once calibration functions are inclued in
the main branch."""
from stacs.measurement import DopplerMeasurement
from pathlib import Path


# Arrange
path2testdata = Path("./testdata/N42_singleDBS_ecal")


# Test that calibrating with only two peaks works
d = DopplerMeasurement(filename=str(path2testdata)+"/Na22.n42",
                       autocompute=False)
s = d.singles[0]

def test_get_energycalibration_two_peaks_only():
    output_folder = Path("./output/single_eres_and_calib/")
    output_folder.mkdir(parents=True, exist_ok=True)
    # Act
    s.calibrate("Na22", use_511_for_initial_guess=True,
                plot_spectrum=f"{output_folder}/two-peaks_show")


# Test Eu152 + Na22 calibration and energy resolution calculation
d = DopplerMeasurement(filename=str(path2testdata)+"/Eu152_Na22.n42",
                       autocompute=False)
s = d["CAB"]


def test_get_energycalibration():
    output_folder = Path("./output/single_eres_and_calib/")
    output_folder.mkdir(parents=True, exist_ok=True)
    # Act
    s.calibrate(
        ["Eu152", "Na22"],
        plot_spectrum=f"{output_folder}/show",
        exception_on_warning=False,
        use_511_for_initial_guess=True,
    )


def test_get_energyresolution():
    output_folder = Path("./output/single_eres_and_calib/")
    output_folder.mkdir(parents=True, exist_ok=True)
    # Act
    s.calculate_eres(plot_fit=f"{output_folder}/show",)

"""
EXPECTED BEHAVIOUR:
- two plots of spectra with Gaussian functions fitted onto the peaks
- 511 keV peak should be highlighted in green and there should be a message 'INFO: 511 keV peak found'
- Each Gaussian with a colored fit plot should have a gray vertical line in its center showing the assigned energy literature value
- 3rd plot window shows the sqrt fit for energy resolution interpolation. The fit usually does not align 'perfectly'

CONSOLE OUTPUT (may change with updates...)

    No Implantation Energy set in n42 file.
    INFO: calibration for detector CAZ
    INFO: parabolic fit disabled, using only two peaks
    INFO: 511 keV peak found
    No Implantation Energy set in n42 file.
    INFO: calibration for detector CAB
    INFO: 511 keV peak found
    INFO: energy resolution for detector CAB
    WARNING: 511 keV peak found in measurement. Removing that peak before fit.
    INFO: using 19 peaks for eres calculation.


"""
