import pytest
import stacs
import numpy as np
import matplotlib.pyplot as plt
import os
from pathlib import Path


class CoincidenceSpectrumDummy(stacs.coinc.CoincidenceSpectrum):
    def __init__(self):
        pass


@pytest.fixture
def coinc_high():
    c = CoincidenceSpectrumDummy()
    c.detpair = ('A', 'D')
    c.hist = np.load("testdata/coinc_spectrum_dummy_high.npy")
    c.window = ((7658, 8682), (7855, 8879))
    c.ecal = ((0, 0.06094, 0), (0, 0.06245, 0))
    c.parentname = "parent.mpa"
    c.counts = np.sum(c.hist)
    c.show = False
    c.verbose = False
    c.export_dir = os.path.join(os.getcwd(), "stacs_projections")
    return c


@pytest.fixture
def coinc_low():
    c = CoincidenceSpectrumDummy()
    c.detpair = ('OAA', 'OAB')
    c.hist = np.load("testdata/coinc_spectrum_dummy_low.npy")
    c.window = ((6600, 7600), (6700, 7700))
    c.ecal = ((-0.21149579, 0.07179369, 0), (-0.02030492, 0.07105294, 0))
    c.parentname = "parent.mpa"
    c.counts = np.sum(c.hist)
    c.show = False
    c.verbose = False
    c.export_dir = os.path.join(os.getcwd(), "stacs_projections")
    return c


@pytest.fixture
def coinc_empty():
    c = CoincidenceSpectrumDummy()
    c.detpair = ('X', 'Y')
    c.hist = np.full((1000, 1000), 0, dtype=np.int64)
    c.window = ((0, 1000), (0, 1000))
    c.ecal = ((0, 1, 0), (0, 1, 0))
    c.parentname = "parent.mpa"
    c.counts = np.sum(c.hist)
    c.show = False
    c.verbose = False
    c.export_dir = os.path.join(os.getcwd(), "stacs_projections")
    return c


class TestCoincidenceSpectrum:
    def test_addition(self, coinc_high):
        # test addition of same detpair
        coinc_new = coinc_high + coinc_high

        assert coinc_new.ecal == coinc_high.ecal
        assert coinc_new.detpair == coinc_high.detpair
        assert coinc_new.window == coinc_high.window
        assert coinc_new.parentname is coinc_high.parentname
        assert np.all(coinc_new.hist == 2 * coinc_high.hist)

    def test_plot_histogram(self, coinc_high):
        output_folder = Path("./output/coinc/")
        output_folder.mkdir(parents=True, exist_ok=True)
        coinc_high.plot_histogram(show=False)
        plt.savefig(output_folder.joinpath("coinc-spectrum-output_plot-histogram.png"))
        plt.close()

    def test_simple_projection(self, coinc_low):
        assert np.all(coinc_low.simple_projection() == np.sum(coinc_low.hist, axis=0))

    def test_get_coinc_counts(self, coinc_low):
        assert coinc_low.get_coinc_counts() == coinc_low.counts

    def test_in_flight_annihilation(self, coinc_high):
        pass

    def test_backgroundcheck(self, coinc_high, coinc_low):
        for coinc, expect in zip([coinc_high, coinc_low],
                                 [(0.2061, 0.3047, 0.3301, 0.2281),
                                  (0.0004, 0.0003, 0.0045, 0.0003)]):
            a, b, c, d = coinc.backgroundcheck()

            assert abs(a - expect[0]) < 0.005
            assert abs(b - expect[1]) < 0.005
            assert abs(c - expect[2]) < 0.005
            assert abs(d - expect[3]) < 0.005

    def test_coincbackground(self, coinc_high, coinc_low):
        for coinc, expect in zip([coinc_high, coinc_low], [0.2171, 0.0004]):
            bg = coinc.coincbackground()

            assert abs(bg - expect) < 0.005

    def test_energyres(self, coinc_high, coinc_low):
        for coinc, expect in zip([coinc_high, coinc_low], [1.17, 1.62]):
            fwhm, err = coinc.energyres()
            assert abs(fwhm - expect) < 0.05
            assert err < 5e-3

    def test_roi_position(self, coinc_high, coinc_low):
        exp_high = (-0.991, 980.172, (486.955, 497.547))
        exp_low = (-0.982, 1006.483, (493.384, 522.053))
        for coinc, expect in zip([coinc_high, coinc_low], [exp_high, exp_low]):
            slope, intercept, (x, y) = coinc.roi_position()
            assert abs(slope - expect[0]) < 0.03
            assert abs(intercept - expect[1]) < 20
            assert abs(x - expect[2][0]) < 1
            assert abs(y - expect[2][1]) < 1

    def test_cut_roi(self, coinc_high, coinc_low):
        output_folder = Path("./output/coinc/")
        output_folder.mkdir(parents=True, exist_ok=True)
        for coinc in [coinc_low, coinc_high]:
            cut_array_1, *_ = coinc.cut_roi(None, w=10)
            cut_array_2, *_ = coinc.cut_roi(None, w=20)

            counts_1 = np.sum(cut_array_1)
            counts_2 = np.sum(cut_array_2)
            assert counts_2 < coinc.counts
            assert counts_1 < counts_2

        coinc.plot_histogram(cut_array_1, show=False)
        plt.savefig(output_folder.joinpath("coinc-spectrum-output_cut-roi.png"))
        plt.close()

    def test_binning(self, coinc_high, coinc_low):
        for coinc in [coinc_high, coinc_low]:
            d_bin_n, projection, div = coinc.binning()

    def test_shortest(self, coinc_high, coinc_low):
        for coinc in [coinc_high, coinc_low]:
            bg = coinc.coincbackground()
            (array, slope, intercept, slope2, intercept2, old_u, old_l,
             center, bg_matrix) \
                = coinc.cut_roi(bg)

            x_m, y_m = center

            y_cal = coinc.ecal[1][1]

            y_axis_corr = [y_m - (y_cal * i) for i in range(550, -550, -1)]

            y_dist = y_axis_corr[-1] - y_axis_corr[0]
            m_corr = y_dist / len(y_axis_corr)

            b_corr = 511 - m_corr * y_m

            bins = []
            d_bin = []

            for val in range(len(bins) - 1):
                d_bin.append(bins[val + 1] - bins[0])

            d_bin_np_r = d_bin.copy()
            d_bin_np_r.reverse()

            d_bin_n = [i * (-1) for i in d_bin]

            d_bin_np = d_bin_np_r + d_bin_n.copy()

            d_bin_np = np.array(d_bin_np)

            d_bin_np /= 1000
            d_bin_np += 511

            d_bin_y = np.array([(i - b_corr) / m_corr for i in d_bin_np])
            d_bin_x = np.array([(i - intercept) / slope for i in d_bin_y])

            d_intercept = [(d_bin_y[i] - slope2 * d_bin_x[i]) * (1) for i in range(len(d_bin_y))]
            d_intercept.reverse()
            countsum, bg_sum = coinc.shortest(array, intercept2, d_intercept,
                                              slope, slope2, old_l, old_u,
                                              bg_matrix, False, False)

    def test_orbitals(self):
        pass

    def test_orbitals_new(self):
        pass
