import pytest
import stacs
from pathlib import Path
import numpy as np
import warnings


class TestInputs:
    """Tests input carrier class
    """
    def __init__(self, path2testdata):
        self.path2testdata = Path(path2testdata)


# Arrange
@pytest.fixture
def testconfig():
    print("Creating instance of TestInputs class")
    instance = TestInputs(path2testdata="./testdata")
    print(instance.__dict__)
    return instance


class TestImporterDopplerMeasurement:
    """Test importer functionality

    Test the importing of datafiles by STACS into the DopplerMeasurement class
    from a file.
    Currently covers the following cases:
        - SingleSpectra from N42 files

    When adding new test cases make sure to account for both single and
    coincidence spectra.

    TODO: Add testfiles for mpa and txt as well as other instruments.
    """
    def test_singles_file_mpa(self, testconfig):
        """
        NOTE : Requres a dummy energy calibration given in the file
        "Calibration.cal"
        """
        path = testconfig.path2testdata / "mpa_singleDBS_singleFile_CDB" / "Messung066.mpa"
        # Act
        m_single = stacs.DopplerMeasurement(str(path.resolve()))

    def test_singles_file_n42(self, testconfig):
        path = testconfig.path2testdata / "N42_singleDBS_singleFile_SLOPE" / "singleDBS_SLOPE.n42"
        # Act
        m_single = stacs.DopplerMeasurement(str(path.resolve()))

    def test_coincident_file_mpa(self, testconfig):
        path = testconfig.path2testdata / "mpa_coincidentDBS_singleFile_CDB_Al" / "Spectrum.mpa"
        # Act
        m_single = stacs.DopplerMeasurement(str(path.resolve()))
        ref_path = testconfig.path2testdata / "coinc_spectrum_dummy_high.npy"
        ref = np.load(str(ref_path.resolve()))
        assert np.all(m_single.coinc[0].hist == ref)

    def test_coincident_file_n42(self, testconfig):
        path = testconfig.path2testdata / "N42_coincidentDBS_singleFile_SLOPE" / "coinctestCu1610087.n42"
        # Act
        m_single = stacs.DopplerMeasurement(str(path.resolve()))

    def test_coincident_file_n42png(self, testconfig):
        # TODO
        warnings.warn("Test of N42-PNG import not yet implemented")


class TestImporterMeasurementCampaign:
    """Test importer functionality

    Test the importing of datafiles by STACS into the MeasurementCampaign class
    from files or a directory.

    When adding new test cases make sure to account for the cases:
    multi file, and folder.

    TODO: Add testfiles for mpa and txt as well as other instruments.
    """
    # mpa files
    def test_singles_files_mpa(self, testconfig):
        path = testconfig.path2testdata / "mpa_singleDBS_depth-resolved_CDB"
        datafiles = list(path.iterdir())
        # Act
        c_single = stacs.MeasurementCampaign(measurements=datafiles)

    """
    #TODO importer error because attempted import of Parameter.txt as 
    #coincidence data file.
    def test_singles_folder_mpa(self, testconfig):
        ""
        NOTE : Requires a dummy energy calibration given in the file
        "Calibration.cal"
        ""
        path = "mpa_singleDBS_depth-resolved_CDB"
        path = testconfig.path2testdata / path
        # Act
        c_single = stacs.MeasurementCampaign(directory=str(path))
    """

    def test_coincident_files_mpa(self, testconfig):
        path = testconfig.path2testdata / "mpa_coincidentDBS_CDB_AlCu"
        datafiles = list(path.iterdir())
        # Act
        c_coinc = stacs.MeasurementCampaign(measurements=datafiles)

    def test_coincident_folder_mpa(self, testconfig):
        path = testconfig.path2testdata / "mpa_coincidentDBS_CDB_AlCu"
        # Act
        c_coinc = stacs.MeasurementCampaign(directory=str(path))

    # N42 files
    def test_singles_files_n42(self, testconfig):
        path = testconfig.path2testdata / "N42_singleDBS_singleFile_SLOPE"
        datafiles = list(path.iterdir())
        # Act
        c_single = stacs.MeasurementCampaign(measurements=datafiles)

    def test_singles_folder_n42(self, testconfig):
        path = "N42_singleDBS_depth-resolved_multi-profile_SLOPE"
        path = testconfig.path2testdata / path
        # Act
        c_single = stacs.MeasurementCampaign(directory=str(path))

    def test_coincident_files_n42(self, testconfig):
        """NOTE
        The testdata only contains one file.
        """
        path = testconfig.path2testdata / "N42_coincidentDBS_singleFile_SLOPE"
        datafiles = list(path.iterdir())
        # Act
        c_coinc = stacs.MeasurementCampaign(measurements=datafiles)

    def test_coincident_folder_n42(self, testconfig):
        """NOTE
        The testdata only contains one file.
        """
        path = testconfig.path2testdata / "N42_coincidentDBS_singleFile_SLOPE"
        # Act
        c_coinc = stacs.MeasurementCampaign(directory=str(path))

    # N42 with coincidence date externalited in PNG files
    def test_coincident_files_n42png(self, testconfig):
        # TODO
        warnings.warn("Test of N42-PNG import not yet implemented")

    def test_coincident_folder_n42png(self, testconfig):
        # TODO
        warnings.warn("Test of N42-PNG import not yet implemented")
